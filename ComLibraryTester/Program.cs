﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using COMClassLibrary.Manager;
using COMClassLibrary.Model;
using System.Text.RegularExpressions;
using COMClassLibrary;
using System.Net;

namespace ComLibraryTester
{
    class Program
    {
        static void Main(string[] args)
        {
            //  TestRandomVolume();
            //TestFileManager();
            //TestSqliteManager();
            //TestRSICalculator();
            //TestInsertOrderScreen();
            // TestCapitalCalculator();

            //TestRSILinearRegression();         
            //Console.WriteLine(y);

            //   TestVolumeCalculator();

            //    TestVolumeMA();

            //TestIntradayStrategy();
            //  TestPlotLineWithRSI();

            //   TestTimeFrame();

            //     TestGetTimeFrameAndCalculateRSI();
            //   TestSaveRSIAndVolume();
            //  TestInsertVolumeMa();
            //CalculateMinLot();
            //    TestGetVolumeMaList();

            // TestGetTimeFrameAndCalculateRSI();

            //TestInsertVolumeMa();
            //CalculateMinLot();
            //TestGetVolumeMaList();

            //TestInsertRecognia();

            // TestQuotationScreenParser();

            //TestGetLastInsert();

            // TestIsThresholdReached();
            //   TestRegEx();

            // TestUpdateQuotationScreen();
            //    TestRSIBreakoutRun();
            //  TestRunStrategy();
            //TestRSIBreakoutRun();
            //TestRunStrategy();

            //  TestCheckOrderStatus();
            //   TestUpdateOrderStatus();
            // TestCache();
            //TestRecogniaRun();
            //  TestIntradayStrategy();
            //    TestOverrideVirtualMethod();

            //  TestINAV();
            Console.Write("test");
            Console.Read();
        }

        private static void TestINAV()
        {
            String url = @"http://www.fmic.mdgms.com/iopv/quote.php";
            
            String responseString = "";
            using (var wb = new WebClient())
            {
                var response = wb.DownloadString(url);
                responseString = response;
            }


            double price = 0;
            const string pattern = @"<td\b[^>]*?>(?<V>[\s\S]*?)</\s*td>";
            foreach (Match match in Regex.Matches(responseString, pattern, RegexOptions.IgnoreCase))
            {
                string value = match.Groups["V"].Value;
                if (Double.TryParse(value, out price))
                {
                    Console.WriteLine(value);
                }
               
            }

        }

        private static void TestOverrideVirtualMethod()
        {
            OrderManager b = new RSIBreakoutStrategy();
            b.G();

            RSIBreakoutStrategy rsi = new RSIBreakoutStrategy();
            rsi.F();
            rsi.G();


            ((RSIBreakoutStrategy)b).G();
        }

        private static void TestIntradayStrategy()
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            String stockName = "DFNN";
            OrderManager orderManager = new IntradayStrategy();
            orderManager.SetParameters(stockName, "askSize", 12101, parameters);
            orderManager.SetParameters(stockName, "bidSize", 12220, parameters);
            orderManager.SetParameters(stockName, "last", 154.00, parameters);
            orderManager.SetParameters(stockName, Constants.IntradayStatus, "", parameters);
            orderManager.SetParameters(stockName, "brokerId", "FMETF-MM", parameters);
            orderManager.SetParameters(stockName, "requiredAbsoluteStopLossFactor", 0.07, parameters);
            orderManager.SetParameters(stockName, "requiredIntradayPTPercentage", 0.1, parameters);
            orderManager.SetParameters(stockName, "volume", 15678, parameters);
            orderManager.SetParameters(stockName, "volumema", 15678, parameters);
            orderManager.SetParameters(stockName, "volumeRequired", 1.3, parameters);
            
            ConfigContext context = new ConfigContext();
            orderManager.Changed += new ChangedEventHandler(m_Changed);
            ((IStrategy)orderManager).Run(stockName, parameters);
        }

        private static void TestRecogniaRun()
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            String stockName = "SMPH PM";
            OrderManager orderManager = new RecogniaStrategy();
            orderManager.SetParameters(stockName, "stockName", stockName, parameters);
            orderManager.SetParameters(stockName, "bid", 153.23, parameters);
            orderManager.SetParameters(stockName, "ask", 154.50, parameters);
            orderManager.SetParameters(stockName, "last", 154.00, parameters);
            orderManager.SetParameters(stockName, "period", 14, parameters);
            orderManager.SetParameters(stockName, "frequency", 1, parameters);
            orderManager.SetParameters(stockName, "timeFrame", "MIN", parameters);
            orderManager.SetParameters(stockName, "status", "RecogniaWaitingMode", parameters);
            orderManager.SetParameters(stockName, "volumePeriod", 14, parameters);
            orderManager.SetParameters(stockName, "requiredVolume", 1.3, parameters);
            orderManager.SetParameters(stockName, "requiredAbsoluteStopLossFactor", 0.07, parameters);
            orderManager.SetParameters(stockName, "techniStockfilePath", @"C:\temp\technistock\orders.csv", parameters);
            orderManager.SetParameters(stockName, "volume", 15678, parameters);//
            orderManager.SetParameters(stockName, "requiredRecogniaPTPercentage", 0.1, parameters);
            
            ConfigContext context = new ConfigContext();
            orderManager.Changed += new ChangedEventHandler(m_Changed);
            ((IStrategy)orderManager).Run(stockName, parameters);
        
        }

        private static void TestCache()
        {

            CapitalCalculator c = new CapitalCalculator();
            int i = 1;
            while (true)
            {
                DateTime d = c.GetDate();
                Console.WriteLine(d.ToString());
                System.Threading.Thread.Sleep(100 * i++);
            }
        }

        private static void TestUpdateOrderStatus()
        {
            OrderManager m = new OrderManager();
            String stockName = "MBT PM";
            //m.UpdateOrderStatus(stockName, "WaitForVolumeThreshold");
            TestCheckOrderStatus();
            
        }

        private static void TestCheckOrderStatus()
        {
            OrderManager m = new OrderManager();
            String stockName = "BPI PM";
            string status = m.GetOrderStatus(stockName);
            Console.WriteLine("Status of {0} is {1}", stockName, status);
        }

        private static void TestRunStrategy()
        {
            TradingController controller = new TradingController();
            String stockName = "SMPH PM";

            //Dictionary<string, object> parameters = new Dictionary<string, object>();
            controller.SetParameters(stockName, typeof(RSIBreakoutStrategy).ToString(), "stockName", stockName);
            controller.SetParameters(stockName, typeof(RSIBreakoutStrategy).ToString(), "bid", 153.23);
            controller.SetParameters(stockName, typeof(RSIBreakoutStrategy).ToString(), "ask", 154.50);
            controller.SetParameters(stockName, typeof(RSIBreakoutStrategy).ToString(), "period", 14);

            controller.SetParameters(stockName, typeof(RSIBreakoutStrategy).ToString(), "startingX", 41);
            controller.SetParameters(stockName, typeof(RSIBreakoutStrategy).ToString(),"frequency", 1);
            controller.SetParameters(stockName, typeof(RSIBreakoutStrategy).ToString(),"timeFrame", "MIN");
            controller.SetParameters(stockName, typeof(RSIBreakoutStrategy).ToString(),"last", 154.00);


            controller.SetParameters(stockName, typeof(RSIBreakoutStrategy).ToString(),"volume", 15678);//
            controller.SetParameters(stockName, typeof(RSIBreakoutStrategy).ToString(),"volumema", 15313);//
            controller.SetParameters(stockName, typeof(RSIBreakoutStrategy).ToString(),"requiredDerivative", 0.95);//
            controller.SetParameters(stockName, typeof(RSIBreakoutStrategy).ToString(),"requiredSeparation", 1.05);
            controller.SetParameters(stockName, typeof(RSIBreakoutStrategy).ToString(),"volumePeriod", 14);
            controller.SetParameters(stockName, typeof(RSIBreakoutStrategy).ToString(),"requiredVolume", 1.3);

            controller.SetParameters(stockName, typeof(RSIBreakoutStrategy).ToString(), "profitTargetLookupPeriod", 10);
            controller.SetParameters(stockName, typeof(RSIBreakoutStrategy).ToString(), "stopLossLookupPeriod", 10);
            //7% loss
            controller.SetParameters(stockName, typeof(RSIBreakoutStrategy).ToString(), "requiredAbsoluteStopLossFactor", 0.93);
            controller.SetParameters(stockName, typeof(RSIBreakoutStrategy).ToString(), "techniStockfilePath", @"C:\temp\technistock\orders.csv");
            controller.SetParameters(stockName, typeof(RSIBreakoutStrategy).ToString(), "capital", 1000000.00);
            //10% of capital
            controller.SetParameters(stockName, typeof(RSIBreakoutStrategy).ToString(), "capitalMultiplier", 0.8);
            //capital divisor is the baseline or the minimum capital
            controller.SetParameters(stockName, typeof(RSIBreakoutStrategy).ToString(), "capitalDivisor", 5);

            controller.SetParameters(stockName, typeof(RSIBreakoutStrategy).ToString(), "lineRegressionRSIPeriod", 40);//
            controller.SetParameters(stockName, typeof(RSIBreakoutStrategy).ToString(), "rsiPeriod", 14);//

            controller.RunStrategy(stockName);
           // Console.WriteLine("Stock: {0} | Error Message from RSIStrategy: {2} | Status:{1}", stockName, controller.GetMessage("RSIStatus:" + stockName), controller.GetMessage("RSIError:" + stockName));



            int a = 0;

            while (a != 32)
            {
                Console.WriteLine("RSI Specific code:{0}", controller.GetParameter(stockName,typeof(RSIBreakoutStrategy).ToString(), Constants.MyRSIErrorCode));

                Console.WriteLine("RSIStrategyRSIValue  value:{0}", controller.GetMessage(stockName, typeof(RSIBreakoutStrategy).ToString(), COMClassLibrary.Constants.RSIStrategyRSIValue));
                Console.WriteLine("RSIStatus  value:{0}", controller.GetMessage(stockName, typeof(RSIBreakoutStrategy).ToString(), Constants.RSIStatus));

                Console.WriteLine("Hit any key and click Enter to continue or hit spacebar and click Enter to exit");

                a = Console.Read();
            }
        }


        private static void TestRSIBreakoutRun()
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            String stockName = "SMPH PM";

            OrderManager orderManager = new RSIBreakoutStrategy();
            orderManager.SetParameters(stockName, "stockName",stockName, parameters);

         
            orderManager.SetParameters(stockName, "bid", 153.23, parameters);
            orderManager.SetParameters(stockName, "ask", 154.50, parameters);
            orderManager.SetParameters(stockName, "last", 154.00, parameters);
            orderManager.SetParameters(stockName, "period", 14, parameters);

           
            orderManager.SetParameters(stockName, "startingX", 41, parameters);
            orderManager.SetParameters(stockName, "frequency", 1, parameters);
            orderManager.SetParameters(stockName, "timeFrame", "MIN", parameters);
            orderManager.SetParameters(stockName, "status", "RSIWaitingMode", parameters);
            orderManager.SetParameters(stockName, "requiredDerivative", 0.95, parameters);
            orderManager.SetParameters(stockName, "requiredSeparation", 1.05, parameters);
            orderManager.SetParameters(stockName, "volumePeriod", 14, parameters);
            orderManager.SetParameters(stockName, "requiredVolume", 1.3, parameters);

            orderManager.SetParameters(stockName, "profitTargetLookupPeriod", 10, parameters);
            orderManager.SetParameters(stockName, "stopLossLookupPeriod", 10, parameters);
            //7% loss
            orderManager.SetParameters(stockName, "requiredAbsoluteStopLossFactor", 0.93, parameters);
            orderManager.SetParameters(stockName, "techniStockfilePath", @"C:\temp\technistock\orders.csv", parameters);
            
            //just pretend we got the right amount
            orderManager.SetParameters(stockName, "capital", 1000000, parameters);

            //pending or partial.  check if that stock has no pending status. or someone already sent an unfilled order
            //a db call on quotation screen.

            orderManager.SetParameters(stockName, "volume", 15678, parameters);//
           
            ConfigContext context = new ConfigContext();

            //10% of capital
            orderManager.SetParameters(stockName, "capitalMultiplier",0.8, parameters);
            //capital divisor is the baseline or the minimum capital
            orderManager.SetParameters(stockName, "capitalDivisor", 5, parameters);
           
            orderManager.Changed += new ChangedEventHandler(m_Changed);
            ((IStrategy)orderManager).Run(stockName, parameters);

           
        }

        static void m_Changed(object sender, EventArgs e)
        {
            //Handle Messages of RSI

            if (e.GetType() == typeof(COMClassLibrary.Manager.ErrorEventArg))
            {
                Console.WriteLine("stock:{0}, code:{1}, error message:{2}", ((ErrorEventArg)e).StockName, ((ErrorEventArg)e).Code, ((ErrorEventArg)e).Message);
            }
            if (e.GetType() == typeof(COMClassLibrary.Manager.MessageEventArg))
            {
                Console.WriteLine("stock:{0}, code:{1}, error message:{2}", ((MessageEventArg)e).StockName, ((MessageEventArg)e).Code, ((MessageEventArg)e).Message);
            }

            //if (sender.GetType() == typeof(COMClassLibrary.Manager.RSIBreakoutStrategy))
            //{
            //    RSIBreakoutStrategy s = (RSIBreakoutStrategy)sender;
            //    Console.WriteLine("RSIStatus:{0}",s.Status);
            //    Console.WriteLine("RSIError:{0}", s.ErrorMessage);

            //}            //Handle Messages or Recognia







        }

        private static void TestUpdateQuotationScreen()
        {
            COMClassLibrary.FMServiceReference.QuotationScreenTO qTO = new COMClassLibrary.FMServiceReference.QuotationScreenTO();
            qTO.TimeSent = "9:18:16 AM";
            qTO.Type = "LMT";
            qTO.AccountId = "F00599-8";
            qTO.StockName = "GMAPXXX";
            qTO.Side = "SELL";
            qTO.Volume = 5000;
            qTO.Price = (decimal)10.50;
            qTO.AveragePrice = "";
            qTO.Status = "O";
            qTO.Matched = (decimal)0.00;
            qTO.Pending = (decimal)200000.00;
            qTO.Expiry = "GTC";
            qTO.Trader = "MIKE";
            COMClassLibrary.FMServiceReference.StockServiceClient c = new COMClassLibrary.FMServiceReference.StockServiceClient();
            c.UpdateQuotationScreen(qTO);
        }

        //private static void TestIntradayStrategy()
        //{
        //    Dictionary<string, object> parameters = new Dictionary<string, object>();
        //    String stockName = "MBT PM";
        //    OrderManager.SetParameters(stockName, "stockName", stockName, parameters);

        //    OrderManager.SetParameters(stockName, "stockName", stockName, parameters);
        //    OrderManager.SetParameters(stockName, "bid", 153.23, parameters);
        //    OrderManager.SetParameters(stockName, "ask", 154.50, parameters);
        //    //just pretend we got the right amount
        //    OrderManager.SetParameters(stockName, "capital", 1000000, parameters);

        //    //pending or partial.  check if that stock has no pending status. or someone already sent an unfilled order
        //    //a db call on quotation screen.
            
        //    OrderManager.SetParameters(stockName, "volume", 15678, parameters);//
        //    OrderManager.SetParameters(stockName, "volumema", 15313, parameters);//
            

        //    //make a call to db  and buidl the list of brokers with their bids and offers. 
        //    //the point is to check if there are no bids or offer for that stock by FM
        //    //use quotationscreen or orderscreen
        //    //the broker could be the account nunmber.

        //    List<String> brokers = new List<string>();
        //    brokers.Add("Broker1");
        //    brokers.Add("Broker2");




        //    OrderManager.SetParameters(stockName, "brokers", brokers, parameters);

        //    IntradayStrategy s = new IntradayStrategy();
        //    s.Run(stockName,parameters);


        //}

        private static void TestRegEx()
        {
            string s = "<table[^>]*>([^<]*)</table>";



            string sampleText = "<html><body><table><tr><td>test</td></tr></table</body></html>";
            //string regExPattern =  @"<TABLE[^>]*>[\s\S]*?</TABLE>\s*";
            //string regExPattern = @"<TABLE[^>]*>[\s\S]*?/TABLE>\s*";
            string regExPattern = @"<TABLE[^>]*>([^<]*)</TABLE>";
            Regex r = new Regex(regExPattern,RegexOptions.IgnoreCase | RegexOptions.Multiline);
            MatchCollection ms = r.Matches(s);
            foreach (Match m in ms)
            {
                Console.WriteLine("Start:{0} End:{1}", m.Index, m.Length);
            }
        }

        private static void TestRandomVolume()
        {
            int volume = 50000;
            TradingController c = new TradingController();
            Console.WriteLine("Vol:{0}", c.RandomizeInteger(volume));
        }
        private static void TestInsertRecognia()
        {
            COMClassLibrary.FMServiceReference.StockServiceClient c = new COMClassLibrary.FMServiceReference.StockServiceClient();
            c.InsertRecognia("First Metro", "TEST", "04/1/2014", "BREAKOUT", "OPEN");

            foreach (COMClassLibrary.FMServiceReference.RecogniaTO item in c.GetOpenRecogniaList())
            {
                Console.WriteLine("Email:{0} status:{1}", item.Keyword, item.Status);
            }

        }

        private static void TestGetVolumeMaList()
        {
            
            COMClassLibrary.FMServiceReference.StockServiceClient c = new COMClassLibrary.FMServiceReference.StockServiceClient();
            COMClassLibrary.FMServiceReference.VolumeMATO[] list =  c.GetVolumeMaList("AP PM", 5, "MIN", 14,DateTime.Now );

            foreach(COMClassLibrary.FMServiceReference.VolumeMATO item in list)
            {
                Console.WriteLine(item.Date);
            }
        }

        private static void TestSaveRSIAndVolume()
        {
            string stockName = "AEV PM";
            int periods = 14;
            double price = 39.55;
            int frequency = 1;
            DateTime now = DateTime.Now;
            string timeFrame = "MIN";
            int volume = 5;
            double bid = 39.00;
            double ask = 40.00;
            double bidSize = 5;
            double askSize = 5;
            double open = 1;
            String strategyClass = "COMClassLibrary.Manager.RSIBreakoutStrategy";
            

            TradingController c = new TradingController();
            c.SaveTimeFrameAndRSI(stockName, price, timeFrame, frequency, periods, DateTime.Now, volume,ask,askSize,bid,bidSize,open);
        }

        private static void TestInsertVolumeMa()
        {
            string stockName = "AP PM";
            int periods = 14;
            double volumeValue = 39.55;
            int frequency = 1;
            DateTime now = DateTime.Now;
            string timeFrame = "MIN";
            COMClassLibrary.FMServiceReference.StockServiceClient c = new COMClassLibrary.FMServiceReference.StockServiceClient();
            c.InsertVolumeMA(stockName, periods, volumeValue, frequency, timeFrame, now);
        }

        private static void TestIsThresholdReached()
        {
            //double currentRSI = 45;
            double previousRSI = 54;
            Single percentage = 0.95F;
            RSICalculator c = new RSICalculator();
            //it means that it hasn't gone down by percentage

            for (int currentRSI = 44; currentRSI < 57; currentRSI++)
            {
                Console.WriteLine("currentRSI:{1} is {3} lower than  of {0}", previousRSI, Convert.ToDouble(currentRSI), percentage, (c.IsThresholdLower(currentRSI, previousRSI, 100 - percentage)));
              
                Console.WriteLine("currentRSI:{1} is {3} higher than  of {0}", previousRSI, Convert.ToDouble(currentRSI), percentage, (c.IsThresholdLower(currentRSI, previousRSI, 1-  percentage)));
                Console.WriteLine("");
            }
            
        }

        private static void TestPlotLineWithRSI()
        {
            String stockName = "PCOR PM";
            String timeFrame = "MIN";
            int period = 40;
            int frequency = 1;
            int x = 85;
            
            RSICalculator calc = new RSICalculator();
            RSI rsi = calc.PlotLineWithRSI(stockName,period,frequency,timeFrame, x, DateTime.Now);
            if (rsi != null)
            {
                Console.Write("RSI x:{0} , predicted y:{1} , slope:{2} , y intercept:{3}", x, rsi.Y, rsi.Derivative, rsi.YIntercept);
            }
        }

        private static void TestGetLastInsert()
        {
            String stockName = "AP PM";
            String timeFrame = "MIN";
            //int period = 14;
            int frequency = 5;
            double price = 109;
            int hoursBack = 24;
            TimeFrameManager m = new TimeFrameManager();
            m.SimulateRandomPriceForAPeriod(stockName,price,timeFrame,frequency,hoursBack);
        }

     

        private static void CalculateMinLot()
        {
            CapitalCalculator c = new CapitalCalculator();
            double price = 11.00;
            Console.WriteLine("Price is {0} and min lot: {1}", price, c.CalculateMinimumLot(price));

            price = 151.00;
            Console.WriteLine("Price is {0} and min lot: {1}", price, c.CalculateMinimumLot(price));
        }

        private static void TestGetTimeFrameAndCalculateRSI()
        {
            String stockName = "SMPH PM";
            String timeFrame = "MIN";
            int period = 14;
            int frequency = 1;
            TimeFrameManager m = new TimeFrameManager();
            COMClassLibrary.FMServiceReference.PriceWithTimeFrameTO[] list = m.GetTimeFrame(stockName, timeFrame, frequency, period, new DateTime());
            RSICalculator rsiCalculator = new RSICalculator();
            List<double> priceList = new List<double>();
            foreach (COMClassLibrary.FMServiceReference.PriceWithTimeFrameTO item in list)
            {
                priceList.Add(Convert.ToDouble(item.Price));
                Console.WriteLine("stock:{0} price:{1} date:{2}", item.StockName, item.Price, item.Date);
            }
            double rsiValue = rsiCalculator.CalculateRSI(priceList, period);
            Console.WriteLine("Period:{0} RSI:{1}", list.Length, rsiValue);


        }



        private static void TestTimeFrame()
        {
            String stockName = "AP PM";
            String timeFrame = "MIN";
            double price = 191.00;
            int frequency = 5;
            int volume = 1000;
            double ask = 192.00;
            double askSize = 5;
            double bid = 190.00;
            double bidSize =  5;
            double open = 1;
            TimeFrameManager m = new TimeFrameManager();
            m.SaveTimeFrame(stockName, price, timeFrame, frequency, volume, new DateTime(), ask, askSize, bid, bidSize,open);
           // m.GetLastInsert(tickerSymbol, timeFrame, frequency);
        }

        private static void TestVolumeMA()
        {
            VolumeCalculator c = new VolumeCalculator();
            List<int> list = new List<int>() { 1, 2, 3, 4, 5, 6, 6, 5, 4, 3, 2, 2, 2, 1, 2, 3, 2, 3, 4, 5, 6, 7, 5, 4, 3, 2, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 };
            Console.WriteLine(c.CalculateMovingAverage(list));
        }

        private static void TestVolumeCalculator()
        {
            VolumeCalculator r = new VolumeCalculator();
            Console.WriteLine(r.IsVolumeOkay(80, 50, 1.3));

        }

        private static void TestRSILinearRegression()
        {
            List<double> ylist = new List<double>() { 1, 2, 3, 4, 5, 6, 6, 5, 4, 3, 2, 2, 2, 1, 2, 3, 2, 3, 4, 5, 6, 7, 5, 4, 3, 2, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 };
            RSICalculator c = new RSICalculator();
            List<double> xlist = new List<double>() { 1,
2,
3,
4,
5,
6,
7,
8,
9,
10,
11,
12,
13,
14,
15,
16,
17,
18,
19,
20,
21,
22,
23,
24,
25,
26,
27,
28,
29,
30,
31,
32,
33,
34,
35,
36};


            RSI rsi = c.GetRSIUsingLinearRegressionWithThreeCriticalPoints(xlist, ylist, 40.0);

        }
        private static void TestCapitalCalculator()
        {
            double rsiLevel = 50;
            double capital = 10000000;
            double capitalMultiplier = 0.8;
            int capitalDivisor = 5;
            double last = 155.0;

            CapitalCalculator calculator = new CapitalCalculator();
            Console.WriteLine("Calculated capital allocation:{0} Base Capital:{1} RSI: {2}", calculator.Calculate(rsiLevel, capital, capitalMultiplier, capitalDivisor), capital,rsiLevel);
            Console.WriteLine("Time Started:{0}", DateTime.Now.ToString());
            for (int i = 0; i < 100; i++)
            {
                Console.WriteLine("Calculate minimum lots: Price:{0}, Lots:{1}", calculator.CalculateMinimumLot(last), last);
            }
            Console.WriteLine("Time Ended:{0}", DateTime.Now.ToString());
        }
        private static void TestInsertOrderScreen()
        {
            Console.WriteLine("Begin Program");
            TradingRuleManager m = new TradingRuleManager();
            m.SaveToDBAsync("EW.New", 80, 90, 20, 30, 20, 3000, DateTime.Now);
            Console.WriteLine("End Program");
        }

    

        
        private static void TestFileManager()
        {
            string path = @"C:\Users\clyde.moreno\Downloads\dropfolder";
            Console.WriteLine("Run FileManager");
            string filter = "*.csv";
            FileManager f = FileManager.Instance;
            f.CreateWatcher(path,filter);
        }

        private static void TestRSICalculator()
        {
            List<double> list = new List<double>();
            list.Add(101);
            list.Add(102);
            list.Add(103);
            list.Add(104);
            list.Add(105);
            list.Add(106);
            list.Add(107);
            list.Add(106);
            list.Add(107);
            list.Add(110);
            list.Add(111);
            list.Add(112);
            list.Add(113);
            list.Add(114);
            RSICalculator c = new RSICalculator();
            c.CalculateRSI(list, 14);
        }

        
    }
}
