﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmailLib
{
    public class EmailManager
    {
        public static void SendEmail(EmailLib.EmailProperties emailProp)
        {
            try
            {
                string path = Environment.ExpandEnvironmentVariables(ConfigurationManager.AppSettings["pathToSendMail"].ToString());
                string commandArgs = String.Format("{0} -f {1} -t {2}; -cc {3} -u {4} -m {5} -s {6}", "", emailProp.From, emailProp.To, emailProp.Cc, emailProp.Subject, emailProp.Body, emailProp.Server);
                ProcessStartInfo processInfo;
                Process process = new Process();

                processInfo = new ProcessStartInfo(String.Format("{0}sendEmail.exe", path));
                processInfo.Arguments = commandArgs;
                processInfo.CreateNoWindow = true;
                processInfo.UseShellExecute = false;
                // *** Redirect the output ***
                processInfo.RedirectStandardError = true;
                processInfo.RedirectStandardOutput = true;
                process.StartInfo = processInfo;
                process.EnableRaisingEvents = true;
                process = Process.Start(processInfo);
                process.WaitForExit();
            }
            catch (Exception e)
            {
                
                Console.WriteLine(e.Message);            
            }

          
        }
    }
}
