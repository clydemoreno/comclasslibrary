﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace EmailLib
{

    public class EmailProperties
    {
        string opening = "<html>";
        string closing = "</html>";

        string from;

        public string From
        {
            get
            {
                if (String.IsNullOrEmpty(from)) from = ConfigurationManager.AppSettings["emailSender"].ToString();
                return
                    from;
            }
            //set { from = value; }
        }

        string to;

        public string To
        {
            get
            {
                if (String.IsNullOrEmpty(to)) to = ConfigurationManager.AppSettings["emailRecipient"].ToString();
                return
                    to;
            }
            // set { to = value; }
        }

        string cc;

        public string Cc
        {
            get
            {
                if (String.IsNullOrEmpty(cc)) cc = ConfigurationManager.AppSettings["emailCc"].ToString();
                return
                    cc;
            }
            // set { cc = value; }
        }

        string subject;

        public string Subject
        {
            get { return subject; }
            set { subject = value; }
        }

        string body;

        public string Body
        {
            get { return body; }
            set { body = opening + value + closing; }
        }

        string server;

        public string Server
        {
            get
            {
                if (String.IsNullOrEmpty(server)) server = ConfigurationManager.AppSettings["emailServer"].ToString();
                return
                    server;
            }
            set { server = value; }
        }
    }
}
