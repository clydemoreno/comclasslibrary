﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using COMClassLibrary.Manager;

namespace MMUnitTestProject
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            string path = String.Format("{0}{1}-violation.log", "C:/Users/cmoreno/Downloads/violationlog/", DateTime.Today.ToString("MMMM-dd-yyyy"));
            bool fileFound = FileManager.Instance.SearchFileContents(path,"Violation");
            //instantiate object
            //run method
            //get return
            //compare with a known value
            Assert.AreEqual(true, fileFound);
        }
    }
}
