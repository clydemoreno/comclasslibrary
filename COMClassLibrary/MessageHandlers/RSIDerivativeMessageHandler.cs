﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using COMClassLibrary.FMServiceReference;

namespace COMClassLibrary.Manager
{
    public class RSIDerivativeMessageHandler : COMClassLibrary.Manager.IMessageHandler
    {
        private OrderManager manager;

        public OrderManager Manager
        {
            get {
                if (manager == null) manager = new OrderManager();
                return manager; 
            }
        }

        public void Handle(Dictionary<String, object> parameters, EventArgs e)
        {
            if(e.GetType() != typeof(COMClassLibrary.Manager.MessageEventArg)) return;
            MessageEventArg eventArg = (MessageEventArg)e;
            if (eventArg.Code == Constants.RSIDerivativeValue)
            {
                System.Diagnostics.Debug.WriteLine(eventArg.StockName);

                //int frequency = Convert.ToInt32(parameters[eventArg.StockName, "frequency"]);
                //String timeFrame = (String)parameters(eventArg.StockName, "timeFrame", parameters);
                //int profitTargetLookupPeriod = Convert.ToInt32(parameters(eventArg.StockName, "profitTargetLookupPeriod", parameters));
                FMServiceReference.StockServiceClient c = new StockServiceClient();
                c.UpdateDerivativeInOrder(eventArg.StockName,eventArg.StrategyClass,Convert.ToDouble( eventArg.Message));
            }
            
        }
    }
}
