﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using COMClassLibrary.FMServiceReference;
using COMClassLibrary.MessageHandlers;

namespace COMClassLibrary.Manager
{
    public class RSIDerivativeIsMetHandler : MessageHandlerBase, COMClassLibrary.Manager.IMessageHandler
    {
      
        public void Handle(Dictionary<String, object> parameters, EventArgs e)
        {
            if(e.GetType() != typeof(COMClassLibrary.Manager.MessageEventArg)) return;
            MessageEventArg eventArg = (MessageEventArg)e;
            if (eventArg.Code == Constants.RSIRequiredDerivativeIsMet)
            {
                SetParameters(String.Format("{0},{1}", eventArg.StockName, eventArg.StrategyClass), Constants.RSIMessage, Constants.RSIRequiredDerivativeIsMet, parameters);
                SetParameters(String.Format("{0},{1}", Constants.EventTab, Constants.EventTab), Constants.EventTab, Constants.RSISheetName, parameters);

            }
            
        }
    }
}
