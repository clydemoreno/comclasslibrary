﻿using System;
namespace COMClassLibrary.Manager
{
    public interface IMessageHandler
    {
        void Handle(System.Collections.Generic.Dictionary<string, object> parameters, EventArgs e);
    }
}
