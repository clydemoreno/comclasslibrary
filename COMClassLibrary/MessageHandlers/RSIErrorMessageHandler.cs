﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using COMClassLibrary.Manager;

namespace COMClassLibrary.MessageHandlers
{
    public class RSIErrorMessageHandler : MessageHandlerBase, IMessageHandler
    {
        public void Handle(Dictionary<string, object> parameters, EventArgs e)
        {
            if (e.GetType() != typeof(COMClassLibrary.Manager.MessageEventArg)) return;
            MessageEventArg eventArg = (MessageEventArg)e;
            if (eventArg.Code == Constants.MyRSIErrorCode)
            {
                SetParameters(String.Format("{0},{1}", eventArg.StockName, eventArg.StrategyClass), Constants.MyRSIErrorCode, eventArg.Message, parameters);
          
                //SetParameters(String.Format("{0},{1}", eventArg.StockName, eventArg.StrategyClass), eventArg.Message, parameters);
                //EmailMessage


            }

        }
    }
}
