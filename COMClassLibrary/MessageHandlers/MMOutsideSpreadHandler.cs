﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using COMClassLibrary.FMServiceReference;
using COMClassLibrary.MessageHandlers;

namespace COMClassLibrary.Manager
{
    public class MMOutsideSpreadHandler : MessageHandlerBase, COMClassLibrary.Manager.IMessageHandler
    {
      
        public void Handle(Dictionary<String, object> parameters, EventArgs e)
        {
            if(e.GetType() != typeof(COMClassLibrary.Manager.MessageEventArg)) return;
            MessageEventArg eventArg = (MessageEventArg)e;
            if (eventArg.Code == Constants.iNAVIsOutsideOfSpread)
            {
                SetParameters(String.Format("{0},{1}", eventArg.StockName, eventArg.StrategyClass), Constants.MarketMakerMessage, Constants.iNAVIsOutsideOfSpread, parameters);
                //EmailMessage

                SetParameters(String.Format("{0},{1}", Constants.EventTab, Constants.EventTab), Constants.EventTab, Constants.MarketMakerSheetName, parameters);

                string toEmail = (string)GetParameter(String.Format("{0},{1}", eventArg.StockName, eventArg.StrategyClass), Constants.toEmail, parameters);
                string fromEmail = (string)GetParameter(String.Format("{0},{1}", eventArg.StockName, eventArg.StrategyClass), Constants.fromEmail, parameters);
                String body = String.Format("Ticker:{0} Message:{1}", eventArg.StockName, Constants.SpreadWidthMessage);
                EmailManager.Notify(fromEmail, toEmail, "Email Notification for Market Making",body);


            }
            
        }
    }
}
