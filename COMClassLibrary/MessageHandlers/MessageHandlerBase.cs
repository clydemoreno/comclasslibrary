﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace COMClassLibrary.MessageHandlers
{
    public class MessageHandlerBase
    {
       
        public virtual Object GetParameter(String stockName, String key, Dictionary<String, object> parameters)
        {
            Object returnValue = null;
            object lockObject = new object();
            lock (lockObject)
            {

                if (parameters != null && parameters.ContainsKey(stockName))
                {
                    //if(stockMessage[stockName].GetType() == typeof(System.String)){ 
                    //    returnValue = (String)stockMessage[stockName];
                    //}
                    Dictionary<String, object> dictionary = (Dictionary<String, object>)parameters[stockName];
                    if (dictionary.ContainsKey(key))
                    {
                        returnValue = dictionary[key];
                    }
                }
            }
            return returnValue;
        }
        public virtual void SetParameters(String stockName, String key, object value, Dictionary<String, object> parameters)
        {
            object lockObject = new object();
            lock (lockObject)
            {

                if (parameters != null)
                {
                    if (!parameters.ContainsKey(stockName))
                    {
                        parameters[stockName] = new Dictionary<String, object>();

                    }
                    Dictionary<String, object> dictionary = (Dictionary<String, object>)parameters[stockName];
                    dictionary[key] = value;
                }
            }
        }
    }
}
