﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using COMClassLibrary.FMServiceReference;
using COMClassLibrary.MessageHandlers;

namespace COMClassLibrary.Manager
{
    public class RecogniaKeywordFoundHandler : MessageHandlerBase, COMClassLibrary.Manager.IMessageHandler
    {
      
        public void Handle(Dictionary<String, object> parameters, EventArgs e)
        {
            if(e.GetType() != typeof(COMClassLibrary.Manager.MessageEventArg)) return;
            MessageEventArg eventArg = (MessageEventArg)e;
            if (eventArg.Code == Constants.RecogniaKeywordFound)
            {
                //SetParameters(String.Format("{0},{1}", eventArg.StockName, eventArg.StrategyClass), Constants.RecogniaMessage, Constants.RecogniaKeywordFound, parameters);

                //set global event tab. so excel displays latest message
                SetParameters(Constants.EventTab, Constants.RecogniaSheetName, Constants.RecogniaKeywordFound, parameters);
                SetParameters(String.Format("{0},{1}", Constants.EventTab, Constants.EventTab), Constants.EventTab, Constants.RecogniaSheetName, parameters);

            }
            
        }
    }
}
