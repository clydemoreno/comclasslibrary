﻿using System;
using System.IO;
using System.Net;
using System.Xml;
using System.Configuration;
using System.Text;
using System.Globalization;

namespace TechnistockDataFeed
{
    public class BidAskData
    {
        public String SecCode;
        public String SecName;
        public float BidPrice;
        public float AskPrice;
        public int BidVol;
        public int AskVol;


    }
    public class DataFeed
    {

        public String XmlInput()
        {
            string s = @"<? xml version = '1.0' encoding = 'ISO-8859-1' ?> <tstock><seccode> FMETF </seccode>< secname > First Metro Philippine Equity ETF </ secname >< bid > 6 </ bid >< bidvol > 2,090 </ bidvol >< bidprice > 129.30 </ bidprice >< ask > 1 </ ask >< askvol > 940 </ askvol >< askprice > 129.50 </ askprice >< status > ok </ status ></ tstock > ";
            return s;
        }
        public BidAskData Parse(string xmlString)
        {
            BidAskData data = new BidAskData();

            // Create an XmlReader
            using (XmlReader reader = XmlReader.Create(new StringReader(xmlString)))
            {
                CultureInfo provider = new CultureInfo("en-US");
                reader.ReadToFollowing("seccode");
                data.SecCode = reader.ReadElementContentAsString();
                //                reader.ReadToFollowing("secname");

                reader.ReadElementContentAsString(); //secname
                reader.ReadElementContentAsString(); //bid
                int bid = 0;
                int.TryParse(reader.ReadElementContentAsString(), System.Globalization.NumberStyles.AllowThousands,provider, out bid);
                data.BidVol = bid;
                // data.BidVol = int.Parse(reader.ReadElementContentAsString(),System.Globalization.NumberStyles.AllowThousands); //bidvol
                                                     //                reader.ReadToFollowing("bidprice");
                data.BidPrice = float.Parse(reader.ReadElementContentAsString());
                reader.ReadElementContentAsString(); //ask
                int ask = 0;
                int.TryParse( reader.ReadElementContentAsString(), System.Globalization.NumberStyles.AllowThousands, provider, out ask); //askvol
                data.AskVol = ask;
                data.AskPrice = float.Parse(reader.ReadElementContentAsString());
            }

            return data;
        }
        public String Connect()
        {

            string endPoint = System.Configuration.ConfigurationManager.AppSettings["bidask_endpoint"].ToString();
            WebClient wc = new WebClient();
            Stream data = wc.OpenRead(endPoint);
            StreamReader reader = new StreamReader(data);
            string s = reader.ReadToEnd();
            Console.WriteLine(s);
            data.Close();
            reader.Close();

            return s;
        }
    }
}
