﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace COMClassLibrary
{
    public partial class Constants
    {
        public static readonly String Diagnostics = "Diagnostics";
        public static readonly String RecogniaService = "RecogniaService";
        public static readonly String WebService = "WebService";
        public static readonly String EmailService = "EmailService";
        public static readonly String DatabaseService = "DatabaseService";
    }
}
