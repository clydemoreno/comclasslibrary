﻿Option Explicit
'Dim tradingManager As New COMClassLibrary.TradingRuleManager

'Dim tradingController As New COMClassLibrary.tradingController
'Dim tc As New COMClassLibrary.tradingController
Dim tradingController As New COMClassLibrary.tradingController



Dim configContext As New configContext

Dim timeFrame As String
Dim frequency As Long
Dim period As Long
Dim fullPath As String

Dim tempPrice As Double
Dim quotationFileName As String
Dim totalNumberOfStocks As Integer

Dim randomX As Integer
Dim volumePeriod As Integer
Dim simulatorMode As Boolean



Function SheetExists(strWSName As String) As Boolean
    Dim ws As Worksheet
    On Error Resume Next
    Set ws = Worksheets(strWSName)
    If Not ws Is Nothing Then SheetExists = True
End Function


Sub DropDown1_Change()

End Sub
 

Sub Button1_Click()

Dim sheetName As String

sheetName = "Configuration_sheet"
randomX = 1
'Message = tradingController.CallWebService(Range("D2").Value)



'If (Message <> "") Then
'    MsgBox Message
'End If



If SheetExists(sheetName) = True Then
   Worksheets(sheetName).Activate
   
    tradingController.Capital = Range("B1").Value
    tradingController.BaseLotSize = Range("B5").Value
    tradingController.ConfigurationString = Range("B2").Value
    tradingController.EndPoint = Range("B6").Value
    
   timeFrame = Range("B7").Value
   frequency = Range("B8").Value
   period = Range("B9").Value
   fullPath = Range("B10").Value
   quotationFileName = Range("B12").Value
   volumePeriod = Range("B13").Value
    tempPrice = 0
   simulatorMode = Range("B15").Value
   ' Call tradingController.SetParameters("requiredDerivative", Range("B14").Value)
    

End If


sheetName = "bloomberg_sheet"

If SheetExists(sheetName) = True Then
   Worksheets(sheetName).Activate
   
End If

 
 If ActiveSheet.Name = "bloomberg_sheet" Then

    

    DoThis
    
    
    StartTime
    
    ScheduleMonitoringOfFile

 End If



'DoThis
'MsgBox Range("A1").Offset(1, 1).Value


End Sub
Public Sub ScheduleMonitoringOfFile()


'tradingController.InstantiateFileManager fullPath, quotationFileName


Application.OnTime Now + TimeSerial(0, 0, 15), Procedure:="MonitorFile", Schedule:=True

End Sub
Private Sub MonitorFile()
   ' MsgBox "test"


    randomX = tradingController.RandomizeStartEnd(1, totalNumberOfStocks)
    
    ScheduleMonitoringOfFile
    
End Sub


Public Sub StartTime()
Application.OnTime Now + TimeSerial(0, 0, 15), Procedure:="TheSub", Schedule:=True

End Sub
Private Sub TheSub()
Dim sheetName As String
sheetName = "bloomberg_sheet"

If SheetExists(sheetName) = True Then

    DoThis
    StartTime
End If

End Sub

Private Sub DoThis()
Dim sheetName As String
sheetName = "bloomberg_sheet"

If ActiveSheet.Name = sheetName Then

Dim volume As Long
Dim x As Integer

Dim ax As String
Dim stockName As String
Dim ask As Double
Dim bid As Double
Dim last As Double
Dim high As Double
Dim low As Double
Dim result As String
    'Range("O2").Value = "Updated on: " & Now
    
    ax = Range("A1").Value
    x = 1
    Do Until ax = ""
    'Range("C1").Offset(x).Value = Range("A1").Offset(x).Value
    ax = Range("A1").Offset(x).Value
    'ticker
    stockName = Range("A1").Offset(x, 0).Value
    'orderScreen. = Range("A1").Offset(x, 0).Value
    'ask
    ask = Range("A1").Offset(x, 3).Value
    'bid
    bid = Range("A1").Offset(x, 2).Value
    
    last = Range("A1").Offset(x, 5).Value
    'high
    
    'high
    high = Range("A1").Offset(x, 7).Value
    'low
    low = Range("A1").Offset(x, 8).Value
    If Range("A1").Offset(x, 9).Value <> " " Then
        volume = Range("A1").Offset(x, 9).Value
    End If
    
    
    
    If stockName <> "" And randomX = x Then

        If tempPrice = 0 Then
            tempPrice = last
        End If
        
        'tempPrice = tradingController.Randomize(tempPrice)
        'last = tempPrice
        'Range("A1").Offset(x, 5).Value = last
        
        If simulatorMode = True Then
        
        last = tradingController.Randomize(last)
        If last < 0 Then
            last = last * -1
        End If
        Range("A1").Offset(x, 5).Value = last
        Range("A1").Offset(x, 5).Interior.Color = RGB(255, 255, 0)
        
        
        'volume = tradingController.RandomizeInteger(volume)
        If volume < 0 Then
            volume = volume * -1
        End If
        Range("A1").Offset(x, 9).Value = volume
        
        bid = tradingController.Randomize(bid)
        If bid < 0 Then
            bid = bid * -1
        End If
        Range("A1").Offset(x, 2).Value = bid
        
        End If
        
        'Get message
        Range("A1").Offset(x, 10).Value = tradingController.GetMessage("RSIStrategyRSIValue" & stockName)
        Range("A1").Offset(x, 11).Value = tradingController.GetMessage("RSIStrategyVolumeMAValue" & stockName)
        Range("A1").Offset(x, 12).Value = tradingController.GetMessage("RSIStatus" & stockName)
        
        Range("A1").Offset(x, 14).Value = tradingController.GetMessage("MyRSIErrorCode" & stockName)
        
        
        'Dim calue As Integer
        
        'calue = 5
        Call tradingController.SetParameters("stockName", stockName)
        Call tradingController.SetParameters("last", last)
        Call tradingController.SetParameters("bid", bid)
        Call tradingController.SetParameters("ask", ask)
        Call tradingController.SetParameters("volume", volume)
        Call tradingController.SetParameters("high", high)
        Call tradingController.SetParameters("low", low)
        Call tradingController.SetParameters("timeFrame", timeFrame)
        Call tradingController.SetParameters("frequency", frequency)
        Call tradingController.SetParameters("period", period)
        
        Dim startingX As Integer
        startingX = 41
         
        Call tradingController.SetParameters("startingX", startingX)
            
        
        
        Call tradingController.SetParameters("status", "WaitForVolumeThreshold")
     


        Call tradingController.SetParameters("volumema", 15313)
        
        Call tradingController.SetParameters("requiredDerivative", 0.95)
        Call tradingController.SetParameters("requiredSeparation", 1.05)
        Call tradingController.SetParameters("volumePeriod", 14)
        Call tradingController.SetParameters("requiredVolume", 1.3)

        Call tradingController.SetParameters("profitTargetLookupPeriod", 10)
        Call tradingController.SetParameters("stopLossLookupPeriod", 10)
        Call tradingController.SetParameters("requiredAbsoluteStopLossFactor", 0.93)
        Call tradingController.SetParameters("techniStockfilePath", "C:\\temp\\technistock\\orders.csv")
        Call tradingController.SetParameters("capital", 1000000)
        Call tradingController.SetParameters("capitalMultiplier", 0.8)
        Call tradingController.SetParameters("capitalDivisor", 5)
        
        
        'Range("A1").Offset(x, 14).Value = tradingController.GetMessage("key")
        
        'temporary only.
        'Call tradingController.SaveBloombergTicker(stockName, last, bid, ask, volume, high, low, Now, timeFrame, frequency, period)
        tradingController.RunStrategy (stockName)
    End If
    
  
    
    x = x + 1
    
    
    Range("A1").Offset(x, 5).Interior.ColorIndex = 0

    Loop

    totalNumberOfStocks = x
End If

End Sub

Private Sub GetColumns()

End Sub
