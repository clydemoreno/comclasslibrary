﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace COMClassLibrary
{
    public partial class Constants
    {

        public static readonly String ClientA = "ClientA";

        public static readonly String ClientB = "ClientB";

        public static readonly String IsBid = "IsBid";
        public static readonly String BidImportedOrderScreenList = "BidImportedOrderScreenList";

        public static readonly String AskImportedOrderScreenList = "AskImportedOrderScreenList";
        public static readonly String MarketMakerOffSet = "MarketMakerOffSet";
        public static readonly String FMETF = "FMETF";
        public static readonly String FMETF_PM = "FMETF PM";
        public static readonly String FMETFMM = "FMETF-MM";

        public static readonly String FMSecSource = "FMSec";

        public static readonly String EventTab = "EventTab";

        public static readonly String RSISheetName = "RSI";
        public static readonly String IntradaySheetName = "Intraday";
        public static readonly String MarketMakerSheetName = "MarketMaker";
        public static readonly String RecogniaSheetName = "Recognia";

        public static readonly String RSIStatus = "RSIStatus";
        public static readonly String RSIDerivativeValue = "RSIDerivativeValue";
        public static readonly String RSIStrategyRSIValue = "RSIStrategyRSIValue";
        public static readonly String RSIStrategyVolumeMAValue = "RSIStrategyVolumeMAValue";
        public static readonly String CapitalCalculatorBoardLotList = "COMClassLibrary.Manager.CapitalCalculator.BoardLotList";
       
        public static readonly String MyRSIErrorCode = "MyRSIErrorCode";

        public static readonly String iNAVIsOutsideOfSpread = "iNAV is outside of spread";
        public static readonly String SpreadWidthMessage = "Spread is too wide";

        public static readonly String MyIntradayErrorCode = "MyIntradayErrorCode";

        public static readonly String iNAV = "iNAV";
        public static readonly String iNAVUnableToGetMessage = "Unable to get iNAV price";
        public static readonly String numberOfFlux = "numberOfFlux";

        public static readonly String bidValue = "bidValue";
        public static readonly String askValue = "askValue";


        public static readonly String toEmail = "toEmail";
        public static readonly String fromEmail = "fromEmail";
        public static readonly String RSIWaitingMode = "RSIWaitingMode";
        public static readonly String RecogniaWaitingMode = "RecogniaWaitingMode";
        public static readonly String IntradayWaitingMode = "IntradayWaitingMode";
        public static readonly String SaveTimeFrameAndRSI = "TradingController.SaveTimeFrameAndRSI";
        public static readonly String OrderFilledCode = "F";
        public static readonly String IntradayStatus = "IntradayStatus";
        public static readonly String IntradayError = "IntradayError";
        public static readonly String IntradayStoppedOut = "Intraday StoppedOut";
        public static readonly String IntraydayProfitTargetHit = "Intraday Profit TargetHit";
        public static readonly String RecogniaStatus = "RecogniaStatus";
        public static readonly String RecogniaError = "RecogniaError";
        public static readonly String RecogniaKeywordFound = "RecogniaKeywordFound";
        public static readonly String RSIStoppedOut = "RSI Stopped Out";
        public static readonly String RecogniaStoppedOut = "Recognia Stopped Out";
        public static readonly String RSIProfitTargetHit = "RSI Profit TargetHit";
        public static readonly String RecogniaProfitTargetHit = "Recognia Profit TargetHit";
        public static readonly String RSIWaitForBreakout = "RSIWaitForBreakout";
        public static readonly String RSIWaitForVolumeThreshold = "RSIWaitForVolumeThreshold";
        public static readonly String RSIWaitingToBeFilled = "RSIWaitingToBeFilled";
        public static readonly String RecogniaWaitingToBeFilled = "RecogniaWaitingToBeFilled";
        public static readonly String IntradayWaitingToBeFilled = "IntradayWaitingToBeFilled";
        public static readonly String RSIOrderFilled = "RSIOrderFilled";
        public static readonly String RSIX_AxisLinearRegressionPeriodList = "RSIX_AxisLinearRegressionPeriodList";
        public static readonly String RSIY_AxisLinearRegressionPeriodList = "RSIY_AxisLinearRegressionPeriodList";
        public static readonly String CurrentRSITO = "CurrentRSITO";
        public static readonly String CurrentRSIDerivative = "CurrentRSIDerivative";
        public static readonly String RSILinearRegressionRequirementPassed = "RSI Linear Regression Requirement Passed";
        public static readonly String RSIRequiredDerivativeIsMet = "RSIRequiredDerivativeIsMet";
        public static readonly String IntradayOrderFilled = "IntradayOrderFilled";
        public static readonly String RecogniaOrderFilled = "RecogniaOrderFilled";
        //public static readonly String FoundRecogniaEntry = "Found Recognia Entry";
        //public static readonly String RecogniaMessageCode = "RecogniaMessageCode";
        public static readonly String CurrentVolumeMATO = "CurrentVolumeMATO";
        public static readonly String iNAVURL = "iNAVURL";
        public static readonly String iNAVPattern = "iNAVPattern";
        public static readonly String IntradayRatioReached = "Intraday Bid to Ask Ratio";
        public static readonly String IntradayCancel = "IntradayCancel";



       
    }
}
