﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using COMClassLibrary.FMServiceReference;
using COMClassLibrary.MessageHandlers;
using System.Diagnostics;
using System.Threading.Tasks;
using COMClassLibrary.Model;
using FirstMetroSystemTrayApp.Model;

namespace COMClassLibrary.Manager
{
    public class MarketMakerStrategy : OrderManager, IStrategy
    {
        private double iNav;
        public void Run(string stockName, Dictionary<string, object> parameters)
        {
            if (!IsDictionaryValid(stockName, parameters)) return;

            try
            {
                GetINAVPrice(stockName, parameters);
                
            }
            catch(Exception ex)
            {
                EventLog.WriteEntry(Constants.FMSecSource, ex.Message, EventLogEntryType.Error);
                throw ex;
            }
            try
            {
                if (IsOutsideOfSpreadDeprecated(stockName, parameters))
                {
                    double iNav = (double)GetParameter(stockName, Constants.iNAV, parameters);

                    SetMessage(stockName, Constants.iNAVIsOutsideOfSpread, iNav.ToString(), parameters, new MMOutsideSpreadHandler());
                }
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry(Constants.FMSecSource, ex.Message, EventLogEntryType.Error);
                throw new Exception("Unable to run outside of spread method");
            }
            try
            {
                if (IsThreshholdOfSpreadBreached(stockName, parameters))
                {
                    string numberOfFlux = (string)GetParameter(stockName, Constants.numberOfFlux, parameters);
                    SetMessage(stockName, Constants.SpreadWidthMessage, numberOfFlux, parameters, new MMSpreadWidthHandler());

                }
            }
            catch (Exception ex)
            {

                EventLog.WriteEntry(Constants.FMSecSource, ex.Message, EventLogEntryType.Error);
                throw new Exception("Unable to run spread breached method");
            }



            //try
            //{
            //    MaintainSpread(stockName, parameters);
            //    //GetAskBid(stockName, parameters);
            //}
            //catch (Exception ex)
            //{
            //    EventLog.WriteEntry(Constants.FMSecSource, ex.Message, EventLogEntryType.Error);
            //    throw ex;
            //}
            
        }
        public AskBid GetBidWithAdjustedOffer(String stockName, Dictionary<String, object> parameters)
        {
            //this method is deprecated

            AskBid askBid = new AskBid();
            double ask = (double)GetParameter(stockName, "ask", parameters);
            double bid = (double)GetParameter(stockName, "bid", parameters);
            double iNAVPrice = (double)GetParameter(stockName, Constants.iNAV, parameters);
            double offset = (double)GetParameter(stockName, Constants.MarketMakerOffSet, parameters);
            double spreadThreshold = (double)GetParameter(stockName, "spreadThreshold", parameters);

            CapitalCalculator c = new CapitalCalculator();

            BoardLotFluctuationPriceTO boardLot = c.GetBoardLotBasedOnPrice(ask);
             

             MatchingEngineParams p = new MatchingEngineParams();
             p.OfferFlucs = boardLot.Fluctuation;
             boardLot = c.GetBoardLotBasedOnPrice(bid);
             p.BidFlucs = boardLot.Fluctuation;
                 
             p.BidFlucs = 0.10;
             p.INAV = 110.50;
             p.Bid= 110.50;
             p.Offer = 121.00;
             p.SkewView = 0.50;
             p.MaxSpread = 10;
             p.MinPostVolume = 50;
             p.MarketOfferVolume = 2000;

            return askBid;

        }
        public AskBid GetAskBid(String stockName, Dictionary<String, object> parameters)
        {
            AskBid askBid = new AskBid();
            double ask = (double)GetParameter(stockName, "ask", parameters);
            double bid = (double)GetParameter(stockName, "bid", parameters);
            double iNAVPrice = (double)GetParameter(stockName, Constants.iNAV, parameters);
            double offset = (double)GetParameter(stockName, Constants.MarketMakerOffSet , parameters);
            double spreadThreshold = (double)GetParameter(stockName, "spreadThreshold", parameters);

            CapitalCalculator c = new CapitalCalculator();

            BoardLotFluctuationPriceTO boardLot = c.GetBoardLotBasedOnPrice(iNAVPrice);

            double spread = (boardLot.Fluctuation * spreadThreshold);
            offset = offset * boardLot.Fluctuation;

            //double numberOfFlux = spread / (boardLot.Fluctuation * spreadThreshold);


            //convert offset to boardlot flucs
            double defaultBid = -1 * Math.Floor(spread / 2) + iNAVPrice + offset;
            double defaultAsk = Math.Ceiling(spread / 2) + iNAVPrice + offset;

            double marketMakerAsk = (ask < defaultAsk) ? 0 : defaultAsk;
            double marketMakerBid = (bid > defaultBid) ? 0 : defaultBid;
            
            askBid.Ask = marketMakerAsk;
            askBid.Bid = marketMakerBid;
            return askBid;
        }

        public void MaintainSpread(string stockName, Dictionary<string, object> parameters)
        {
            MatchingEngineParams p = new MatchingEngineParams();

            double bid = (double)GetParameter(stockName, "bid", parameters);
            double ask = (double)GetParameter(stockName, "ask", parameters);
            double skewView = Convert.ToDouble(GetParameter(stockName, "skewView", parameters));
            double minPostVolume = Convert.ToInt32(GetParameter(stockName, "minPostVolume", parameters));
            int bidSize = Convert.ToInt32(GetParameter(stockName, "bidSize", parameters));
            int askSize = Convert.ToInt32(GetParameter(stockName, "askSize", parameters));

            int bidVolume = Convert.ToInt32(GetParameter(stockName, "bidVolume", parameters));
            int askVolume = Convert.ToInt32(GetParameter(stockName, "askVolume", parameters));
            String marketMakerFilePath = (String)GetParameter(stockName, "marketMakerFilePath", parameters);
            String marketMakerBuyer = (String)GetParameter(stockName, "marketMakerBuyer", parameters);
            String marketMakerSeller = (String)GetParameter(stockName, "marketMakerSeller", parameters);
            String cancelFilePath = (String)GetParameter(stockName, "cancelFilePath", parameters);    

            //bool isBid = (bool)GetParameter(stockName, Constants.IsBid, parameters);
            List<ImportedOrderScreen> askImportedOrderScreenList = (List<ImportedOrderScreen>)GetParameter(stockName, Constants.AskImportedOrderScreenList, parameters);
            List<ImportedOrderScreen> bidImportedOrderScreenList = (List<ImportedOrderScreen>)GetParameter(stockName, Constants.BidImportedOrderScreenList, parameters);

            if (askImportedOrderScreenList == null) askImportedOrderScreenList = new List<ImportedOrderScreen>();
            if (bidImportedOrderScreenList == null) bidImportedOrderScreenList = new List<ImportedOrderScreen>() ;

            //spreadThreshold is maxSpread
            double spreadThreshold = (double)GetParameter(stockName, "spreadThreshold", parameters);

            if (!DoesKeyExist(stockName, Constants.iNAV, parameters)) throw new Exception(Constants.iNAVUnableToGetMessage);


            double iNAVprice = (double)GetParameter(stockName, Constants.iNAV, parameters);

            if (iNAVprice == 0) throw new Exception(Constants.iNAVUnableToGetMessage);

            p.INAV = iNAVprice;
            p.Bid = bid;
            p.Offer = ask;
            //p.BestBid = bid;
            //p.BestOffer = ask;
            p.SkewView = skewView;
            p.MaxSpread = spreadThreshold;
            p.MinPostVolume = minPostVolume;
            p.MarketBidVolume = bidSize;
            p.MarketOfferVolume = askSize;
            p.BidVolume = bidVolume;
            p.AskVolume = askVolume;
            p.FilePath = marketMakerFilePath;
            p.CancelFilePath = cancelFilePath;
            p.Buyer = marketMakerBuyer;
            p.Seller = marketMakerSeller;

            SpreadAnchor sAnchor = new SpreadAnchor();
           
            sAnchor.SetBid(p, bidImportedOrderScreenList);
            sAnchor.SetOffer(p, askImportedOrderScreenList);

            SetParameters(stockName, Constants.BidImportedOrderScreenList, bidImportedOrderScreenList, parameters);
            SetParameters(stockName, Constants.AskImportedOrderScreenList, askImportedOrderScreenList, parameters);

            p.AskImportedOrderScreenList = askImportedOrderScreenList;
            p.BidImportedOrderScreenList = bidImportedOrderScreenList;
            OnChanged(parameters, new MMMaintainSpreadEventArg(p));


        }

        public new void SetErrorMessage(String stockName, string value, Dictionary<String, object> parameters)
        {
            base.SetErrorMessage(AddClassNameToStockName(stockName), value, parameters);
        }

        private async void GetINAVPrice(string stockName, Dictionary<string, object> parameters)
        {

            try
            {
                String iNavURL = (String)GetParameter(stockName, Constants.iNAVURL, parameters);
                String iNavPattern = (String)GetParameter(stockName, Constants.iNAVPattern, parameters);
                //double iNAVprice = 
                Task<double> task = Task<double>.Run(() => iNAVController.GetPrice(iNavURL, iNavPattern));

                //iNav = iNAVController.GetPrice(iNavURL, iNavPattern);

                double iNavPrice = await task;

                
                
                SetParameters(stockName, Constants.iNAV,  iNavPrice, parameters);
                MaintainSpread(stockName, parameters);
                OnChanged(parameters, new MessageEventArg(stockName, Constants.iNAV, iNavPrice.ToString(), this.GetType().ToString(), null));

            }
            catch (Exception ex)
            {
                SetMessage(stockName, Constants.MyRSIErrorCode, Constants.iNAVUnableToGetMessage, parameters, null);
                EventLog.WriteEntry(Constants.FMSecSource, ex.Message, EventLogEntryType.Error);
            }

            //MaintainSpread(stockName, parameters);

        }

        private async void testTestMethod()
        {
            Task<string> task = Task<string>.Run(() => testMethod());
            //String test = await task;
            String test = await task;
            Debug.WriteLine(test);

        }
        private  string testMethod()
        {
            return "true";
        }
        public bool IsOutsideOfSpreadDeprecated(String stockName, Dictionary<String,object> parameters)
        {
            
            String iNavURL = (String)GetParameter(stockName, Constants.iNAVURL, parameters);
            String iNavPattern = (String)GetParameter(stockName, Constants.iNAVPattern, parameters);

            if(!DoesKeyExist(stockName, Constants.iNAV, parameters)) return false;
            double iNAVprice = (double)GetParameter(stockName, Constants.iNAV, parameters);


            double bid = (double)GetParameter(stockName, "bid", parameters) ;
            double ask = (double)GetParameter(stockName, "ask", parameters) ;


            //double iNAVprice = iNAVController.GetPrice(iNavURL, iNavPattern);
            SetParameters(stockName, Constants.iNAV, iNAVprice, parameters);
            
            //if price is outside of ask size or bidsize, it means it is outside of the spread
            return (iNAVprice > ask || iNAVprice < bid);
          
        }

        public bool IsThreshholdOfSpreadBreached(String stockName, Dictionary<String, object> parameters)
        {
            //get board lot
            CapitalCalculator c = new CapitalCalculator();

            double bid = (double)GetParameter(stockName, "bid", parameters);
            double ask = (double)GetParameter(stockName, "ask", parameters);
            double spreadThreshold = (double)GetParameter(stockName, "spreadThreshold", parameters);
            
            BoardLotFluctuationPriceTO boardLot = c.GetBoardLotBasedOnPrice( bid);

            double spread = ask - bid;

            double numberOfFlux = spread / boardLot.Fluctuation;

            //SetParameters(stockName, Constants.numberOfFlux, numberOfFlux, parameters);

            //FMServiceReference.BoardLotFluctuationPriceTO[] list = c.BoardLotList;

            SetMessage(stockName, Constants.numberOfFlux, numberOfFlux.ToString(), parameters, new MMSpreadWidthHandler());
            // divide bid n ask by flux
            return (numberOfFlux > spreadThreshold);
            
        }

        public override Object GetParameter(String stockName, String key, Dictionary<String, object> parameters)
        {
            return base.GetParameter(String.Format("{0},{1}", stockName, this.GetType()), key, parameters);
        }

        public override bool DoesKeyExist(String stockName, String key, Dictionary<String, object> parameters)
        {
            return base.DoesKeyExist(String.Format("{0},{1}", stockName, this.GetType()), key, parameters);
        }

        public override void SetParameters(String stockName, String key, object value, Dictionary<String, object> parameters)
        {
            base.SetParameters(AddClassNameToStockName(stockName), key, value, parameters);
        }

        public bool IsDictionaryValid(string stockName, Dictionary<string, object> parameters)
        {
            bool returnValue = true;

            try
            {
                //status could be blank but the key must exist
                if (!DoesKeyExist(stockName, Constants.iNAVPattern, parameters)) throw new Exception("Missing iNAVPattern parameter");
                if (!DoesKeyExist(stockName, Constants.iNAVURL, parameters)) throw new Exception("Missing iNAVURL parameter");

                if (!DoesKeyExist(stockName, "bid", parameters)) throw new Exception("Missing bid parameter");
                if (!DoesKeyExist(stockName, "ask", parameters)) throw new Exception("Missing ask parameter");
                if (!DoesKeyExist(stockName, "skewView", parameters)) throw new Exception("Missing skewView parameter");
                if (!DoesKeyExist(stockName, "minPostVolume", parameters)) throw new Exception("Missing minPostVolume parameter");

                if (!DoesKeyExist(stockName, "bidSize", parameters)) throw new Exception("Missing bidSize parameter");
                if (!DoesKeyExist(stockName, "askSize", parameters)) throw new Exception("Missing askSize parameter");

                if (!DoesKeyExist(stockName, "spreadThreshold", parameters)) throw new Exception("Missing spreadThreshold parameter");
                if (!DoesKeyExist(stockName, Constants.AskImportedOrderScreenList, parameters)) throw new Exception("Missing ImportedOrderScreenList parameter");

 
                if ((double)GetParameter(stockName, "bid", parameters) <= 0)
                {
                    throw new Exception("Bid should be more than zero");
                }
                if ((double)GetParameter(stockName, "bid", parameters) <= 0)
                {
                    throw new Exception("Ask should be more than zero");
                }
                returnValue = true;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry(Constants.FMSecSource, ex.Message, EventLogEntryType.Error);
                SetMessage(stockName, Constants.MyRSIErrorCode,String.Format("Stock:{0} {1}",stockName, ex.Message), parameters, new RSIErrorMessageHandler());
 
                returnValue = false;
            }
            return returnValue;

        }
    }
}
