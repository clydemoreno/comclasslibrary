﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using COMClassLibrary.FMServiceReference;
using COMClassLibrary.MessageHandlers;

namespace COMClassLibrary.Manager
{
    public class RSIErrorEventArg : EventArgs
    {
        //public String ErrorMessage;
        public String Code = Constants.MyRSIErrorCode;
        public RSIErrorEventArg()
        {
            
        }

    }
    
    public class RSIBreakoutStrategy : OrderManager,IStrategy
    {



        double stopLoss;

        public double StopLoss
        {
            get { return stopLoss; }
            set { stopLoss = value; }
        }
        double profitTarget;

        public double ProfitTarget
        {
            get { return profitTarget; }
            set { profitTarget = value; }
        }

        private double requiredDerivative;

        public double RequiredDerivative
        {
            get { return requiredDerivative; }
            set { requiredDerivative = value; }
        }
        //private string status;

        private RSICalculator rsiCalculator;

        public RSICalculator RSICalculator
        {
            get {


                if (rsiCalculator == null)
                {
                    rsiCalculator = new RSICalculator();
                }
                return rsiCalculator; 
            }
            set { rsiCalculator = value; }
        }

        public double CalculateStopLoss(List<double> list)
        {
            double lowest = 0;
            foreach (double price in list)
            {
                if (price < lowest)
                {
                    lowest = price;
                }
            }
            return lowest;
        }

        public double CalculateProfitTarget(List<double> list)
        {
            double highest = 0;
            foreach (double price in list)
            {
                if (price < highest)
                {
                    highest = price;
                }
            }
            return highest;
        }

        private bool IsAbsoluteStopLossReached(double percentage, double currentPrice, double entryPrice)
        {
            bool returnValue = false;
            if (entryPrice == 0) return returnValue;
            if (currentPrice / entryPrice <= percentage)
            {
                returnValue = true;
            }
            return returnValue;
        }

        public bool exitTrade()
        {
            return false;
        }

        //private int currentX = 0;
        //private List<double> currentXList;
        //private List<double> currentYList;
        //private RSI rsi = new RSI();
        //private double breakoutY = 0;

        public override bool DoesKeyExist(String stockName, String key, Dictionary<String, object> parameters)
        {
            return base.DoesKeyExist(String.Format("{0},{1}", stockName, this.GetType()), key, parameters);
        }

        public bool IsDictionaryValid(String stockName, Dictionary<string, object> parameters)
        {
            bool returnValue = true;
            //status wil be blank we can check the key later
            if (!DoesKeyExist(stockName,Constants.RSIStatus,parameters)) throw new Exception("Missing Status parameter");
            if (!DoesKeyExist(stockName, "cutoffDate", parameters)) throw new Exception("Missing cutoffDate parameter");
            if (!DoesKeyExist(stockName, "startingX",parameters)) throw new Exception("Missing startingX parameter");
            if (!DoesKeyExist(stockName, "requiredDerivative",parameters)) throw new Exception("Missing requiredDerivative parameter");
            if (!DoesKeyExist(stockName, "period",parameters)) throw new Exception("Missing period parameter");
            if (!DoesKeyExist(stockName, "frequency",parameters)) throw new Exception("Missing frequency parameter");
            if (!DoesKeyExist(stockName, "timeFrame",parameters)) throw new Exception("Missing timeFrame parameter");
            if (!DoesKeyExist(stockName, "requiredSeparation",parameters)) throw new Exception("Missing requiredSeparation parameter");
            if (!DoesKeyExist(stockName, "volumePeriod",parameters)) throw new Exception("Missing volumePeriod parameter");
            if (!DoesKeyExist(stockName, "last",parameters)) throw new Exception("Missing last parameter");
            if (!DoesKeyExist(stockName, "requiredVolume",parameters)) throw new Exception("Missing requiredVolume parameter");
            if (!DoesKeyExist(stockName, "profitTargetLookupPeriod",parameters)) throw new Exception("Missing profitTargetLookupPeriod parameter");
            if (!DoesKeyExist(stockName, "stopLossLookupPeriod",parameters)) throw new Exception("Missing stopLossLookupPeriod parameter");
            if (!DoesKeyExist(stockName, "requiredAbsoluteStopLossFactor",parameters)) throw new Exception("Missing requiredAbsoluteStopLossFactor parameter");
            if (!DoesKeyExist(stockName, "techniStockfilePath",parameters)) throw new Exception("Missing techniStockfilePath parameter");
            if (!DoesKeyExist(stockName, "capital",parameters)) throw new Exception("Missing capital parameter");
            if (!DoesKeyExist(stockName, "capitalMultiplier",parameters)) throw new Exception("Missing capitalMultiplier parameter");
            if (!DoesKeyExist(stockName, "capitalDivisor",parameters)) throw new Exception("Missing capitalDivisor parameter");
            if (!DoesKeyExist(stockName, "lineRegressionRSIPeriod",parameters)) throw new Exception("Missing lineRegressionRSIPeriod parameter");
            if (!DoesKeyExist(stockName, "rsiPeriod", parameters)) throw new Exception("Missing rsiPeriod parameter");
            if (!DoesKeyExist(stockName, Constants.RSIX_AxisLinearRegressionPeriodList,parameters)) throw new Exception("Missing RSIX_AxisLinearRegressionPeriodList parameter");
            if (!DoesKeyExist(stockName, Constants.RSIY_AxisLinearRegressionPeriodList,parameters)) throw new Exception("Missing RSIY_AxisLinearRegressionPeriodList parameter");
           
            return returnValue;
        }

        public override void G() {
            
            Console.WriteLine("B.G {0}", this.GetType()); 
        }

        new public void F()
        {
            Console.WriteLine("B.F {0}", this.GetType()); 
        }

        public override void SetParameters(String stockName, String key, object value, Dictionary<String, object> parameters)
        {
            base.SetParameters(AddClassNameToStockName(stockName), key, value, parameters);
        }



        public void GetRSIList(string stockName, Dictionary<String, object> parameters)
        {
            //todo:  add some caching capabilities.

            try
            {

                RSICalculator calc = new RSICalculator();
                DateTime cutoffDate = (DateTime)GetParameter(stockName, "cutoffDate", parameters);
                RSITO[] rsilist = calc.GetRSIFromDb(stockName, (int)GetParameter(stockName, "lineRegressionRSIPeriod", parameters)
                    , (int)GetParameter(stockName, "frequency", parameters)
                    , (string)GetParameter(stockName, "timeFrame", parameters)
                    , cutoffDate);
                

                List<double> xList = new List<double>();
                List<double> yList = new List<double>();

                for (int i = 0; i < rsilist.Length; i++)
                {
                    xList.Add(Convert.ToDouble(i + 1));
                    yList.Add(rsilist[i].RsiValue);
                }

                SetParameters(stockName, Constants.RSIX_AxisLinearRegressionPeriodList, xList, parameters);

                SetParameters(stockName, Constants.RSIY_AxisLinearRegressionPeriodList, yList, parameters);


                //set the current RSI also
                // currentRSI
                RSITO currentRSI = rsilist.Length > 0 ? rsilist[rsilist.Length - 1] : null;

                SetParameters(stockName, Constants.CurrentRSITO, currentRSI, parameters);

            }
            catch (Exception ex)
            {
                SetMessage(stockName, Constants.MyRSIErrorCode, "Error: GetRSIList: stack trace: " + ex.Message, parameters, new RSIErrorMessageHandler());

                //throw new Exception("Error: GetRSIList: stack trace: " + ex.Message);
            }


        }

        public void GetOrderStatus(string stockName, Dictionary<string, object> parameters)
        {
            try
            {
                OrderTO to = (OrderTO)GetParameter(stockName, Constants.RSIOrderTOCode, parameters);
                if (to == null)
                    to = new OrderTO();
               // SetParameters(stockName, Constants.RSIStatus, base.GetOrderStatus(stockName),parameters);
                SetParameters(stockName, Constants.RSIStatus, to != null ? to.Status : "" , parameters);
                SetParameters(stockName, Constants.RSIOrderTOCode, to, parameters);
                //set y intercept to the length of the RSI Linear regression until we figure out a way to persist the value
                RSI rsi = new RSI();
                rsi.Derivative = to.DerivativeValue;
               
                rsi.YIntercept = (int)GetParameter(stockName, "lineRegressionRSIPeriod", parameters);

                SetParameters(stockName, Constants.CurrentRSIDerivative, rsi, parameters);
                
                
            }
            catch (Exception ex)
            {
                SetMessage(stockName, Constants.MyRSIErrorCode, ex.Message, parameters, new RSIErrorMessageHandler());
                //this.SetErrorMessage(stockName, ex.Message, parameters);
            }
        }

        public  new void SetErrorMessage(String stockName, string value, Dictionary<String, object> parameters)
        {
            base.SetErrorMessage(AddClassNameToStockName( stockName),value, parameters);
        }
        public void Run(String stockName, Dictionary<string, object> parameters)
        {
            this.StockName = stockName;

            //RSICalculator calculator = new RSICalculator();
            //call web service
            //  GetStatus(string stockName);
            //check status

            try
            {
                IsDictionaryValid(stockName, parameters);


                if (IsRSIRequiredDerivativeIsMet(stockName, parameters))
                {
                    SetMessage(stockName, Constants.RSIRequiredDerivativeIsMet, Constants.RSIRequiredDerivativeIsMet, parameters, new RSIDerivativeIsMetHandler());
                    SetStatus(stockName, Constants.RSIRequiredDerivativeIsMet, parameters);
                }

                //if (this.CapCalculator.ConfigContext == null)
                //{
                //    throw new Exception("Missing config context");
                //}
            }
            catch (Exception ex)
            {
                //log the exceptions
                //this.ErrorMessage = ex.Message;
                //this.SetErrorMessage(stockName, ex.Message, parameters);
                SetMessage(stockName, Constants.MyRSIErrorCode, ex.Message, parameters, new RSIErrorMessageHandler());

                return;
            }
        }

        public bool IsRSIRequiredDerivativeIsMet(String stockName, Dictionary<string, object> parameters)
        {
            bool returnValue = false;
            RSI rsi = (RSI) GetParameter (stockName,Constants.CurrentRSIDerivative,parameters);
            double requiredDerivative = (double)GetParameter(stockName, "requiredDerivative", parameters);
            if (rsi != null)
            {
                returnValue = (rsi.Derivative < requiredDerivative);
            }
            
            return returnValue;
        }

        public RSI GetRSI(String stockName, Dictionary<string, object> parameters)
        {
            List<double> xList = null;
            List<double> yList = null;
            RSI rsi = null;
            try
            {
                xList = (List<double>)GetParameter(stockName, Constants.RSIX_AxisLinearRegressionPeriodList, parameters);
                yList = (List<double>)GetParameter(stockName, Constants.RSIY_AxisLinearRegressionPeriodList, parameters);
            }
            catch
            {
                throw new Exception("Unable to cast parameter Linear Regression List to right array in RSI Breakout strategy");
            }

            if (xList == null || (xList != null && xList.Count == 0))
            {
                SetMessage(stockName, Constants.RSIMessage, "RSI X list is not fully populated yet", parameters, new RSIMessageHandler());
                return null;
            }
            if (yList == null || (yList != null && yList.Count == 0))
            {
                SetMessage(stockName, Constants.RSIMessage, "RSI Y list is not fully populated yet", parameters, new RSIMessageHandler());
                return null;
            }

            //RSI rsi = RSICalculator.GetRSIUsingLinearRegressionWithThreeCriticalPoints(xList, yList, startingX);
            //use all the points intead and find out if it is negative.

            rsi = RSICalculator.GetRSIUsingLinearRegressionWithAllPoints(xList, yList);
            if (rsi != null)
            {
                SetMessage(stockName, Constants.RSIDerivativeValue, rsi.Derivative.ToString(), parameters, new RSIDerivativeMessageHandler());
            }
            return rsi;
        }
        public void RunStrategy(String stockName, Dictionary<string, object> parameters)
        {
            this.StockName = stockName;

            //RSICalculator calculator = new RSICalculator();
            //call web service
            //  GetStatus(string stockName);
            //check status

            try
            {
                IsDictionaryValid(stockName,parameters);

                //if (this.CapCalculator.ConfigContext == null)
                //{
                //    throw new Exception("Missing config context");
                //}
            }
            catch (Exception ex)
            {
                //log the exceptions
                //this.ErrorMessage = ex.Message;
                //this.SetErrorMessage(stockName, ex.Message, parameters);
                SetMessage(stockName, Constants.MyRSIErrorCode, ex.Message, parameters, new RSIErrorMessageHandler());

                return;
            }


            String timeFrame = String.Empty;
            int frequency = 0;
            double last = 0;
            int volume = 0;
            int startingX = 40;
            try
            {
                startingX = Convert.ToInt32(GetParameter(stockName, "startingX", parameters));
                this.RequiredDerivative = (double)GetParameter(stockName, "requiredDerivative", parameters);
                this.TechniStockfilePath = (string)GetParameter(stockName, "techniStockfilePath", parameters);

                frequency = (int)GetParameter(stockName, "frequency", parameters);
                timeFrame = (string)GetParameter(stockName, "timeFrame", parameters);

                volume = Convert.ToInt32(GetParameter(stockName, "volume", parameters));

                last = (double)GetParameter(stockName, "last", parameters);
            }
            catch (Exception ex)
            {
                
                throw new Exception("Error in setting the parameters: " + ex.Message);
            }


            if (String.IsNullOrEmpty(GetStatus(stockName, parameters)))
            {
                
                SetStatus(stockName, Constants.RSIWaitingMode, parameters);

            }

            if (GetStatus(stockName, parameters) == Constants.RSIWaitingMode)
            {
                

                //List<double> xList = null;
                //List<double> yList = null;

                //try
                //{
                //    xList = (List<double>)GetParameter(stockName, Constants.RSIX_AxisLinearRegressionPeriodList, parameters);
                //    yList = (List<double>)GetParameter(stockName, Constants.RSIY_AxisLinearRegressionPeriodList, parameters);
                //}
                //catch
                //{
                //    throw new Exception("Unable to cast parameter Linear Regression List to right array in RSI Breakout strategy");
                //}

                //if (xList == null || (xList != null && xList.Count == 0))
                //{
                //    SetMessage(stockName, Constants.RSIMessage, "RSI X list is not fully populated yet", parameters, new RSIMessageHandler());
                //    return;
                //}
                //if (yList == null || (yList != null && yList.Count == 0))
                //{
                //    SetMessage(stockName, Constants.RSIMessage,  "RSI Y list is not fully populated yet", parameters,new RSIMessageHandler());
                //    return;
                //}

                ////RSI rsi = RSICalculator.GetRSIUsingLinearRegressionWithThreeCriticalPoints(xList, yList, startingX);
                ////use all the points intead and find out if it is negative.

                //RSI rsi = RSICalculator.GetRSIUsingLinearRegressionWithAllPoints(xList, yList);
                

                //check the negative slope if not within criteria then just return
                //modify this so we can go down

                //send calculated derivative to Excel
                RSI rsi = GetRSI(stockName, parameters);

                //this.SetMessage(Constants.RSIDerivativeValue + this.StockName, rsi.Derivative.ToString());

                if (rsi.Derivative > this.RequiredDerivative){
                    //did not meet the requirement
                    return;
                }
                //it means we made it thru the requirement
                SetStatus(stockName, Constants.RSIRequiredDerivativeIsMet, parameters);
                
                SetParameters(stockName,Constants.CurrentRSIDerivative,rsi, parameters);
                return;
            }


            if (GetStatus(stockName, parameters) == Constants.RSIRequiredDerivativeIsMet)
            {
                int incrementingX  = 0;
                RSI rsi = (RSI)GetParameter(stockName, Constants.CurrentRSIDerivative, parameters);
                


                //make sure you send a message when this happens.
                if (rsi == null) return;
                object obj = GetParameter(stockName, Constants.IncrementingX, parameters);
                if(obj != null)
                    incrementingX  = Convert.ToInt32(GetParameter(stockName, Constants.IncrementingX, parameters));
                incrementingX += 1;
                SetParameters(stockName, Constants.IncrementingX, incrementingX, parameters);
                incrementingX = Convert.ToInt32(GetParameter(stockName, Constants.IncrementingX, parameters));
                double y = (startingX * rsi.Derivative) + rsi.YIntercept;

                if (y == 0)
                {
                    throw new Exception("The derivative and x value produced a y value of zero");
                }

                RSITO rsiTO = (RSITO)GetParameter(stockName, Constants.CurrentRSITO, parameters);
                double requiredSeparation = (double)GetParameter(stockName, "requiredSeparation", parameters);
                if ((rsiTO.RsiValue / y) > requiredSeparation)
                {
                    SetStatus(stockName, Constants.RSIWaitForVolumeThreshold, parameters);
                    //this.Status = "WaitForVolumeThreshold";
                    incrementingX = 0;
                    SetParameters(stockName, Constants.IncrementingX, incrementingX, parameters);
                    SetMessage(stockName, Constants.IncrementingX, incrementingX.ToString() , parameters,null);
                }

            }
            
                

            
            if ((String)GetParameter(stockName, Constants.RSIStatus, parameters) == Constants.RSIWaitForVolumeThreshold)
            {

                //calculate profit target
                //calculate stop loss
                //buy stocks
                //create a file
                // set buy price or limit price from GetParameter(stockName,"last",parameters)
                //FileManager.Instance;
                //calculate lots

                bool volumeSignal = CheckForVolume(stockName,parameters);
                if (!volumeSignal)
                    return;

               
                //unused right now.
               // bool orderStatusBrokerSignal  = CheckStatusFromByBroker(stockName, parameters);

                //todo: implement check of status when the status is available in technistock
                //if (!orderStatusBrokerSignal)
                //    return;
                
                 
                
                //calculate the highest

                CalculateProfitTargetAndStopLoss(stockName, parameters, timeFrame, frequency, last);

                if (last < this.StopLoss)
                {
                    SetMessage(stockName,Constants.RSIMessage, Constants.RSICurrentPriceLowerThanStopLoss,parameters,null);
                    return;
                }
                if (last > this.ProfitTarget)
                {
                    SetMessage(stockName, Constants.RSIMessage, Constants.RSICurrentPriceHigherThanProfitTarget, parameters,null);
                    return;
                }
                //if conditions are good, go ahead and send stoploss and profit target messages
                SetMessage(stockName, Constants.RSIProfitTargetCode, this.profitTarget.ToString(), parameters,null);

                SetMessage(stockName, Constants.RSIStopLossCode, this.StopLoss.ToString(), parameters,null);

               // FMServiceReference.BoardLotFluctuationPriceTO[] boardLotList = c.GetBoardLotListTO();
                double minlots = this.CapCalculator.CalculateMinimumLot(last);
                //calculate the capital needed
                 // get the latest RSI
                
                 
                 //RSITO[] rsilist = RSICalculator.GetRSIFromDb(this.StockName, 1,frequency,timeFrame);
                RSITO rsiTO = (RSITO)GetParameter(stockName, Constants.CurrentRSITO, parameters);

                if (rsiTO == null)
                {
                    SetErrorMessage(stockName, "Unable to get the current RSI from dictionary", parameters);
                    return;
                }
                double currentRSIValue = rsiTO.RsiValue;
                 //if (rsilist.Length > 0) currentRSI = rsilist[0].RsiValue;
                    

                 //fire an event and let excel know
                 this.SetMessage(stockName,Constants.RSIStrategyRSIValue,currentRSIValue.ToString(),parameters,null);
                 //this.ErrorMessage = "error:" + currentRSI.ToString();             
                //double   //  (currentRSI);
                 double capital = (double)GetParameter(stockName,"capital",parameters);
                 double capitalMultiplier = (double)GetParameter(stockName,"capitalMultiplier",parameters);
                 int capitalDivisor = (int)GetParameter(stockName,"capitalDivisor",parameters);
                 
                 double allocatedCapital = this.CapCalculator.Calculate(currentRSIValue,capital,capitalMultiplier,capitalDivisor);
                 SetMessage(stockName, Constants.RSIAllocatedCapitalCode, allocatedCapital.ToString(), parameters,null);
                 //buyStock(stockName, orderType, lastPrice, lots);
                //double last =  (double)GetParameter(stockName,"last",parameters);
                double maxLots = allocatedCapital / last;
                int lots = 0;
                //
                double ratio = Math.Floor((maxLots / minlots));
                double product = ratio * minlots;
                lots = Convert.ToInt32(product);
                //int lots = Math.Floor(Convert.ToDouble( maxLots / minlots));
                SetMessage(stockName, Constants.RSILotsCode, lots.ToString(), parameters,null);

                BuyOrder((string)GetParameter(stockName,"stockName",parameters), last ,  lots, this.StopLoss, this.ProfitTarget);

                SetStatus(stockName, Constants.RSIWaitingToBeFilled, parameters);
                //}
            }
            else if (GetStatus(stockName, parameters) == Constants.RSIWaitingToBeFilled)
            {
                //check if status has been filled
                //if status == filled
                //set status = orderfilled
                if (CheckQuotationOrderStatus(this.StockName) == Constants.OrderFilledCode)
                {
                    SetStatus(stockName, Constants.RSIOrderFilled, parameters);
                }

            }
            else if (GetStatus(stockName,parameters) == Constants.RSIOrderFilled)
            {
                //get order info and compare.
                //get stoploss, pt, and lots

                OrderTO orderTO = (OrderTO)GetParameter(stockName, Constants.RSIOrderTOCode, parameters);
                
                if (orderTO == null) return;



                //notify trader
                //display to excel
                //monitor if stop loss or profit target has been hit
                //temp only. find out if we need to calc it here
                //CapitalCalculator capCalc = new CapitalCalculator();
                //double profitTarget = capCalc.GetHighestPrice(new List<double>());
                //double stopLoss = capCalc.GetAbsoluteStopLoss((double)GetParameter(stockName,"last",parameters));
                
                //if hit
                //set status = orderexit
                //call web service updateStatus(stockName, "RSIMode")
                //get the order first


                if(last <=  orderTO.Stoploss){
                    //get the lots first
                    // via db call
                    //todo: temp only
                    SellOrder(this.StockName, last, orderTO.LotSize, Constants.RSIWaitingMode);
                    this.SetMessage(stockName,Constants.RSIStoppedOut, Constants.RSIStoppedOut, parameters,null);
                 
                    
                    return;
                }
                if (last >= orderTO.ProfitTarget)
                {
                    SellOrder(this.StockName, last, orderTO.LotSize, Constants.RSIWaitingMode);
                    this.SetMessage(stockName, Constants.RSIProfitTargetHit, Constants.RSIProfitTargetHit, parameters,null);
                  
                    return;
                }
            }
            

        }

        private void CalculateProfitTargetAndStopLoss(String stockName, Dictionary<string, object> parameters, String timeFrame, int frequency, double last)
        {
 
            int profitTargetLookupPeriod = (int)GetParameter(stockName, "profitTargetLookupPeriod", parameters);
            //call this as soon as an entry is determined.
            //make this settable.
            PriceWithTimeFrameTO[] priceWithTimeFrameTOList = (PriceWithTimeFrameTO[])GetParameter(stockName, Constants.RSIPriceWithTimeFrameList, parameters);


            List<double> backwardPeriodList = new List<double>();
            foreach (PriceWithTimeFrameTO item in priceWithTimeFrameTOList)
            {
                backwardPeriodList.Add(Convert.ToDouble(item.Price));
            }
            this.ProfitTarget = this.CapCalculator.GetHighestPrice(backwardPeriodList);
            

            //calculate lowest

            // priceWithTimeFrameTOList = c.GetPriceWithTimeFrameList(stockName, frequency, timeFrame, stopLossLookupPeriod);

            //List<double> stopLossPeriodList = new List<double>();
            //foreach (PriceWithTimeFrameTO item in priceWithTimeFrameTOList)
            //{
            //    stopLossPeriodList.Add(Convert.ToDouble(item.Price));
            //}

            int stopLossLookupPeriod = (int)GetParameter(stockName, "stopLossLookupPeriod", parameters);
            
            double requiredAbsoluteStopLossFactor = (double)GetParameter(stockName, "requiredAbsoluteStopLossFactor", parameters);
            this.StopLoss = this.CapCalculator.GetLowestPrice(backwardPeriodList);

            double lowestAllowedStopLoss = this.CapCalculator.GetAbsoluteStopLoss(last, requiredAbsoluteStopLossFactor);
            //if 10 period stop loss is lower than the allowable absolute stop loss then make the abs stop loss the stop loss target
            if (this.StopLoss < lowestAllowedStopLoss)
            {
                this.StopLoss = lowestAllowedStopLoss;
            }
            

        }

        private bool CheckStatusFromByBroker(String stockName, Dictionary<string,object> parameters)
        {
           
 
            FMServiceReference.StockServiceClient c = new FMServiceReference.StockServiceClient();

            string broker = "FM";
            return c.CheckOrderStatusAndBroker(stockName, (String)GetParameter(stockName, Constants.RSIStatus, parameters), broker);
          
        }

        public override Object GetParameter(String stockName, String key, Dictionary<String, object> parameters)
        {
            return base.GetParameter(String.Format("{0},{1}", stockName, this.GetType()), key, parameters);
        }

        public  bool CheckForVolume(String stockName, Dictionary<string, object> parameters)
        {
            
            bool volumeSignal = false;
            double requiredVolume = (double)GetParameter(stockName, "requiredVolume", parameters);
            int volume = Convert.ToInt32(GetParameter(stockName, "volume", parameters));
            VolumeCalculator vCalc = new VolumeCalculator();

            //todo:  instead of passing a list, just pass one object (VolumeMATO)
            VolumeMATO volumeMA = (VolumeMATO)GetParameter(stockName, Constants.CurrentVolumeMATO, parameters);


            if (volumeMA != null)
            {

                
                volumeSignal = vCalc.IsVolumeOkay(volume, volumeMA.VolumeMAValue, requiredVolume);
            }
            
            return volumeSignal;
        }

        //public void CreateOrder(string stockName, double price, double stopLoss, double profitTarget)
        //{
        //    FileManager fm = FileManager.Instance;
        //    fm.WriteToAFile(String.Format("{0},{1},buy",stockName,price,stopLoss,profitTarget));           
        //}


        //private void buyStock(string stockName, string orderType, double price, double lots)
        //{
        //    OrderManager o = new OrderManager();
        //    o.BuyOrder(stockName, price, (int)lots);
        //    FMServiceReference.StockServiceClient c = new FMServiceReference.StockServiceClient();
        //    FMServiceReference.OrderTO oTO = new FMServiceReference.OrderTO();
        //    oTO.StockName = stockName;
        //    oTO.Price = price;
        //    oTO.OrderType = orderType;
        //    oTO.LotSize = (int)lots;
        //    c.InsertOrder(oTO);
        //}


        //bool IStrategy.IsDictionaryValid(Dictionary<string, object> parameters)
        //{
        //    throw new NotImplementedException();
        //}
    }
}
