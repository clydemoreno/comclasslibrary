﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using COMClassLibrary.FMServiceReference;
using COMClassLibrary.MessageHandlers;

namespace COMClassLibrary.Manager
{
    public class RecogniaStrategy : OrderManager, IStrategy
    {
        public void Run(String stockName, Dictionary<string, object> parameters)
        {
            this.StockName = stockName;
            try
            {
                IsDictionaryValid(stockName, parameters);
            }
            catch (Exception ex)
            {
                //log the exceptions
                //this.SetErrorMessage(stockName, ex.Message, parameters);
                SetMessage(stockName, Constants.MyRSIErrorCode, ex.Message, parameters, new RSIErrorMessageHandler());

                return;
            }

            LookForKeyword(stockName,parameters);
        }
        public void LookForKeyword(String stockName, Dictionary<string, object> parameters)
        {
            //bool recogniaSignal = false;
            foreach (RecogniaTO rTo in CheckOpenEntries())
            {
                if (rTo.Keyword == stockName)
                {
                    SetMessage(stockName, Constants.RecogniaMessage, Constants.RecogniaKeywordFound, parameters, new RecogniaKeywordFoundHandler());
                    break;
                }
            }
            //return recogniaSignal;
        }
        public void RunStrategy(String stockName, Dictionary<string, object> parameters)
        {
            this.StockName = stockName;
            try
            {
                IsDictionaryValid(stockName, parameters);
            }
            catch (Exception ex)
            {
                //log the exceptions
                //this.SetErrorMessage(stockName, ex.Message, parameters);
                SetMessage(stockName, Constants.MyRSIErrorCode, ex.Message, parameters, new RSIErrorMessageHandler());

                return;
            }

            double last, requiredAbsoluteStopLossFactor, requiredRecogniaPTPercentage;
            int volumePeriod, frequency, volume;
            string status = String.Empty, timeFrame = String.Empty;
            bool recogniaSignal = false;
            DateTime date;
            try
            {
                last = (double)GetParameter(stockName, "last", parameters);
                requiredAbsoluteStopLossFactor = (double)GetParameter(stockName, "requiredAbsoluteStopLossFactor", parameters);
                requiredRecogniaPTPercentage = (double)GetParameter(stockName, "requiredRecogniaPTPercentage", parameters);
                volumePeriod = (int)GetParameter(stockName, "volumePeriod", parameters);
                frequency = (int)GetParameter(stockName, "frequency", parameters);
                volume = (int)GetParameter(stockName, "volume", parameters);
                status = (string)GetParameter(stockName, "status", parameters);
                timeFrame = (string)GetParameter(stockName, "timeFrame", parameters);
                date = (DateTime)GetParameter(stockName, "cutOffDate", parameters);
            }
            catch (Exception ex)
            {
                throw new Exception("Error in setting the parameters: " + ex.Message);
            }
            foreach (RecogniaTO rTo in CheckOpenEntries())
            {
                if (rTo.Keyword == stockName)
                {
                    recogniaSignal = true;
                    SetMessage(stockName, Constants.RecogniaMessage, Constants.RecogniaKeywordFound, parameters,null);
                    break;
                }
            }

            if (recogniaSignal)
            {
                VolumeCalculator vCalc = new VolumeCalculator();
                VolumeMATO[] list = vCalc.GetVolumeList(stockName, volumePeriod, frequency, timeFrame,date);
                if (list.Length == 0) return;
                VolumeMATO volumeMA = list[list.Length - 1];
                if (vCalc.IsVolumeOkay(volume, volumeMA.VolumeMAValue, 1.3))
                {
                    //calculate lots
                    CapitalCalculator capCalc = new CapitalCalculator();
                    double profitTarget = last + capCalc.GetAbsoluteProfitTarget(last, requiredRecogniaPTPercentage);
                    double stopLoss = last - capCalc.GetAbsoluteStopLoss(last, requiredAbsoluteStopLossFactor);
                    double lots = capCalc.CalculateMinimumLot(last);
                    BuyOrder(stockName, last, Convert.ToInt32(lots), stopLoss, profitTarget);
                    SetStatus(stockName, Constants.RecogniaWaitingToBeFilled, parameters);
                }
            }

            else if (GetStatus(stockName, parameters) == Constants.RSIWaitingToBeFilled)
            {
                if (CheckQuotationOrderStatus(this.StockName) == Constants.OrderFilledCode)
                {
                    SetStatus(stockName, Constants.RecogniaOrderFilled, parameters);
                    StockServiceClient c = new StockServiceClient();
                    c.UpdateOrderStatus(this.StockName, this.GetType().ToString(), status);
                }
            }

            else if (GetStatus(stockName, parameters) == Constants.RecogniaOrderFilled)
            {
                StockServiceClient c = new StockServiceClient();
                OrderTO oTO = null;
                oTO = c.GetOrderInfo(this.StockName);

                if (oTO == null)
                {
                    SetStatus(stockName, Constants.RecogniaError, parameters);
                    return;
                }
                if (last <= oTO.Stoploss)
                {
                    SellOrder(this.StockName, last, oTO.LotSize, Constants.RecogniaStoppedOut);
                    this.SetMessage(stockName, Constants.RecogniaStoppedOut, Constants.RecogniaStoppedOut, parameters,null);
                    SetStatus(stockName, Constants.RecogniaWaitingMode, parameters);
                    return;
                }

                if (last >= oTO.ProfitTarget)
                {
                    SellOrder(this.StockName, last, oTO.LotSize, Constants.RecogniaProfitTargetHit);
                    this.SetMessage(stockName, Constants.RecogniaProfitTargetHit, Constants.RecogniaProfitTargetHit, parameters,null);
                    SetStatus(stockName, Constants.RecogniaWaitingMode, parameters);
                    return;
                }
            }

        }

        public RecogniaTO[] CheckOpenEntries()
        {
            COMClassLibrary.FMServiceReference.StockServiceClient c = new COMClassLibrary.FMServiceReference.StockServiceClient();
            RecogniaTO[] recogniaList = c.GetOpenRecogniaList();
            //if (recogniaList.Count() > 0)
            //{
            //    CreateFile();
            //}
            return recogniaList;
        }

        private void CreateFile()
        {
            var dir = @"C:\First Metro\log";  // folder location

            if (!Directory.Exists(dir))  // if it doesn't exist, create
                Directory.CreateDirectory(dir);

            // use Path.Combine to combine 2 strings to a path
            File.WriteAllText(Path.Combine(dir, "trade.txt"), "Sample");
        }

        //public void CreateOrder(string stockName, double price, double stopLoss, double profitTarget)
        //{
        //}


        public new void SetErrorMessage(String stockName, string value, Dictionary<String, object> parameters)
        {
            base.SetErrorMessage(AddClassNameToStockName(stockName), value, parameters);
        }

        public bool IsDictionaryValid(String stockName, Dictionary<string, object> parameters)
        {
            bool returnValue = true;

            if (!DoesKeyExist(stockName, "stockName", parameters)) throw new Exception("Missing stockName parameter in Recognia");
            if (!DoesKeyExist(stockName, "volumePeriod", parameters)) throw new Exception("Missing volumePeriod parameter in Recognia");
            if (!DoesKeyExist(stockName, "volume", parameters)) throw new Exception("Missing volume parameter in Recognia");
            if (!DoesKeyExist(stockName, "status", parameters)) throw new Exception("Missing status parameter in Recognia");
            if (!DoesKeyExist(stockName, "requiredAbsoluteStopLossFactor", parameters)) throw new Exception("Missing requiredAbsoluteStopLossFactor parameter in Recognia");
            if (!DoesKeyExist(stockName, "last", parameters)) throw new Exception("Missing last parameter in Recognia");
            if (!DoesKeyExist(stockName, "volume", parameters)) throw new Exception("Missing volume parameter in Recognia");
            if (!DoesKeyExist(stockName, "requiredRecogniaPTPercentage", parameters)) throw new Exception("Missing requiredRecogniaPTPercentage parameter in Recognia");
            if (!DoesKeyExist(stockName, "requiredVolume", parameters)) throw new Exception("Missing requiredVolume parameter in Recognia");
            if (!DoesKeyExist(stockName, "techniStockfilePath", parameters)) throw new Exception("Missing techniStockfilePath parameter in Recognia");
            if (!DoesKeyExist(stockName, "frequency", parameters)) throw new Exception("Missing frequency parameter in Recognia");
            if (!DoesKeyExist(stockName, "timeFrame", parameters)) throw new Exception("Missing timeFrame parameter in Recognia");
            return returnValue;
        }
    }
}
