﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace COMClassLibrary.Manager
{
    public class Report
    {
        private string groupID;

        public string GroupID
        {
            get { return groupID; }
            set { groupID = value; }
        }
        private string violationType;

        public string ViolationType
        {
            get { return violationType; }
            set { violationType = value; }
        }
    }
}
