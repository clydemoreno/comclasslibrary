﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using COMClassLibrary.FMServiceReference;

namespace COMClassLibrary.Manager
{
    public class VolumeCalculator
    {
        public bool IsVolumeOkay(double currentVolume, double VolumeMA, double percentage)
        {
            if (VolumeMA == 0) return false;
            return (currentVolume / VolumeMA) >= percentage;
        }

        public void SaveVolume(String stockName,double volumema, int frequency, string timeFrame, int period, DateTime myDate){
            FMServiceReference.StockServiceClient c = new FMServiceReference.StockServiceClient();
            c.InsertVolumeMA(stockName, period, volumema, frequency, timeFrame, myDate);
        }
        public VolumeMATO[] GetVolumeList(String stockName, int period, int frequency, string timeFrame, DateTime myDate)
        {
            //should return an array of volumes
            FMServiceReference.StockServiceClient client = new FMServiceReference.StockServiceClient();
            return client.GetVolumeMaList(stockName, frequency, timeFrame, period,  myDate);
            //from the price_with_time_frame
        }
        public double CalculateMovingAverage(List<int> list)
        {
           
            double sum = 0;
            foreach (double item in list)
            {
                sum += item;
            }


            return sum / list.Count;

        }
    }
}
