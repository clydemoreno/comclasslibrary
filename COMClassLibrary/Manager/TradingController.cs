﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using COMClassLibrary.FMServiceReference;

namespace COMClassLibrary.Manager
{
    [ClassInterface(ClassInterfaceType.AutoDual)]
    public class TradingController
    {
        private RSIDerivativeMessageHandler messageHandler;
        private RSIBreakoutStrategy orderManager;

        public RSIBreakoutStrategy OrderManager
        {
            get {
                if (orderManager == null)
                    orderManager = new RSIBreakoutStrategy();
                return orderManager; 
            }
        }
        public RSIDerivativeMessageHandler MessageHandler
        {
            get
            {
                if (messageHandler == null)
                    messageHandler = new RSIDerivativeMessageHandler();
                return messageHandler;
            }
        }


        private Dictionary<String , object> parameters = new Dictionary<string,object>();

        public void SetParameters(String stockName, String strategyClass, String key, object value)
        {
            
            String name = String.Format("{0},{1}", stockName, strategyClass);
            if(!String.IsNullOrEmpty(name))
                SetParameters(name, key, value);
        }

        private  void SetParameters(String stockName,  String key, object value)
        {
            object lockObject = new object();
            lock (lockObject)
            {

                if (parameters != null)
                {
                    
                    if (!parameters.ContainsKey(stockName))
                    {
                        parameters[stockName] = new Dictionary<String, object>();

                    }
                    Dictionary<String, object> dictionary = (Dictionary<String, object>)parameters[stockName];
                    dictionary[key] = value;
                }
            }
        }


        public string GetMessage(String stockName, String strategyClass, String key)
        {
            return Convert.ToString( GetParameter(stockName, strategyClass, key));
            //i need to fix this by setting the right value.  maybe i can set it in excel
            //OrderManager m = new OrderManager();
           // return OrderManager.GetParameter(stockName, key, parameters);
            //Object returnValue = null;
            //if (parameters != null && parameters.ContainsKey(stockName))
            //{
            //    Dictionary<String, object> dictionary = (Dictionary<String, object>)parameters[stockName];
            //    if (dictionary.ContainsKey(key))
            //    {
            //        returnValue = dictionary[key];
            //    }
            //}
            //return returnValue;
        }
        private String endPoint;

        public String EndPoint
        {
            get { return endPoint; }
            set { endPoint = value; }
        }
        private COMClassLibrary.Model.ConfigContext configContext;

        public COMClassLibrary.Model.ConfigContext ConfigContext
        {
            get {
                if (configContext == null) configContext = new Model.ConfigContext();
                return 
                    configContext;
            }
            set { configContext = value; }
        }

        public void setConfigContext(COMClassLibrary.Model.ConfigContext configContext)
        {
            this.configContext = configContext;
        }

        private double capital;

        public double Capital
        {
            get { return capital; }
            set { capital = value; }
        }
        private string configurationString;

        public string ConfigurationString
        {
            get { return configurationString; }
            set { configurationString = value; }
        }
        private float capitalMultiplier;

        public float CapitalMultiplier
        {
            get { return capitalMultiplier; }
            set { capitalMultiplier = value; }
        }
        private float baseLotSize;

        public float BaseLotSize
        {
            get { return baseLotSize; }
            set { baseLotSize = value; }
        }
        private float baseLotSizeMultiplier;

        public float BaseLotSizeMultiplier
        {
            get { return baseLotSizeMultiplier; }
            set { baseLotSizeMultiplier = value; }
        }

        public double Randomize(double price)
        {
            TimeFrameManager m = new TimeFrameManager();
            return m.Randomize(price);
        }
        public double RandomizeStartEnd(int start, int end)
        {
            TimeFrameManager m = new TimeFrameManager();
            return m.RandomizeInteger(start, end);
        }


        public void IsDictionaryValid(String stockName, Dictionary<string, object> parameters)
        {
            //OrderManager orderManager = new OrderManager();
            //if (!orderManager.DoesKeyExist(stockName, "lineRegressionRSIPeriod", parameters))
            //{
            //    throw new Exception("Missing lineRegressionRSIPeriod parameter");
            //}

            
            //if (!parameters.ContainsKey("startingX")) throw new Exception("Missing Status parameter");
            //if (!parameters.ContainsKey("requiredDerivative")) throw new Exception("Missing Status parameter");
            //if (!parameters.ContainsKey("period")) throw new Exception("Missing Status parameter");
            //if (!parameters.ContainsKey("frequency")) throw new Exception("Missing Status parameter");
            //if (!parameters.ContainsKey("timeFrame")) throw new Exception("Missing Status parameter");
            //if (!parameters.ContainsKey("requiredDerivative")) throw new Exception("Missing Status parameter");


        }


        public virtual Object GetParameter(String stockName, String strategyClass, String key)
        {
            Object returnValue = null;
            object lockObject = new object();
            lock (lockObject)
            {
                stockName = String.Format("{0},{1}", stockName, strategyClass);
                if (parameters != null && parameters.ContainsKey(stockName))
                {
                    
                    Dictionary<String, object> dictionary = (Dictionary<String, object>)parameters[stockName];
                    if (dictionary.ContainsKey(key))
                    {
                        returnValue = dictionary[key];
                    }
                }
            }
            return returnValue;
        }

        public void UpdateQuotationScreenStatus(String stockName, String status)
        {
            OrderManager manager = new OrderManager();
            
            manager.UpdateQuotationScreenStatus(stockName, status);
        }

        public void RunDiagnostics()
        {
            DiagnosticsManager m = new DiagnosticsManager();
            this.SetParameters(Constants.Diagnostics,Constants.Diagnostics, Constants.WebService, m.RunWebServiceDiagnostics());
        }

        public void RunMarketMaker(String stockName)
        {
            MarketMakerStrategy mm = new MarketMakerStrategy();
            mm.Changed += new ChangedEventHandler(rsi_Changed);

            Task taskC = Task.Factory.StartNew(() =>
            {
                //get status
                try
                {

                    mm.Run(stockName, parameters);
                }
                catch (Exception ex)
                {
                    //todo
                    this.SetParameters(stockName, typeof(MarketMakerStrategy).ToString(), Constants.MyRSIErrorCode, String.Format("Error from: MM Msg: {0}", ex.Message));
                }
            });
        }

        public String RunStrategy(String stockName)
        {

            //IsDictionaryValid(stockName,this.parameters);

            OrderManager rsi = new RSIBreakoutStrategy();
            rsi.Changed += new ChangedEventHandler(rsi_Changed);
            //IStrategy intra = new IntradayStrategy();
            
            OrderManager rec = new RecogniaStrategy();


            rec.Changed += new ChangedEventHandler(rsi_Changed);
            //rec.Run(parameters);


            // TaskA is a top level task.
            Task taskA = Task.Factory.StartNew(() =>
            {
                //get status
                try
                {
                    rsi.GetOrderInfo(stockName, parameters);
                    ((RSIBreakoutStrategy)rsi).GetOrderStatus(stockName,parameters);
                    ((RSIBreakoutStrategy)rsi).GetRSIList(stockName, this.parameters);
                    
                    ((RSIBreakoutStrategy)rsi).GetProfitTargetAndStopLossPeriod(stockName, parameters);
                    ((RSIBreakoutStrategy)rsi).GetLastVolumeMA(stockName, parameters);
                    ((IStrategy)rsi).Run(stockName, parameters);
                   // this.SetParameters(Constants.MyRSIErrorCode + stockName, "");
                }
                catch (Exception ex)
                {

                    this.SetParameters(stockName , typeof(RSIBreakoutStrategy).ToString(),   Constants.MyRSIErrorCode, String.Format("Error from: RSI Msg: {0}",ex.Message));
                }
            });

            // TaskB is a top level task.
            Task taskB = Task.Factory.StartNew(() =>
            {
                try
                {
                    //GetStatusFromDB(stockName);
                    //((IStrategy)rec).Run(stockName, parameters);

                }
                catch (Exception ex)
                {

                    this.SetParameters(stockName, typeof(RecogniaStrategy).ToString(), Constants.MyRSIErrorCode, String.Format("Error from: Recognia Msg: {0}", ex.Message));
                }

            });





            IntradayStrategy intra = new IntradayStrategy();
            intra.Changed += new ChangedEventHandler(rsi_Changed);

            Task taskD = Task.Factory.StartNew(() =>
            {
                //get status
                try
                {

                    intra.Run(stockName, parameters);
                }
                catch (Exception ex)
                {
                    //todo
                    this.SetParameters(stockName, typeof(IntradayStrategy).ToString(), Constants.MyRSIErrorCode, String.Format("Error from: MM Msg: {0}", ex.Message));
                }
            });


            return "some type of event message";
        }

       

 



        

        void rsi_Changed(object sender, EventArgs e)
        {
            //Handle all the even here.

            //Handle Messages of RSI




            //don't do anything if the sender is not dictionary object
            if(sender.GetType() != typeof(Dictionary<String, object>)) return;


 
            Dictionary<String, object> parameters = (Dictionary<String, object>)sender;


            //Handle Errors

            //if (e.GetType() == typeof(COMClassLibrary.Manager.ErrorEventArg))
            //{
            //    this.SetParameters(((ErrorEventArg)e).StockName, ((ErrorEventArg)e).Code, ((ErrorEventArg)e).Message);
            //}



            if (e.GetType() == typeof(COMClassLibrary.Manager.MessageEventArg))
            {

                MessageEventArg eventArg = (MessageEventArg)e;

                IMessageHandler handler = eventArg.MessageHandler;
                handler.Handle(parameters, e);
            }

           // this.MessageHandler.Handle(parameters, e);


           
          

        }

        public double RandomizeInteger(int volume)
        {
            TimeFrameManager m = new TimeFrameManager();
            return m.RandomizeInteger(volume);
        }
        public void InstantiateFileManager(String path, String fileName)
        {
            FileManager f = FileManager.Instance;
            //f.CreateWatcher(path);
            f.GetFile(path, fileName);
        }

        //public String SaveBloombergTickerWS(string stockName,double last,
        // double bid,
        // double ask,
        // double volume,
        // double high,
        // double low,
        //    DateTime date)
        //{
        //    String returnValue = String.Empty;
        //    FMServiceReference.StockServiceClient client = new FMServiceReference.StockServiceClient();
        //    returnValue = client.SaveToDB(stockName, last, bid, ask, high, low, volume, date);
            
        //}
        public String SaveBloombergTicker(string stockName, double last,
         double bid,
         double ask,
         int volume,
         double high,
         double low,
            DateTime date,
            String timeFrame,
                int frequency,
                int period, double askSize, double bidSize
            ,double open
            
            )
        {
            string returnValue = String.Empty;
            try
            {
                
               // TradingRuleManager m = new TradingRuleManager();
                //m.SaveToDBAsync(stockName, last, bid, ask, high, low, volume, date);


                // TaskA is a top level task.
                Task taskA = Task.Factory.StartNew(() =>
                {
                    try
                    {
                        SaveTimeFrameAndRSI(stockName, last, timeFrame, frequency, period, date, volume,ask,askSize,bid,bidSize,open);
                       
                    }
                    catch (Exception ex)
                    {
                        this.SetParameters(stockName, typeof(TradingController).ToString(), Constants.MyRSIErrorCode, String.Format("Error on:SaveTimeFrameAndRSI : {0} ", ex.Message));
                    }

                });
              

                //if(list.Length 


                //FMServiceReference.StockServiceClient client = new FMServiceReference.StockServiceClient();
                ////client.Endpoint.Address =  new System.ServiceModel.EndpointAddress(endPoint);
                //returnValue = client.SaveToDB(stockName, last, bid, ask, high, low, volume, date);
            }
            catch (Exception ex)
            {

                this.SetParameters(stockName, typeof(RSIBreakoutStrategy).ToString(), Constants.MyRSIErrorCode, ex.Message);
            }
            return String.Empty;

        }

        public void SaveTimeFrame(string stockName, double last, String timeFrame, int frequency, int period, DateTime date, int volume, double ask, double askSize, double bid, double bidSize, double open)
        {
            TimeFrameManager tm = new TimeFrameManager();

            tm.SaveTimeFrame(stockName, last, timeFrame, frequency, volume, date, ask, askSize, bid, bidSize,open);
 
        }

        public void SaveTimeFrameAndRSI(string stockName, double last, String timeFrame, int frequency, int period, DateTime date, int volume, double ask, double askSize, double bid, double bidSize, double open)
        {


            TimeFrameManager tm = new TimeFrameManager();

            tm.SaveTimeFrame(stockName, last, timeFrame, frequency, volume, date, ask, askSize, bid, bidSize,open);

            FMServiceReference.PriceWithTimeFrameTO[] list = null;
            
            list = tm.GetTimeFrame(stockName, timeFrame, frequency, period,date);

            if (list.Length != period) return;

            RSICalculator rsiCalculator = new RSICalculator();
            List<double> priceList = new List<double>();
            List<int> volumeList = new List<int>();

            for (int i = list.Length - 1; i >= 0; i--)
            {
                System.Diagnostics.Debug.WriteLine(i);
                priceList.Add(Convert.ToDouble(list[i].Price));
                volumeList.Add(list[i].Volume);
            }

            //foreach (FMServiceReference.PriceWithTimeFrameTO item in list)
            //{
            //    priceList.Add(Convert.ToDouble(item.Price));
            //    volumeList.Add(item.Volume);
            //}
            double rsiValue = rsiCalculator.CalculateRSI(priceList, period);


            //todo:  we might just use the setMessage to save this value
            rsiCalculator.InsertRSI(stockName, rsiValue, frequency, period, timeFrame, date);
            this.SetParameters(stockName, typeof(RSIBreakoutStrategy).ToString(), Constants.RSIStrategyRSIValue, rsiValue);

           // int volumePeriod = (int)this.GetMessage(stockName, "volumePeriod");

            VolumeCalculator vc = new VolumeCalculator();
            if (volumeList.Count == 0) return;

            double volumema = vc.CalculateMovingAverage(volumeList);
            if (volumema > 0)
            {
                vc.SaveVolume(stockName, volumema, frequency, timeFrame, period, date);
                this.SetParameters(stockName, typeof(RSIBreakoutStrategy).ToString(), Constants.RSIStrategyVolumeMAValue, volumema);
            }


            

        }

        public String TestDLL(String message)
        {
            return message;
        }

        public String CallWebService(String message)
        {
            string returnValue = String.Empty;
            try
            {
                FMServiceReference.StockServiceClient client = new FMServiceReference.StockServiceClient();
                returnValue = client.DoWork(message);
                client.Close();
            }
            catch (Exception ex)
            {
                returnValue = ex.Message;
            }
            return returnValue;
        }


    }
}
