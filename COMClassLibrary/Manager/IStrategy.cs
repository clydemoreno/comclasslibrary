﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace COMClassLibrary.Manager
{
    public interface IStrategy
    {
        void Run(String stockName, Dictionary<string, object> parameters);
        
        bool IsDictionaryValid(String stockName, Dictionary<string, object> parameters);


    }
}
