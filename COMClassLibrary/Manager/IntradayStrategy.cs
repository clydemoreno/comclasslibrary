﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using COMClassLibrary.FMServiceReference;
using COMClassLibrary.MessageHandlers;
using COMClassLibrary.Model;

namespace COMClassLibrary.Manager
{
    public class IntradayStrategy : OrderManager, IStrategy
    {
        public void Run(String stockName, Dictionary<string, object> parameters)
        {
            try
            {
                IsDictionaryValid(stockName, parameters);
            }
            catch (Exception ex)
            {
                //this.SetErrorMessage(stockName, ex.Message, parameters);
                SetMessage(stockName, Constants.MyRSIErrorCode, ex.Message, parameters,new RSIErrorMessageHandler());
                return;
            }

            if (IsAskSizeToBidSizeThresholdBreached(stockName, parameters))
            {
                SetMessage(stockName, Constants.IntradayRatioReached, Constants.IntradayRatioReached, parameters, new IntradayRatioReachedHandler());
                return;
            }
        }

        public bool IsAskSizeToBidSizeThresholdBreached(String stockName, Dictionary<string, object> parameters)
        {
            double askSize = (double)GetParameter(stockName, "askSize", parameters);
            double bidSize = (double)GetParameter(stockName, "bidSize", parameters);
            double intradayAskToBidRatio = (double)GetParameter(stockName, "intradayAskToBidRatio", parameters);

            double ratio = askSize / bidSize;
            return ((ratio) <= intradayAskToBidRatio);

        }



        public double GetStopLoss(double price, double fluc, double offset)
        {
            return price - (fluc * offset);
        }

        public double GetProfitTarget(double price, double fluc, double offset)
        {
            return price + (fluc * offset);
        }

        public bool HasReachedStopLoss(double stopLoss, double lastPrice)
        {
            return lastPrice <= stopLoss;
        }
        public bool HasReachedProfitTarget(double profitTarget, double lastPrice)
        {
            return lastPrice >= profitTarget;
        }
        public BloombergTickerTO[] GeTBloombergTickerList(String stockName)
        {
            StockServiceClient c = new StockServiceClient();
            BloombergTickerTO[] list = c.GetTickData(stockName);


            return list;
        }

        public PriceWithTimeFrameTO[] GePricetimeFrameList(String stockName, int frequency, String timeFrame, int period)
        {
            StockServiceClient c = new StockServiceClient();
            PriceWithTimeFrameTO[] ticks = c.GetPriceWithTimeFrameList(stockName, frequency,timeFrame, period);



            return ticks;
        }
        public PriceWithTimeFrameTO[] GeBatchPricetimeFrameList(String stockNameList, int frequency, String timeFrame, int period)
        {
            PriceWithTimeFrameTO[] tempTicks = null;
            PriceWithTimeFrameTO[] ticks = null;

            List<PriceWithTimeFrameTO> priceList = new List<PriceWithTimeFrameTO>();
            StockServiceClient c = new StockServiceClient();
            String[] list = stockNameList.Split(",".ToCharArray());

            

            foreach (String stockName in list)
            {
                tempTicks  = c.GetPriceWithTimeFrameList(stockName, frequency, timeFrame, period);
                
                if (tempTicks != null && tempTicks.Length > 0)
                {
                    priceList.Add(tempTicks[0]);
                }
            }
            ticks = priceList.ToArray<PriceWithTimeFrameTO>();
            return ticks;
        }

        public PriceWithTimeFrameTO GetStockByDate(String stockName, int frequency, String timeFrame, int period, DateTime
            cutoffDate)
        {
            return null;
        }

        public void RunIntraday(String stockName, Dictionary<string, object> parameters)
        {
            
            try
            {
                IsDictionaryValid(stockName, parameters);
            }
            catch (Exception ex)
            {
                this.SetErrorMessage(stockName, ex.Message, parameters);
                return;
            }

            double last, requiredAbsoluteStopLossFactor, requiredIntradayPTPercentage;
            bool bidAskSignal = false, volumeSignal = false, orderStatusBrokerSignal = false;
            int volume = 0;
            int volumeMA = 0;
            double askSize = 0;
            double bidSize = 0;
            double volumeRequired = 0;
            double capital = 0;
            double capitalMultiplier = 0;
            int capitalDivisor = 0;
            double intradayAskToBidRatio = 0;

            string status = String.Empty, brokerId = String.Empty;
            try
            {
                capital = (double)GetParameter(stockName, "capital", parameters);
                capitalMultiplier = (double)GetParameter(stockName, "capitalMultiplier", parameters);
                capitalDivisor = (int)GetParameter(stockName, "capitalDivisor", parameters);
                last = (double)GetParameter(stockName, "last", parameters);
                volume = (int)GetParameter(stockName, "volume", parameters);
                volumeMA = (int)GetParameter(stockName, "volumema", parameters);
                askSize = (double)GetParameter(stockName, "askSize", parameters);
                bidSize = (double)GetParameter(stockName, "bidSize", parameters);
                status = (string)GetParameter(stockName, Constants.IntradayStatus, parameters);
                brokerId = (string)GetParameter(stockName, "brokerId", parameters);
                requiredAbsoluteStopLossFactor = (double)GetParameter(stockName, "requiredAbsoluteStopLossFactor", parameters);
                requiredIntradayPTPercentage = (double)GetParameter(stockName, "requiredIntradayPTPercentage", parameters);
                volumeRequired = (double)GetParameter(stockName, "volumeRequired", parameters);
                intradayAskToBidRatio = (double)GetParameter(stockName, "intradayAskToBidRatio", parameters);
            }

            catch (Exception ex)
            {

                throw new Exception("Error in setting the parameters: " + ex.Message);
            }

            if (status == Constants.IntradayWaitingMode || String.IsNullOrEmpty(status))
            {
                double ratio = askSize / bidSize;
                if ((ratio) < intradayAskToBidRatio)
                {
                    bidAskSignal = true;
                    SetMessage(stockName, Constants.IntradayRatioReached, ratio.ToString(), parameters, new IntradayRatioReachedHandler());
                    SetStatus(stockName, Constants.IntradayRatioReached, parameters);
                    //calculate the stop loss in the handler and save to db.
                    return;
                }
                else
                {
                    SetMessage(stockName, Constants.IntradayCancel, ratio.ToString(), parameters, new IntradayRatioReachedHandler());
                }
            }
            if (status == Constants.IntradayRatioReached)
            {
                StockServiceClient c = new StockServiceClient();
                orderStatusBrokerSignal = c.CheckOrderStatusAndBroker(stockName, Constants.OrderFilledCode, brokerId);
                FMServiceReference.BoardLotFluctuationPriceTO[] boardLotList = c.GetBoardLotListTO();
                VolumeCalculator vc = new VolumeCalculator();
                volumeSignal = vc.IsVolumeOkay(volume, volumeMA, volumeRequired);
                if (!volumeSignal)
                    return;
                if (!orderStatusBrokerSignal)
                    return;
                if (!bidAskSignal)
                    return;
                CapitalCalculator cc = new CapitalCalculator();

                double minLots = cc.CalculateMinimumLot(last);
                double allocatedCapital = this.CapCalculator.Calculate(50, capital, capitalMultiplier, capitalDivisor);
                double maxLots = allocatedCapital / last;

                double stopLoss = 0;
                double profitTarget = 0;

                int lots = Convert.ToInt32(maxLots / minLots);

                profitTarget = last + (last * requiredIntradayPTPercentage);
                stopLoss = last - (last * requiredAbsoluteStopLossFactor);
                BuyOrder(stockName, last, (int)lots, stopLoss, profitTarget);
                SetStatus(stockName, Constants.IntradayWaitingToBeFilled, parameters);
            }

            if (GetStatus(stockName, parameters) == Constants.IntradayWaitingToBeFilled)
            {
                if (CheckQuotationOrderStatus(stockName) == Constants.OrderFilledCode)
                {
                    SetStatus(stockName, Constants.IntradayOrderFilled, parameters);
                    StockServiceClient c = new StockServiceClient();
                    c.UpdateOrderStatus(this.StockName,this.GetType().ToString(), status);
                }
            }

            if (GetStatus(stockName, parameters) == Constants.IntradayOrderFilled)
            {
                StockServiceClient c = new StockServiceClient();
                OrderTO oTO = null;
                oTO = c.GetOrderInfo(this.StockName);

                if (oTO == null)
                {
                    SetStatus(stockName, Constants.IntradayError, parameters);
                    return;
                }

                if (last <= oTO.Stoploss)
                {
                    SellOrder(this.StockName, last, oTO.LotSize, Constants.IntradayStoppedOut);
                    this.SetMessage(stockName, Constants.IntradayStoppedOut, Constants.IntradayStoppedOut, parameters,null);
                    SetStatus(stockName, Constants.IntradayWaitingMode, parameters);
                    return;
                }

                if (last >= oTO.ProfitTarget)
                {
                    SellOrder(this.StockName, last, oTO.LotSize, Constants.IntraydayProfitTargetHit);
                    this.SetMessage(stockName, Constants.IntraydayProfitTargetHit, Constants.IntraydayProfitTargetHit, parameters,null);
                    SetStatus(stockName, Constants.IntradayWaitingMode, parameters);
                    return;
                }
            }
        }

        public new  void SetErrorMessage(String stockName, string value, Dictionary<String, object> parameters)
        {
            base.SetErrorMessage(AddClassNameToStockName(stockName), value, parameters);
        }

        public override bool DoesKeyExist(String stockName, String key, Dictionary<String, object> parameters)
        {
            return base.DoesKeyExist(String.Format("{0},{1}", stockName, this.GetType()), key, parameters);
        }

        public override void SetParameters(String stockName, String key, object value, Dictionary<String, object> parameters)
        {
            base.SetParameters(AddClassNameToStockName(stockName), key, value, parameters);
        }

        public override Object GetParameter(String stockName, String key, Dictionary<String, object> parameters)
        {
            return base.GetParameter(String.Format("{0},{1}", stockName, this.GetType()), key, parameters);
        }

        public bool IsDictionaryValid(String stockName, Dictionary<string, object> parameters)
        {
            bool returnValue = true;
            //todo: temp for now until we implement status tracking
            //if (!DoesKeyExist(stockName, Constants.IntradayStatus, parameters)) throw new Exception("Missing status parameter in Intraday");

            if (!DoesKeyExist(stockName, "last", parameters)) throw new Exception("Missing last parameter in Intraday");
            if (!DoesKeyExist(stockName, "volume", parameters)) throw new Exception("Missing volume parameter in Intraday");
            if (!DoesKeyExist(stockName, "volumema", parameters)) throw new Exception("Missing volumema parameter in Intraday");
            if (!DoesKeyExist(stockName, "askSize", parameters)) throw new Exception("Missing askSize parameter in Intraday");
            if (!DoesKeyExist(stockName, "bidSize", parameters)) throw new Exception("Missing bidSize parameter in Intraday");
            if (!DoesKeyExist(stockName, "brokerId", parameters)) throw new Exception("Missing brokerId parameter in Intraday");
            if (!DoesKeyExist(stockName, "requiredAbsoluteStopLossFactor", parameters)) throw new Exception("Missing requiredAbsoluteStopLossFactor parameter in Intraday");
            if (!DoesKeyExist(stockName, "requiredIntradayPTPercentage", parameters)) throw new Exception("Missing requiredRecogniaPTPercentage parameter in Intraday");
            if (!DoesKeyExist(stockName, "requiredVolume", parameters)) throw new Exception("Missing volumeRequired parameter in Intraday");
            if (!DoesKeyExist(stockName, "intradayAskToBidRatio", parameters)) throw new Exception("Missing intradayAskToBidRatio parameter in Intraday");
            
            return returnValue;
        }
    }
}
