﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;
//using Newtonsoft.Json;
//using Newtonsoft.Json.Linq;

namespace COMClassLibrary.Manager
{
    public class LogManager
    {

        public static void logError(string message)
        {
            string path = Environment.ExpandEnvironmentVariables(ConfigurationManager.AppSettings["errorLogPath"].ToString());
            path = System.IO.Path.Combine(path, DateTime.Now.ToString("MMM-yyy"));
            System.IO.Directory.CreateDirectory(path);
            string logPath = System.IO.Path.Combine(path, String.Format("{0}-MarketMakerError.log", DateTime.Now.ToString("MMM-dd")));
            message = DateTime.Now.ToString() + "   " + message;
            FileManager.Instance.AppendToFile(logPath, message);
        }

        /*
         *logs retrieved data on pathfolder/currentdate.log 
         */
        public static void logData(Security fmetfSec, int count, string status)
        {
            string path = Environment.ExpandEnvironmentVariables(ConfigurationManager.AppSettings["logPath"].ToString());
            path = System.IO.Path.Combine(path, DateTime.Now.ToString("MMM-yyy"));
            System.IO.Directory.CreateDirectory(path);
            string logPath = System.IO.Path.Combine(path, String.Format("{0}-MarketMaker.log", DateTime.Now.ToString("MMM-dd")));
            FileManager.Instance.AppendToFile(logPath,String.Format("{6}: {0},Bids:{1},Ask:{2},Spread:{3},Bid Vol:{4},Ask Vol:{5}", DateTime.Now, fmetfSec.Bids[0].price, fmetfSec.Asks[0].price, count, fmetfSec.Bids[0].volume, fmetfSec.Asks[0].volume, status));
        }

        /*
         * additionally logs violation data on a different folder (violationpathfolder/currentdate.log)
         */
        public static void violationLogData(Security fmetfSec, int count, string status)
        {
            string path = String.Format("{0}", Environment.ExpandEnvironmentVariables(ConfigurationManager.AppSettings["violationLogPath"].ToString()));
            path = System.IO.Path.Combine(path, DateTime.Now.ToString("MMM-yyy"));
            System.IO.Directory.CreateDirectory(path);
            string logPath = System.IO.Path.Combine(path, String.Format("{0}-Violation.log", DateTime.Now.ToString("MMM-dd")));
            FileManager.Instance.AppendToFile(logPath, String.Format("{6}: {0},Bids:{1},Ask:{2},Spread:{3},Bid Vol:{4},Ask Vol:{5}", DateTime.Now, fmetfSec.Bids[0].price, fmetfSec.Asks[0].price, count, fmetfSec.Bids[0].volume, fmetfSec.Asks[0].volume, status));                    
        }

        //public static void uploadTest(String message)
        //{
        //    WebClient client = new WebClient();
        //    client.Headers[HttpRequestHeader.ContentType] = "application/json";
        //    var values = new JObject();

        //    values["date"] = "2015-08-08";
        //    values["id"] = "c#test";
        //    values["adx"] = 10;
        //    string svalue = SerializeWithoutQuote(values);
        //    client.UploadString("https://fmsbcproptradersignal.appspot.com/_ah/api/fmsbcproptradersignal/v1.0/save/signal", svalue);
        //    client.Dispose();
        //}

        //public static void uploadErrorLog(String message)
        //{
        //    var t = DateTime.Now;
        //    WebClient client = new WebClient();
        //    client.Headers[HttpRequestHeader.ContentType] = "application/json";
        //    var values = new JObject();

        //    values["date"] = t.ToString("MM/dd/yyy hh:mm");
        //    values["log"] = message;
        //    string svalue = SerializeWithoutQuote(values);
        //    try
        //    {
        //        client.UploadString("https://fsmbc-logs.appspot.com/_ah/api/fmsbclogs/v1.0/error/log", svalue);
        //    }
        //    catch (Exception ex)
        //    {
        //        LogManager.logError(ex.Message);
        //    }
        //    client.Dispose();

        //}
        public static void uploadErrorLog(String message)
        {
        }

        /*
         * uploads data on Google Datastore
         */
        public static void uploadData(Security fmetfSec, int count, String status)
        {
        }
        //    var t = DateTime.Now;
        //    WebClient client = new WebClient();
        //    client.Headers[HttpRequestHeader.ContentType] = "application/json";
        //    var values = new JObject();
        //    if (fmetfSec != null)
        //    {


            //        values["status"] = status;
            //        values["date"] = DateTime.Now.ToString("MM/dd/yyy");
            //        values["time"] = DateTime.Now.TimeOfDay.ToString();
            //        values["bids"] = fmetfSec.Bids[0].price.ToString();
            //        values["ask"] = fmetfSec.Asks[0].price.ToString();
            //        values["bidVolume"] = fmetfSec.Bids[0].volume.ToString();
            //        values["askVolume"] = fmetfSec.Asks[0].volume.ToString();
            //        values["spread"] = count.ToString();

            //    }
            //    else
            //    {
            //        values["status"] = status;
            //        values["date"] = DateTime.Now.ToString("MM/dd/yyy");
            //        values["time"] = DateTime.Now.TimeOfDay.ToString();
            //        values["bids"] = 0;
            //        values["ask"] = 0;
            //        values["bidVolume"] = 0;
            //        values["askVolume"] = 0;
            //        values["spread"] = 0;

            //    }
            //    string svalue = SerializeWithoutQuote(values);
            //    client.UploadString("https://fsmbc-logs.appspot.com/_ah/api/fmsbclogs/v1.0/save/log", svalue);
            //    client.Dispose();
            //}

            /*
             * Model of the json object for uploadData()
             */
        public class Values
        {
            public string status;
            public string date;
            public string time;
            public string bids;
            public string ask;
            public string bidVolume;
            public string askVolume;
            public string spread;
        }

        //private static string SerializeWithoutQuote(object value)
        //{
        //    var serializer = JsonSerializer.Create(null);

        //    var stringWriter = new StringWriter();

        //    using (var jsonWriter = new JsonTextWriter(stringWriter))
        //    {
        //        jsonWriter.QuoteName = false;

        //        serializer.Serialize(jsonWriter, value);

        //        return stringWriter.ToString();
        //    }
        //}

        public static string jsonEncoded { get; set; }


    }
}
