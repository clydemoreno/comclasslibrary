﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;

namespace COMClassLibrary.Manager
{
    public class ViolationManager
    {
        public static void Beep()
        {
            SoundPlayer simpleSound = new SoundPlayer(ConfigurationManager.AppSettings["beepPath"].ToString());           
            simpleSound.Play();                          
        }
        
        public static void Email(Security fmetfSec, int count, int violationCount)
        {
            EmailLib.EmailProperties email = new EmailLib.EmailProperties();
            switch (violationCount)
            {
                case 0:
                    break;
                case 1:
                    email.Subject = "MARKET MAKER 3-MINUTE WARNING";
                    email.Body = "<p>" + String.Format("Violation: {0},Bids:{1},Ask:{2},Spread:{3},Bid Vol:{4},Ask Vol:{5}", DateTime.Now, fmetfSec.Bids[0].price, fmetfSec.Asks[0].price, count, fmetfSec.Bids[0].volume, fmetfSec.Asks[0].volume) + "</p>";
                   //EmailLib.EmailManager.SendEmail(email);
                    break;
                case 2:
                    email.Subject = "MARKET MAKER 2-MINUTE WARNING";
                    email.Body = "<p>" + String.Format("Violation: {0},Bids:{1},Ask:{2},Spread:{3},Bid Vol:{4},Ask Vol:{5}", DateTime.Now, fmetfSec.Bids[0].price, fmetfSec.Asks[0].price, count, fmetfSec.Bids[0].volume, fmetfSec.Asks[0].volume) + "</p>";
                   // EmailLib.EmailManager.SendEmail(email);
                    break;
                case 3:
                    email.Subject = "MARKET MAKER 90 secs TO VIOLATION";
                    email.Body = "<p>" + String.Format("Violation: {0},Bids:{1},Ask:{2},Spread:{3},Bid Vol:{4},Ask Vol:{5}", DateTime.Now, fmetfSec.Bids[0].price, fmetfSec.Asks[0].price, count, fmetfSec.Bids[0].volume, fmetfSec.Asks[0].volume) + "</p>";                                     
                    EmailLib.EmailManager.SendEmail(email);
                    break;
                default:
                    email.Subject = "MARKET MAKER VIOLATION";
                    email.Body = "<p>" + String.Format("Violation: {0},Bids:{1},Ask:{2},Spread:{3},Bid Vol:{4},Ask Vol:{5}", DateTime.Now, fmetfSec.Bids[0].price, fmetfSec.Asks[0].price, count, fmetfSec.Bids[0].volume, fmetfSec.Asks[0].volume) + "</p>"
                                        + "<p>We are under VIOLATION</p>";
                    EmailLib.EmailManager.SendEmail(email);
                    break;
            }
        }

    }
    public class BeepManager
    {
        public static async void BeepWarning(int count, int frequency, int duration, int interval)
        {
            for(int i = 0; i < count;i++){
                Console.Beep(frequency*1000, duration*1000);
             await Task.Delay(interval*1000);
            }
        }
    }

}