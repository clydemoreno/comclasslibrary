﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace COMClassLibrary.Manager
{
    public class AskBid
    {
        private double ask;

        public double Ask
        {
            get { return ask; }
            set { ask = value; }
        }
        private double bid;

        public double Bid
        {
            get { return bid; }
            set { bid = value; }
        }
    }
}
