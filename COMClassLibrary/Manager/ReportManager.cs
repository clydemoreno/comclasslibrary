﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMClassLibrary.Manager
{
    public class ReportManager
    {
        public static void CreateMonthlyReport(string violationLogPath, string logPath)
        {
            EmailLib.EmailProperties email = new EmailLib.EmailProperties();
            DateTime d = DateTime.Now;
            DateTime previousMonth = d.AddDays(-1);
            violationLogPath = Environment.ExpandEnvironmentVariables(System.IO.Path.Combine(violationLogPath, previousMonth.ToString("MMM-yyyy")));
            logPath = Environment.ExpandEnvironmentVariables(System.IO.Path.Combine(logPath, previousMonth.ToString("MMM-yyy")));
            
            /*checks if the current month has a violation folder
             * if it does not exist, EmailManager will just send a confirmation email with 100% participation
             * if exists, go to else
             */

            if (!Directory.Exists(violationLogPath)) {
                email.Subject = "Monthly Report";
                string body = "<h1>There are no violations for the month of " + previousMonth.ToString("MMMM") + "</h1>"
                                + "<p> Participation rate for the month is 100%</p>";
                email.Body = body;
                EmailLib.EmailManager.SendEmail(email);
                return; 
            }
            else
            {
                //stores the path of all the VIOLATION log files under the current month's folder
                string[] violationLogFiles = Directory.GetFiles(violationLogPath, "*.log", SearchOption.AllDirectories);
                email.Subject = "Monthly Report with Breach";
                string body = "<p>Violation Logs: </p><br />";

                //stores the path of all the log files under the current month's folder
                string[] allLogs = Directory.GetFiles(logPath, "*.log", SearchOption.AllDirectories);
                string[] allLogsAsOne = { };

                //put every log in one big array
                    foreach (string pathToFile in allLogs)
                    {
                        string[] contents = FileManager.Instance.getFileContents(pathToFile);
                        allLogsAsOne = allLogsAsOne.Concat(contents).ToArray();
                    }
               
                
                double totalMinutesForTheMonth = allLogsAsOne.Length;
               
                string[] logOfAllViolations = { };

                //put every violation log in one big array
                foreach (string pathToFile in violationLogFiles)
                {
                    body = body + "<br /><p>From log file: <b>"+ pathToFile +"</b></p>";
                    string[] contents = FileManager.Instance.getFileContents(pathToFile);
                    foreach (string line in contents)
                    {
                        body = body + "<p>" + line + "</p>";
                    }
                    logOfAllViolations = logOfAllViolations.Concat(contents).ToArray();
                }
                double totalMinutesOfViolations = logOfAllViolations.Length;
                
                /*compute for the participation percent using the length of allLogs and violationLogFiles
                 * you can use the length since every instances of the array represents every minute-logged data
                 * check getMonthlyParticipation method below
                 */
                double participation = getMonthlyParticipation(totalMinutesOfViolations, totalMinutesForTheMonth);
                body += "<hr /><p>Market Maker should post and maintain orders for at least 80% of the time for one month</p>";
                body = body + "<h2>Total Participation for the month of " + previousMonth.ToString("MMMM") + ": <b>" + String.Format("{0:N2}%", participation) + "</b></h2>";
                body = body + "<br /><p>Total Minutes: <b>" + totalMinutesForTheMonth + "</b></p>";
                body = body + "<p>Minutes in Violation for the month: <b>" + totalMinutesOfViolations + "</b></p>";

                email.Body = body;
                EmailLib.EmailManager.SendEmail(email);
            }

        }
        public static void Create(string path)
        {
            EmailLib.EmailProperties email = new EmailLib.EmailProperties();
            string[] violationLog = FileManager.Instance.getFileContents(path);
            string participation = String.Format("{0:N2}%", getParticipation(violationLog.Length));         
            email.Subject = "Daily Report With Breach";
            string body = "<h><b>Violation Log: </b></h>";
            foreach (string line in violationLog)
            {
                body += "<p>" + line + "</p>";
            }
            body += "<br /><p>Minutes in violation: <b>" + violationLog.Length +" minutes<b></p>";
       
            body += "<p>Daily participation should be more than 50% </p>";
            body += "<br /><p>Participation: " + participation + "</p>";
            email.Body = body;
            EmailLib.EmailManager.SendEmail(email);
        }
        public static void Create()
        {
            EmailLib.EmailProperties email = new EmailLib.EmailProperties();
            email.Subject = "Daily Report";
            email.Body = "<h1>Market Maker has no violation for today</h1>"
                +  "<p>Participation is 100% </p>";
            EmailLib.EmailManager.SendEmail(email);
        }
        private static double getParticipation(double minutes)
        {
            return ((4.5 - (minutes)/60) / 4.5) * 100;
        }
        private static double getMonthlyParticipation(double minutesInViolation, double totalMinutes)
        {
            return ((totalMinutes - minutesInViolation) / totalMinutes)* 100;
        }


    }
}
