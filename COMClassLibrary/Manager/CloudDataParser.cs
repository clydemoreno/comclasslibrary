﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Strategy.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace COMClassLibrary.Manager
{
    public class CloudDataParser
    {
        public static Dictionary<String, Object> getJsonResponse()
        {
            string url = "https://fsmbc-logs.appspot.com/_ah/api/fmsbclogs/v1.0/latestInfo";
            WebClient wb = new WebClient();
            byte[] arr = wb.DownloadData(url);

            string response = Encoding.ASCII.GetString(arr);
            Dictionary<String, Object> jsonObject = JsonConvert.DeserializeObject<Dictionary<String, Object>>(response);
           
            return jsonObject;
        }
    }

    public class saveStocksOnCloud
    {

        public static async void cloudSave(StockParser s)
        {
            string[] tickers = TickerParser.getTickers();
            foreach (String a in tickers)
            {
                var quotes = s.Request("FMETF");
                WebClient client = new WebClient();
                client.Headers[HttpRequestHeader.ContentType] = "application/json";

                var values = new JObject();
                values["code"] = a;
                values["date"] = DateTime.Now.ToString("yyy/MM/dd");
                values["lastPrice"] = quotes.lastPrice;
                values["open"] = quotes.open;
                values["high"] = quotes.high;
                values["low"] = quotes.low;
                values["close"] = quotes.close;
                values["volume"] = quotes.volume;
                string svalue = SerializeWithoutQuote(values);
                Console.WriteLine("Done uploading " + a);
                client.UploadString("https://fsmbc-logs.appspot.com/_ah/api/fmsbclogs/v1.0/saveStrategy", svalue);
                client.Dispose();
                await Task.Delay(2000);
            }        
        }
        private static string SerializeWithoutQuote(object value)
        {
            var serializer = JsonSerializer.Create(null);

            var stringWriter = new StringWriter();

            using (var jsonWriter = new JsonTextWriter(stringWriter))
            {
                jsonWriter.QuoteName = false;

                serializer.Serialize(jsonWriter, value);

                return stringWriter.ToString();
            }
        }

    }
}
