﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MathNet.Numerics.LinearAlgebra.Double;
using COMClassLibrary.FMServiceReference;


namespace COMClassLibrary.Manager
{
    public class RSI
    {
        private double y;

        public double Y
        {
            get { return y; }
            set { y = value; }
        }
        private double derivative;

        public double Derivative
        {
            get { return derivative; }
            set { derivative = value; }
        }
        private double yIntercept;

        public double YIntercept
        {
            get { return yIntercept; }
            set { yIntercept = value; }
        }
    }


    public class RSICalculator
    {
        public void InsertRSI(String stockName, double rsiValue, int frequency, int rsiPeriod, String timeFrame, DateTime date)
        {
            //todo: ask carlo to add the missing columns
            COMClassLibrary.FMServiceReference.StockServiceClient c = new COMClassLibrary.FMServiceReference.StockServiceClient();
            c.InsertRSI(stockName,rsiPeriod,rsiValue, frequency, timeFrame,date);

        }
        public void GetLastInsert(string timeFrame, int frequency, int rsiPeriod)
        {
            DateTime currentTime = DateTime.Now;

            TimeSpan timeSpan = currentTime - GetLastInsertedDateFromDB(timeFrame, frequency);

            Console.WriteLine("Total Minutes:{0}", timeSpan.TotalMinutes);

            

            GenerateTimePattern(currentTime, timeSpan, frequency, rsiPeriod);

        }
        private DateTime GetLastInsertedDateFromDB(string timeFrame, int frequency)
        {
            //temp code;
            //get from rsi table
            return DateTime.Now.AddHours(-.5);
        }
        private DateTime GetDateByPeriod(DateTime currentDate,int period, int frequency){
            //DateTime currentDate = DateTime.Now;
            
            return currentDate.AddMinutes(-period * frequency);
            //return endDate;
        }
        public void GenerateTimePattern(DateTime endTime, TimeSpan timeSpan, int minuteMultiplier, int rsiPeriod)
        {

            int period = Convert.ToInt32(timeSpan.TotalMinutes / minuteMultiplier);

            for (int i = 0; i < period; i++)
            {
                DateTime date = endTime.AddMinutes(-(i * minuteMultiplier));
                Console.WriteLine(@"{0:d/M/yyyy HH:mm\%}", date);
                //db call 
                //double rsi = 

                
                //get date by rsi period
                // return a list of rsi prices
                //calculate rsi
                //save rsi
            }





        }

        public FMServiceReference.RSITO[] GetRSIFromDb(String stockName, int period, int frequency, String timeFrame, DateTime date)
        {
            FMServiceReference.StockServiceClient c = new FMServiceReference.StockServiceClient();
            return c.GetRSIList(stockName, frequency, timeFrame, period, date);
        }

        public bool IsThresholdHigher(double currentRSI, double previousRSI, Single percentage)
        {
            //bool returnValue = false;
            return ((previousRSI != 0 && (currentRSI / previousRSI >= percentage)));
            //return returnValue;

        }
        public bool IsThresholdLower(double currentRSI, double previousRSI, Single percentage)
        {
            //bool returnValue = false;
            return ((previousRSI != 0 && (currentRSI / previousRSI <= percentage)));
            //return returnValue;

        }
        public void MonitorRSIBreakout(List<double> xlist, List<double> ylist, double x, int period)
        {
            RSI rsi = GetRSIUsingLinearRegressionWithThreeCriticalPoints(xlist, ylist, period);
        }

        public RSI PlotLineWithRSI(String stockName, int period, int frequency, String timeFrame, int anyX, DateTime date)
        {
            List<double> xList = new List<double>();
            List<double> yList = new List<double>();
            FMServiceReference.RSITO[] list = GetRSIFromDb(stockName,period,frequency,timeFrame,date);
            if (list == null) return null;
            int i = 1;
            foreach (RSITO item in list)
            {
                yList.Add(item.RsiValue);
                xList.Add(i++);
            }
            return GetRSIUsingLinearRegressionWithThreeCriticalPoints(xList, yList, anyX);
        }

        public RSI GetRSIUsingLinearRegressionWithAllPoints(List<double> xlist, List<double> ylist)
        {
            int x = 0;
            RSI rsi = CalculateRSILinearRegression(x,xlist.ToArray<double>(), ylist.ToArray<double>());
            return rsi;
        }

        public RSI GetRSIUsingLinearRegressionWithThreeCriticalPoints(List<double> xlist, List<double> ylist, double x)
        {
            //List<double> list, int period, double 
            // data points

            int firstSegment = Convert.ToInt32( (ylist.Count * .33));
            int secondSegment = Convert.ToInt32( ylist.Count * .66);

            double firstPoint = ylist[firstSegment-1];
            double secondPoint = ylist[secondSegment-1];
            double thirdPoint = ylist[ylist.Count-1];


            


            double yfirstApex = 0;
            double xfirstApex = 0;
            // double yTempFirstApex = 0;
            for (int i = 0; i < firstSegment - 1; i++)
            {
                if (ylist[i] > yfirstApex)
                {
                    yfirstApex = ylist[i];
                    xfirstApex = i + 1;
                }

            }

            double ysecondApex = 0;
            double xsecondApex = 0;
 
            // double yTempFirstApex = 0;
            for (int i = Convert.ToInt32(firstSegment); i < secondSegment - 1; i++)
            {
                if (ylist[i] > ysecondApex)
                {
                    ysecondApex = ylist[i];
                    xsecondApex = i + 1;

                }

            }

            double ythirdApex = 0;
            double xthirdApex = 0;

            // double yTempFirstApex = 0;
            for (int i = Convert.ToInt32(secondSegment); i < ylist.Count; i++)
            {
                if (ylist[i] > ythirdApex)
                {
                    ythirdApex = ylist[i];
                    xthirdApex = i + 1;

                }

            }


            //var xdata = new double[] { 10, 20, 30 };
            //var ydata = new double[] { 24, 22, 15 };
            //var xdata = new double[] { 8, 18, 26, 36 };
            //var ydata = new double[] { 6,4,7,11 };

            var xdata = new double[] { xfirstApex, xsecondApex, xthirdApex};
            var ydata = new double[] { yfirstApex, ysecondApex, ythirdApex };

            // build matrices
            RSI rsi = CalculateRSILinearRegression(x, xdata, ydata);
            return rsi;

        }

        public RSI CalculateRSILinearRegression(double x, double[] xdata, double[] ydata)
        {
            var X = DenseMatrix.CreateFromColumns(
              new[] { new DenseVector(xdata.Length, 1), new DenseVector(xdata) });
            var y = new DenseVector(ydata);

            // solve
            var p = X.QR().Solve(y);
            var a = p[0];
            var b = p[1];

            //Console.WriteLine(String.Format("b:{0}, a: {1}" ,b,a));

            RSI rsi = new RSI();
            rsi.Derivative = b;
            rsi.Y = (b * x) + a; ;
            rsi.YIntercept = a;
            return rsi;
        }
        public RSI GetYFromRSILine(List<double> list, int period, double x)
        {
            if (list.Count > 0 && list.Count < period) return new RSI();
            //double startingPeriod = 1;
            //double endingPeriod = period;
            //m = delta y / delta x	
            double y2 = list[list.Count - 1];
            double y1 = list[0];
            double x2 = period;
            double x1 = 1;
            if ((x2 - x1) == 0) return new RSI();
            double m = (y2 - y1) / (x2 - x1);
            double y = m * (x - 1) + list[0];

            RSI rsi = new RSI();
            rsi.Derivative = m;
            rsi.Y = y;
            rsi.YIntercept = list[0];
            return rsi;
        }
        public double CalculateRSI(List<double> list, int period){
            //number of items in the array should be more than or equal the period
            if (list.Count > 0 && list.Count < period) return 0;


            double previousItem = 0;
            double currentItem = 0;
            List< double> up = new List<double>();
            List<double> down = new List<double>();
            double upSum = 0;
            double downSum = 0;
            double upSumAve = 0;
            double downSumAve = 0;
            double rs = 0;
            double rsi = 0;
            for (int i = 1; i < list.Count;i++ )
            {
                //if (i == 1)
                //{
                //    previousItem = 0;

                //}
                //else
                //{
                    previousItem = list[i - 1];
                //}
                currentItem = list[i];
                if (currentItem > previousItem)
                {
                    upSum += (currentItem - previousItem);
                    downSum += (0);
                }
                else
                {
                    //down.Add(previousItem - currentItem);
                    //up.Add(0);
                    downSum += (previousItem - currentItem);
                    upSum += (0);
                }


                
            }

            if ((period - 1) > 0)
            {
                upSumAve = upSum / (period - 1);
                downSumAve = downSum / (period - 1);
            }
            if (downSumAve > 0)
            {
                rs = upSumAve / downSumAve;
            }

            rsi = 100 - (100 / (1 + rs));

            if(downSumAve == 0)
            {
                rsi = 100;
            }
            if (upSumAve == 0)
            {
                rsi = 0;
            }
            //100 - (100 / (1 + B17))
            
            
            return rsi;
        }
    }
}
