﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using COMClassLibrary.Model;
using System.Runtime.Caching;
using System.Configuration;

namespace COMClassLibrary.Manager
{
    public class CapitalCalculator
    {
        private static ObjectCache cache = MemoryCache.Default; 
        List<BoardLot> pseMinimumLots;
        //FMServiceReference.BoardLotFluctuationPriceTO[] boardLotList;
        Manager.BoardLotFluctuationPriceTO[] boardLotList;
        public  CapitalCalculator()
        {
        }

     

        public DateTime GetDate()
        {
            DateTime? d = null;
            lock (new object())
            {
                d = (DateTime?)cache["test"];
                if (d == null)
                {

                    d = DateTime.Now;
                    cache.Add(new CacheItem("test", d),
                      new CacheItemPolicy
                      {
                          Priority = CacheItemPriority.NotRemovable,
                          AbsoluteExpiration = DateTime.UtcNow.AddSeconds(5),
                         // SlidingExpiration = TimeSpan.FromSeconds(5)
                      });
                }
            }
            return (DateTime)d;
        }

        public Manager.BoardLotFluctuationPriceTO[] BoardLotList
        {
            get
            {


                ObjectSave os = new ObjectSave();

                Manager.BoardLotFluctuationPriceTO[] boardLotList = 
                    (Manager.BoardLotFluctuationPriceTO[])cache[Constants.CapitalCalculatorBoardLotList];
                if (boardLotList == null)
                {
                    //FMServiceReference.StockServiceClient c = new FMServiceReference.StockServiceClient();
                    //boardLotList = c.GetBoardLotListTO();
                    boardLotList = (BoardLotFluctuationPriceTO[])os.DeSerializeObject<BoardLotFluctuationPriceTO[]>(ConfigurationManager.AppSettings["boardlotlistPath"].ToString());
                }
                //os.SerializeObject(boardLotList, ConfigurationManager.AppSettings["boardlotlistPath"].ToString());
                //var config = new List<string>();
                //var cache = new MemoryCache("myMemCache", config);
                cache.Add(new CacheItem(Constants.CapitalCalculatorBoardLotList, boardLotList),
                new CacheItemPolicy
                {
                    Priority = CacheItemPriority.NotRemovable,
                    //SlidingExpiration = TimeSpan.FromMinutes(30)
                    AbsoluteExpiration = System.DateTime.UtcNow.AddDays(5)
                });

                //if (boardLotList == null) 
                //    boardLotList = new List<FMServiceReference.BoardLotFluctuationPriceTO>().ToArray<FMServiceReference.BoardLotFluctuationPriceTO>();
                return boardLotList;
            }
        }

        public List<BoardLot> PseMinimumLots
        {
            get { 
                if(pseMinimumLots == null) pseMinimumLots = new List<BoardLot>();
                return 
                    pseMinimumLots; 
            }
            
        }

        //private ConfigContext configContext;

        //public ConfigContext ConfigContext
        //{
        //    get { return configContext; }
        //    set { configContext = value; }
        //}
        public double GetLowestPrice(List<double> list)
        {
            //get the next higher resistance
            double price = Double.MaxValue;
            foreach (double item in list)
            {
                if (item < price)
                {
                    price = item;
                }
            }
            return price;
        }

        public double GetHighestPrice(List<double> list)
        {
            //get the next higher resistance
            double price = 0;
            
            foreach (double item in list)
            {
                if (item > price)
                {
                    price = item;
                }
            }
            return price;
        }

        public double GetAbsoluteProfitTarget(double entryPrice, double requiredRecogniaPTPercentage)
        {
            return entryPrice * requiredRecogniaPTPercentage;
        }

        public double GetAbsoluteStopLoss(double entryPrice, double requiredAbsoluteStopLossFactor)
        {

            return entryPrice * requiredAbsoluteStopLossFactor;
        }

        private double GetRSIMultiplier(double rsiLevel)
        {
            
            return 50 / rsiLevel;
        }

        public double Calculate(double rsiLevel, double capital, double capitalMultiplier, int capitalDivisor)
        {
            //ConfigContext c = new ConfigContext();
            double initialCapital = capital * capitalMultiplier;
            if (capitalDivisor > 0)
            {
                initialCapital = initialCapital / capitalDivisor;
            }
            return initialCapital * GetRSIMultiplier(rsiLevel);
            //double baseLotSizeCapital = 

        }

        public double RoundToFlucs(double price)
        {
            BoardLotFluctuationPriceTO lot = GetBoardLotBasedOnPrice(price);
            int count = BitConverter.GetBytes(Decimal.GetBits(Convert.ToDecimal(lot.Fluctuation))[3])[2];
            return Math.Round(price, count);
            //return price;
        }


        public int GetFlucsBetweenBidAndAsk(double bid, double ask)
        {
            //double price = iNavPrice;
            BoardLotFluctuationPriceTO boardLot = null;
            int count = 0;
            //double diff = price - bid;
            decimal dAsk = Convert.ToDecimal(ask);
            decimal dBid = Convert.ToDecimal(bid);
            while (Convert.ToDecimal(ask) > Convert.ToDecimal(bid))
            {
                boardLot = GetBoardLotBasedOnPrice(bid);
                bid = bid + boardLot.Fluctuation;
                ++count;
            }
            return count;

        }


        public double GetFlucsBetweenINavAndBid(double iNavPrice, double bid)
        {
            double price = iNavPrice;
            BoardLotFluctuationPriceTO boardLot = null;
            int count = 0;
            //double diff = price - bid;
            while (price >= bid)
            {
                boardLot = GetBoardLotBasedOnPrice(price);
                price = price - boardLot.Fluctuation;
                ++count;
            }
            return count;

        }

        public double GetFlucsBetweenINavAndAsk(double iNavPrice, double ask)
        {
            double price = iNavPrice;
            BoardLotFluctuationPriceTO boardLot = null;
            int count = 0;
            //double diff = price - bid;
            while (price <= ask)
            {
                boardLot = GetBoardLotBasedOnPrice(price);
                price = price + boardLot.Fluctuation;
                ++count;
            }
            return count;

        }

        public double GetLowerPriceByFlucOffset(double price, double lowerOffset)
        {
            
            double difference = Math.Abs(price * .00000001);
            BoardLotFluctuationPriceTO boardLot = null;
            for (int i = 1; i <= lowerOffset; i++)
            {
                boardLot = GetBoardLotBasedOnPrice(price);
                if (boardLot == null) throw new Exception(String.Format("Value of Default Bid: {0} did not get any matching Board Lot", price));
                //close. try to use the price and let it go down
                if (Math.Abs(price - boardLot.StartPrice) <= difference)
                {
                    boardLot = GetBoardLotBasedOnPrice(price - boardLot.Fluctuation);
                }
                price = price + (boardLot.Fluctuation * -1);
            }
            //defaBid = skewNAV;
            //defaBid = -1 * (lowerHalf * bidFlucs ) + SkewNAV;
            return (RoundToFlucs(price));

        }


        public double GetNumberOfShares(double capital, double price)
        {
            if (price == 0) return 0;
            return capital / price;
        }
        public double GetCapital(double multiplier, double price)
        {
            double baseCapital = 1000000 * multiplier;
            double basePrice = 100;
            double baseFluc = .05;
            BoardLotFluctuationPriceTO boardLot = GetBoardLotBasedOnPrice(price);
            double fluc = boardLot.Fluctuation;
            double fps = baseCapital / basePrice * baseFluc;

            double newCapital = fps * price / fluc;

            return newCapital;
        }

        public double GetHigherPriceByFlucOffset(double price, double higherOffset)
        {
            BoardLotFluctuationPriceTO boardLot;
            double tempPrice = price;

            //CapitalCalculator calculator = new CapitalCalculator();
            for (int i = 1; i <= higherOffset; i++)
            {
                boardLot = GetBoardLotBasedOnPrice(tempPrice);
                if (boardLot == null) throw new Exception(String.Format("Value of Pirce: {0} did not get any matching Board Lot"
                    , tempPrice));
                //no need to handle the boundaries because the flucs flow is going up

                tempPrice = tempPrice + (boardLot.Fluctuation) * 1;
            }
            return RoundToFlucs(tempPrice);
        }



        //public void initializeBoardLot()
        //{
        //    BoardLot l = new BoardLot();
        //    l.StartingPrice = 10.00;
        //    l.EndingPrice = 100.00;
        //    l.Fluctuation = .02;
        //    l.Lot = 1000;
        //    PseMinimumLots.Add(l);

        //    l = new BoardLot();
        //    l.StartingPrice = 101.00;
        //    l.EndingPrice = 200.00;
        //    l.Fluctuation = .02;
        //    l.Lot = 500;

        //    PseMinimumLots.Add(l);
        //}

        public BoardLotFluctuationPriceTO GetBoardLotBasedOnPrice(double price)
        {

            Manager.BoardLotFluctuationPriceTO boardLot =
                    (Manager.BoardLotFluctuationPriceTO)cache[Math.Round( price,10).ToString()];
            if (boardLot == null)
            {
                foreach (Manager.BoardLotFluctuationPriceTO lot in BoardLotList)
                {
                    if (Math.Round(price, 10) <= lot.EndPrice && Math.Round(price, 10) >= lot.StartPrice)
                    {
                        boardLot = lot;

               
                        cache.Add(new CacheItem(Math.Round(price, 10).ToString(), lot),
                new CacheItemPolicy
                {
                    Priority = CacheItemPriority.NotRemovable,
                    //SlidingExpiration = TimeSpan.FromMinutes(30)
                    AbsoluteExpiration = System.DateTime.UtcNow.AddDays(35)
                });

                        break;
                    }
                }
            }

            //BoardLotFluctuationPriceTO boardLot = null;
           
            
            return boardLot;
        }
        public double CalculateMinimumLot(double price)
        {
            //initializeBoardLotFromDb();
            //call db to get the table.  add this to memory one time only.
            //double returnValue = 0;

            //foreach (BoardLot lot in PseMinimumLots)
            //{
            //    if(price <= lot.EndingPrice && price >= lot.StartingPrice){
            //        returnValue = lot.Lot;
            //    }
            //}
            //return returnValue;


            //call db to get the table.  add this to memory one time only.
            double returnValue = 0;

            BoardLotFluctuationPriceTO boardLot = GetBoardLotBasedOnPrice(price);
            if (boardLot != null)
            {
                returnValue = boardLot.BoardLot;
            }

            //foreach (FMServiceReference.BoardLotFluctuationPriceTO lot in BoardLotList)
            //{
            //    if (price <= lot.EndPrice && price >= lot.StartPrice)
            //    {
            //        returnValue = lot.BoardLot;
            //    }
            //}
            return returnValue;
            
        }

    }
}
