﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using COMClassLibrary.Model;

namespace COMClassLibrary.Manager
{
    public class MMMaintainSpreadEventArg : EventArgs
    {
        public MatchingEngineParams matchingEngineParams;
        public MMMaintainSpreadEventArg(MatchingEngineParams p)
        {
            this.matchingEngineParams = p;
        }

    }
}
