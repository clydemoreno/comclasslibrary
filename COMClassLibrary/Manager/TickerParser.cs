﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strategy.Helpers
{
    public class TickerParser
    {
        public static string[] getTickers()
        {
            string[] tickers = null;
            String line = "";
            using (StreamReader sr = new StreamReader("tickers.txt"))
            {
               line = sr.ReadToEnd();
            }

            tickers = line.Split(new string[] { "," }, StringSplitOptions.None);

            return tickers;
        }
    }
}
