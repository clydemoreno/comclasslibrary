﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using MySqlDb.FirstMetroATS;
using System.Threading.Tasks;
namespace COMClassLibrary.Manager
{
    [ClassInterface(ClassInterfaceType.AutoDual)]
    public class TradingRuleManager
    {
        
        public TradingRuleManager()
        {
        }
        public String GetValuesFromEngine()
        {
            FileManager m = FileManager.Instance;
            return m.Data;
        }
        void QueueTasks()
        {
            // TaskA is a top level task.
            Task taskA = Task.Factory.StartNew(() =>
            {
                Console.WriteLine("I was enqueued on the thread pool's global queue.");

                // TaskB is a nested task and TaskC is a child task. Both go to local queue.
                Task taskB = new Task(() => Console.WriteLine("I was enqueued on the local queue."));
                Task taskC = new Task(() => Console.WriteLine("I was enqueued on the local queue, too."),
                                        TaskCreationOptions.AttachedToParent);

                taskB.Start();
                taskC.Start();

            });
        }
        //public String GetStock(String stockSymbol)
        //{
        //    FileManager m = FileManager.Instance;
        //    m.WriteToAFile(stockSymbol);
        //    return "Stock symbol is" + stockSymbol;
       // }
        public String SaveToDB( string stockName,double last,
         double bid,
         double ask,
         double volume,
         double high,
         double low,
            DateTime date)
        {
            //FileManager m = FileManager.Instance;
            //m.WriteToAFile(String.Format("{0},{1},{2}",stockName,ask,bid));
            //os.updated_on = DateTime.Now;
            String returnValue = "";
            try
            {
                //OrderScreen os = OrderScreen.GetInstance();
                //OrderScreen os = OrderScreen.GetInstance();



               // os.InsertStockData(stockName,last, bid, ask, high, low, volume,date);

            }
            catch (Exception ex)
            {
                returnValue = ex.Message;
            }
            return returnValue;
        }

        public String SaveToDBAsync( string stockName,
        double last,
         double bid,
         double ask,
         double volume,
         double high,
         double low,
            DateTime date)
        {
            //FileManager m = FileManager.Instance;
            //m.WriteToAFile(String.Format("{0},{1},{2}",stockName,ask,bid));
            //os.updated_on = DateTime.Now;
            String returnValue = "";
            FMServiceReference.StockServiceClient client = new FMServiceReference.StockServiceClient();

                // TaskA is a top level task.
                Task taskA = Task.Factory.StartNew(() =>
                {
                    try
                    {
                        returnValue = client.SaveToDB(stockName, last, bid, ask, high, low, volume, date);
                    }
                    catch (Exception ex)
                    {
                        throw (ex);
                    }
    
                });


            return returnValue;
        }
    

    }

}
