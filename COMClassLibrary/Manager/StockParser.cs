﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Net;
using System.Collections.Specialized;
using System.Configuration;
using TechnistockDataFeed;

namespace COMClassLibrary.Manager
{
    public class StockParser : IDisposable
    {
        private WebClient webClient;
        String url;
        string theUrl;
        NameValueCollection formData;
        //~StockParser()
        //{
        //    this.webClient.Dispose();
            
        //}

        public void Dispose()
        {
            // Dispose of unmanaged resources.
            
            this.Dispose();
            // Suppress finalization.
            WebClient.Dispose();

            GC.SuppressFinalize(this);
        }
        public WebClient WebClient
        {
            get {
                if (webClient == null) webClient = new WebClient();
                return webClient; }
        }


        public static Dictionary<string, decimal> GetStockClosingPrice(List<string> stocks)
        {
            Dictionary<string, decimal> stockPrices = new Dictionary<string, decimal>();

            DataFeed d = new DataFeed();
            BidAskData bidAskData = null;
            try
            {
                foreach (var stock in stocks)
                {
                    bidAskData = d.Parse(d.Connect());
                    stockPrices.Add(stock, Convert.ToDecimal(bidAskData.AskPrice));
                    Console.WriteLine(bidAskData.AskPrice);
                    Console.WriteLine(bidAskData.SecCode);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }


            //StockParser s = new StockParser();
            //foreach (string stock in stocks)
            //{
            //    var quotes = Request(stock);
            //    stockPrices.Add(stock, Convert.ToDecimal(quotes.lastPrice));
            //    //changed this from close to last price.  
            //    //stockPrices.Add(stock, Convert.ToDecimal(quotes.close));
            //}

            return stockPrices;
        }


        public void Connect()
        {
            try
            {

                Security sec = null; ;
                String lastPrice = String.Empty;
                WebClient client = this.WebClient;

                //byte[] a = client.DownloadData(q);
                //string responsex = Encoding.ASCII.GetString(a);

                client.Headers["User-Agent"] = "";
                //byte[] arr = client.DownloadData("https://www.firstmetrosec.com.ph/misc/verifylogin.asp");
                //String url = "https://uat.firstmetrosec.com.ph";
                //url = "https://www.firstmetrosec.com.ph";
                url = ConfigurationManager.AppSettings["url"].ToString();

                theUrl = String.Format("{0}/misc/verifylogin.asp", url);
                formData = new NameValueCollection();
                String userName = ConfigurationManager.AppSettings["parserAccountUser"].ToString();
                String password = ConfigurationManager.AppSettings["parserAccountPassword"].ToString();
                formData["username"] = userName;
                formData["password"] = password;

                byte[] arr = WebClient.UploadValues(theUrl, "POST", formData);

                string response = Encoding.ASCII.GetString(arr);

                response = Encoding.ASCII.GetString(arr);
            }
            catch (Exception ex)
            {
                LogManager.logError(ex.Message);
            }
 
        }

        //public Security GetPrice(string q)
        //{
        //    Security sec = null; ;
        //    String lastPrice = String.Empty;
        //    using (WebClient client = new WebClient())
        //    {


        //        //byte[] a = client.DownloadData(q);
        //        //string responsex = Encoding.ASCII.GetString(a);

        //        client.Headers["User-Agent"] = "";
        //        //byte[] arr = client.DownloadData("https://www.firstmetrosec.com.ph/misc/verifylogin.asp");
        //        //String url = "https://uat.firstmetrosec.com.ph";
        //        String url  = ConfigurationManager.AppSettings["url"].ToString();
        //        string theUrl = String.Format("{0}/misc/verifylogin.asp", url);
        //        NameValueCollection formData = new NameValueCollection();
        //        String userName = "maan-test account";
        //        String password = "Firstmetr0";
        //        formData["username"] = userName;
        //        formData["password"] = password;

        //       // sec = Request(q, sec, client, url, theUrl, formData);
        //    }
        //    return sec;

        //}
        public Security RequestV2()
        {
            TechnistockDataFeed.DataFeed df = new TechnistockDataFeed.DataFeed();
            Security sec = new Security();
            TechnistockDataFeed.BidAskData ba = df.Parse(df.Connect());

            sec.Bids.Add(
                new BidAsk((1).ToString()
                           , "Bid", (decimal)ba.BidPrice,ba.BidVol,ba.BidVol)
    );
            sec.Asks.Add(
                new BidAsk((1).ToString()
                           , "Ask", (decimal)ba.AskPrice, ba.AskVol, ba.AskVol)
    );
            return sec;
        }
        
        
        public Security Request(string q)
        {
            //Console.WriteLine(response);
            try
            {
                string url = ConfigurationManager.AppSettings["url"].ToString();
                string stockInfoPage = ConfigurationManager.AppSettings["stockInfoPage"].ToString();
                string theUrlGet = String.Format("{0}/quotes/{1}?code=", url, stockInfoPage);

                string code = q;
                Security sec = new Security();
                if (code != String.Empty)
                {
                    WebClient wb = new WebClient();
                    byte[] arr = wb.DownloadData(theUrlGet + code);

                    string response = Encoding.ASCII.GetString(arr);
                    StockParser parser = new StockParser();
                    sec = parser.ParseStockXML(response);

                    wb.Dispose();

                }
                return sec;
            }
            catch (Exception ex)
            {
                LogManager.logError(ex.Message);
                return null;
            }
        }
        public Security ParseStockXML(String xml)
        {
            //String lastPrice = string.Empty;
            //string open = string.Empty;
            //string high = string.Empty;
            //string low = string.Empty;
            //String close = string.Empty; 
            //String volume = string.Empty;

            Security sec = new Security();

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);

            XmlNodeList nodes = doc.DocumentElement.SelectNodes("/tstock/security");
            //ListBoo
            foreach (XmlNode node in nodes)
            {
                XmlNode stockInfoNode = node.SelectSingleNode("stockinfo");
                if (stockInfoNode == null) break;
                sec.lastPrice = stockInfoNode.SelectSingleNode("last").InnerText;
                sec.close = stockInfoNode.SelectSingleNode("prevclose").InnerText;
                sec.open = stockInfoNode.SelectSingleNode("open").InnerText;
                sec.high = stockInfoNode.SelectSingleNode("high").InnerText;
                sec.low = stockInfoNode.SelectSingleNode("low").InnerText;
                sec.volume = stockInfoNode.SelectSingleNode("volume").InnerText;
                sec.status = stockInfoNode.SelectSingleNode("status").InnerText;
                XmlNode bidNode = stockInfoNode.SelectSingleNode("bid");
                XmlNodeList bidVolNodes = bidNode.SelectNodes("vol");
                XmlNodeList bidPriceNodes = bidNode.SelectNodes("price");

                XmlNode askNode = stockInfoNode.SelectSingleNode("ask");
                XmlNodeList askVolNodes = askNode.SelectNodes("vol");
                XmlNodeList askPriceNodes = askNode.SelectNodes("price");

                decimal price;
                int volume;
                int numberOfBids = 1;
                int numberOfAsks = 1;

                for (int i = 0; i < bidPriceNodes.Count; i++)
                {
                    Decimal.TryParse(bidPriceNodes[i].InnerText, out price);
                    int.TryParse(bidVolNodes[i].InnerText.Replace(",",""), out volume);

                    sec.Bids.Add(
                        new BidAsk((i + 1).ToString()
                            , "Bid", price, volume, numberOfBids)
                        );
                }

                for (int i = 0; i < askPriceNodes.Count; i++)
                {
                    Decimal.TryParse(askPriceNodes[i].InnerText, out price);
                    int.TryParse(askVolNodes[i].InnerText.Replace(",", ""), out volume);

                    sec.Asks.Add(
                        new BidAsk((i + 1).ToString()
                            , "Ask", price, volume, numberOfAsks)
                        );
                }


            }
            return sec;
        }
    }
    public class BidAsk
    {
        public BidAsk() { }
        public BidAsk(string rank, string type, decimal price, int volume, int numberOfBids)
        {
            this.rank = rank;
            this.type = type;
            this.price = price;
            this.volume = volume;
            this.numberOfBids = numberOfBids;
        }
        public string rank;
        public string type;
        public decimal price;
        public int volume;
        public int numberOfBids;
    }
    public class Security
    {
        public string code;
        public String lastPrice;
        public string open;
        public string high;
        public string low;
        public String close;
        public String volume;
        private List<BidAsk> bids;

        public List<BidAsk> Bids
        {
            get
            {
                if (bids == null) bids = new List<BidAsk>();

                return bids;
            }
        }
        private List<BidAsk> asks;

        public List<BidAsk> Asks
        {
            get
            {
                if (asks == null) asks = new List<BidAsk>();
                return asks;
            }
        }


        public string status { get; set; }
    }
    public class Transaction
    {
        public string side;

    }
}
