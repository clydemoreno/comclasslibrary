﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using COMClassLibrary.Model;
using FirstMetroSystemTrayApp.Model;

namespace COMClassLibrary.Manager
{
    public class SpreadAnchor
    {
        public void SetOffer(MatchingEngineParams param, List<ImportedOrderScreen> sellerList)
        {
            
            

            double defaultOffer = param.DefaOffer;

            foreach (ImportedOrderScreen postedOrder in sellerList)
            {

                if (postedOrder.price == param.BestOffer)
                {
                    param.PriceWithRemainderOfferVolume = postedOrder.price;
                    param.PostedOfferVolume = postedOrder.volume;
                    //priceLevel = order.price;
                    //volumeRemainder = order.volume - param.MarketBidVolume; 
                }

                if (postedOrder.price <= param.MMOffer)
                {
                    postedOrder.action = "Cancel";
                }
            }



        }
        public void SetBid(MatchingEngineParams param, List<ImportedOrderScreen> buyerList)
        {

            double defaultBid = param.DefaBid;

            //find the level of the market bid and subtract the volume.  if zero then don't do anything, if more than 
            // 50 then that is the level we want to start with.
            // if negative then prompt for a control-O
            //double priceLevel = 0;
            //int volumeRemainder = 0;
            foreach (ImportedOrderScreen postedOrder in buyerList)
            {
                if (postedOrder.price == param.BestBid)
                {
                    param.PriceWithRemainderBidVolume = postedOrder.price;
                    param.PostedBidVolume = postedOrder.volume;
                    //priceLevel = order.price;
                    //volumeRemainder = order.volume - param.MarketBidVolume; 
                }
                //this will run the code to figure out the balance
                if (postedOrder.price >= param.MMBid)
                {
                    postedOrder.action = "Cancel";
                }
            }





        }

    }
}
