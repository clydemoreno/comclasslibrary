﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using COMClassLibrary.FMServiceReference;

namespace COMClassLibrary.Manager
{
    public class DiagnosticsManager
    {
        // An event that clients can use to be notified whenever the
        // elements of the list change.
        public event ChangedEventHandler Changed;

        public delegate void ChangeHandler(OrderManager m, EventArgs e);
        // Invoke the Changed event; called whenever list changes
        protected virtual void OnChanged(Object sender, EventArgs e)
        {
            if (Changed != null)
                Changed(sender, e);
        }
        public bool IsPriceServerRunning(int anyFrequency)
        {
            bool isCurrent = false;
            FMServiceReference.StockServiceClient c = new FMServiceReference.StockServiceClient();
            try
            {
                anyFrequency = 5;
                PriceWithTimeFrameTO[] list = c.GetTimeFrameDescending(Constants.FMETF_PM, anyFrequency, "SEC", 1, DateTime.Now);
                if (list != null && list.Length > 0)
                {
                    DateTime d = list[0].Date;
                    //int result = d.CompareTo(DateTime.Now.AddMinutes(anyFrequency));
                    //isCurrent = (result >= 1);



                    DateTime todaysDateTime = DateTime.Now;

                    TimeSpan span = todaysDateTime.Subtract(d);
                    double totalMins = span.TotalMinutes;

                    isCurrent = (totalMins <= 2);
                }
            }
            catch (Exception ex)
            {
                isCurrent = false;
                EventLog.WriteEntry(Constants.FMSecSource, ex.Message,EventLogEntryType.Error);
            }
            finally
            {
                c.Close();
            }
            //isCurrent =  (list != null && list.Length > 0) ;
            return isCurrent;
        }
        public String RunWebServiceDiagnostics()
        {
            String returnValue = String.Empty;
            try
            {
                string test = "test";
                FMServiceReference.StockServiceClient c = new FMServiceReference.StockServiceClient();
                returnValue = test == c.DoWork("test") ? "Up" : "Down";
            }
            catch(Exception ex)
            {
                returnValue = "Down";
            }
            return returnValue;
        }
    }
}
