﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using COMClassLibrary.FMServiceReference;

namespace COMClassLibrary.Manager
{
    public class ErrorEventArg : EventArgs
    {
        public String Code;
        public String StockName;
        public String Message;
        public String StrategyClass;

        public ErrorEventArg(String stockName, String code, String message)
        {
            this.StockName = stockName;
            this.Code = code;
            this.Message = message;
           // this.StrategyClass = strategyClass;
        }

    }
    public class MessageEventArg : EventArgs
    {
        public String Code;
        public String StockName;
        public String Message;
        public String StrategyClass;
        public IMessageHandler MessageHandler;
        public MessageEventArg(String stockName, String code, String message, String strategyClass, IMessageHandler messageHandler)
        {
            this.StockName = stockName;
            this.Code = code;
            this.Message = message;
            this.StrategyClass = strategyClass;
            this.MessageHandler = messageHandler;
        }

        
    }
    
    // A delegate type for hooking up change notifications.
    public delegate void ChangedEventHandler(object sender, EventArgs e);
    public class OrderManager
    {

        private String techniStockfilePath;
        public String TechniStockfilePath
        {
            get { return techniStockfilePath; }
            set { techniStockfilePath = value; }
        }
        private CapitalCalculator capCalculator = null;

        public CapitalCalculator CapCalculator
        {
            get {
                if (capCalculator == null) capCalculator = new CapitalCalculator();
                return capCalculator; 
            }
            
        }

        private String stockName;

        public String StockName
        {
            get { return stockName; }
            set { stockName = value; }
        }

        public String AddClassNameToStockName(String stockName)
        {
            return String.Format("{0},{1}", stockName, this.GetType());
        }
        public String GetStatus(String stockName, Dictionary<String, object> parameters)
        {
            object obj = GetParameter(stockName, Constants.RSIStatus, parameters);
            return obj != null ? (String)GetParameter(stockName, Constants.RSIStatus, parameters) : String.Empty;
        }

        //public Object GetParameter(String stockName, String key, Dictionary<String, object> parameters)
        //{
        //    Object returnValue = null;
        //    Object lockObject = new object();
        //    lock (lockObject)
        //    {
        //        if (parameters != null && parameters.ContainsKey(stockName))
        //        {
        //            Dictionary<String, object> dictionary = (Dictionary<String, object>)parameters[stockName];
        //            if (dictionary.ContainsKey(key))
        //            {
        //                returnValue = dictionary[key];
        //            }
        //        }
        //    }
        //    return returnValue;
        //}

        //public void GetOrderInfo(String stockName,  Dictionary<String, object> parameters)
        //{
        //    try{

        //        FMServiceReference.StockServiceClient c = new StockServiceClient();
        //        OrderTO order = c.GetOrderInfo(String.Format("{0},{1}", stockName, this.GetType()));
         
        //    if (order != null)
        //    {
        //        SetParameters(stockName, Constants.RSIOrderTOCode, order, parameters);
        //    }
        //   // SetParameters(stockName, Constants.RSIPriceWithTimeFrameList, c.GetPriceWithTimeFrameList(stockName, frequency, timeFrame, profitTargetLookupPeriod), parameters);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception("Error: Getting Order Info: stack trace: " + ex.Message);
        //    }
        //}

        public void SetStatus(String stockName, String value, Dictionary<String, object> parameters,bool persist)
        {


            SetParameters(stockName, Constants.RSIStatus, value, parameters);
            if (persist)
            {
                UpdateOrderStatus(stockName, value);
            }
            OnChanged(parameters, new MessageEventArg(stockName, Constants.RSIStatus, value, this.GetType().ToString(),null));

        }
        public void SetStatus(String stockName, String value, Dictionary<String, object> parameters)
        {

            SetStatus(stockName, value, parameters, true);
            //SetParameters(stockName, Constants.RSIStatus, value, parameters);
            //UpdateOrderStatus(stockName, value);
            //OnChanged(parameters,new MessageEventArg(stockName,Constants.RSIStatus,value));
            
        }

        private String message;

        public String Message
        {
            get { return message; }
            
        }
        //public void SetMessage(String code, String message)
        //{
        //    MessageEventArg e = new MessageEventArg(code);
        //    this.message = message;
        //        OnChanged(e);

        //}

      
        public virtual bool DoesKeyExist(String stockName, String key, Dictionary<String, object> parameters)
        {
            bool returnValue = false;
            object lockObject = new object();
            lock (lockObject)
            {
                if (parameters != null && parameters.ContainsKey(stockName))
                {
                    //if(stockMessage[stockName].GetType() == typeof(System.String)){ 
                    //    returnValue = (String)stockMessage[stockName];
                    //}
                    Dictionary<String, object> dictionary = (Dictionary<String, object>)parameters[stockName];
                    if (dictionary.ContainsKey(key))
                    {
                        returnValue = dictionary.ContainsKey(key);
                    }
                }
            }
            return returnValue;
        }

        //public void GetLastVolumeMA(String stockName,  Dictionary<String, object> parameters)
        //{
        //    try
        //    {
        //        DateTime date = (DateTime)GetParameter(stockName, "cutoffDate", parameters);
        //        int onePeriod = 1;
        //        VolumeCalculator vCalc = new VolumeCalculator();
        //        VolumeMATO[] list = vCalc.GetVolumeList(stockName, onePeriod, (int)GetParameter(stockName, "frequency", parameters), (string)GetParameter(stockName, "timeFrame", parameters),date);
        //        VolumeMATO volumeMA = null;
        //        if (list.Length > 0)
        //            volumeMA = list[list.Length - 1];

        //        SetParameters(stockName, Constants.CurrentVolumeMATO, volumeMA, parameters);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception("Error: Setting VolumeMA: stack trace: " + ex.Message);
        //    }
        //}

        public virtual Object GetParameter(String stockName, String key, Dictionary<String, object> parameters)
        {
            Object returnValue = null;
            object lockObject = new object();
            lock (lockObject)
            {

                if (parameters != null && parameters.ContainsKey(stockName))
                {
                    //if(stockMessage[stockName].GetType() == typeof(System.String)){ 
                    //    returnValue = (String)stockMessage[stockName];
                    //}
                    Dictionary<String, object> dictionary = (Dictionary<String, object>)parameters[stockName];
                    if (dictionary.ContainsKey(key))
                    {
                        returnValue = dictionary[key];
                    }
                }
            }
            return returnValue;
        }

        //public static Object GetMessage(String stockName, String key, Dictionary<String,object> parameters)
        //{
        //    Object returnValue = null;
        //                object lockObject = new object();
        //                lock (lockObject)
        //                {

        //                    if (parameters != null && parameters.ContainsKey(stockName))
        //                    {
        //                        //if(stockMessage[stockName].GetType() == typeof(System.String)){ 
        //                        //    returnValue = (String)stockMessage[stockName];
        //                        //}
        //                        Dictionary<String, object> dictionary = (Dictionary<String, object>)parameters[stockName];
        //                        if (dictionary.ContainsKey(key))
        //                        {
        //                            returnValue = dictionary[key];
        //                        }
        //                    }
        //                }
        //    return returnValue;
        //}

        public void F()
        {
            Console.WriteLine("A.F"); 
        }

        public virtual void G() { Console.WriteLine("A.G"); }
       
        public virtual void SetParameters(String stockName, String key, object value, Dictionary<String, object> parameters)
        {
            object lockObject = new object();
            lock (lockObject)
            {

                if (parameters != null)
                {
                    if (!parameters.ContainsKey(stockName))
                    {
                        parameters[stockName] = new Dictionary<String, object>();

                    }
                    Dictionary<String, object> dictionary = (Dictionary<String, object>)parameters[stockName];
                    dictionary[key] = value;
                }
            }
        }

        public void SetErrorMessage(String stockName, string value, Dictionary<String,object> parameters)
        {
            SetParameters(stockName, Constants.MyRSIErrorCode, value, parameters);
            OnChanged(parameters,new ErrorEventArg(stockName,Constants.MyRSIErrorCode,value));

        }
        public void SetMessage(String stockName, String key, String value, Dictionary<String, object> parameters,IMessageHandler messageHandler)
        {
            SetParameters(stockName, key, value, parameters);
            OnChanged(parameters, new MessageEventArg(stockName, key, value, this.GetType().ToString(),messageHandler));
   //         OnChanged(parameters, new RSIErrorEventArg());

        }
        // An event that clients can use to be notified whenever the
        // elements of the list change.
        public event ChangedEventHandler Changed;

        public delegate void ChangeHandler(OrderManager m, EventArgs e);
        // Invoke the Changed event; called whenever list changes
        protected virtual void OnChanged(Object sender, EventArgs e)
        {
            if (Changed != null)
                Changed(sender, e);
        }

        //public  void GetProfitTargetAndStopLossPeriod(String stockName, Dictionary<string,object> parameters)
        //{
        //    try
        //    {
                
        //        int frequency = Convert.ToInt32(GetParameter(stockName, "frequency", parameters));
        //        String timeFrame = (String)GetParameter(stockName, "timeFrame", parameters);
        //        int profitTargetLookupPeriod = Convert.ToInt32(GetParameter(stockName, "profitTargetLookupPeriod", parameters));
        //        FMServiceReference.StockServiceClient c = new StockServiceClient();
        //        SetParameters(stockName, Constants.RSIPriceWithTimeFrameList, c.GetPriceWithTimeFrameList(stockName, frequency, timeFrame, profitTargetLookupPeriod), parameters);
        //    }
        //    catch (Exception ex)
        //    {

        //        throw new Exception("Error on GetPriceWithTimeFrameList: " + ex.Message);
        //    }
            
        //}

        //private void BuyOrSellStock(string stockName, string orderType, double price, double lots, double stopLoss, double profitTarget)
        //{
        //    FMServiceReference.StockServiceClient c = new FMServiceReference.StockServiceClient();
        //    FMServiceReference.OrderTO oTO = new FMServiceReference.OrderTO();
        //    oTO.StockName = stockName;
        //    oTO.Price = price;
        //    oTO.OrderType = orderType;
        //    oTO.LotSize = (int)lots;
        //    c.InsertOrder(oTO);
        //}

        //public void BuyOrder(String stockName, double price, int lots, double stopLoss, double profitTarget)
        //{

        //    try
        //    {
        //        FileManager fm = FileManager.Instance;
        //        fm.WriteToAFile(String.Format("{0},{1},{2},buy", stockName, price, lots), TechniStockfilePath);
        //        BuyOrSellStock(stockName, "Buy", price, lots, stopLoss, profitTarget);
        //    }
        //    catch (Exception ex)
        //    {

        //        throw new Exception("Unable to buy order:" + ex.Message);
        //    }

        //}

        //public void SellOrder(String stockName, double price, int lots, String status)
        //{
        //    FileManager fm = FileManager.Instance;
        //    fm.WriteToAFile(String.Format("{0},{1},sell", stockName, price), TechniStockfilePath);
        //    UpdateOrderStatus(stockName, status);
        //}

        //public void UpdateQuotationScreenStatus(String stockName, String status)
        //{
        //    FMServiceReference.StockServiceClient c = new FMServiceReference.StockServiceClient();

        //    c.UpdateQuotationScreenStatus(stockName, status);

        //}
    
        public void UpdateOrderStatus(String stockName, String status)
        {
            //FMServiceReference.StockServiceClient c = new FMServiceReference.StockServiceClient();
            //c.UpdateOrderStatus(stockName, this.GetType().ToString(), status);
        }

        //public String CheckQuotationOrderStatus(String stockName)
        //{
        //    FMServiceReference.StockServiceClient c = new FMServiceReference.StockServiceClient();
        //    String status = c.CheckOrderStatusFromTechnistock(stockName);
        //    return status;
        //}
        //public String GetOrderStatus(String stockName)
        //{
        //    FMServiceReference.StockServiceClient c = new FMServiceReference.StockServiceClient();
        //    String status = c.GetOrderStatus(String.Format("{0},{1}", stockName, this.GetType()));
        //    return status;
        //}

    }
}
