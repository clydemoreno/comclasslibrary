﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using MySqlDb.FirstMetroATS;
using System.Configuration;

namespace COMClassLibrary.Manager
{
    public class FileManager
    {
        private static volatile FileManager instance;
        private static object syncRoot = new Object();
        private String data;
        FileSystemWatcher watcher = null;
        public String Data
        {
            get { return data; }
            set { data = value; }
        }
        private FileManager() {

            //CreateWatcher(path);
        }
        public void CreateFileSystemWatcher(String fileSystemPath)
        {
            //String path = @"C:\Users\clyde.moreno\Downloads\dropfolder";
           
        }
        public void AppendToFile(String lineEntry)
        {
            string logFilePath = ConfigurationManager.AppSettings["logFilePath"].ToString();
            AppendToFile(logFilePath, lineEntry);
        }
        public void AppendToFile(String logFilePath, String lineEntry)
        {
            lock (syncRoot)
            {
               
                //string path = Environment.ExpandEnvironmentVariables(@"%userprofile%\Downloads\marketmaker.log");
                string path = Environment.ExpandEnvironmentVariables(@logFilePath);
                
                if (!File.Exists(path))
                {
                    using (File.Create(path))
                    {
                    }
                }
                using (FileStream f = new FileStream(path, FileMode.Append))
                {
                    using (StreamWriter s = new StreamWriter(f))
                    {
                        s.WriteLine(lineEntry);

                    }
                }
            }

            
        }
        public void WriteToAFile(String lineEntry, String path)
        {
            //using (FileStream f = new FileStream(path,FileMode.Create)){
              
            //}

            //using (StreamWriter writer = new StreamWriter(@path, false))
            //{
            //    writer.WriteLine(lineEntry);
            //    writer.Close();
            //}
            File.WriteAllText(path, lineEntry);
        }
        public static FileManager Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new FileManager();
                    }
                }

                return instance;
            }
        }
        public void CreateWatcher(string path, string filter)
        {
            //Create a new FileSystemWatcher.
            watcher = new FileSystemWatcher();
            //Set the filter to only catch TXT files.
            watcher.Filter = "*.csv";
            watcher.Filter = filter;

            //Subscribe to the Created event.
            watcher.Created += new
            FileSystemEventHandler(watcher_FileCreated);

            //Set the path to C:\Temp\
            watcher.Path = @path;

            //Enable the FileSystemWatcher events.
            watcher.EnableRaisingEvents = true;
        }
        public void watcher_FileCreated(object sender, FileSystemEventArgs e)
        {
            //A new .TXT file has been created in C:\Temp\
            System.Diagnostics.Debug.WriteLine("A new *.txt file has been created!");

            data = e.Name;
            GetFile(e.FullPath,e.Name);
        }
        public string[] getFileContents(String path)
        {
            return System.IO.File.ReadAllLines(@path);
        }

        
        public bool SearchFileContents(String path,String text)
        {
            bool returnValue = false;
            string[] lines = System.IO.File.ReadAllLines(@path);

            if (lines.Length >= 0)
            {
             if (lines[lines.Length - 1].IndexOfAny(text.ToCharArray()) > -1)
             {
                 
                 returnValue = true;
             }  
            }
            //foreach (String line in lines)
            //{
            //    if (line.IndexOfAny(text.ToCharArray()) > 0)
            //    {
            //        returnValue = true;
            //    }
            //}
            return returnValue; 
        }
        public void GetFile(String path, String name)
        {
            string[] lines = System.IO.File.ReadAllLines(@path);
            string[] items = null;
            // Display the file contents by using a foreach loop.
            System.Console.WriteLine("Contents of WriteLines2.txt = ");

            if (name == "orderscreen.csv")
            {
                //parse
            }
            else if (name == "quotationscreen.csv")
            {
                //parse
                //for (int i = 1; i < lines.Length; i++)
                //{
                //    // Use a tab to indent each line of the file.
                //    Console.WriteLine("\t" + lines[i]);
                //    items = lines[i].Split(',');

                //    SaveToDB(items);
                //}
                //temp 
                //QuotationScreenParser parser = new QuotationScreenParser();
                //parser.ParseQuotationScreenAsync(path);

            }
            
        }

        public void SaveToDB(string[] items)
        {
            //string stockName = items[0];
            //double last = Convert.ToDouble(items[1]);
            //double bid = Convert.ToDouble(items[2]);
            //double ask = Convert.ToDouble(items[3]);
            //double volume = Convert.ToDouble(items[9]);
            //double high = Convert.ToDouble(items[7]);
            //double low = Convert.ToDouble(items[8]);
            //DateTime date = Convert.ToDateTime(items[9]);
            ////os.updated_on = DateTime.Now;
            //QuotationScreen os = QuotationScreen.GetInstance();
            //os.InsertStockData(stockName,last, bid, ask, high, low, volume,date);
        }
    }
}
