﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Text.RegularExpressions;

namespace COMClassLibrary.Manager
{
    public class iNAVController
    {
        public static double GetPrice(String iNAVURL, String pattern)
        {
           
            // = 

            // String iNAVURL =   @"http://www.fmic.mdgms.com/iopv/quote.php";
            
            String responseString = "";
            using (var wb = new WebClient())
            {
                var response = wb.DownloadString(iNAVURL);
                responseString = response;
            }


            double price = 0;
            //pattern = @"<td\b[^>]*?>(?<V>[\s\S]*?)</\s*td>";
            foreach (Match match in Regex.Matches(responseString, pattern, RegexOptions.IgnoreCase))
            {
                string value = match.Groups["V"].Value;

                if (Double.TryParse(value, out price))
                {

                    Console.WriteLine(value);
                }


            }
            return price;
        }
    }
}
