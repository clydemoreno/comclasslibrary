﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMClassLibrary.Manager
{
    public class TimeFrameManager
    {
        private static readonly object syncLock = new object();
        private DateTime GetLastInsertedDateFromDB(string stockName, string timeFrame, int frequency, int hoursBack)
        {
            //temp code;
            //FMServiceReference.StockServiceClient c = new FMServiceReference.StockServiceClient();
            //return c.GetLastInsertedDate(stockName, frequency, timeFrame);
            
            return DateTime.Now.AddHours(-hoursBack);
        }
        public void SaveTimeFrame(String stockName, double price, String timeFrame, int frequency, int volume, DateTime currentTime, double ask, double askSize, double bid, double bidSize,double open)
        {
            // TaskA is a top level task.
            Task taskA = Task.Factory.StartNew(() =>
            {
                FMServiceReference.StockServiceClient c = new FMServiceReference.StockServiceClient();
                try
                {
                    DateTime? myDate = null;
                    if (timeFrame == "MIN")
                    {
                        myDate = GetModuloMinute(frequency, currentTime);
                    }
                    else if (timeFrame == "SEC")
                    {
                        myDate = currentTime;
                    }

                    if (myDate != null)
                    {
                        c.InsertPriceWithTimeFrame(stockName, price, frequency, timeFrame, (DateTime)myDate, volume,ask,askSize,bid,bidSize,open);
                    }
                }
                catch (Exception ex)
                {
                    
                }
                finally
                {
                    if (c != null)

                        c.Close();
                }
                
            });


        }

        public  FMServiceReference.PriceWithTimeFrameTO[]  GetTimeFrame(String stockName, String timeFrame, int frequency, int period, DateTime cutoffDate)
        {
            FMServiceReference.PriceWithTimeFrameTO[] list = null;

            FMServiceReference.StockServiceClient c = new FMServiceReference.StockServiceClient();
            //  PriceWithTimeFrameDbManager m = new PriceWithTimeFrameDbManager();
            try
            {

                list = c.GetTimeFrameDescending(stockName, frequency, timeFrame, period, cutoffDate);
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry(Constants.FMSecSource, ex.Message, EventLogEntryType.Error);
            }
            //                Task<FMServiceReference.PriceWithTimeFrameTO[]> task = Task.Run(() => c.GetTimeFrameDescending(stockName, frequency, timeFrame, period, cutoffDate));
            //    list = await task;
            finally
            {
                c.Close();
            }
                return list;
        }

        public void SimulateRandomPriceForAPeriod(String stockName, double price,  String timeFrame, int frequency, int hoursBack)
        {
            //todo:
            //get last inserted date pattern of that timeframe,frequency,period
            //convert to date

            DateTime currentTime = DateTime.Now;
            //4:27
            DateTime? myDate = GetModuloMinute(frequency, currentTime);


            TimeSpan timeSpan = currentTime - GetLastInsertedDateFromDB(stockName, timeFrame, frequency, hoursBack);

            Console.WriteLine("Total Minutes:{0}", timeSpan.TotalMinutes);

            // if true, means that date now is with the frequency for example: 4:08 is current time
            // and no entry yet or 4:05 is latest
            if (Math.Abs( timeSpan.TotalMinutes) < frequency) 
            {
                //InsertPriceTimeFrame(stockName, price, frequency, timeFrame, myDate);
            }
            else
            {
                if(myDate != null)
                GenerateTimePattern((DateTime)myDate, timeSpan, frequency,stockName,timeFrame);
            }
            
            //

        }

        public static DateTime? GetModuloMinute(int frequency, DateTime currentTime)
        {
            if (frequency > 1440) throw new Exception("Program doesn't support frequency higher than 1440");
            DateTime? myDate = null;
            int currentMinutes = 0;
            if (frequency == 1440)
            {
                myDate = new DateTime(currentTime.Year, currentTime.Month, currentTime.Day, 0, 0, 0, 0);

                return myDate;
            }

            double factor = 0;
            if (frequency > 60)
            {
                factor = Math.Floor(Convert.ToDouble(frequency) / 60);
                //factor = 
                double remainder = currentTime.Hour % factor;
                if (remainder > 0)
                {
                    int hour = currentTime.Hour - Convert.ToInt32(remainder);
                    currentTime = currentTime.AddHours(-remainder);
                }
                myDate = new DateTime(currentTime.Year, currentTime.Month, currentTime.Day, currentTime.Hour, 0, 0, 0);
                return myDate;

            }
            else
            {
                currentMinutes = currentTime.Minute;
            }

           
            
            
            int divisor = frequency;
            if (frequency > 0)
            {
                int x = currentMinutes % divisor;


                myDate = new DateTime(currentTime.Year, currentTime.Month, currentTime.Day, currentTime.Hour, currentMinutes - x, 0, 0);


            }

      
            return   myDate;
        }
        private String InsertPriceTimeFrame(String stockName, double price, int frequency, string timeFrame, DateTime date, int volume,double ask,double askSize,double bid,double bidSize,double open)
        {
            string returnValue = string.Empty;
            FMServiceReference.StockServiceClient client = new FMServiceReference.StockServiceClient();
            try
            {
                // TaskA is a top level task.
                Task taskA = Task.Factory.StartNew(() =>
                {
                    returnValue = client.InsertPriceWithTimeFrame(stockName, price, frequency, timeFrame, date, volume, ask, askSize, bid, bidSize,open);
                    client.Close();
                    
                });


            }
            catch (Exception ex)
            {
                returnValue = ex.Message;
            }

            return returnValue;

           
        }

        public int RandomizeInteger(int start, int end)
        {
            int i = 0;
            Random r = new Random();
            lock (syncLock)
            {
                i = r.Next(start, end);
               
            }
            return i;
        }

        public int RandomizeInteger(int price)
        {
            double tempPrice = price;
            Random r = new Random();
            lock (syncLock)
            {
                int i =  r.Next(Convert.ToInt32(-tempPrice * 0.1), Convert.ToInt32(tempPrice * .1));
                //price += i==0? i * -1 : i;
                price += (int)(i);
                //if (tempPrice == price)
                //    price--;
            }
            return price;
        }

        public double Randomize(double price)
        {
            double tempPrice = price;
            Random r = new Random();
            lock (syncLock)
            {
                int i = r.Next(-30, 30);
                //price += i==0? i * -1 : i;
                price += (i/27.3);
                //if (tempPrice == price)
                //    price--;
            }
            return price;
        }

        public void GenerateTimePattern(DateTime endTime, TimeSpan timeSpan, int frequency, String stockName, String timeFrame)
        {

            int period = Convert.ToInt32(timeSpan.TotalMinutes / frequency);

            if (period > 0)
            {


                double price = 100.00;
                for (int i = 0; i < period; i++)
                {
                    price = Randomize(price);
                    DateTime date = endTime.AddMinutes(-(i * frequency));

                    //InsertPriceTimeFrame(stockName, price, frequency, timeFrame, date);
                    Console.WriteLine(@"{0:d/M/yyyy HH:mm:00} {1}", date,price);
                }


            }
          


        }
    }
}
