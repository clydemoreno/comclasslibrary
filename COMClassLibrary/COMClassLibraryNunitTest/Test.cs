﻿using NUnit.Framework;
using System;
using COMClassLibrary.Manager;
namespace COMClassLibraryNunitTest
{
    [TestFixture()]
    public class Test
    {
        [Test()]
        public void TestCase()
        {
            StockParser parser = new COMClassLibrary.Manager.StockParser();
            Security security = parser.Request();
            Assert.True(security.Asks[0].price > 0); 
            Assert.True(security.Bids[0].price > 0);

            Assert.True(security.Asks[0].volume > 0);
            Assert.True(security.Bids[0].volume > 0);

        }
    }
}
