﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.Configuration;

namespace EmailAgentApp
{
    class Utility
    {
         
        public static void SendEmail(string subject, string body, string email, string fromAddress, string cc, string pathAndFileName)
        {
            string str = "";
            try
            {
                MailMessage message = new MailMessage();
                message.IsBodyHtml = true;
                foreach (string addresses in email.Split(";".ToCharArray()))
                    message.To.Add(addresses);
                if (!string.IsNullOrEmpty(cc))
                {
                    foreach (string addresses in cc.Split(";".ToCharArray()))
                        message.CC.Add(addresses);
                }
                message.From = new MailAddress(fromAddress);
                message.Body = body;
                message.Subject = subject;
                if (!String.IsNullOrEmpty(pathAndFileName))
                {
                    //todo:  loop thru attachments separated by ;
                    String[] list = pathAndFileName.Split(";".ToCharArray());
                    Attachment attachement = null;
                    foreach (string item in list)
                    {
                        attachement = new Attachment(item);
                        message.Attachments.Add(attachement);

                    }
                }
                new SmtpClient()
                {
                    
                    Host = ((object)ConfigurationManager.AppSettings["smtpServer"]).ToString()
                }.Send(message);
                str = "Success sending: " + subject;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Unable to send email Error:{0} from: {1} to: {2} ", (object)ex.Message, (object)fromAddress, (object)email));
            }
        }

    }
}
