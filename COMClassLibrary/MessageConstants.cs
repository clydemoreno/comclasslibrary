﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace COMClassLibrary
{
    public partial class Constants
    {
        public static readonly String RSIMessage = "RSIMessage";
        //public static readonly String MyRSIMessageCode = "MyRSIMessageCode";

        public static readonly String IntradayMessage = "IntradayMessage";
        //public static readonly String MyIntradayMessageCode = "MyIntradayMessageCode";

        public static readonly String RecogniaMessage = "RecogniaMessage";

        public static readonly String MarketMakerMessage = "MarketMakerMessage";
        //public static readonly String MarketMakerMessageCode = "MarketMakerErrorCode";
        //public static readonly String MyMarketMakerErrorCode = "MyMarketMakerErrorCode";
    }
}
