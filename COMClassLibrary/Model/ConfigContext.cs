﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace COMClassLibrary.Model
{
    [ClassInterface(ClassInterfaceType.AutoDual)]
    public class ConfigContext
    {
        private int capitalDivisor;

        public int CapitalDivisor
        {
            get { return capitalDivisor; }
            set { capitalDivisor = value; }
        }
        private double capital;

        public double Capital
        {
            get { return capital; }
            set { capital = value; }
        }
        private string configurationString;

        public string ConfigurationString
        {
            get { return configurationString; }
            set { configurationString = value; }
        }
        private double capitalMultiplier;

        public double CapitalMultiplier
        {
            get { return capitalMultiplier; }
            set { capitalMultiplier = value; }
        }
        private double baseLotSize;

        public double BaseLotSize
        {
            get { return baseLotSize; }
            set { baseLotSize = value; }
        }
        private double baseLotSizeMultiplier;

        public double BaseLotSizeMultiplier
        {
            get { return baseLotSizeMultiplier; }
            set { baseLotSizeMultiplier = value; }
        }

    }
}
