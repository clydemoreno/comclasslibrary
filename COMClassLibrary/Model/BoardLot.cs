﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace COMClassLibrary.Model
{
    public class BoardLot
    {
        double startingPrice;

        public double StartingPrice
        {
            get { return startingPrice; }
            set { startingPrice = value; }
        }
        double endingPrice;

        public double EndingPrice
        {
            get { return endingPrice; }
            set { endingPrice = value; }
        }
        double fluctuation;

        public double Fluctuation
        {
            get { return fluctuation; }
            set { fluctuation = value; }
        }
        double lot;

        public double Lot
        {
            get { return lot; }
            set { lot = value; }
        }
    }
}
