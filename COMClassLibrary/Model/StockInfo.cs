﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMClassLibrary.Model
{
    public class StockInfo
    {
        private double open;

        public double Open
        {
            get { return open; }
            set { open = value; }
        }
        private double currentPrice;

        public double CurrentPrice
        {
            get { return currentPrice; }
            set { currentPrice = value; }
        }
    }
}
