﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using COMClassLibrary.FMServiceReference;
using COMClassLibrary.Manager;
using FirstMetroSystemTrayApp.Model;

namespace COMClassLibrary.Model
{
    public class MatchingEngineParams
    {

        public string BuildTextForImportedOrderScreenList(List<ImportedOrderScreen> importedOrderScreenList)
        {
            String text = "Account ID = {0}\r\nStock = {1}\r\nSide = {2}\r\nPrice {3}= {4}\r\n";
            foreach (ImportedOrderScreen o in importedOrderScreenList)
            {
                if (o.action == "Cancel")
                {
                    text = String.Format(text,  o.side == "Buy" ? this.buyer: this.seller, o.stock, o.side, o.side == "Buy" ? ">" : "<", o.price);
                    break;
                }
            }
            return text;
        }


        private String accountID;

        public String AccountID
        {
            get { return accountID; }
            set { accountID = value; }
        }

        private int bidVolume;

        public int BidVolume
        {
            get { return bidVolume; }
            set { bidVolume = value; }
        }
        private int askVolume;

        public int AskVolume
        {
            get { return askVolume; }
            set { askVolume = value; }
        }

        private String cancelFilePath;

        public String CancelFilePath
        {
            get { return cancelFilePath; }
            set { cancelFilePath = value; }
        }


        //todo: find out where the filename is being used.
        private String filePath;

        public String FilePath
        {
            get { return filePath; }
            set { filePath = value; }
        }

        private double bid;

        public double Bid
        {
            get { return bid; }
            set { bid = value; }
        }
        private double offer;

        public double Offer
        {
            get { return offer; }
            set { offer = value; }
        }

        private CapitalCalculator calculator;

        public CapitalCalculator Calculator
        {
            get {
                if (calculator == null)
                    calculator = new CapitalCalculator();
                return 
                    calculator; 
            }

        }
        
        
        private double bidFlucs = 1;

        public double BidFlucs
        {
            get { return bidFlucs; }
            set { bidFlucs = value; }
        }

        private double offerFlucs = 1;

        public double OfferFlucs
        {
            get { return offerFlucs; }
            set { offerFlucs = value; }
        }


        private double iNAV;

        public double INAV
        {
            get { 
                return 
                iNAV; 
            }
            set { iNAV = value; }
        }
        private double skewView;

        public double SkewView
        {
            get { return skewView; }
            set { skewView = value; }
        }
        private double skewNAV;

        public double RoundToFlucs(double price)
        {
            BoardLotFluctuationPriceTO lot = Calculator.GetBoardLotBasedOnPrice(price);
            int count = BitConverter.GetBytes(Decimal.GetBits(Convert.ToDecimal(lot.Fluctuation))[3])[2];
            return Math.Round(price, count); 
            //return price;
        }


        public double SkewNAV
        {
            get {
                skewNAV = skewView + iNAV;
                return skewNAV; 
            }
            //set { skewNAV = value; }
        }
        private double maxSpread;

        public double MaxSpread
        {
            get { return maxSpread; }
            set { maxSpread = value; }
        }


        private double defaBid;

        public double DefaBid
        {
            get
            {
                defaBid = GoDownInFlucsBy(iNAV, Math.Abs( skewView));
                return RoundToFlucs(defaBid);
            }
        }
        public double DefaBidOld
        {
            get {
                lowerHalf = Convert.ToInt32(Math.Floor(maxSpread / 2));
                // lot = Calculator.GetBoardLotBasedOnPrice(SkewNAV);
                defaBid = SkewNAV;
                
                BoardLotFluctuationPriceTO boardLot = null;
                double difference = Math.Abs(defaBid * .00000001);
                for (int i = 1; i <= lowerHalf; i++)
                {
                    boardLot = Calculator.GetBoardLotBasedOnPrice(defaBid);
                    if (boardLot == null) throw new Exception(String.Format("Value of Default Bid: {0} did not get any matching Board Lot", defaBid));
                    //close. try to use the price and let it go down
                    if (Math.Abs(defaBid - boardLot.StartPrice) <= difference)
                    {
                        boardLot = Calculator.GetBoardLotBasedOnPrice(defaBid - boardLot.Fluctuation);
                    }
                    defaBid = defaBid + (boardLot.Fluctuation * -1);
                }
                //defaBid = skewNAV;
                //defaBid = -1 * (lowerHalf * bidFlucs ) + SkewNAV;
                return RoundToFlucs(defaBid); 
            }
            //set { defaBid = value; }
        }

        private int lowerHalf;
        private int higherHalf;


        private double defaOffer;

        public double DefaOffer
        {
            get
            {
                defaOffer = GoUpInFlucsBy(DefaBid, maxSpread);
                return RoundToFlucs(defaOffer);
            }
        }

        public double DefaOfferOld
        {
            get {


                higherHalf = Convert.ToInt32(Math.Ceiling(maxSpread / 2));

                BoardLotFluctuationPriceTO boardLot = null;

                defaOffer = SkewNAV;

                double difference = Math.Abs(defaBid * .00000001);
                for (int i = 1; i <= higherHalf; i++)
                {
                    boardLot = Calculator.GetBoardLotBasedOnPrice(defaOffer);
                    if (boardLot == null) throw new Exception(String.Format("Value of Default Offer: {0} did not get any matching Board Lot"
                        , defaOffer));
                    //no need to handle the boundaries because the flucs flow is going up
                    //if (Math.Round(defaBid,10) == Math.Round(boardLot.StartPrice,10))
                    //{
                    //    boardLot = Calculator.GetBoardLotBasedOnPrice(defaOffer - boardLot.Fluctuation);
                    //}
                    defaOffer = defaOffer + (boardLot.Fluctuation);
                }

                
                //double defaOffer = (Math.Ceiling(maxSpread / 2) * offerFlucs ) + SkewNAV;

                return RoundToFlucs(defaOffer); 
            }
            //set { defaOffer = value; }
        }


        private int GetPostedBidVolume()
        {


            if (bidImportedOrderScreenList == null) return 0;
            foreach (ImportedOrderScreen i in BidImportedOrderScreenList)
            {
                
            }
            return 0;
        }

        private double adjustedBidVolume;

        public double AdjustedBidVolume
        {
            get { return adjustedBidVolume; }
            set { adjustedBidVolume = value; }
        }
        private double adjustedOfferVolume;

        public double AdjustedOfferVolume
        {
            get { return adjustedOfferVolume; }
            set { adjustedOfferVolume = value; }
        }

        private double bestBid;

        double bidBalance;

        public double BidBalance
        {
            get { return bidBalance; }
            set { bidBalance = value; }
        }

        public double BestBid
        {
            get {
                bestBid = Bid;
                if (bidImportedOrderScreenList == null) return bestBid;

                var bidList = 
                    from list in bidImportedOrderScreenList where list.price == Bid 
                      select list;

                if (bidList.Count() > 2) throw new Exception("The price is not aggregate");
                if (bidList.Count() == 1)
                {
                    
                    ImportedOrderScreen importedOrderScreen =  bidList.FirstOrDefault<ImportedOrderScreen>();
                    if (importedOrderScreen != null)
                    {
                        //todo:  find out how to fix this.
                        //bidBalance = this.MarketBidVolume - importedOrderScreen.pending;
                        //balance is greater fire an events.
                        
                        if (bidBalance > minPostVolume)
                        {
                            bestBid = importedOrderScreen.price;
                        }
                        else if (bidBalance == 0)
                        {
                            //essentially, there is no bid or too low to make any effect
                            bestBid = 0;
                        }
                        else if (bidBalance < 0)
                        {
                            //balance is negative, fire another revent
                        }

                    }
                }
                return bestBid; 
            }
            //set { bestBid = value; }
        }

        double offerBalance;

        public double OfferBalance
        {
            get { return offerBalance; }
            set { offerBalance = value; }
        }

        private double bestOffer;

        public double BestOffer
        {
            get {
                bestOffer = Offer;
                if (askImportedOrderScreenList == null) return bestOffer;
                var offerList =
                    from list in askImportedOrderScreenList
                    where list.price == offer
                    select list;

                if (offerList.Count() > 2) throw new Exception("The price is not aggregate");
                if (offerList.Count() == 1)
                {

                    ImportedOrderScreen importedOrderScreen = offerList.FirstOrDefault<ImportedOrderScreen>();
                    if (importedOrderScreen != null)
                    {
                        //offerBalance = importedOrderScreen.pending - this.MarketOfferVolume;
                        //if (balance > minPostVolume)
                        //{
                        //    bestOffer = importedOrderScreen.price;
                        //}
                        if (offerBalance > minPostVolume)
                        {
                            bestOffer = importedOrderScreen.price;
                        }
                        else if (offerBalance == 0)
                        {
                            //essentially, there is no bid or too low to make any effect
                            bestOffer = 999999.00;
                        }
                        else if (offerBalance < 0)
                        {
                            //balance is negative, fire another revent
                        }
                    }
                }
                return bestOffer; 
            }
            //set { bestOffer = value; }
        }
        private double spread;

        public double Spread
        {
            get {
                spread = BestOffer - BestBid;
                return spread; 
            }
            //set { spread = value; }
        }
        private double minPostVolume;

        public double MinPostVolume
        {
            get { return minPostVolume; }
            set { minPostVolume = value; }
        }
        private String buyer;

        public String Buyer
        {
            get { return buyer; }
            set { buyer = value; }
        }
        private String seller;

        public String Seller
        {
            get { return seller; }
            set { seller = value; }
        }


        private double adjustedOfferWithSpread;

        public double AdjustedOfferWithSpread
        {
            get { return adjustedOfferWithSpread; }
            //set { adjustedOfferWithSpread = value; }
        }

        private double adjustedBidWithSpread;

        public double AdjustedBidWithSpread
        {
            get { return adjustedBidWithSpread; }
            //set { adjustedBidWithSpread = value; }
        }


        public double GoDownInFlucsBy(double price, double count)
        {
            BoardLotFluctuationPriceTO boardLot;
            double tempPrice = price;
            for (int i = 1; i <= count; i++)
            {
                boardLot = Calculator.GetBoardLotBasedOnPrice(tempPrice);
                if (boardLot == null) throw new Exception(String.Format("Value of Pirce: {0} did not get any matching Board Lot"
                    , tempPrice));
                //no need to handle the boundaries because the flucs flow is going up
                if (Math.Round(tempPrice, 10) == Math.Round(boardLot.StartPrice, 10))
                {
                    boardLot = Calculator.GetBoardLotBasedOnPrice(tempPrice - boardLot.Fluctuation);
                }
                tempPrice = tempPrice + (boardLot.Fluctuation) * -1;
            }
            return tempPrice;
        }

        public double GoUpInFlucsBy(double price, double count)
        {
            BoardLotFluctuationPriceTO boardLot;
            double tempPrice = price;
            for (int i = 1; i <= count; i++)
            {
                boardLot = Calculator.GetBoardLotBasedOnPrice(tempPrice);
                if (boardLot == null) throw new Exception(String.Format("Value of Pirce: {0} did not get any matching Board Lot"
                    , tempPrice));
                //no need to handle the boundaries because the flucs flow is going up
               
                tempPrice = tempPrice + (boardLot.Fluctuation) * 1;
            }
            return tempPrice;
        }


        private double mmBid;

        public double MMBid
        {
            get {
                mmBid = DefaBid;

                if (BestBid > DefaBid)
                {
                    mmBid = DefaBid;
                }
                else 
                {

                    if (BestOffer < DefaOffer)
                    {

                        BoardLotFluctuationPriceTO boardLot = null;
                        double tempDefaOffer = BestOffer;
                       
                        for (int i = 1; i <= MaxSpread; i++)
                        {
                            boardLot = Calculator.GetBoardLotBasedOnPrice(tempDefaOffer);
                            if (boardLot == null) throw new Exception(String.Format("Value of Default Offer: {0} did not get any matching Board Lot"
                                , tempDefaOffer));
                            //no need to handle the boundaries because the flucs flow is going up
                            if (Math.Round(tempDefaOffer, 10) == Math.Round(boardLot.StartPrice, 10))
                            {
                                boardLot = Calculator.GetBoardLotBasedOnPrice(tempDefaOffer - boardLot.Fluctuation);
                            }
                            tempDefaOffer = tempDefaOffer + (boardLot.Fluctuation) * -1;
                        }

                        mmBid = tempDefaOffer;


                        //mmBid = BestOffer - MaxSpread;
                        //for (int i = 1; i <= MaxSpread; i++)
                        //{
                        //    boardLot = Calculator.GetBoardLotBasedOnPrice(mmBid);
                        //    mmBid = BestOffer + (boardLot.Fluctuation * -i);
                        //}
 
                    }
                }
                //check if in this price level, there is a remainder grated than the minvolume
                //if (PostedBidVolume - MarketBidVolume >= MinPostVolume)
                //{
                //    mmBid = PriceWithRemainderBidVolume;
                //}
                //adjustedOfferWithSpread = mmBid + (maxSpread * offerFlucs);
                return RoundToFlucs(mmBid); 
            }
            //set { mmBid = value; }
        }
        private double mmOffer;

        public double MMOffer
        {
            get {
                mmOffer = DefaOffer;

                if (BestOffer < DefaOffer)
                {
                    mmOffer = DefaOffer;
                }

                else
                {

                    if (BestBid > DefaBid)
                    {

                        BoardLotFluctuationPriceTO boardLot = null;
                        double tempDefaBid = BestBid;

                        for (int i = 1; i <= MaxSpread; i++)
                        {
                            boardLot = Calculator.GetBoardLotBasedOnPrice(tempDefaBid);
                            if (boardLot == null) throw new Exception(String.Format("Value of Default Offer: {0} did not get any matching Board Lot"
                                , tempDefaBid));
                            //no need to handle the boundaries because the flucs flow is going up
                            //if (Math.Round(tempDefaOffer, 10) == Math.Round(boardLot.StartPrice, 10))
                            //{
                            //    boardLot = Calculator.GetBoardLotBasedOnPrice(tempDefaOffer - boardLot.Fluctuation);
                            //}
                            tempDefaBid = tempDefaBid + (boardLot.Fluctuation) * 1;
                        }

                        mmOffer = tempDefaBid;

                    }
                }

                //check if in this price level, there is a remainder grated than the minvolume
                //find out if this is actually needed. it overrides the mmoffer and reset the adjustedbid

                //if (PostedOfferVolume - MarketOfferVolume >= MinPostVolume)
                //{
                //    mmOffer = PriceWithRemainderOfferVolume;
                //}
                //adjustedBidWithSpread = mmOffer - (maxSpread * offerFlucs);
                return RoundToFlucs(mmOffer); 
            }
            //set { mmOffer = value; }
        }
        private int marketBidVolume;

        public int MarketBidVolume
        {
            get { return marketBidVolume; }
            set { marketBidVolume = value; }
        }
        private int postedBidVolume;

        public int PostedBidVolume
        {
            get { return postedBidVolume; }
            set { postedBidVolume = value; }
        }

        private int marketOfferVolume;

        public int MarketOfferVolume
        {
            get { return marketOfferVolume; }
            set { marketOfferVolume = value; }
        }
        private int postedOfferVolume;

        public int PostedOfferVolume
        {
            get { return postedOfferVolume; }
            set { postedOfferVolume = value; }
        }

        private double priceWithRemainderBidVolume;

        public double PriceWithRemainderBidVolume
        {
            get { return priceWithRemainderBidVolume; }
            set { priceWithRemainderBidVolume = value; }
        }

        private double priceWithRemainderOfferVolume;

        public double PriceWithRemainderOfferVolume
        {
            get { return priceWithRemainderOfferVolume; }
            set { priceWithRemainderOfferVolume = value; }
        }

        private List<ImportedOrderScreen> bidImportedOrderScreenList;

        public List<ImportedOrderScreen> BidImportedOrderScreenList
        {
            get { return bidImportedOrderScreenList; }
            set { bidImportedOrderScreenList = value; }
        }
        private List<ImportedOrderScreen> askImportedOrderScreenList;

        public List<ImportedOrderScreen> AskImportedOrderScreenList
        {
            get { return askImportedOrderScreenList; }
            set { askImportedOrderScreenList = value; }
        }

        public bool AreThereAnyBidsToCancel()
        {
            bool isTrue = false;
            if (bidImportedOrderScreenList == null) return isTrue;

            foreach (ImportedOrderScreen order in bidImportedOrderScreenList)
            {
                if (order.action == "Cancel")
                {
                    isTrue = true;
                    break;
                }
            }
            return isTrue;
        }

        public bool AreThereAnyOffersToCancel()
        {
            bool isTrue = false;
            if (askImportedOrderScreenList == null) return isTrue;

            foreach (ImportedOrderScreen order in askImportedOrderScreenList)
            {
                if (order.action == "Cancel")
                {
                    isTrue = true;
                    break;
                }
            }
            return isTrue;
        }
   


    }
}
