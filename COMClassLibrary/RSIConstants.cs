﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace COMClassLibrary
{
    public partial class Constants
    {
        public static readonly String RSIStopLossCode = "RSIStopLossCode";
        public static readonly String RSIProfitTargetCode = "RSIProfitTargetCode";
        public static readonly String RSIPurchasePriceCode = "RSIPurchasePriceCode";
        public static readonly String RSILotsCode = "RSIPurchasePriceCode";
        public static readonly String RSIAllocatedCapitalCode = "RSIAllocatedCapitalCode";
        public static readonly String RSIPriceWithTimeFrameList = "RSIPriceWithTimeFrameList";
        public static readonly String IncrementingX = "IncrementingX";
        public static readonly String RSICurrentPriceLowerThanStopLoss = "RSICurrentPriceLowerThanStopLoss";
        public static readonly String RSICurrentPriceHigherThanProfitTarget = "RSICurrentPriceHigherThanProfitTarget";
        public static readonly String RSIOrderTOCode = "RSIOrderTOCode";

        //public static readonly String RSINegativeSlope = "RSINegativeSlope";
        //public static readonly String RSINegativeSlopeMessage = "RSI Linear Regression has negative slope";

    }
}
