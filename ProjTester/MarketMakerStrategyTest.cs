﻿using COMClassLibrary.Manager;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using COMClassLibrary;

namespace ProjTester
{
    
    
    /// <summary>
    ///This is a test class for MarketMakerStrategyTest and is intended
    ///to contain all MarketMakerStrategyTest Unit Tests
    ///</summary>
    [TestClass()]
    public class MarketMakerStrategyTest
    {
        Dictionary<string, object> parameters = null;
        String stockName = "AEV PM";
        MarketMakerStrategy target;


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

     

        [TestInitialize]
        public void Initialize()
        {
            target = new MarketMakerStrategy();
            parameters = new Dictionary<string, object>();
            stockName = "AEV PM";
            target.SetParameters(stockName, "stockName", stockName, parameters);


            target.SetParameters(stockName, "bidSize", 153.23, parameters);
            target.SetParameters(stockName, "bid", 153.23, parameters);
            target.SetParameters(stockName, "askSize", 153.23, parameters);
            target.SetParameters(stockName, "ask", 154.50, parameters);
            target.SetParameters(stockName, "last", 154.00, parameters);
            target.SetParameters(stockName, "period", 14, parameters);


            target.SetParameters(stockName, "startingX", 41, parameters);
            target.SetParameters(stockName, "frequency", 1440, parameters);
            target.SetParameters(stockName, "timeFrame", "MIN", parameters);
            target.SetParameters(stockName, Constants.RSIStatus, Constants.RSIWaitingMode, parameters);
            target.SetParameters(stockName, "requiredDerivative", 0.001, parameters);
            target.SetParameters(stockName, "requiredSeparation", 1.05, parameters);
            target.SetParameters(stockName, "volumePeriod", 14, parameters);
            target.SetParameters(stockName, "requiredVolume", 1.3, parameters);

            target.SetParameters(stockName, "profitTargetLookupPeriod", 10, parameters);
            target.SetParameters(stockName, "stopLossLookupPeriod", 10, parameters);
            //7% loss
            target.SetParameters(stockName, "requiredAbsoluteStopLossFactor", 0.93, parameters);
            target.SetParameters(stockName, "techniStockfilePath", @"C:\temp\technistock\orders.csv", parameters);

            //just pretend we got the right amount
            target.SetParameters(stockName, "capital", 1000000.00, parameters);
            target.SetParameters(stockName, "capitalMultiplier", 0.8, parameters);
            //capital divisor is the baseline or the minimum capital
            target.SetParameters(stockName, "capitalDivisor", 5, parameters);

            //pending or partial.  check if that stock has no pending status. or someone already sent an unfilled order
            //a db call on quotation screen.

            target.SetParameters(stockName, "volume", 15678, parameters);//
            target.SetParameters(stockName, "lineRegressionRSIPeriod", 40, parameters);//
            target.SetParameters(stockName, "rsiPeriod", 14, parameters);//
            target.SetParameters(stockName, "cutoffDate", DateTime.Now, parameters);//

            target.SetParameters(stockName, Constants.iNAVURL,  @"http://www.fmic.mdgms.com/iopv/quote.php", parameters);//
            target.SetParameters(stockName, Constants.iNAVPattern, @"<td\b[^>]*?>(?<V>[\s\S]*?)</\s*td>", parameters);//

            target.SetParameters(stockName, Constants.fromEmail, "clydemoreno@kyoobiq.com", parameters);
            target.SetParameters(stockName, Constants.toEmail, "clydemoreno@mac.com", parameters);


            target.Changed += new ChangedEventHandler(target_Changed);

            

        }

        void target_Changed(object sender, EventArgs e)
        {
            if (e.GetType() == typeof(COMClassLibrary.Manager.MessageEventArg))
            {

                MessageEventArg eventArg = (MessageEventArg)e;

                IMessageHandler handler = eventArg.MessageHandler;
                handler.Handle(parameters, e);


                if (eventArg.Code == Constants.MyRSIErrorCode)
                {
                   String error = (String) target.GetParameter(stockName, Constants.MyRSIErrorCode, parameters);
                   Assert.IsTrue(error == eventArg.Message);
                }
            }

            //if (e.GetType() == typeof(COMClassLibrary.Manager.ErrorEventArg))
            //{
            //    String[] array = ((ErrorEventArg)e).StockName.Split(",".ToCharArray());
            //    Assert.IsTrue(array.Length == 2);
            //    //this.SetParameters(((ErrorEventArg)e).StockName, ((ErrorEventArg)e).Code, ((ErrorEventArg)e).Message);
            //}
        }




        /// <summary>
        ///A test for IsOutsideOfSpread
        ///</summary>
        [TestMethod()]
        public void IsOutsideOfSpreadTest()
        {
            //String iNavURL =   (String)target.GetParameter(stockName, Constants.iNAVURL, parameters);
            //String iNavPattern = (String)target.GetParameter(stockName, Constants.iNavPattern, parameters);

            double iNAVprice = 154.51; //;iNAVController.GetPrice(iNavURL, iNavPattern);
            double bid = 155.56;
            double ask = 155.00;



            target.SetParameters(stockName, "bid", bid, parameters);
            target.SetParameters(stockName, "ask", ask, parameters);
            target.SetParameters(stockName, Constants.iNAV, iNAVprice, parameters);     
     
            bool expected = false; 
            bool actual;
            actual = target.IsOutsideOfSpreadDeprecated(stockName, parameters);

 
        
            
            Assert.AreEqual(expected, actual);
            bid = 154.00;
 
            //iNAVprice = 154.51;
            target.SetParameters(stockName, "bid", bid, parameters);
            target.SetParameters(stockName, "ask", ask, parameters);
            target.SetParameters(stockName, Constants.iNAV, iNAVprice, parameters);

            expected = true; 
            
            actual = target.IsOutsideOfSpreadDeprecated(stockName, parameters);





            Assert.AreEqual(expected, actual);

            new System.Threading.ManualResetEvent(false).WaitOne(5000);
        }

        /// <summary>
        ///A test for Run
        ///</summary>
        [TestMethod()]
        public void RunTest()
        {
            //String iNavURL =   (String)target.GetParameter(stockName, Constants.iNAVURL, parameters);
            //String iNavPattern = (String)target.GetParameter(stockName, Constants.iNavPattern, parameters);



            double bid = 511.20;
            double ask = 154.50;
            // temp only
            bid = 110.45;
            ask = 111.5;

            target.SetParameters(stockName, "bid", bid, parameters);
            target.SetParameters(stockName, "ask", ask, parameters);
         //   target.SetParameters(stockName, Constants.iNAV, iNAVprice, parameters);

            target.Run(stockName, parameters);

            target.SetParameters(stockName, "spreadThreshold", 0.1, parameters);
            target.Run(stockName, parameters);

            new System.Threading.ManualResetEvent(false).WaitOne(5000);
        }

        [TestMethod()]
        public void GetAskBidTest()
        {
            double bid = 95.00;
            double ask = 101.00;
            double spreadThreshold = 3.0;
            double iNavPrice = 100.00;
            double offset = -2;
            int volume = 18500;

            AskBid expectedAskBid = new AskBid();
            expectedAskBid.Ask = 100.8;
            expectedAskBid.Bid = 98.8;

            target.SetParameters(stockName, "bid", bid, parameters);
            target.SetParameters(stockName, "ask", ask, parameters);
            target.SetParameters(stockName, "spreadThreshold", spreadThreshold, parameters);
            target.SetParameters(stockName, "volume", volume, parameters);
            target.SetParameters(stockName, Constants.iNAV, iNavPrice, parameters);
            target.SetParameters(stockName, Constants.MarketMakerOffSet, offset, parameters);

            AskBid actualAskBid = target.GetAskBid(stockName,parameters);
            Assert.IsTrue(actualAskBid.Ask == expectedAskBid.Ask && actualAskBid.Bid == expectedAskBid.Bid);

            bid = 95.00;
            ask = 101.00;
            spreadThreshold = 3.0;
            iNavPrice = 100.00;
            offset = -2;

            expectedAskBid.Ask = 100;
            expectedAskBid.Bid = 97;

            target.SetParameters(stockName, "bid", bid, parameters);
            target.SetParameters(stockName, "ask", ask, parameters);
            target.SetParameters(stockName, "spreadThreshold", spreadThreshold, parameters);
            target.SetParameters(stockName, Constants.iNAV, iNavPrice, parameters);
            target.SetParameters(stockName, Constants.MarketMakerOffSet, offset, parameters);

            actualAskBid = target.GetAskBid(stockName, parameters);
            Assert.IsTrue(actualAskBid.Ask == expectedAskBid.Ask && actualAskBid.Bid == expectedAskBid.Bid); 
         
        }

        /// <summary>
        ///A test for IsThreshholdOfSpreadBreached
        ///</summary>
        [TestMethod()]
        public void IsThreshholdOfSpreadBreachedTest()
        {
            double bid = 511.00;
            double ask = 520.50;
            double spreadThreshold = 10.0;

            target.SetParameters(stockName, "bid", bid, parameters);
            target.SetParameters(stockName, "ask", ask, parameters);
            target.SetParameters(stockName, "spreadThreshold", spreadThreshold, parameters);
 
            bool expected = true; // TODO: Initialize to an appropriate value
            bool actual;
            actual = target.IsThreshholdOfSpreadBreached(stockName, parameters);
            Assert.AreEqual(expected, actual);

            bid = 110.00;
            ask = 115.00;

            target.SetParameters(stockName, "bid", bid, parameters);
            target.SetParameters(stockName, "ask", ask, parameters);

            expected = true; // TODO: Initialize to an appropriate value
            actual = target.IsThreshholdOfSpreadBreached(stockName, parameters);
            Assert.AreEqual(expected, actual);

            bid = 110.00;
            ask = 110.10;

            target.SetParameters(stockName, "bid", bid, parameters);
            target.SetParameters(stockName, "ask", ask, parameters);

            expected = false; // TODO: Initialize to an appropriate value
            actual = target.IsThreshholdOfSpreadBreached(stockName, parameters);
            Assert.AreEqual(expected, actual);

        }
    }
}
