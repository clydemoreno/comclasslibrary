﻿using COMClassLibrary.Manager;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using COMClassLibrary;
using COMClassLibrary.FMServiceReference;

namespace ProjTester
{
    
    
    /// <summary>
    ///This is a test class for IntradayStrategyTest and is intended
    ///to contain all IntradayStrategyTest Unit Tests
    ///</summary>
    [TestClass()]
    public class IntradayStrategyTest
    {
        Dictionary<string, object> parameters = null;
        String stockName = "AEV PM";
        IntradayStrategy target;

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        [TestInitialize]
        public void Initialize()
        {

            target = new IntradayStrategy();
            parameters = new Dictionary<string, object>();
            stockName = "AEV PM";

            target.SetParameters(stockName, "stockName", stockName, parameters);


            target.SetParameters(stockName, "bidSize", 153.23, parameters);
            target.SetParameters(stockName, "bid", 153.23, parameters);
            target.SetParameters(stockName, "askSize", 153.23, parameters);
            target.SetParameters(stockName, "ask", 154.50, parameters);
            target.SetParameters(stockName, "last", 154.00, parameters);
            target.SetParameters(stockName, "period", 14, parameters);


            target.SetParameters(stockName, "startingX", 41, parameters);
            target.SetParameters(stockName, "frequency", 1440, parameters);
            target.SetParameters(stockName, "timeFrame", "MIN", parameters);
            target.SetParameters(stockName, Constants.IntradayStatus, Constants.IntradayWaitingMode, parameters);

            target.SetParameters(stockName, "requiredDerivative", 0.001, parameters);
            target.SetParameters(stockName, "requiredSeparation", 1.05, parameters);
            target.SetParameters(stockName, "volumePeriod", 14, parameters);
            target.SetParameters(stockName, "requiredVolume", 1.3, parameters);
            target.SetParameters(stockName, "orderId", "FMETF-MM", parameters);
            target.SetParameters(stockName, "requiredAbsoluteStopLossFactor", 0.07, parameters);
            target.SetParameters(stockName, "requiredIntradayPTPercentage", 0.1, parameters);
            target.SetParameters(stockName, "volume", 15678, parameters);
            target.SetParameters(stockName, "volumema", 15678, parameters);
            target.SetParameters(stockName, "intradayAskToBidRatio", 0.1, parameters);
            target.SetParameters(stockName, "brokerId", "findout", parameters);
            target.Changed += new ChangedEventHandler(target_Changed);

        }


        /// <summary>
        ///A test for Run
        ///</summary>
        [TestMethod()]
        public void RunTest()
        {
            target.SetParameters(stockName, "bidSize", 1000.00, parameters);
            target.SetParameters(stockName, "askSize", 100.00, parameters);
            target.Run(stockName, parameters);

            new System.Threading.ManualResetEvent(false).WaitOne(5000);
        }

        void target_Changed(object sender, EventArgs e)
        {
            if (e.GetType() == typeof(COMClassLibrary.Manager.MessageEventArg))
            {

                MessageEventArg eventArg = (MessageEventArg)e;

                IMessageHandler handler = eventArg.MessageHandler;
                handler.Handle(parameters, e);

                if (eventArg.Code == Constants.IntradayRatioReached)
                {
                    String message = (string)target.GetParameter(eventArg.StockName, Constants.IntradayMessage, parameters);
                    Assert.IsTrue(message == Constants.IntradayRatioReached);
                }
               // SetParameters(String.Format("{0},{1}", eventArg.StockName, eventArg.StrategyClass), Constants.IntradayMessage, Constants.IntradayRatioReached, parameters);

            }
        }

        /// <summary>
        ///A test for IsDictionaryValid
        ///</summary>
        [TestMethod()]
        public void GetCapitalTest()
        {
            CapitalCalculator c = new CapitalCalculator();
            double price = 5.00;
            double fluc = 0.01;
            double multiplier = 1;
            double expectedCapital = 250000; // TODO: Initialize to an appropriate value
            double actual;
            actual = c.GetCapital(multiplier, price);
            Assert.AreEqual(expectedCapital, actual);

            price = 49.5;
            fluc = 0.05;


            expectedCapital = 495000; // TODO: Initialize to an appropriate value
            
            actual = c.GetCapital(multiplier, price);
            Assert.AreEqual(expectedCapital, actual);

            price = 3.5;
            fluc = 0.01;


            expectedCapital = 175000; // TODO: Initialize to an appropriate value

            actual = c.GetCapital(multiplier, price);

            Assert.AreEqual(expectedCapital, actual);


        }

        //[TestMethod]
        //public void combineArrayTest()
        //{
        //    int[] front = { 1, 2, 3, 4 };
        //    int[] back = { 5, 6, 7, 8 };
        //    int[] combined = front.Concat(back).ToArray();
        //}

        [TestMethod()]
        public void IsBloombergTickerWSWorkingTest()
        {

            CapitalCalculator c = new CapitalCalculator();
             int frequency = 5;
            String timeFrame = "SEC";
            int period = 10;

            String listOfTickers =  "FMETF PM,MBT PM,AEV PM,TEL PM, AP PM, BLOOM PM, MEG PM, BDO PM, AC PM , ALI PM, DNL PM";
            PriceWithTimeFrameTO[] list = target.GeBatchPricetimeFrameList(listOfTickers, frequency, timeFrame, period);
            foreach (PriceWithTimeFrameTO item in list)
            {
                double price = Convert.ToDouble(item.Price);
                //BoardLotFluctuationPriceTO boardLot = c.GetBoardLotBasedOnPrice(price);
                double capital =  c.GetCapital(1, price);
                //Console.WriteLine(item.Bid);
                System.Diagnostics.Debug.WriteLine("Date: {7}, Stock:{6}, Price:{0}, Bid:{1}, Ask:{2}, Vol:{3}, Stop Loss:{4}, Profit Target:{5}, # of shares: {7}"
                    , item.Price, item.Bid, item.Ask, item.Volume
                    ,c.GetLowerPriceByFlucOffset(price,4.00)
                    ,c.GetHigherPriceByFlucOffset(price,8.00)
                    , item.StockName
                    , c.GetNumberOfShares( c.GetCapital(1,price), price) 
                    , item.Date.ToShortTimeString());

            }
            Assert.IsTrue(list.Length == 4);
           
        }

        /// <summary>
        ///A test for IsDictionaryValid
        ///</summary>
        [TestMethod()]
        public void IsDictionaryValidTest()
        {
            bool expected = true; // TODO: Initialize to an appropriate value
            bool actual;
            actual = target.IsDictionaryValid(stockName, parameters);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for IsAskSizeToBidSizeThresholdBreached
        ///</summary>
        [TestMethod()]
        public void IsAskSizeToBidSizeThresholdBreachedTest()
        {
            target.SetParameters(stockName, "bidSize", 100.00, parameters);
            target.SetParameters(stockName, "askSize", 1100.00, parameters);

            bool expected = false; 
            bool actual;
            actual = target.IsAskSizeToBidSizeThresholdBreached(stockName, parameters);
            Assert.AreEqual(expected, actual);

            target.SetParameters(stockName, "bidSize", 1000.00, parameters);
            target.SetParameters(stockName, "askSize", 100.00, parameters);
            expected = true;
            actual = target.IsAskSizeToBidSizeThresholdBreached(stockName, parameters);
            Assert.AreEqual(expected, actual);


       
        }



        [TestMethod]
        public void CalculatePriceByFlucOffsetTest()
        {
            double price = 100.5;
            double lowerHalf = 10;

            CapitalCalculator c = new CapitalCalculator();

            price = c.GetLowerPriceByFlucOffset(price, lowerHalf);


            Assert.IsTrue(price == 99.75);

            price = c.GetHigherPriceByFlucOffset(price, 30);

           
            Assert.IsTrue(price == 102.5);


        }




    }
}
