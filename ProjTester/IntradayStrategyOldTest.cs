﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using COMClassLibrary.Manager;
using COMClassLibrary;
using System.Net;
using System.Text.RegularExpressions;

namespace ProjTester
{
    [TestClass]
    public class IntradayStrategyOldTest
    {
        Dictionary<string, object> parameters = null;
        String stockName;

        private void ChangeStatusToIntradayWaitingMode()
        {
            OrderManager m = new OrderManager();
            m.UpdateOrderStatus(stockName, Constants.IntradayWaitingMode);
        }

        [TestInitialize]
        public void Initialize()
        {
            OrderManager orderManager = new OrderManager();
            parameters = new Dictionary<string, object>();
            stockName = "SMPH PM";
            orderManager.SetParameters(stockName, "stockName", stockName, parameters);
            orderManager.SetParameters(stockName, "askSize", 153.23, parameters);
            orderManager.SetParameters(stockName, "bidSize", 154.50, parameters);
            orderManager.SetParameters(stockName, "last", 154.00, parameters);
            orderManager.SetParameters(stockName, Constants.IntradayStatus, Constants.IntradayWaitingMode, parameters);
            orderManager.SetParameters(stockName, "orderId", "FMETF-MM", parameters);
            orderManager.SetParameters(stockName, "requiredAbsoluteStopLossFactor", 0.07, parameters);
            orderManager.SetParameters(stockName, "requiredRecogniaPTPercentage", 0.1, parameters);
            orderManager.SetParameters(stockName, "volume", 15678, parameters);
            orderManager.SetParameters(stockName, "volumema", 15678, parameters);
            orderManager.SetParameters(stockName, "volumeRequired", 1.3, parameters);
            orderManager.SetParameters(stockName, "intradayAskToBidRatio",0.1, parameters);
            
        }

        [TestMethod]
        public void TestBuyOrderWithRightStatus()
        {
            OrderManager orderManager = new IntradayStrategy();
            orderManager.SetParameters(stockName, Constants.IntradayStatus, String.Empty, parameters);
            ChangeStatusToIntradayWaitingMode();
            
            orderManager.Changed += new ChangedEventHandler(mm_Changed);
            ((IStrategy)orderManager).Run(stockName, parameters);
            new System.Threading.ManualResetEvent(false).WaitOne(5000);
        }

        void mm_Changed(object sender, EventArgs e)
        {
            
        }

        [TestMethod]
        public void TestINAV()
        {
            String url = @"http://www.fmic.mdgms.com/iopv/quote.php";
            
            String responseString = "";
            using (var wb = new WebClient())
            {
                var response = wb.DownloadString(url);
                responseString = response;
            }


            double price = 0;
            const string pattern = @"<td\b[^>]*?>(?<V>[\s\S]*?)</\s*td>";
            foreach (Match match in Regex.Matches(responseString, pattern, RegexOptions.IgnoreCase))
            {
                string value = match.Groups["V"].Value;

                if (Double.TryParse(value, out price))
                {

                    Console.WriteLine(value);
                }
                
               
            }

            Assert.IsTrue(price > 0);

        }
    }
}
