﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using COMClassLibrary.Manager;
using System.Diagnostics;

namespace ProjTester
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestRSIBuyOrder()
        {

        }
        [TestMethod]
        public void TestPassByReference()
        {
            String key = "key";
            String value = "value";
            String newValue = "newvalue";
            Dictionary<string,object> dictionary = new Dictionary<string,object>();
            dictionary[key] = value;

            
            TestMethod(key, newValue, value, dictionary);

            Assert.IsTrue((String)dictionary[key] == newValue);

        }

        private void TestMethod(String key,String newvalue, String value,Dictionary<string, object> dictionary)
        {
            Assert.IsTrue((String)dictionary[key] == value);
            dictionary[key] = newvalue;

        }

       

   
        [TestMethod]
        public void TestQuotationScreenStatus()
        {
     
           


            String testValue = "F";
            RSIBreakoutStrategy m = new RSIBreakoutStrategy();
            Assert.IsTrue(m.CheckQuotationOrderStatus("MBT TEST") == testValue);
        }
        [TestMethod]
        public void TestSettingAndGettingParameters()
        {
            String stockName = "MBT TEST";
            String key = "key";
            String testValue = "test";
            TradingController con = new TradingController();
            con.SetParameters(stockName, typeof(RSIBreakoutStrategy).ToString(), key,testValue);
            String obj = con.GetMessage(stockName, typeof(RSIBreakoutStrategy).ToString(), key);
            Assert.IsTrue(testValue == obj);


            stockName = "AEV TEST";
            key = "key";
            testValue = "test2";
            con.SetParameters(stockName, typeof(RSIBreakoutStrategy).ToString(), key, testValue);
            obj = (String)con.GetMessage(stockName, typeof(RSIBreakoutStrategy).ToString(), key);
            Assert.IsTrue(testValue == obj);



            stockName = "NPV TEST";
            key = "key";
            testValue = "test3";
            con.SetParameters(stockName, typeof(RSIBreakoutStrategy).ToString(), key, testValue);
            obj = (String)con.GetMessage(stockName, typeof(RSIBreakoutStrategy).ToString(), key);
            Assert.IsTrue(testValue == obj);

        }
        [TestMethod]
        public void TestUpdateStockStatus()
        {
            String stockName = "MBT TEST";
            String testValue = "F";
            TradingController con = new TradingController();
            con.UpdateQuotationScreenStatus(stockName, testValue);
            OrderManager m = new OrderManager();
            Assert.IsTrue   ( m.CheckQuotationOrderStatus(stockName) == testValue);
        }
        [TestMethod]
        public void TestGetYFromRSILineUsingLinearRegression()
        {
            List<double> xlist = new List<double>();
            List<double> ylist = new List<double>();
            for(int i = 1; i <= 40; i++){
                xlist.Add(i);
            }


            for (int i = 1; i <= 40; i++)
            {
                ylist.Add(3.0);
            }

            //set position 10, 20, 30 top 3 highest points
            ylist[9] = 10.0;
            ylist[19] = 5.0;
            ylist[29] = 8.0;


            
            double x = 41.0;
            RSICalculator calc = new RSICalculator();
     
            RSI rsi = calc.GetRSIUsingLinearRegressionWithThreeCriticalPoints(xlist,ylist,x);

            double y = (x * rsi.Derivative) + rsi.YIntercept;
            Assert.IsTrue(rsi.Derivative < -0.1);
            Assert.IsTrue(y > 5.5 && y < 5.6);

            ylist[3] = 10.0;
            ylist[21] = 5.0;
            ylist[33] = 8.0;

            rsi = calc.GetRSIUsingLinearRegressionWithThreeCriticalPoints(xlist, ylist, x);

            Assert.IsTrue(rsi.Derivative < 0);


            ylist[3] = 30.0;
            ylist[21] = 25.0;
            ylist[33] = 68.0;

            rsi = calc.GetRSIUsingLinearRegressionWithThreeCriticalPoints(xlist, ylist, x);

            Assert.IsTrue(rsi.Derivative > 0);



        }

        [TestMethod]
        public void TestCalculateRSILinearRegression()
        {
            double x = 41;
            double xfirstApex = 10, xsecondApex = 20, xthirdApex = 30;
            double yfirstApex = 10, ysecondApex = 5, ythirdApex = 8;
            var xdata = new double[] { xfirstApex, xsecondApex, xthirdApex };
            var ydata = new double[] { yfirstApex, ysecondApex, ythirdApex };
            RSICalculator calc = new RSICalculator();
            RSI rsi = calc.CalculateRSILinearRegression(x, xdata, ydata);

            //-0.10000000000000034 derivative
            //9.66666666666668  yintercept
            double y = (x * rsi.Derivative) + rsi.YIntercept;
            Assert.IsTrue(rsi.Derivative < -0.1);
            Assert.IsTrue(y > 5.5 && y < 5.6);
        }


        [TestMethod]
        public void CreateFlucsTableTest()
        {
            double minLot = 0;
            CapitalCalculator calculator = new CapitalCalculator();
            for (double i = 0.0001; i <= .0099; i += 0.0001)
            {
                minLot = calculator.CalculateMinimumLot(i);
                //Console.WriteLine(minLot);
                Assert.IsTrue(minLot == 1000000.00);
                Debug.WriteLine("Price:{0}",i);
            }
            for (double i = 0.010; i <= .049; i += .001)
            {
                minLot = calculator.CalculateMinimumLot(i);
                //Console.WriteLine(minLot);
                Debug.WriteLine("Price:{0}", i);
                Assert.IsTrue(minLot == 100000.00);
            }
            for (double i = 0.050; i <= 0.249; i += 0.001)
            {
                minLot = calculator.CalculateMinimumLot(i);
                //Console.WriteLine(minLot);
                Debug.WriteLine("Price:{0}", i);
                Assert.IsTrue(minLot == 10000.00);
            }
            for (double i = 0.250; i <= 0.495; i += 0.005)
            {
                minLot = calculator.CalculateMinimumLot(i);
                Debug.WriteLine("Price:{0}", i);
                //Console.WriteLine(minLot);
                Assert.IsTrue(minLot == 10000.00);
            }
            for (double i = 0.50; i <= 4.99; i += 0.01)
            {
                minLot = calculator.CalculateMinimumLot(i);
                //Console.WriteLine(minLot);
                Debug.WriteLine("Price:{0}", i);
                Assert.IsTrue(minLot == 1000.00);
            }
            for (double i = 5.00; i <= 9.99; i += 0.01)
            {
                minLot = calculator.CalculateMinimumLot(i);
                Debug.WriteLine("Price:{0}", i);
                //Console.WriteLine(minLot);
                Assert.IsTrue(minLot == 100.00);
            }
            for (double i = 10.00; i <= 19.98; i += 0.02)
            {
                minLot = calculator.CalculateMinimumLot(i);
                Debug.WriteLine("Price:{0}", i);
                //Console.WriteLine(minLot);
                Assert.IsTrue(minLot == 100.00);
            }
            for (double i = 20.00; i <= 49.95; i += 0.05)
            {
                minLot = calculator.CalculateMinimumLot(i);
                Debug.WriteLine("Price:{0}", i);
                //Console.WriteLine(minLot);
                Assert.IsTrue(minLot == 100.00);
            }
            for (double i = 50.00; i <= 99.95; i += 0.05)
            {
                Debug.WriteLine("Price:{0}", i);
                minLot = calculator.CalculateMinimumLot(i);
                //Console.WriteLine(minLot);
                Assert.IsTrue(minLot == 10.00);
            }

            for (double i = 100.00; i <= 199.90; i += 0.10)
            {
                minLot = calculator.CalculateMinimumLot(i);
                Debug.WriteLine("Price:{0}", i);
                //Console.WriteLine(minLot);
                Assert.IsTrue(minLot == 10.00);
            }
            for (double i = 200.00; i <= 499.80; i += 0.20)
            {
                Debug.WriteLine("Price:{0}", i);
                minLot = calculator.CalculateMinimumLot(i);
                //Console.WriteLine(minLot);
                Assert.IsTrue(minLot == 10.00);
            }
            for (double i = 500.00; i <= 999.50; i += 0.50)
            {
                minLot = calculator.CalculateMinimumLot(i);
                Debug.WriteLine("Price:{0}", i);
                //Console.WriteLine(minLot);
                Assert.IsTrue(minLot == 10.00);
            }
            for (double i = 1000.00; i <= 1999.00; i += 1.0)
            {
                minLot = calculator.CalculateMinimumLot(i);
                Debug.WriteLine("Price:{0}", i);
                //Console.WriteLine(minLot);
                Assert.IsTrue(minLot == 5.00);
            }

            for (double i = 2000.00; i <= 4998.00; i += 2.0)
            {
                minLot = calculator.CalculateMinimumLot(i);
                Debug.WriteLine("Price:{0}", i);
                //Console.WriteLine(minLot);
                Assert.IsTrue(minLot == 5.00);
            }
            for (double i = 5000.00; i <= 10000000.00; i += 5.0)
            {
                minLot = calculator.CalculateMinimumLot(i);
                Debug.WriteLine("Price:{0}", i);
                //Console.WriteLine(minLot);
                Assert.IsTrue(minLot == 5.00);
            }
        }

        [TestMethod]
        public void TestMinimumLotCalculator()
        {
            double minLot = 0;
            CapitalCalculator calculator = new CapitalCalculator();
            for (double i = 0.0001; i <= .0099; i+=0.0001)
            {
                minLot = calculator.CalculateMinimumLot(i);
                //Console.WriteLine(minLot);
                Assert.IsTrue(minLot == 1000000.00);
            }
            for (double i = 0.010; i <= .049; i += .001)
            {
                minLot = calculator.CalculateMinimumLot(i);
                //Console.WriteLine(minLot);
                Assert.IsTrue(minLot == 100000.00);
            }
            for (double i = 0.010; i <= 0.049; i += .001)
            {
                minLot = calculator.CalculateMinimumLot(i);
                //Console.WriteLine(minLot);
                Assert.IsTrue(minLot == 100000.00);
            }
            for (double i = 0.050; i <= 0.249; i += 0.001)
            {
                minLot = calculator.CalculateMinimumLot(i);
                //Console.WriteLine(minLot);
                Assert.IsTrue(minLot == 10000.00);
            }
            for (double i = 0.250; i <= 0.495; i += 0.005)
            {
                minLot = calculator.CalculateMinimumLot(i);
                //Console.WriteLine(minLot);
                Assert.IsTrue(minLot == 10000.00);
            }
            for (double i = 0.50; i <= 4.99; i += 0.01)
            {
                minLot = calculator.CalculateMinimumLot(i);
                //Console.WriteLine(minLot);
                Assert.IsTrue(minLot == 1000.00);
            }
            for (double i = 5.00; i <= 9.99; i += 0.01)
            {
                minLot = calculator.CalculateMinimumLot(i);
                //Console.WriteLine(minLot);
                Assert.IsTrue(minLot == 100.00);
            }
            for (double i = 10.00; i <= 19.98; i += 0.02)
            {
                minLot = calculator.CalculateMinimumLot(i);
                //Console.WriteLine(minLot);
                Assert.IsTrue(minLot == 100.00);
            }
            for (double i = 20.00; i <= 49.95; i += 0.05)
            {
                minLot = calculator.CalculateMinimumLot(i);
                //Console.WriteLine(minLot);
                Assert.IsTrue(minLot == 100.00);
            }
            for (double i = 50.00; i <= 99.95; i += 0.05)
            {
                minLot = calculator.CalculateMinimumLot(i);
                //Console.WriteLine(minLot);
                Assert.IsTrue(minLot == 10.00);
            }

            for (double i = 100.00; i <= 199.90; i += 0.10)
            {
                minLot = calculator.CalculateMinimumLot(i);
                //Console.WriteLine(minLot);
                Assert.IsTrue(minLot == 10.00);
            }
            for (double i = 200.00; i <= 499.80; i += 0.20)
            {
                minLot = calculator.CalculateMinimumLot(i);
                //Console.WriteLine(minLot);
                Assert.IsTrue(minLot == 10.00);
            }
            for (double i = 500.00; i <= 999.50; i += 0.50)
            {
                minLot = calculator.CalculateMinimumLot(i);
                //Console.WriteLine(minLot);
                Assert.IsTrue(minLot == 10.00);
            }
            for (double i = 1000.00; i <= 1999.00; i += 1.0)
            {
                minLot = calculator.CalculateMinimumLot(i);
                //Console.WriteLine(minLot);
                Assert.IsTrue(minLot == 5.00);
            }

            for (double i = 2000.00; i <= 4998.00; i += 2.0)
            {
                minLot = calculator.CalculateMinimumLot(i);
                //Console.WriteLine(minLot);
                Assert.IsTrue(minLot == 5.00);
            }
            for (double i = 5000.00; i <= 10000000.00; i += 5.0)
            {
                minLot = calculator.CalculateMinimumLot(i);
                //Console.WriteLine(minLot);
                Assert.IsTrue(minLot == 5.00);
            }
        }


        [TestMethod]
        public void CompareDateBy10MinutesTest()
        {
            DateTime baseLineDate = DateTime.Parse("01/01/2014 01:00 AM");

            Assert.IsTrue(baseLineDate.Hour == 1);

            DateTime currentDate = DateTime.Parse("01/01/2014 01:05 AM");

            

            Assert.IsTrue(currentDate.AddMinutes(-10) < baseLineDate);

            Assert.IsFalse(currentDate.AddMinutes(-10) < baseLineDate.AddMinutes(-60));

        }


        [TestMethod]
        public void TestRSICalculator()
        {
            List<double> list = new List<double>();
            list.Add(101);
            list.Add(102);
            list.Add(103);
            list.Add(104);
            list.Add(105);
            list.Add(106);
            list.Add(107);
            list.Add(106);
            list.Add(107);
            list.Add(110);
            list.Add(111);
            list.Add(112);
            list.Add(113);
            list.Add(114);
            RSICalculator c = new RSICalculator();
            double rsi = c.CalculateRSI(list, 14);
            Assert.IsTrue(rsi > 93.33);
        }


    }
}
