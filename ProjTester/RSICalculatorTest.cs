﻿using COMClassLibrary.Manager;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using COMClassLibrary.FMServiceReference;

namespace ProjTester
{
    
    
    /// <summary>
    ///This is a test class for RSICalculatorTest and is intended
    ///to contain all RSICalculatorTest Unit Tests
    ///</summary>
    [TestClass()]
    public class RSICalculatorTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for CalculateRSI
        ///</summary>
        [TestMethod()]
        public void CalculateRSITest()
        {

            double last = 150; // TODO: Initialize to an appropriate value
            int period = 10;
            double expected = 100; // TODO: Initialize to an appropriate value
            double actual;
            RSICalculator rsiCalculator = new RSICalculator();
            TimeFrameManager m = new TimeFrameManager();

            List<double> priceList = new List<double>();
          
                for (int i = 1; i <= period; i++)
                {
                    priceList.Add(last + i);
                }
            
            
            
         

            RSICalculator target = new RSICalculator(); // TODO: Initialize to an appropriate value

            actual = target.CalculateRSI(priceList, period);
            Assert.AreEqual(expected, actual);

            last = 159;
            for (int i = 1; i <= 4; i++)
            {
                priceList.Add(last + -i);
            }
            expected = 64.28;
            actual = target.CalculateRSI(priceList, 14);

            Assert.AreEqual(Math.Floor( expected), Math.Floor(actual));



            priceList.Clear();

            for (int i = 1; i <= period; i++)
            {
                priceList.Add(last + -i);
            }



            actual = rsiCalculator.CalculateRSI(priceList, period);
            expected = 0;
            Assert.AreEqual(Math.Floor(expected), Math.Floor(actual));

        }
    }
}
