﻿using COMClassLibrary.Manager;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using COMClassLibrary;
using COMClassLibrary.FMServiceReference;

namespace ProjTester
{
    

    
    /// <summary>
    ///This is a test class for OrderManagerTest and is intended
    ///to contain all OrderManagerTest Unit Tests
    ///</summary>
    [TestClass()]
    public class OrderManagerTest
    {
        Dictionary<string, object> parameters = null;
        private RSIBreakoutStrategy orderManager;
        string stockName;
        private TestContext testContextInstance;
        [TestInitialize]
        public void Initialize()
        {
            orderManager = new RSIBreakoutStrategy();
            parameters = new Dictionary<string, object>();
            stockName = "SMPH PM";
            orderManager.SetParameters(stockName, "stockName", stockName, parameters);


            orderManager.SetParameters(stockName, "bid", 153.23, parameters);
            orderManager.SetParameters(stockName, "ask", 154.50, parameters);
            orderManager.SetParameters(stockName, "last", 154.00, parameters);
            orderManager.SetParameters(stockName, "period", 14, parameters);


            orderManager.SetParameters(stockName, "startingX", 41, parameters);
            orderManager.SetParameters(stockName, "frequency", 1440, parameters);
            orderManager.SetParameters(stockName, "timeFrame", "MIN", parameters);
            orderManager.SetParameters(stockName, Constants.RSIStatus, Constants.RSIWaitingMode, parameters);
            orderManager.SetParameters(stockName, "requiredDerivative", 0.001, parameters);
            orderManager.SetParameters(stockName, "requiredSeparation", 1.05, parameters);
            orderManager.SetParameters(stockName, "volumePeriod", 14, parameters);
            orderManager.SetParameters(stockName, "requiredVolume", 1.3, parameters);

            orderManager.SetParameters(stockName, "profitTargetLookupPeriod", 10, parameters);
            orderManager.SetParameters(stockName, "stopLossLookupPeriod", 10, parameters);
        }
        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for GetProfitTargetAndStopLossPeriod
        ///</summary>
        [TestMethod()]
        public void GetProfitTargetAndStopLossPeriodTest()
        {
            orderManager.GetProfitTargetAndStopLossPeriod(stockName, parameters);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for SetVolumeMA
        ///</summary>
        [TestMethod()]
        public void SetVolumeMATest()
        {

            VolumeCalculator vc = new VolumeCalculator();
            int frequency = (int)orderManager.GetParameter(stockName, "frequency", parameters);
            string timeFrame = (string)orderManager.GetParameter(stockName, "timeFrame", parameters);
            int period = (int)orderManager.GetParameter(stockName, "period", parameters);

            //add volume here using date
            DateTime date = DateTime.Parse("04/14/2014");

             vc.SaveVolume(stockName, 56, frequency, timeFrame, period, date);

            

            orderManager.SetParameters(stockName, "cutoffDate", date, parameters);


            orderManager.GetLastVolumeMA(stockName, parameters);

            // get the list
            // check the volume ma

           VolumeMATO v = (VolumeMATO)  orderManager.GetParameter(stockName, Constants.CurrentVolumeMATO, parameters);
           Assert.IsTrue(v.VolumeMAValue == 56);
           Assert.IsTrue(v.StockName == stockName);
        }
    }
}
