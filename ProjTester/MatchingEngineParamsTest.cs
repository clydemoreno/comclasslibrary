﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using COMClassLibrary.Model;
using COMClassLibrary.Manager;
using FirstMetroSystemTrayApp.Model;
using COMClassLibrary;
using COMClassLibrary.FMServiceReference;

namespace ProjTester
{
    /// <summary>
    /// Summary description for MatchingEngineParamsTest
    /// </summary>
    [TestClass]
    public class MatchingEngineParamsTest
    {
        public MatchingEngineParamsTest()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion


        [TestMethod]
        public void GoDownInFlucsByTest()
        {
            MatchingEngineParams p = new MatchingEngineParams();
            Double expected = 99.80;
            Double actual = p.GoDownInFlucsBy(100.10, 5);
            Assert.IsTrue(Math.Round(actual, 2) == expected);
        }
        [TestMethod]
        public void GoUpInFlucsByTest()
        {
            MatchingEngineParams p = new MatchingEngineParams();
            Double expected = 100.10;
            Double actual = p.GoUpInFlucsBy(99.80, 5);
            Assert.IsTrue(Math.Round(actual, 2) == expected);
        }
        [TestMethod]
        public void SkewedDefaBidAndDefaOfferTest()
        {
            MatchingEngineParams p = new MatchingEngineParams();
            p.INAV = 112.1934;
            p.Bid = 110.41;
            p.Offer = 111.41;
            p.SkewView = 3;
            p.MaxSpread = 3;
            p.MinPostVolume = 50;
            p.MarketOfferVolume = 3000;
            p.MarketBidVolume = 5000;
            Assert.IsTrue(Math.Round(p.DefaBid, 2) == 111.90);
            Assert.IsTrue(Math.Round(p.DefaOffer, 2) == 112.20);

        }

        [TestMethod]
        public void MMBidAndMMOfferTest()
        {
            MatchingEngineParams p = new MatchingEngineParams();
            p.INAV = 111.0124;
            p.Bid = 110.41;
            p.Offer = 111.41;
            p.SkewView = 0;
            p.MaxSpread = 10;
            p.MinPostVolume = 50;
            p.MarketOfferVolume = 3000;
            p.MarketBidVolume = 3000;

            Assert.IsTrue(Math.Round(p.DefaBid,2) == 110.51);

            Assert.IsTrue(Math.Round(p.DefaOffer, 2) == 111.51);

            Assert.IsTrue(p.BestBid == p.Bid);
            Assert.IsTrue(Math.Round(p.MMBid,2) == 110.41);
        }


        [TestMethod]
        public void CreateTextToCancelBidAndOfferTest()
        {
            MatchingEngineParams p = new MatchingEngineParams();
            p.INAV = 200.00;
            p.Bid = 201.0;
            p.Offer = 201.2;
            p.SkewView = 0;
            p.MaxSpread = 10;
            p.MinPostVolume = 50;
            p.MarketOfferVolume = 2000;
            p.MarketBidVolume = 3000;
            p.Buyer = Constants.FMETFMM;
            p.Seller = "GC-01";
            
            List<ImportedOrderScreen> buyerList = new List<ImportedOrderScreen>();
            List<ImportedOrderScreen> sellerList = new List<ImportedOrderScreen>();

            ImportedOrderScreen imp = null;
            for (double i = 0.0; i < 1.00; i += 0.20)
            {
                imp = new ImportedOrderScreen();
                imp.price = 201.00 + (i * -1);
                imp.pending = 3000.00;
                imp.side = "Buy";
                imp.stock = Constants.FMETF;
                buyerList.Add(imp);
            }



            buyerList[3].action = "Cancel";

            String text = "Account ID = {0}\r\nStock = {1}\r\nSide = {2}\r\nPrice {3}= {4}\r\n";
            
            foreach (ImportedOrderScreen o in buyerList)
            {
                if (o.action == "Cancel")
                {
                    text = String.Format(text,p.Buyer,o.stock,o.side, o.side == "Buy" ? ">" : "<",o.price   );
                    break;
                }
            }

            String actualText = "Account ID = FMETF-MM\r\nStock = FMETF\r\nSide = Buy\r\nPrice >= 200.4\r\n";

            Assert.IsTrue(actualText == text);


        }

        

        [TestMethod]
        public void BestBidAndBestOfferWithPostedBidAndPostedVolumeListTest()
        {
            MatchingEngineParams p = new MatchingEngineParams();
            p.INAV = 200.00;
            p.Bid = 201.0;
            p.Offer = 201.2;
            p.SkewView = 0;
            p.MaxSpread = 10;
            p.MinPostVolume = 50;
            p.MarketOfferVolume = 2000;
            p.MarketBidVolume = 3000;

            List<ImportedOrderScreen> buyerList = new List<ImportedOrderScreen>();
            List<ImportedOrderScreen> sellerList = new List<ImportedOrderScreen>();

            ImportedOrderScreen imp = null;
            for (double i = 0.0; i < 1.00; i += 0.20)
            {
                imp = new ImportedOrderScreen();
                imp.price = 201.00 + (i * -1);
                imp.pending = 3000.00;
                buyerList.Add(imp);
            }

            p.BidImportedOrderScreenList = buyerList;

            for (double i = 0.0; i < 1.00; i += 0.20)
            {
                imp = new ImportedOrderScreen();
                imp.price = 201.2 + (i);
                sellerList.Add(imp);
            }

            p.AskImportedOrderScreenList = sellerList;


            Assert.IsTrue(Math.Round(p.BestBid, 10) == Math.Round(p.Bid, 10));


            buyerList[0].pending = 2000.00;
            Assert.IsTrue(Math.Round(p.BestBid, 10) == Math.Round(p.Bid, 10));

            p.Bid = 201.60;
            Assert.IsTrue(Math.Round(p.BestBid, 10) == Math.Round(p.Bid, 10));

            //Assert.IsTrue(Math.Round(p.MMOffer, 10) == 203.00);

            //Assert.IsTrue(Math.Round(p.MMBid, 10) == 199.5);

        }


        [TestMethod]
        public void BOAndBBInsideDODBTest()
        {
            MatchingEngineParams p = new MatchingEngineParams();
            p.INAV = 200.00;
            p.Bid = 201.0;
            p.Offer = 201.2;
            p.SkewView = 0;
            p.MaxSpread = 10;
            p.MinPostVolume = 50;
            p.MarketOfferVolume = 2000;

            Assert.IsTrue(Math.Round(p.MMOffer, 10) == 203.00);
            Assert.IsTrue(Math.Round(p.MMOffer, 10) == 203.00);

            Assert.IsTrue(Math.Round(p.MMBid, 10) == 199.5);


        }

        [TestMethod]
        public void MMOfferFlucsCrossingBoundariesWithTest()
        {
            //50 to 99.95 is .05
            //100 to 199.90 is .10

            MatchingEngineParams p = new MatchingEngineParams();
            p.INAV = 110.58;
            p.Bid = 110.20;
            p.Offer = 112.00;
            p.SkewView = 0;
            p.MaxSpread = 10;
            p.MinPostVolume = 50;
            p.MarketOfferVolume = 2000;



            Assert.IsTrue(Math.Round(p.DefaBid, 10) == 110.08);

            Assert.IsTrue(Math.Round(p.DefaOffer, 10) == 111.08);


            p.Offer = 111.05;
            Assert.IsTrue(Math.Round(p.MMOffer, 10).Equals(111.08));
            //cancel posted offers if there are any.  do a test separately
            p.Offer = 120.00;
            p.Bid = 110.09;
            Assert.IsTrue(Math.Round(p.MMOffer, 10).Equals(111.09));

        }



        [TestMethod]
        public void MMBidFlucsCrossingBoundariesWithTest()
        {
            //50 to 99.95 is .05
            //100 to 199.90 is .10

            MatchingEngineParams p = new MatchingEngineParams();
            p.INAV = 110.58;
            p.Bid = 110.20;
            p.Offer = 112.00;
            p.SkewView = 0;
            p.MaxSpread = 10;
            p.MinPostVolume = 50;
            p.MarketOfferVolume = 2000;
           



            Assert.IsTrue(Math.Round(p.DefaBid, 10) == 110.08);

            Assert.IsTrue(Math.Round(p.DefaOffer, 10) == 111.08);

            Assert.IsTrue(Math.Round(p.MMBid, 10).Equals(110.08));

            p.Bid = 110.06;
            Assert.IsTrue(Math.Round(p.MMBid, 10).Equals(110.08));
            //cancel posted bids if there are any.  do a test separately

            p.Offer = 111.00;
            p.Bid = 110.06;
            Assert.IsTrue(Math.Round(p.MMBid, 10).Equals(110.00));

        }


        [TestMethod]
        public void BidFlucsCrossingBoundariesTest()
        {
            //50 to 99.95 is .05
            //100 to 199.90 is .10

            MatchingEngineParams p = new MatchingEngineParams();
            p.OfferFlucs = 0.10;
            p.BidFlucs = 0.10;
            p.INAV = 99.5;
            p.Bid = 110.50;
            p.Offer = 121.00;
            p.SkewView = 1;
            p.MaxSpread = 20;
            p.MinPostVolume = 50;
            p.MarketOfferVolume = 2000;

            Assert.IsTrue(Math.Round(p.DefaBid,10) == 99.75);

            Assert.IsTrue( Math.Round(p.DefaOffer,10).Equals(101.50));

           

        }


        [TestMethod]
        public void BidOfferFlucsDailyTest()
        {
            
            MatchingEngineParams p = new MatchingEngineParams();
            p.INAV = 110.58;
            p.Bid = 110.20;
            p.Offer = 121.00;
            p.SkewView = 0;
            p.MaxSpread = 10;
            p.MinPostVolume = 50;
            p.MarketOfferVolume = 2000;

            Assert.IsTrue(Math.Round( p.DefaBid, 10) == 110.08);

            Assert.IsTrue(Math.Round(p.DefaOffer,10) == 111.08);
            //Assert.IsTrue(p.MMOffer == 111.50);
            //Assert.IsTrue(p.AdjustedBidWithSpread == 110.5);

            //p.BestBid = 110.60;
            //Assert.IsTrue(p.DefaBid == 110.50);
            //Assert.IsTrue(p.MMBid == 110.50);
            //Assert.IsTrue(p.AdjustedBidWithSpread == 111.5);



        }
        [TestMethod]
        public void CrossingTheSpreadTest()
        {
            MatchingEngineParams p = new MatchingEngineParams();
            p.INAV = 95.5035;
            p.Bid = 120.0;
            p.Offer = 120.04;
            p.SkewView = 0.50;
            p.MaxSpread = 10;
            p.MinPostVolume = 50;
            p.MarketOfferVolume = 2000;

            Assert.IsTrue(Math.Round(p.MMBid,10) == Math.Round(p.DefaBid,10));
            Assert.IsTrue(Math.Round(p.MMOffer,10) == Math.Round(121.00,10)); 

        }

        [TestMethod]
        public void BidOfferFlucsTest()
        {
            //50 to 99.95 is .05
            //100 to 199.90 is .10

            MatchingEngineParams p = new MatchingEngineParams();
            p.INAV = 110.50;
            p.Bid = 110.50;
            p.Offer = 121.00;
            p.SkewView = 0.50;
            p.MaxSpread = 10;
            p.MinPostVolume = 50;
            p.MarketOfferVolume = 2000;

            Assert.IsTrue(p.DefaBid == 110.50);

            Assert.IsTrue(p.DefaOffer == 111.50);
            Assert.IsTrue(p.MMOffer == 111.50);
            Assert.IsTrue(p.AdjustedBidWithSpread == 110.5);

            p.Bid = 110.60;
            Assert.IsTrue(p.DefaBid == 110.50);
            Assert.IsTrue(p.MMBid == 110.50);
            Assert.IsTrue(p.AdjustedBidWithSpread == 111.5);


            
        }
        [TestMethod]
        public void MinOfferVolumeAndVolumeRemainderTest()
        {
            MatchingEngineParams p = new MatchingEngineParams();

            p.INAV = 100;
            p.Bid = 100.00;
            p.Offer = 106.00;
            p.SkewView = 1;
            p.MaxSpread = 7;
            p.MinPostVolume = 50;
            p.MarketOfferVolume = 2000;

            Assert.IsTrue(p.DefaOffer == 105.00);
            Assert.IsTrue(p.MMOffer == 105.00);
            Assert.IsTrue(p.AdjustedBidWithSpread == 98.00);

            p.Offer = 104.00;
            Assert.IsTrue(p.MMOffer == 104.00);
            Assert.IsTrue(p.AdjustedBidWithSpread == 97.00);

            
            SpreadAnchor sAnchor = new SpreadAnchor();
            List<ImportedOrderScreen> sellerList = new List<ImportedOrderScreen>();
            ImportedOrderScreen s = new ImportedOrderScreen();

            s.accountID = "";
            s.price = 103.00;
            s.volume = 3000;
            s.side = "SELL";
            sellerList.Add(s);

            s = new ImportedOrderScreen();
            s.accountID = "";
            s.side = "SELL";
            s.price = 104.00;
            s.volume = 3000;

            sellerList.Add(s);

            s = new ImportedOrderScreen();
            s.accountID = "";
            s.side = "SELL";
            s.price = 105.00;
            s.volume = 3000;

            sellerList.Add(s);



            sAnchor.SetOffer(p, sellerList);




            Assert.IsTrue(sellerList[0].action == "Cancel");
            Assert.IsTrue(sellerList[1].action == "Cancel");
            Assert.IsTrue(String.IsNullOrEmpty(sellerList[2].action));



        
        }


        public void MinBidVolumeAndVolumeRemainderTest()
        {
            MatchingEngineParams p = new MatchingEngineParams();

            p.INAV = 100;
            p.Bid = 100.00;
            p.Offer = 104.00;
            p.SkewView = 1;
            p.MaxSpread = 7;
            p.MinPostVolume = 50;
            p.MarketBidVolume = 2000;

            SpreadAnchor sAnchor = new SpreadAnchor();
            List<ImportedOrderScreen> buyerList = new List<ImportedOrderScreen>();
            ImportedOrderScreen s = new ImportedOrderScreen();

            s.accountID = "";
            s.price = 98.00;
            s.volume = 3000;
            s.side = "BUY";
            buyerList.Add(s);

            s = new ImportedOrderScreen();
            s.accountID = "";
            s.side = "BUY";
            s.price = 99.00;
            s.volume = 3000;

            buyerList.Add(s);

            s = new ImportedOrderScreen();
            s.accountID = "";
            s.side = "BUY";
            s.price = 100.00;
            s.volume = 3000;

            buyerList.Add(s);



            sAnchor.SetBid(p, buyerList);


            Assert.IsTrue(p.MMBid == 100.00);
            Assert.IsTrue(p.AdjustedOfferWithSpread == 106.00);







        }




        [TestMethod]
        public void InstantiateTest()
        {
            MatchingEngineParams p = new MatchingEngineParams();

            p.INAV = 99;
            p.Bid = 0.00;
            p.Offer = 999.00;
            p.SkewView = 1;
            p.MaxSpread = 10;
            p.MinPostVolume = 50;
            //figure out how this will play a role

            Assert.IsTrue(p.DefaBid == 95.00);
            Assert.IsTrue(p.DefaOffer == 105.00);

            Assert.IsTrue(p.MMBid == 95.00);
            Assert.IsTrue(p.AdjustedOfferWithSpread == 105.00);


            Assert.IsTrue(p.MMOffer == 105.00);
            Assert.IsTrue(p.AdjustedBidWithSpread == 95.00);



            p.INAV = 100;
            p.Bid = 0.00;
            p.Offer = 999.00;
            p.SkewView = 1;
            p.MaxSpread = 10;
            p.MinPostVolume = 50;
            p.MarketBidVolume = 3000;
            p.MarketOfferVolume = 3000;
            //figure out how this will play a role

            Assert.IsTrue(p.DefaBid == 96.00);
            Assert.IsTrue(p.DefaOffer == 106.00);

            Assert.IsTrue(p.MMBid == 96.00);
            Assert.IsTrue(p.AdjustedOfferWithSpread == 106.00);


            Assert.IsTrue(p.MMOffer == 106.00);
            Assert.IsTrue(p.AdjustedBidWithSpread == 96.00);



            p.INAV = 100;
            p.Bid = 100.00;
            p.Offer = 104.00;
            p.SkewView = 1;
            p.MaxSpread = 5;
            p.MinPostVolume = 50;
            //figure out how this will play a role

            Assert.IsTrue(p.SkewNAV == 101.00);
            Assert.IsTrue(p.DefaBid == 99.00);

            Assert.IsTrue(p.DefaOffer == 104.00);

            Assert.IsTrue(p.MMBid == 100.00);
            Assert.IsTrue(p.AdjustedOfferWithSpread == 105.00);


            Assert.IsTrue(p.MMOffer == 104.00);
            Assert.IsTrue(p.AdjustedBidWithSpread == 99.00);



            SpreadAnchor sAnchor = new SpreadAnchor();
            List<ImportedOrderScreen> buyerList = new List<ImportedOrderScreen>();
            ImportedOrderScreen s = new ImportedOrderScreen();
            
            s.accountID = "";
            s.price = 98.00;
            s.volume = 3000;
           
            buyerList.Add(s);

            s = new ImportedOrderScreen();
            s.accountID = "";
            s.price = 99.00;
            s.volume = 3000;

            buyerList.Add(s);

            s = new ImportedOrderScreen();
            s.accountID = "";
            s.price = 100.00;
            s.volume = 3000;
            
            buyerList.Add(s);

            

            sAnchor.SetBid(p, buyerList);


            Assert.IsTrue(String.IsNullOrEmpty( buyerList[0].action));
            Assert.IsTrue(String.IsNullOrEmpty(buyerList[1].action));
            Assert.IsTrue(buyerList[2].action == "Cancel"); 
            



        }
    }
}
