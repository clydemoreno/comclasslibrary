﻿using COMClassLibrary.Manager;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using COMClassLibrary.FMServiceReference;
using System.Collections.Generic;
using COMClassLibrary;

namespace ProjTester
{
    
    
    /// <summary>
    ///This is a test class for TimeFrameManagerTest and is intended
    ///to contain all TimeFrameManagerTest Unit Tests
    ///</summary>
    [TestClass()]
    public class TimeFrameManagerTest
    {

        Dictionary<string, object> parameters = null;
        String stockName = "AEV PM";
        TimeFrameManager target;

        [TestInitialize]
        public void Initialize()
        {
            target = new TimeFrameManager();
            parameters = new Dictionary<string, object>();
            stockName = "AEV PM";




        }


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        [TestMethod()]
        public void SubtractTest(){

            DateTime d = DateTime.Now.AddMinutes(1);
                    int result = d.CompareTo(DateTime.Now.AddMinutes(1));
                   bool isCurrent = (result >= 1);


                   DateTime todaysDateTime = DateTime.Now;
                   
                   TimeSpan span = d.Subtract(todaysDateTime);
                   double totalMins = span.TotalMinutes;
                   Assert.IsTrue(totalMins <= 1);
    }


        /// <summary>
        ///A test for GetModuloMinute
        ///</summary>
        [TestMethod()]
        public void ModuloTest()
        {
            int frequency = 5; // TODO: Initialize to an appropriate value
            DateTime currentTime = new DateTime(2014,1,1,1,3,0); // TODO: Initialize to an appropriate value
            DateTime expected = new DateTime(2014, 1, 1, 1, 0, 0); // TODO: Initialize to an appropriate value
            Nullable<DateTime> actual;
            actual = TimeFrameManager.GetModuloMinute(frequency, currentTime);
            Assert.AreEqual(expected, actual);

            frequency = 15;
            actual = TimeFrameManager.GetModuloMinute(frequency, currentTime);
            Assert.AreEqual(expected, actual);

            frequency = 60;
            actual = TimeFrameManager.GetModuloMinute(frequency, currentTime);
            Assert.AreEqual(expected, actual);


            currentTime = new DateTime(2014, 1, 1, 1, 53, 0); // TODO: Initialize to an appropriate value
            actual = TimeFrameManager.GetModuloMinute(frequency, currentTime);
            Assert.AreEqual(expected, actual);


            frequency = 120;
            expected = new DateTime(2014, 1, 1, 0, 0, 0);
            actual = TimeFrameManager.GetModuloMinute(frequency, currentTime);
            Assert.AreEqual(expected, actual);

            frequency = 240;
            expected = new DateTime(2014, 1, 1, 0, 0, 0);
            actual = TimeFrameManager.GetModuloMinute(frequency, currentTime);
            Assert.AreEqual(expected, actual);

            currentTime = new DateTime(2014, 1, 1, 5, 53, 0); // TODO: Initialize to an appropriate value
            expected = new DateTime(2014, 1, 1, 4, 0, 0);
            
            actual = TimeFrameManager.GetModuloMinute(frequency, currentTime);
            Assert.AreEqual(expected, actual);



            frequency = 1440;
            expected = new DateTime(2014, 1, 1, 0, 0, 0);
            actual = TimeFrameManager.GetModuloMinute(frequency, currentTime);
            Assert.AreEqual(expected, actual);

            currentTime = new DateTime(2014, 1, 1, 23, 53, 0); // TODO: Initialize to an appropriate value
            expected = new DateTime(2014, 1, 1, 0, 0, 0);
            actual = TimeFrameManager.GetModuloMinute(frequency, currentTime);
            Assert.AreEqual(expected, actual);


            

        }
        [TestMethod]
        [ExpectedException(typeof(Exception),
    "Program doesn't support frequency higher than 1440")]
        public void TestFrequencyException(){
            TimeFrameManager.GetModuloMinute(1445, new DateTime());
        }

        /// <summary>
        ///A test for SaveTimeFrame
        ///</summary>
        [TestMethod()]
        public void SaveTimeFrameTest()
        {
            TimeFrameManager target = new TimeFrameManager(); // TODO: Initialize to an appropriate value
            string stockName = "AEV PM"; // TODO: Initialize to an appropriate value
            double price = 150F; // TODO: Initialize to an appropriate value
            string timeFrame = "MIN"; // TODO: Initialize to an appropriate value
            int frequency = 1440; // TODO: Initialize to an appropriate value
            int volume = 150; // TODO: Initialize to an appropriate value
            double ask = 151F;
            double bid = 149F;
            double askSize = 5;
            double bidSize = 5;
            double open = 1;
            DateTime currentTime = new DateTime(2013,1,1,1,0,0); // TODO: Initialize to an appropriate value
            target.SaveTimeFrame(stockName, price, timeFrame, frequency, volume, currentTime, ask, askSize, bid, bidSize,open);


            COMClassLibrary.FMServiceReference.StockServiceClient c = new COMClassLibrary.FMServiceReference.StockServiceClient();
            PriceWithTimeFrameTO[] list = c.GetTimeFrameDescending(stockName, frequency, timeFrame, 1,currentTime);
            if (list != null && list.Length > 0)
            {
               Assert.IsTrue( Double.Equals(Convert.ToDouble(list[0].Price), price));
            }


        }

    

        /// <summary>
        ///A test for GetTimeFrame
        ///</summary>
        [TestMethod()]
        public void GetTimeFrameTest()
        {
            TimeFrameManager target = new TimeFrameManager();
            string stockName = "FMETF PM"; 
            string timeFrame = "MIN"; 
            int frequency = 1; 
            int period = 1; // TODO: Initialize to an appropriate value
            DateTime cutoffDate = DateTime.Now ; // TODO: Initialize to an appropriate value
            
            PriceWithTimeFrameTO[] actual;
            actual = target.GetTimeFrame(stockName, timeFrame, frequency, period, cutoffDate);
            Assert.IsTrue(1 == actual.Length);
        }
    }
}
