﻿using COMClassLibrary.Manager;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace ProjTester
{
    
    
    /// <summary>
    ///This is a test class for VolumeCalculatorTest and is intended
    ///to contain all VolumeCalculatorTest Unit Tests
    ///</summary>
    [TestClass()]
    public class VolumeCalculatorTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for CalculateMovingAverage
        ///</summary>
        [TestMethod()]
        public void CalculateMovingAverageTest()
        {
            VolumeCalculator target = new VolumeCalculator(); // TODO: Initialize to an appropriate value
            List<int> list = new List<int>();
            
            list.Add(2);

            list.Add(4);
            list.Add(6);
            list.Add(8);
            double expected = 5; // TODO: Initialize to an appropriate value
            double actual;
            actual = target.CalculateMovingAverage(list);
            Assert.AreEqual(expected, actual);
        }
    }
}
