﻿using COMClassLibrary.Manager;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using COMClassLibrary;

namespace ProjTester
{
    
    
    /// <summary>
    ///This is a test class for RSIBreakoutStrategyTest and is intended
    ///to contain all RSIBreakoutStrategyTest Unit Tests
    ///</summary>
    [TestClass()]
    public class RSIBreakoutStrategyTest
    {
        Dictionary<string, object> parameters = null;
        String stockName;
        RSIBreakoutStrategy target = null;

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        [TestInitialize]
        public void Initialize()
        {
            target = new RSIBreakoutStrategy();
            parameters = new Dictionary<string, object>();
            //stockName = "AEV PM";
            stockName = "FMETF PM";
            target.SetParameters(stockName, "stockName", stockName, parameters);


            target.SetParameters(stockName, "bid", 153.23, parameters);
            target.SetParameters(stockName, "ask", 154.50, parameters);
            target.SetParameters(stockName, "last", 154.00, parameters);
            target.SetParameters(stockName, "period", 14, parameters);


            target.SetParameters(stockName, "startingX", 41, parameters);
            target.SetParameters(stockName, "frequency", 1440, parameters);
            target.SetParameters(stockName, "timeFrame", "MIN", parameters);
            target.SetParameters(stockName, Constants.RSIStatus, Constants.RSIWaitingMode, parameters);
            target.SetParameters(stockName, "requiredDerivative", 0.001, parameters);
            target.SetParameters(stockName, "requiredSeparation", 1.05, parameters);
            target.SetParameters(stockName, "volumePeriod", 14, parameters);
            target.SetParameters(stockName, "requiredVolume", 1.3, parameters);

            target.SetParameters(stockName, "profitTargetLookupPeriod", 10, parameters);
            target.SetParameters(stockName, "stopLossLookupPeriod", 10, parameters);
            //7% loss
            target.SetParameters(stockName, "requiredAbsoluteStopLossFactor", 0.93, parameters);
            target.SetParameters(stockName, "techniStockfilePath", @"C:\temp\technistock\orders.csv", parameters);

            //just pretend we got the right amount
            target.SetParameters(stockName, "capital", 1000000.00, parameters);
            target.SetParameters(stockName, "capitalMultiplier", 0.8, parameters);
            //capital divisor is the baseline or the minimum capital
            target.SetParameters(stockName, "capitalDivisor", 5, parameters);

            //pending or partial.  check if that stock has no pending status. or someone already sent an unfilled order
            //a db call on quotation screen.

            target.SetParameters(stockName, "volume", 15678, parameters);//
            target.SetParameters(stockName, "lineRegressionRSIPeriod", 40, parameters);//
            target.SetParameters(stockName, "rsiPeriod", 14, parameters);//
            target.SetParameters(stockName, "cutoffDate", DateTime.Now, parameters);//


        }
        /// <summary>
        ///A test for IsAbsoluteStopLossReached
        ///</summary>
        [TestMethod()]
        [DeploymentItem("COMClassLibrary.dll")]
        public void IsAbsoluteStopLossReachedTest()
        {
            RSIBreakoutStrategy_Accessor target = new RSIBreakoutStrategy_Accessor(); // TODO: Initialize to an appropriate value
            double percentage = 0.93; // TODO: Initialize to an appropriate value
            double currentPrice = 150; // TODO: Initialize to an appropriate value
            double entryPrice = 130; // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            actual = target.IsAbsoluteStopLossReached(percentage, currentPrice, entryPrice);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for GetRSIList
        ///</summary>
        [TestMethod()]
        public void GetRSIListTest()
        {

            
            DateTime date = DateTime.Now;// DateTime.Parse("06/07/2014");
   
            target.SetParameters(stockName, "cutoffDate", date, parameters);
            target.GetRSIList(stockName, parameters);
            int length = (int)target.GetParameter(stockName, "lineRegressionRSIPeriod", parameters);
            List<double> yList = (List<double>) target.GetParameter(stockName, Constants.RSIX_AxisLinearRegressionPeriodList, parameters);
            Assert.IsTrue(length == yList.Count);
            
         }

        /// <summary>
        ///A test for GetRSI
        ///</summary>
        [TestMethod()]
        public void GetRSITest()
        {
            List<double> xlist = new List<double>();
            List<double> ylist = new List<double>();
            for (int i = 1; i <= 40; i++)
            {
                xlist.Add(i);
            }


            for (int i = 1; i <= 40; i++)
            {
                ylist.Add(3.0);
            }

            //set position 10, 20, 30 top 3 highest points
            ylist[9] = 10.0;
            ylist[19] = 5.0;
            ylist[29] = 8.0;

            target.SetParameters(stockName, Constants.RSIX_AxisLinearRegressionPeriodList,xlist, parameters);
            target.SetParameters(stockName, Constants.RSIY_AxisLinearRegressionPeriodList,ylist, parameters);

       
            RSI actual;
            actual = target.GetRSI(stockName, parameters);
            Assert.IsTrue(actual.Derivative < 0);
        }
   } 
}
