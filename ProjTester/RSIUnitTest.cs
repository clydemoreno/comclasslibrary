﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using COMClassLibrary.Manager;
using COMClassLibrary;
using COMClassLibrary.FMServiceReference;

namespace ProjTester
{
    [TestClass]
    public class RSIUnitTest
    {

        RSIDerivativeMessageHandler messageHandler;
        Dictionary<string, object> parameters = null;
        String stockName;
        RSIBreakoutStrategy orderManager = null;

        private void ChangeStatusToRSIWaitingMode()
        {
            orderManager.UpdateOrderStatus(stockName, Constants.RSIWaitingMode);
            

        }

        public RSIDerivativeMessageHandler MessageHandler
        {
            get
            {
                if (messageHandler == null)
                    messageHandler = new RSIDerivativeMessageHandler();
                return messageHandler;
            }
        }

        [TestMethod]
        public void TestGetOrderInfo()
        {
            
            orderManager.GetOrderInfo(stockName,parameters);
            OrderTO to = (OrderTO) orderManager.GetParameter(stockName, Constants.RSIOrderTOCode, parameters);
            Assert.IsTrue(to.StockName == stockName);
        }

        [TestMethod]
        public void TestGetOrderStatus(){

            orderManager = new RSIBreakoutStrategy();

            orderManager.GetOrderInfo(stockName,parameters);
            orderManager.GetOrderStatus(stockName, parameters);
        }

        [TestMethod]
        public void TestUpdateOrderStatus()
        {

            orderManager = new RSIBreakoutStrategy();

            orderManager.UpdateOrderStatus(stockName, Constants.RSIWaitingMode);
            orderManager.GetOrderInfo(stockName, parameters);
            orderManager.GetOrderStatus(stockName, parameters);

            String status = (String) orderManager.GetParameter(stockName, Constants.RSIStatus, parameters);
            Assert.IsTrue(status ==  Constants.RSIWaitingMode);
        }

        [TestMethod]
        public void TestGetRSIList()
        {
           
            orderManager.GetRSIList(stockName, parameters);
        }

        [TestInitialize]
        public void Initialize()
        {
            orderManager = new RSIBreakoutStrategy();
            parameters = new Dictionary<string, object>();
            stockName = "SMPH PM";
            orderManager.SetParameters(stockName, "stockName", stockName, parameters);


            orderManager.SetParameters(stockName, "bid", 153.23, parameters);
            orderManager.SetParameters(stockName, "ask", 154.50, parameters);
            orderManager.SetParameters(stockName, "last", 154.00, parameters);
            orderManager.SetParameters(stockName, "period", 14, parameters);


            orderManager.SetParameters(stockName, "startingX", 41, parameters);
            orderManager.SetParameters(stockName, "frequency", 1, parameters);
            orderManager.SetParameters(stockName, "timeFrame", "MIN", parameters);
            orderManager.SetParameters(stockName, Constants.RSIStatus, Constants.RSIWaitingMode, parameters);
            orderManager.SetParameters(stockName, "requiredDerivative", 0.001, parameters);
            orderManager.SetParameters(stockName, "requiredSeparation", 1.05, parameters);
            orderManager.SetParameters(stockName, "volumePeriod", 14, parameters);
            orderManager.SetParameters(stockName, "requiredVolume", 1.3, parameters);

            orderManager.SetParameters(stockName, "profitTargetLookupPeriod", 10, parameters);
            orderManager.SetParameters(stockName, "stopLossLookupPeriod", 10, parameters);
            //7% loss
            orderManager.SetParameters(stockName, "requiredAbsoluteStopLossFactor", 0.93, parameters);
            orderManager.SetParameters(stockName, "techniStockfilePath", @"C:\temp\technistock\orders.csv", parameters);

            //just pretend we got the right amount
            orderManager.SetParameters(stockName, "capital", 1000000.00, parameters);
            orderManager.SetParameters(stockName, "capitalMultiplier", 0.8, parameters);
            //capital divisor is the baseline or the minimum capital
            orderManager.SetParameters(stockName, "capitalDivisor", 5, parameters);

            //pending or partial.  check if that stock has no pending status. or someone already sent an unfilled order
            //a db call on quotation screen.

            orderManager.SetParameters(stockName, "volume", 15678, parameters);//
            orderManager.SetParameters(stockName, "lineRegressionRSIPeriod", 40, parameters);//
            orderManager.SetParameters(stockName, "rsiPeriod", 14, parameters);//
            orderManager.SetParameters(stockName, "cutoffDate", DateTime.Now, parameters);//

            BuildXAndYList(stockName);
    
        }

        private void GetOrderStatus(string stockName, String expectedStatus)
        {
            //OrderManager m = new OrderManager();
            string actualStatus = orderManager.GetOrderStatus(stockName);
            Assert.IsTrue(expectedStatus == actualStatus);
        }

        [TestMethod]
        public void TestDerivativeValue()
        {
          


          
        }



        public bool TestFileSearchText(String text)
        {

            String filePath = (String)orderManager.GetParameter(stockName, "techniStockfilePath", parameters);
            FileManager f = FileManager.Instance;
            return f.SearchFileContents(@filePath, text);


        }
        [TestMethod]
        public void TestCalculateRSILinearRegression()
        {
            double x = 41;
            double xfirstApex = 10, xsecondApex = 20, xthirdApex = 30;
            double yfirstApex = 10, ysecondApex = 5, ythirdApex = 8;
            var xdata = new double[] { xfirstApex, xsecondApex, xthirdApex };
            var ydata = new double[] { yfirstApex, ysecondApex, ythirdApex };
            RSICalculator calc = new RSICalculator();
            RSI rsi = calc.CalculateRSILinearRegression(x, xdata, ydata);

            //-0.10000000000000034 derivative
            //9.66666666666668  yintercept
            double y = (x * rsi.Derivative) + rsi.YIntercept;
            Assert.IsTrue(rsi.Derivative < -0.1);
            Assert.IsTrue(y > 5.5 && y < 5.6);
        }





        //[TestMethod]
        //public void TestGetRSIList()
        //{
        //    TradingController.GetRSIList(stockName, parameters);

        //    RSITO[] list = (RSITO[]) OrderManager.GetMessage(stockName, Constants.RSILinearRegressionPeriodList, parameters);

        //    Assert.IsTrue(list.Length == (int)OrderManager.GetMessage(stockName, "lineRegressionRSIPeriod", parameters));
        //}


        private void BuildXAndYList(string stockName)
        {
            List<double> xList = new List<double>();
            List<double> yList = new List<double>();

            for (int i = 0; i < 40; i++)
            {
                xList.Add(Convert.ToDouble(i + 1));
                yList.Add(5.0);
            }

            //set position 10, 20, 30 top 3 highest points
            yList[9] = 10.0;
            yList[19] = 11.0;
            yList[29] = 8.0;


            orderManager.SetParameters(stockName, Constants.RSIX_AxisLinearRegressionPeriodList, xList, parameters);

            orderManager.SetParameters(stockName, Constants.RSIY_AxisLinearRegressionPeriodList, yList, parameters);
        }



        [TestMethod]
        public void TestBuyOrderWithRightStatusAndRequiredDerivativeIsNotPassed()
        {

            //TradingController.GetRSIList(stockName, parameters);
            BuildXAndYList(stockName);
            //override
            orderManager.SetParameters(stockName, Constants.RSIStatus, String.Empty, parameters);
            orderManager.SetParameters(stockName, "requiredDerivative", -0.95, parameters);

            

            ChangeStatusToRSIWaitingMode();

           // OrderManager orderManager = new RSIBreakoutStrategy();

            //10% of capital

            orderManager.Changed += new ChangedEventHandler(mm_Changed);
            ((IStrategy)orderManager).Run(stockName, parameters);

        }

        void mm_Changed(object sender, EventArgs e)
        {
            if (e.GetType() == typeof(COMClassLibrary.Manager.MessageEventArg) && ((MessageEventArg)e).Code == Constants.RSIMessage)
            {
                string message = ((MessageEventArg)e).Message;
                Assert.IsTrue(message != Constants.RSILinearRegressionRequirementPassed);

            }
        }

        [TestMethod]
        public void TestCheckForVolume()
        {
            orderManager.SetParameters(stockName, "requiredVolume", 1.30, parameters);

            VolumeMATO volumeMA = new VolumeMATO();
            volumeMA.StockName = stockName;
            volumeMA.VolumeMAValue = 5000;
            orderManager.SetParameters(stockName, Constants.CurrentVolumeMATO, volumeMA, parameters);
            orderManager.SetParameters(stockName, "volume", 7000, parameters);
            
            Assert.IsTrue(((RSIBreakoutStrategy) orderManager).CheckForVolume(stockName, parameters));
        }

        private void PopulateGetPriceWithTimeFrameList()
        {
            PriceWithTimeFrameTO[]  priceList =  new PriceWithTimeFrameTO[10];
            for (int i = 0; i < 10; i++)
            {
                priceList[i] = new PriceWithTimeFrameTO();
                priceList[i].Price = Convert.ToDecimal((i + 150)); 
            }
            orderManager.SetParameters(stockName, Constants.RSIPriceWithTimeFrameList, priceList, parameters);
        }

        [TestMethod]
        public void TestBuyOrderRSIOrderFilledProfitTargetIsHit()
        {
            orderManager.SetParameters(stockName, Constants.RSIStatus, Constants.RSIOrderFilled, parameters);
           // OrderManager orderManager = new RSIBreakoutStrategy();
            orderManager.Changed += new ChangedEventHandler(rsiOrderFilledProfitTargetIsHit_Changed);

            OrderTO order = new OrderTO();
            order.LotSize = 50;
            order.Stoploss = 150.00;
            order.ProfitTarget = 159.00;
            
            orderManager.SetParameters(stockName, "last", 160.00, parameters);
            orderManager.SetParameters(stockName, Constants.RSIOrderTOCode, order, parameters);
            ((IStrategy)orderManager).Run(stockName, parameters);

        
            

        }
        void rsiOrderFilledProfitTargetIsHit_Changed(object sender, EventArgs e)
        {
            if (e.GetType() == typeof(COMClassLibrary.Manager.MessageEventArg) && ((MessageEventArg)e).Code == Constants.RSIStatus)
            {
                Assert.IsTrue(((MessageEventArg)e).Message == Constants.RSIWaitingMode);
                GetOrderStatus(stockName, ((MessageEventArg)e).Message);
                // this.SetParameters(((MessageEventArg)e).StockName, ((MessageEventArg)e).Code, ((MessageEventArg)e).Message);
                Assert.IsTrue(TestFileSearchText(stockName));
            }

            if (e.GetType() == typeof(COMClassLibrary.Manager.MessageEventArg) && ((MessageEventArg)e).Code == Constants.RSIProfitTargetHit)
            {
                Assert.IsTrue(((MessageEventArg)e).Message == Constants.RSIProfitTargetHit);
            }
           
        }


        [TestMethod]
        public void TestBuyOrderRSIOrderFilledStopLossIsHit()
        {
            orderManager.SetParameters(stockName, Constants.RSIStatus, Constants.RSIOrderFilled, parameters);
            //OrderManager orderManager = new RSIBreakoutStrategy();
            orderManager.Changed += new ChangedEventHandler(rsiOrderFilledStopLossIsHit_Changed);

            OrderTO order = new OrderTO();
            order.LotSize = 50;
            order.Stoploss = 150.00;
            order.ProfitTarget = 159.00;

            orderManager.SetParameters(stockName, "last", 149.00, parameters);
            orderManager.SetParameters(stockName, Constants.RSIOrderTOCode, order, parameters);
            ((IStrategy)orderManager).Run(stockName, parameters);




        }
        void rsiOrderFilledStopLossIsHit_Changed(object sender, EventArgs e)
        {
            if (e.GetType() == typeof(COMClassLibrary.Manager.MessageEventArg) && ((MessageEventArg)e).Code == Constants.RSIStatus)
            {
                Assert.IsTrue(((MessageEventArg)e).Message == Constants.RSIWaitingMode);
                GetOrderStatus(stockName, ((MessageEventArg)e).Message);
                // this.SetParameters(((MessageEventArg)e).StockName, ((MessageEventArg)e).Code, ((MessageEventArg)e).Message);
                Assert.IsTrue(TestFileSearchText(stockName));
            }

            if (e.GetType() == typeof(COMClassLibrary.Manager.MessageEventArg) && ((MessageEventArg)e).Code == Constants.RSIStoppedOut)
            {
                Assert.IsTrue(((MessageEventArg)e).Message == Constants.RSIStoppedOut);
            }

        }



        [TestMethod]
        public void TestBuyOrderRSIWaitForVolumeThreshold()
        {

            //TradingController.GetRSIList(stockName, parameters);
            //override
            orderManager.SetParameters(stockName, Constants.RSIStatus, Constants.RSIWaitForVolumeThreshold, parameters);
            orderManager.SetParameters(stockName, "requiredVolume", 1.30, parameters);
            RSITO rto = new RSITO();
            rto.StockName = stockName;
            rto.RsiValue = 30.0;
            orderManager.SetParameters(stockName, Constants.CurrentRSITO, rto, parameters);
            //RSI rsi = new RSI();
            //rsi.Derivative = -0.0005;
            //rsi.Y = 5.45;
            //rsi.YIntercept = 5.45;
            //orderManager.SetParameters(stockName, Constants.CurrentRSIDerivative,rsi , parameters);

            VolumeMATO volumeMA = new VolumeMATO();
            volumeMA.VolumeMAValue = 5000;
            orderManager.SetParameters(stockName, Constants.CurrentVolumeMATO, volumeMA, parameters);

            PopulateGetPriceWithTimeFrameList();
            
            OrderManager m = new RSIBreakoutStrategy();

            //10% of capital
            orderManager.SetParameters(stockName, "volume", 5000, parameters);
            m.Changed += new ChangedEventHandler(rsiWaitForVolumeThreshold_Changed);

            ((IStrategy)m).Run(stockName, parameters);
           
            orderManager.SetParameters(stockName, "volume", 3000, parameters);
            ((IStrategy)m).Run(stockName, parameters);

            orderManager.SetParameters(stockName, "volume", 7190, parameters);
            ((IStrategy)m).Run(stockName, parameters);
            
            

        }
        void rsiWaitForVolumeThreshold_Changed(object sender, EventArgs e)
        {
            if (e.GetType() == typeof(COMClassLibrary.Manager.MessageEventArg) && ((MessageEventArg)e).Code == Constants.RSIStatus)
            {
                Assert.IsTrue(((MessageEventArg)e).Message == Constants.RSIWaitingToBeFilled);
                GetOrderStatus(stockName, ((MessageEventArg)e).Message);
                // this.SetParameters(((MessageEventArg)e).StockName, ((MessageEventArg)e).Code, ((MessageEventArg)e).Message);
                Assert.IsTrue(TestFileSearchText(stockName));
            }
            if (e.GetType() == typeof(COMClassLibrary.Manager.MessageEventArg) && ((MessageEventArg)e).Code == Constants.RSIStopLossCode)
            {

                double stopLoss = Convert.ToDouble(orderManager.GetParameter(stockName, Constants.RSIStopLossCode, parameters));
                // this.SetParameters(((MessageEventArg)e).StockName, ((MessageEventArg)e).Code, ((MessageEventArg)e).Message);
                Assert.IsTrue(stopLoss == 150);
            }
            if (e.GetType() == typeof(COMClassLibrary.Manager.MessageEventArg) && ((MessageEventArg)e).Code == Constants.RSIProfitTargetCode)
            {

                double profitTarget = Convert.ToDouble(orderManager.GetParameter(stockName, Constants.RSIProfitTargetCode, parameters));
                // this.SetParameters(((MessageEventArg)e).StockName, ((MessageEventArg)e).Code, ((MessageEventArg)e).Message);
                Assert.IsTrue(profitTarget == 159.00);
            }

            if (e.GetType() == typeof(COMClassLibrary.Manager.MessageEventArg) && ((MessageEventArg)e).Code == Constants.RSIAllocatedCapitalCode)
            {

                double capital = Convert.ToDouble(orderManager.GetParameter(stockName, Constants.RSIAllocatedCapitalCode, parameters));
                // this.SetParameters(((MessageEventArg)e).StockName, ((MessageEventArg)e).Code, ((MessageEventArg)e).Message);
                Assert.IsTrue(capital > 266666.00 && capital < 266667.00);
            }
            if (e.GetType() == typeof(COMClassLibrary.Manager.MessageEventArg) && ((MessageEventArg)e).Code == Constants.RSILotsCode)
            {

                double lots = Convert.ToDouble(orderManager.GetParameter(stockName, Constants.RSILotsCode, parameters));
                // this.SetParameters(((MessageEventArg)e).StockName, ((MessageEventArg)e).Code, ((MessageEventArg)e).Message);
                Assert.IsTrue(lots == 1730);
            }
            
        }

        [TestMethod]
        public void TestBuyOrderDerivativeIsPassed()
        {

            //TradingController.GetRSIList(stockName, parameters);
            BuildXAndYList(stockName);
            //override
            orderManager.SetParameters(stockName, Constants.RSIStatus, Constants.RSIRequiredDerivativeIsMet, parameters);
            orderManager.SetParameters(stockName, "requiredSeparation", 1.05, parameters);
            RSITO rto = new RSITO();
            rto.StockName = stockName;
            rto.RsiValue = 5.45;
            orderManager.SetParameters(stockName, Constants.CurrentRSITO,rto , parameters);
            RSI rsi = new RSI();
            rsi.Derivative = -0.0005;
            rsi.Y = 5.45;
            rsi.YIntercept = 5.45;
            orderManager.SetParameters(stockName, Constants.CurrentRSIDerivative,rsi , parameters);
           

           // OrderManager orderManager = new RSIBreakoutStrategy();

            //10% of capital

            orderManager.Changed += new ChangedEventHandler(mmm_Changed);

            ((IStrategy)orderManager).Run(stockName, parameters);
           
            rto.RsiValue = 5.50;
            orderManager.SetParameters(stockName, Constants.CurrentRSITO, rto, parameters);
            ((IStrategy)orderManager).Run(stockName, parameters);

            rto.RsiValue = 6.50;
            orderManager.SetParameters(stockName, Constants.CurrentRSITO, rto, parameters);
            ((IStrategy)orderManager).Run(stockName, parameters);
            
            

        }
        void mmm_Changed(object sender, EventArgs e)
        {
            if (e.GetType() == typeof(COMClassLibrary.Manager.MessageEventArg) && ((MessageEventArg)e).Code == Constants.RSIStatus)
            {
                Assert.IsTrue(((MessageEventArg)e).Message == Constants.RSIWaitForVolumeThreshold);
                GetOrderStatus(stockName, ((MessageEventArg)e).Message);
                // this.SetParameters(((MessageEventArg)e).StockName, ((MessageEventArg)e).Code, ((MessageEventArg)e).Message);
            }
            if (e.GetType() == typeof(COMClassLibrary.Manager.MessageEventArg) && ((MessageEventArg)e).Code == Constants.IncrementingX)
            {

                double incrementingX = Convert.ToDouble(orderManager.GetParameter(stockName, Constants.IncrementingX, parameters));
                // this.SetParameters(((MessageEventArg)e).StockName, ((MessageEventArg)e).Code, ((MessageEventArg)e).Message);
                Assert.IsTrue(incrementingX == 0.00);
            }

        }

        [TestMethod]
        public void TestBuyOrderWithNoStatusAndRequiredDerivativeIsPassed()
        {

            //TradingController.GetRSIList(stockName, parameters);
            BuildXAndYList(stockName);
            //override
            orderManager.SetParameters(stockName, Constants.RSIStatus, String.Empty, parameters);

            ChangeStatusToRSIWaitingMode();

            //OrderManager orderManager = new RSIBreakoutStrategy();
      
            //10% of capital

            orderManager.Changed += new ChangedEventHandler(m_Changed);
            orderManager.Run(stockName, parameters);
           // ((IStrategy)orderManager).Run(stockName, parameters);

  
        }


        void m_Changed(object sender, EventArgs e)
        {
            //expected events
            //if could come from any object.

            //this.MessageHandler.Handle(parameters, e);

           //(( IMessageHandler)


            if(e.GetType() != typeof(COMClassLibrary.Manager.MessageEventArg)) return;
   
            MessageEventArg eventArg = (MessageEventArg)e;

            IMessageHandler handler = eventArg.MessageHandler;
            if(handler != null) handler.Handle(parameters,e);

            if (eventArg.Code == Constants.RSIDerivativeValue)
            {
                orderManager.GetOrderInfo(stockName, parameters);
                OrderTO order = (OrderTO)orderManager.GetParameter(stockName, Constants.RSIOrderTOCode, parameters);
                double expectedDerivative =  Math.Round( Convert.ToDouble(eventArg.Message),3);
                double actualDerivative = Math.Round(order.DerivativeValue,3);
                Assert.IsTrue(Double.Equals(actualDerivative, expectedDerivative));
            }


            if (e.GetType() == typeof(COMClassLibrary.Manager.MessageEventArg) && ((MessageEventArg)e).Code == Constants.RSIStatus
                && ((MessageEventArg)e).Message == Constants.RSIWaitingMode)
            {
                Assert.IsTrue(((MessageEventArg)e).Message ==  Constants.RSIWaitingMode );
                GetOrderStatus(stockName,((MessageEventArg)e).Message);
               // this.SetParameters(((MessageEventArg)e).StockName, ((MessageEventArg)e).Code, ((MessageEventArg)e).Message);
            }
            if (e.GetType() == typeof(COMClassLibrary.Manager.MessageEventArg) && ((MessageEventArg)e).Code == Constants.RSIStatus
    && ((MessageEventArg)e).Message == Constants.RSIRequiredDerivativeIsMet)
            {
                //todo:  save the status and set the derivative. make sure you finish the handler here.
                GetOrderStatus(stockName, ((MessageEventArg)e).Message);
                // this.SetParameters(((MessageEventArg)e).StockName, ((MessageEventArg)e).Code, ((MessageEventArg)e).Message);
            }

            if (e.GetType() == typeof(COMClassLibrary.Manager.MessageEventArg) && ((MessageEventArg)e).Code == Constants.RSIDerivativeValue)
            {
                double number;
                string derivativeMSG = ((MessageEventArg)e).Message    ;
                Assert.IsTrue(Double.TryParse(derivativeMSG,out number));
                
            }
            if (e.GetType() == typeof(COMClassLibrary.Manager.MessageEventArg) && ((MessageEventArg)e).Code == Constants.RSIMessage)
            {
                string message = ((MessageEventArg)e).Message    ;
                Assert.IsTrue(message == Constants.RSILinearRegressionRequirementPassed);
                
            }
           
            
        }
    }
}
