﻿using COMClassLibrary.Manager;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using COMClassLibrary.FMServiceReference;
using COMClassLibrary;
namespace ProjTester
{
    
    
    /// <summary>
    ///This is a test class for TradingControllerTest and is intended
    ///to contain all TradingControllerTest Unit Tests
    ///</summary>
    [TestClass()]
    public class TradingControllerTest
    {
        TradingController target;
        string stockName = "AEV PM"; // TODO: Initialize to an appropriate value
        double last = 150; // TODO: Initialize to an appropriate value
        double bid = 149.00;
        double ask = 151.00;
        double askSize = 5;
        double bidSize = 5;
        string timeFrame = "MIN"; // TODO: Initialize to an appropriate value
        int frequency = 1440; // TODO: Initialize to an appropriate value
        int rsiperiod = 14; // TODO: Initialize to an appropriate value
        // TODO: Initialize to an appropriate value
        int volume = 5000; // TODO: Initialize to an appropriate value
        double open = 1;
        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        public void IntializeIntradayParameters()
        {
        }


        [TestInitialize]
        public void Intialize()
        {
            target = new TradingController();

            IntializeIntradayParameters();

            InitializeRSIBreakout();

           // target.SetParameters();
        }

        private void InitializeRSIBreakout()
        {
            target.SetParameters(stockName, typeof(RSIBreakoutStrategy).ToString(), "stockName", stockName);
            target.SetParameters(stockName, typeof(RSIBreakoutStrategy).ToString(), "bid", 153.23);
            target.SetParameters(stockName, typeof(RSIBreakoutStrategy).ToString(), "ask", 154.50);
            target.SetParameters(stockName, typeof(RSIBreakoutStrategy).ToString(), "last", 154.00);
            target.SetParameters(stockName, typeof(RSIBreakoutStrategy).ToString(), "period", 14);


            target.SetParameters(stockName, typeof(RSIBreakoutStrategy).ToString(), "startingX", 41);
            target.SetParameters(stockName, typeof(RSIBreakoutStrategy).ToString(), "frequency", 1);
            target.SetParameters(stockName, typeof(RSIBreakoutStrategy).ToString(), "timeFrame", "MIN");
            target.SetParameters(stockName, typeof(RSIBreakoutStrategy).ToString(), Constants.RSIStatus, Constants.RSIWaitingMode);
            target.SetParameters(stockName, typeof(RSIBreakoutStrategy).ToString(), "requiredDerivative", 0.001);
            target.SetParameters(stockName, typeof(RSIBreakoutStrategy).ToString(), "requiredSeparation", 1.05);
            target.SetParameters(stockName, typeof(RSIBreakoutStrategy).ToString(), "volumePeriod", 14);
            target.SetParameters(stockName, typeof(RSIBreakoutStrategy).ToString(), "requiredVolume", 1.3);

            target.SetParameters(stockName, typeof(RSIBreakoutStrategy).ToString(), "profitTargetLookupPeriod", 10);
            target.SetParameters(stockName, typeof(RSIBreakoutStrategy).ToString(), "stopLossLookupPeriod", 10);
            //7% loss
            target.SetParameters(stockName, typeof(RSIBreakoutStrategy).ToString(), "requiredAbsoluteStopLossFactor", 0.93);
            target.SetParameters(stockName, typeof(RSIBreakoutStrategy).ToString(), "techniStockfilePath", @"C:\temp\technistock\orders.csv");

            //just pretend we got the right amount
            target.SetParameters(stockName, typeof(RSIBreakoutStrategy).ToString(), "capital", 1000000.00);
            target.SetParameters(stockName, typeof(RSIBreakoutStrategy).ToString(), "capitalMultiplier", 0.8);
            //capital divisor is the baseline or the minimum capital
            target.SetParameters(stockName, typeof(RSIBreakoutStrategy).ToString(), "capitalDivisor", 5);

            //pending or partial.  check if that stock has no pending status. or someone already sent an unfilled order
            //a db call on quotation screen.

            target.SetParameters(stockName, typeof(RSIBreakoutStrategy).ToString(), "volume", 15678);//
            target.SetParameters(stockName, typeof(RSIBreakoutStrategy).ToString(), "lineRegressionRSIPeriod", 40);//
            target.SetParameters(stockName, typeof(RSIBreakoutStrategy).ToString(), "rsiPeriod", 14);//
            target.SetParameters(stockName, typeof(RSIBreakoutStrategy).ToString(), "cutoffDate", DateTime.Now);//
            
        }
        /// <summary>
        ///A test for SaveTimeFrameAndRSI
        ///</summary>
        [TestMethod()]
        public void SaveTimeFrameAndRSITest()
        {
            //TradingController target = new TradingController(); // TODO: Initialize to an appropriate value
 
            //List<DateTime> dateList = new List<DateTime>();
            DateTime date = DateTime.MinValue ;


            int daysPositive = 5;
            int daysNegative = 4;

            for (int j = 1; j <= 5; j++)
            {
                for (int i = 1; i <= daysPositive  ; i++)
                {
                    date = new DateTime(2014, j, i, 0, 0, 0, 0);
                    last += 1;
                    //dateList.Add(date);
                    target.SaveTimeFrameAndRSI(stockName, last, timeFrame, frequency, rsiperiod, date, volume + i,ask,askSize,bid,bidSize,open);
                }
                for (int i = daysPositive; i <= daysPositive + daysNegative; i++)
                {
                    date = new DateTime(2014, j, i, 0, 0, 0, 0);
                    last -= 1;
                    //dateList.Add(date);
                    target.SaveTimeFrameAndRSI(stockName, last, timeFrame, frequency, rsiperiod, date, volume + i, ask, askSize, bid, bidSize,open);
                }
                new System.Threading.ManualResetEvent(false).WaitOne(500);

            }


            //for (int j = 1; j <= 4; j++)
            //{
            //    for (int i = 1; i <= 3; i++)
            //    {
            //        date = new DateTime(2014, j, i, 0, 0, 0, 0);
            //        //dateList.Add(date);
            //        target.SaveTimeFrameAndRSI(stockName, last - i - j, timeFrame, frequency, rsiperiod, date, volume + i);
            //    }
            //}


            TimeFrameManager m = new TimeFrameManager();
            PriceWithTimeFrameTO[] list = m.GetTimeFrame(stockName, timeFrame, frequency, rsiperiod,date);
            Assert.IsTrue(list.Length == rsiperiod);
            foreach (PriceWithTimeFrameTO item in list)
            {
                Assert.IsTrue(item.Frequency == frequency && item.StockName == stockName);
            }
            

           
        }

        /// <summary>
        ///A test for RunStrategy
        ///</summary>
        [TestMethod()]
        public void RunStrategyTest()
        {
            //TradingController target = new TradingController(); // TODO: Initialize to an appropriate value
            string expected = string.Empty; // TODO: Initialize to an appropriate value
            string actual;

            COMClassLibrary.FMServiceReference.StockServiceClient c = new StockServiceClient();
            c.UpdateDerivativeInOrder(stockName, "COMClassLibrary.Manager.RSIBreakoutStrategy", -0.005);
            c.UpdateOrderStatus(stockName, "COMClassLibrary.Manager.RSIBreakoutStrategy", Constants.RSIRequiredDerivativeIsMet);



            target.RunStrategy(stockName);
            //Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");

            new System.Threading.ManualResetEvent(false).WaitOne(5000);

            Assert.IsTrue(target.GetMessage(stockName, typeof(RSIBreakoutStrategy).ToString(), Constants.CurrentRSIDerivative) != null);

            //while(target.GetMessage(stockName,Constants.CurrentRSIDerivative) == null){

            //}
        }

        /// <summary>
        ///A test for RunMarketMaker
        ///</summary>
        [TestMethod()]
        public void RunMarketMakerTest()
        {
            target.RunMarketMaker(stockName);
            new System.Threading.ManualResetEvent(false).WaitOne(12000);
            string error = (string)target.GetMessage(stockName, typeof(MarketMakerStrategy).ToString(), Constants.MyRSIErrorCode);
            Assert.IsTrue(error != "");
        }

        /// <summary>
        ///A test for RunDiagnostics
        ///</summary>
        [TestMethod()]
        public void RunDiagnosticsTest()
        {
            TradingController target = new TradingController(); // TODO: Initialize to an appropriate value
            target.RunDiagnostics();

            string expected = "Up";

            string actual = (string) target.GetParameter("Diagnostics", "Diagnostics", "WebService");
            Assert.AreEqual(expected, actual);
        }
    }
}
