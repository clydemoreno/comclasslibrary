﻿using COMClassLibrary.Manager;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace ProjTester
{
    
    
    /// <summary>
    ///This is a test class for EmailManagerTest and is intended
    ///to contain all EmailManagerTest Unit Tests
    ///</summary>
    [TestClass()]
    public class EmailManagerTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for Notify
        ///</summary>
        [TestMethod()]
        public void NotifyTest()
        {
            string fromEmail = @"clydemoreno@mac.com"; // TODO: Initialize to an appropriate value
            string toEmail = @"clydemoreno@gmail.com"; // TODO: Initialize to an appropriate value
             // 
            Guid g = Guid.NewGuid();
            string subject = g.ToString();
            string host = string.Empty; // TODO: Initialize to an appropriate value
            string body = "test"; 
            EmailManager.Notify(fromEmail, toEmail, subject, body);
            Assert.IsTrue(EmailManager.CheckEmail(fromEmail,toEmail,subject));
            //Assert.Inconclusive("temp disabled.");
        }
    }
}
