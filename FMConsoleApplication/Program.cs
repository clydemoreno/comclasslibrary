﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using COMClassLibrary.FMServiceReference;
using COMClassLibrary.Manager;

namespace FMConsoleApplication
{
    class Program
    {
        static int Main(string[] args)
        {
            String pattern = @"<td\b[^>]*?>(?<V>[\s\S]*?)</\s*td>";
            String iNAVURL =   @"http://www.fmic.mdgms.com/iopv/quote.php";
            
            COMClassLibrary.Manager.CapitalCalculator calculator = new COMClassLibrary.Manager.CapitalCalculator();
            BoardLotFluctuationPriceTO[]  list = calculator.BoardLotList;

            double inavPrice = iNAVController.GetPrice(iNAVURL,pattern);
            double lowerPrice = calculator.GetLowerPriceByFlucOffset(inavPrice, 10);
            double higherPrice = calculator.GetHigherPriceByFlucOffset(inavPrice, 10);

            StockParser parser = new StockParser();
            Security fmetfSec = parser.GetPrice("FMETF");

            if(fmetfSec.Bids == null || fmetfSec.Bids.Count == 0) {
                Console.Error.WriteLine("No bids");
                return 1;
            }

            //calculator.GetFlucsBetweenINavAndBid(inavPrice, Convert.ToDouble( fmetfSec.Bids[0].price));
            //calculator.GetFlucsBetweenINavAndAsk(inavPrice, Convert.ToDouble(fmetfSec.Bids[0].price));
            int count  = calculator.GetFlucsBetweenBidAndAsk(Convert.ToDouble(fmetfSec.Bids[0].price), Convert.ToDouble(fmetfSec.Asks[0].price));

            Console.WriteLine("Spread between bid:{0} with bid vol:{3} and ask:{1} with ask vol:{4} is {2}", fmetfSec.Bids[0].price, fmetfSec.Asks[0].price, count, fmetfSec.Bids[0].volume, fmetfSec.Asks[0].volume);
            FileManager.Instance.AppendToFile(String.Format("{0},{1},{2},{3},{4},{5}", DateTime.Now, fmetfSec.Bids[0].price, fmetfSec.Asks[0].price, count, fmetfSec.Bids[0].volume, fmetfSec.Asks[0].volume));
            //fmetfSec.Asks
            
            
            Console.Read();
            return 0;
        }
    }
}
