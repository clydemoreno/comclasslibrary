﻿using System;
using System.Configuration;
using System.Drawing;
using System.Reflection;
namespace FirstMetroSystemTrayApp
{
    partial class SpreadAlertWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SpreadAlertWindow));
            this.runTimer = new System.Windows.Forms.Timer(this.components);
            this.violationTimer = new System.Windows.Forms.Timer(this.components);
            this.violationTime = new System.Windows.Forms.Label();
            this.priceLabel = new System.Windows.Forms.Label();
            this.notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.messageLabel = new System.Windows.Forms.Label();
            this.inavAskLbl = new System.Windows.Forms.Label();
            this.inavBidLbl = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // runTimer
            // 
            this.runTimer.Enabled = true;
            this.runTimer.Tick += new System.EventHandler(this.runTimer_Tick);
            // 
            // violationTimer
            // 
            this.violationTimer.Tick += new System.EventHandler(this.violationTimer_Tick);
            // 
            // violationTime
            // 
            this.violationTime.AutoSize = true;
            this.violationTime.Location = new System.Drawing.Point(70, 9);
            this.violationTime.Name = "violationTime";
            this.violationTime.Size = new System.Drawing.Size(33, 13);
            this.violationTime.TabIndex = 2;
            this.violationTime.Text = "Timer";
            // 
            // priceLabel
            // 
            this.priceLabel.AutoSize = true;
            this.priceLabel.Location = new System.Drawing.Point(39, 27);
            this.priceLabel.Name = "priceLabel";
            this.priceLabel.Size = new System.Drawing.Size(0, 13);
            this.priceLabel.TabIndex = 0;
            // 
            // notifyIcon
            // 
            this.notifyIcon.BalloonTipText = "FMSec App";
            this.notifyIcon.BalloonTipTitle = "FMSec App";
            this.notifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon.Icon")));
            this.notifyIcon.Text = "notifyIcon";
            this.notifyIcon.Visible = true;
            this.notifyIcon.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon_MouseDoubleClick);
            // 
            // messageLabel
            // 
            this.messageLabel.AutoSize = true;
            this.messageLabel.Location = new System.Drawing.Point(12, 87);
            this.messageLabel.Name = "messageLabel";
            this.messageLabel.Size = new System.Drawing.Size(0, 13);
            this.messageLabel.TabIndex = 1;
            // 
            // inavAskLbl
            // 
            this.inavAskLbl.AutoSize = true;
            this.inavAskLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.inavAskLbl.Location = new System.Drawing.Point(182, 4);
            this.inavAskLbl.Name = "inavAskLbl";
            this.inavAskLbl.Size = new System.Drawing.Size(82, 20);
            this.inavAskLbl.TabIndex = 3;
            this.inavAskLbl.Text = "inav flucs: ";
            // 
            // inavBidLbl
            // 
            this.inavBidLbl.AutoSize = true;
            this.inavBidLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.inavBidLbl.Location = new System.Drawing.Point(182, 28);
            this.inavBidLbl.Name = "inavBidLbl";
            this.inavBidLbl.Size = new System.Drawing.Size(82, 20);
            this.inavBidLbl.TabIndex = 4;
            this.inavBidLbl.Text = "inav flucs: ";
            // 
            // SpreadAlertWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(176, 57);
            this.Controls.Add(this.inavBidLbl);
            this.Controls.Add(this.inavAskLbl);
            this.Controls.Add(this.messageLabel);
            this.Controls.Add(this.priceLabel);
            this.Controls.Add(this.violationTime);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "SpreadAlertWindow";
            this.Text = "SpreadAlertWindow";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SpreadAlertWindow_FormClosing);
            this.Load += new System.EventHandler(this.SpreadAlertWindow_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer runTimer;
        private System.Windows.Forms.Timer violationTimer;
        private System.Windows.Forms.Label priceLabel;
        private System.Windows.Forms.NotifyIcon notifyIcon;
        private System.Windows.Forms.Label messageLabel;
        private System.Windows.Forms.Label violationTime;
        private System.Windows.Forms.Label inavAskLbl;
        private System.Windows.Forms.Label inavBidLbl;
    }
}