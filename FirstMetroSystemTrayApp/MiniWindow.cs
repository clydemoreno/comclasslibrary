﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using COMClassLibrary;
using COMClassLibrary.Manager;
using COMClassLibrary.Model;
using FirstMetroSystemTrayApp.Model;

namespace FirstMetroSystemTrayApp
{
    public partial class MiniWindow : Form
    {
        private double currentMMBid;
        private double currentMMOffer;
        private FMSecForm fmSecForm;
        


        public FMSecForm FmSecForm
        {
            get {
                if (fmSecForm == null)
                {
                    fmSecForm = new FMSecForm();
                    fmSecForm.WindowState = FormWindowState.Minimized;
                    fmSecForm.Show(this);
                    fmSecForm.Hide();
                    fmSecForm.MarketMakerStrategy.Changed += marketMakerStrategy_Changed;
                    fmSecForm.Load += fmSecForm_Load;
                    fmSecForm.TimeBetweenRefreshTextBox.LostFocus += timeBetweenRefreshTextBox_LostFocus;

                }
                return fmSecForm;
            }
            
        }

        void miniGroupBox_DoubleClick(object sender, EventArgs e)
        {
            if (!this.FmSecForm.IsDisposed)
            {

                this.FmSecForm.Show();
                this.FmSecForm.WindowState = FormWindowState.Normal;
                this.Hide();
            }
        }

        void fmSecForm_Load(object sender, EventArgs e)
        {
           // checkManualRefreshTimer.Interval = 
            SetInterval();
        }

        void timeBetweenRefreshTextBox_LostFocus(object sender, EventArgs e)
        {
            // checkManualRefreshTimer.Interval = 
            SetInterval();
        }

        private void SetInterval()
        {
            TextBox timeBetweenRefreshTextBox = this.FmSecForm.TimeBetweenRefreshTextBox;
            if (timeBetweenRefreshTextBox != null)
            {
                timeBetweenRefreshTextBox.Enabled = true;
                int time = 0;
                bool isOk = Int32.TryParse(timeBetweenRefreshTextBox.Text, out time);
                if (!isOk) time = 10 ;
                //check only every time interval 1000 millisecond minute ahead by subtracting one
                checkManualRefreshTimer.Interval = (time) * 1000;
                checkManualRefreshTimer.Start();
            }
        }
        public MiniWindow()
        {
            InitializeComponent();
        }


        private void marketMakerStrategy_Changed(object sender, EventArgs e)
        {
            // test this first
            if (sender.GetType() != typeof(Dictionary<String, object>)) return;



            Dictionary<String, object> parameters = (Dictionary<String, object>)sender;


            if (e.GetType() == typeof(COMClassLibrary.Manager.MMMaintainSpreadEventArg))
            {
                MMMaintainSpreadEventArg mmEventArg = (MMMaintainSpreadEventArg)e;


                //marketMakingLabel.Text = String.Format("MMBid:{0}   iNAV:{1}  MMOffer:{2}"
                //    , mmEventArg.matchingEngineParams.MMBid
                //    , mmEventArg.matchingEngineParams.INAV
                //    , mmEventArg.matchingEngineParams.MMOffer
                //    );
                //mmEventArg.matchingEngineParams

                //update inav


                //bool t = (currentMMBid != mmEventArg.matchingEngineParams.MMBid);
                //t = (currentMMOffer != mmEventArg.matchingEngineParams.MMOffer);
                if ((currentMMBid != mmEventArg.matchingEngineParams.MMBid))
                {
                    //mmBidPanel.BackColor = Color.Green;
                    //mmBidlLinkLabel.BackColor = SystemColors.Control;
                    //mmBidlLinkLabel.LinkColor = Color.Green;
                    //mmBidlLinkLabel.Font = new Font(mmBidlLinkLabel.Font, FontStyle.Bold);
                    mmBidButton.ForeColor = Color.Green;
                    mmBidButton.Font = new Font(mmBidButton.Font, FontStyle.Bold);


                  //  mmBidLinkLabel.BackColor = Color.White;
                }
                if ((currentMMOffer != mmEventArg.matchingEngineParams.MMOffer))
                {
                    //todo: to be deleted: aug 7, 14
                    //mmOfferPanel.BackColor = Color.Green;
                    //mmOfferLinkLabel.BackColor = SystemColors.Control;

                    //mmOfferLinkLabel.LinkColor = Color.Green;
                    //mmOfferLinkLabel.Font = new Font(mmOfferLinkLabel.Font, FontStyle.Bold);

                    mmOfferButton.ForeColor = Color.Green;
                   
                    mmOfferButton.Font = new Font(mmOfferButton.Font, FontStyle.Bold);
                }

                currentMMBid = mmEventArg.matchingEngineParams.MMBid;
                currentMMOffer = mmEventArg.matchingEngineParams.MMOffer;

                inavLinkLabel.Text = String.Format("{0:0.00}", mmEventArg.matchingEngineParams.INAV);
                //mmBidlLinkLabel.Text = String.Format("Php{0:0.00}", mmEventArg.matchingEngineParams.MMBid);
                mmBidButton.Text = String.Format("{0:0.00}", mmEventArg.matchingEngineParams.MMBid);
                //mmOfferLinkLabel.Text =  String.Format("Php{0:0.00}", mmEventArg.matchingEngineParams.MMOffer);
                mmOfferButton.Text = String.Format("{0:0.00}", mmEventArg.matchingEngineParams.MMOffer); ;


                //find out from gonz what 
                if (mmEventArg.matchingEngineParams.BidBalance < 0 || mmEventArg.matchingEngineParams.OfferBalance < 0)
                {
                    //marketMakingLabel.ForeColor = Color.Red;
                    //marketMakingLabel.BackColor = SystemColors.Control;
                }




                if (mmEventArg.matchingEngineParams.AreThereAnyBidsToCancel())
                {
                    //mmBidPanel.BackColor = Color.Red;
                    //mmBidlLinkLabel.BackColor = SystemColors.Control;
                    mmBidButton.ForeColor = Color.Red;
                    mmBidButton.Font = new Font(mmBidButton.Font, FontStyle.Bold);

                }
                //else
                //{
                //    mmBidButton.ForeColor = Color.Blue;
                //    mmBidButton.Font = new Font(mmBidButton.Font, FontStyle.Regular);
                //}
                if (mmEventArg.matchingEngineParams.AreThereAnyOffersToCancel())
                {
                    //mmOfferPanel.BackColor = Color.Red;
                    //mmOfferLinkLabel.BackColor = SystemColors.Control;
                    mmOfferButton.ForeColor = Color.Red;
                    mmOfferButton.Font = new Font(mmOfferButton.Font, FontStyle.Bold);
                }
                //else
                //{
                //    mmOfferButton.ForeColor = Color.Blue;
                //    mmOfferButton.Font = new Font(mmOfferButton.Font, FontStyle.Regular);
                //}

                Cursor.Current = Cursors.Default;
                Application.UseWaitCursor = false;




                


            }
            if (e.GetType() == typeof(COMClassLibrary.Manager.MessageEventArg))
            {

                MessageEventArg eventArg = (MessageEventArg)e;
                if (eventArg.Code == Constants.MyRSIErrorCode && eventArg.Message == Constants.iNAVUnableToGetMessage)
                {
                    //iNavTextPanel.BackColor = Color.Red;
                    
                    //inavLinkLabel.ForeColor = Color.Red;
                    //inavLinkLabel.BackColor = Color.White;
                    inavBackgroundPanel.BackColor = Color.Red;
                    iNavTextPanel.BackColor = SystemColors.Control;
                     
                    
                }
            }
            



//            groupBox1.Paint += PaintBorderlessGroupBox;

//private void PaintBorderlessGroupBox(object sender, PaintEventArgs p)
//{
//  GroupBox box = (GroupBox)sender;
//  p.Graphics.Clear(SystemColors.Control);
//  p.Graphics.DrawString(box.Text, box.Font, Brushes.Black, 0, 0);
//}


            if (e.GetType() == typeof(DiagnosticsEventArg) && ((DiagnosticsEventArg)e).code == "price")
            {
                bool isPriceServerRunning = (bool)sender;
                // priceServiceLabel.Text =isPriceServerRunning ? "Connected" : "Disconnected";

                if (isPriceServerRunning)
                {
                    this.BackColor = SystemColors.Control;
                }
                else
                {
                    this.BackColor = Color.Red;
                }

            }  

            //if (e.GetType() == typeof(DiagnosticsEventArg) && ((DiagnosticsEventArg)e).code == "price")
            //{
            //    bool isPriceServerRunning = (bool)sender;

                

            //    if (isPriceServerRunning)
            //    {
            //        this.BackColor = SystemColors.Control;
            //    }
            //    else
            //    {
            //        this.BackColor = Color.Red;
            //    }

            //}


        }


        private void MiniWindow_Load(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            Application.UseWaitCursor = true;

            mmBidButton.ForeColor = Color.Blue;
            mmOfferButton.ForeColor = Color.Blue;

            FMSecForm form = this.FmSecForm;
            this.Focus();
            Button miniButton = form.MiniButton;
            if (miniButton != null)
            {
                miniButton.Click += miniButton_Click;
                
            }
           
            
        }

        void miniGroupBox_Paint(object sender, PaintEventArgs p)
        {
            GroupBox box = (GroupBox)sender;
            p.Graphics.Clear(SystemColors.Control);
            p.Graphics.DrawString(box.Text, box.Font, Brushes.Black, 0, 0);
        }

        void miniButton_Click(object sender, EventArgs e)
        {
            this.Show();
            this.FmSecForm.Hide();
        }


        private void MiniWindow_DoubleClick(object sender, EventArgs e)
        {
            if (!this.FmSecForm.IsDisposed)
            {

                this.FmSecForm.Show();
                this.FmSecForm.WindowState = FormWindowState.Normal;
                this.WindowState = FormWindowState.Normal;
                this.Hide();
            }
        }

        private void notifyIcon_DoubleClick(object sender, EventArgs e)
        {
            this.Show();
            WindowState = FormWindowState.Normal;

        }

        //private void MiniWindow_ResizeEnd(object sender, EventArgs e)
        //{
        //    Minimize(sender, e);
        //}
        private void MinimizeOld(object sender, System.EventArgs e)
        {


            this.WindowState = FormWindowState.Minimized;
            if (FormWindowState.Minimized == WindowState)
            {
                Hide();    
            }
            //if (FormWindowState.Maximized == WindowState)
            //{
            //    MiniWindow_DoubleClick(sender, e);
                
            //}
            
        }

        private void MiniWindow_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.FmSecForm.Dispose();
           

        }

        //private void MiniWindow_SizeChanged(object sender, EventArgs e)
        //{
        //    Minimize(sender, e);
        //}

        private void checkManualRefreshTimer_Tick(object sender, EventArgs e)
        {

            //if true then form has been manually updated with control O data.
            //this.marketMakingLabel.ForeColor = this.FmSecForm.IsFormManuallyRefreshed() ? Color.Black : Color.Red;
           
            //temp only.  check if we can disable this for good
            if (!this.FmSecForm.IsFormManuallyRefreshed())
            {
                //tempCtrolOTextBox.BackColor = Color.Red;
                dropBoxTextBox.BackColor = Color.Red;
            }
            else
            {
                dropBoxTextBox.BackColor  = SystemColors.Control;
            }

            this.BackColor = this.FmSecForm.BackColor == Color.Red ? Color.Red :  SystemColors.Control ;
            this.groupPanel.BackColor = SystemColors.Control;
            rsiMiniWindowLabel.Enabled = this.FmSecForm.RSINegativeLabel.Enabled;
            rsiMiniWindowLabel.Text = this.FmSecForm.RSIStockLabel.Text;

            //inavLinkLabel.TabStop = iNavTextPanel.ForeColor == Color.Red;
        }

        private async void ParseText()
        {
            MatchingEngineParams p = this.fmSecForm.MatchingEngineParams;
            String text = dropBoxTextBox.Text;
            if(String.IsNullOrEmpty(text)) return;

            dropBoxTextBox.BackColor = Color.Green;
            //await Task.Delay(3000);

            Task<List<ImportedOrderScreen>> task = Task.Run(() => ImportPopupWindowController.Popup(text, p.Buyer, p.Seller));
            List<ImportedOrderScreen> list = await task;


            this.fmSecForm.ImportPopWindow_Changed(list, new ImportWindowPopUpEventArg());
            
            

            //OnChanged(list, new ImportWindowPopUpEventArg());

           
            dropBoxTextBox.Text = "";
            dropBoxTextBox.BackColor = SystemColors.Control;

        }

        //public async MyMethod()
        //{
            //String text = tempCtrolOTextBox.Text;
            //tempCtrolOTextBox.BackColor = Color.Green; 
            
            
            ////var result = await Task.Delay(2000);

            //tempCtrolOTextBox.Text = "";
            //tempCtrolOTextBox.BackColor = SystemColors.Control;
            
        //}

        private void tempCtrolOTextBox_TextChanged(object sender, EventArgs e)
        {
            //Task.Run(() => Foo());
            if (this.fmSecForm.MatchingEngineParams == null)
            {
                MessageBox.Show("No data yet");
                return;
            }
            
            ParseText();
            
        }

        private void miniGroupBox_Enter(object sender, EventArgs e)
        {

        }

        

        private void groupPanel_DoubleClick(object sender, EventArgs e)
        {
            if (!this.FmSecForm.IsDisposed)
            {

                this.FmSecForm.Show();
                this.FmSecForm.WindowState = FormWindowState.Normal;
                this.Hide();
            }
        }

        //private void mmBidLinkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        //{

        //    String displayMessage = String.Empty;
        //    if (mmBidPanel.BackColor == Color.Red)
        //    {

        //        LinkLabel link = (LinkLabel)sender;


        //         displayMessage = String.Format("Account ID:{0}\r\nPrice:{1}\r\n", "FMETF-MM", "111.02");
                
        //    }
        //    if (mmBidPanel.BackColor == Color.Green)
        //    {
        //         displayMessage = String.Format("Saving text file to {0}",@"c:\temp");

        //    }
        //    DialogResult result2 = MessageBox.Show(displayMessage,
        //    "CTRL-O >> Batch",
        //    MessageBoxButtons.OKCancel,
        //    MessageBoxIcon.Question);

        //        if (result2 == DialogResult.OK)
        //        {
        //            mmBidPanel.BackColor = SystemColors.Control;
        //        }
        //}

        private void inavLinkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            String displayMessage = string.Empty;


           // inavBackgroundPanel.BackColor == Color.Red;
            if (inavBackgroundPanel.BackColor != Color.Red) return;


                displayMessage = String.Format("iNAV is not updating. Please contact your System Administrator.");
                DialogResult result = MessageBox.Show(displayMessage,
    "iNAV",
    MessageBoxButtons.OKCancel,
    MessageBoxIcon.Information);

                if (result == DialogResult.OK)
                {
                    iNavTextPanel.BackColor = SystemColors.Control;
                }


        }


        const int WM_SYSCOMMAND = 0x0112;
        const int SC_MAXIMIZE = 0xF030;
        const int SC_MINIMIZE = 0xF020;

        protected override void WndProc(ref Message m)
        {
            if (m.Msg == WM_SYSCOMMAND)
            {
                switch ((int)m.WParam)
                {
                    case SC_MAXIMIZE:
                        //MessageBox.Show("Maximized");
                        MiniWindow_DoubleClick(null, EventArgs.Empty);

                        return;

                    case SC_MINIMIZE:
                        //MessageBox.Show("Minimized");

                        MinimizeOld(null, EventArgs.Empty);
                        return;
                }
            }
            base.WndProc(ref m);
        }


        //private void mmOfferLinkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        //{
        //    LinkLabel link = (LinkLabel)sender;


        //    String displayMessage = String.Format("Account ID:{0}\r\nPrice:{1}\r\n", "FMETF-MM", "111.02");
        //    DialogResult result2 = MessageBox.Show(displayMessage,
        //"CTRL-O >> Batch",
        //MessageBoxButtons.OKCancel,
        //MessageBoxIcon.Question);

        //    if (result2 == DialogResult.OK)
        //    {
        //        mmOfferPanel.BackColor = SystemColors.Control;
        //        mmOfferLinkLabel.BackColor = SystemColors.Control;
        //    }

        //}

        private void tempCtrolOTextBox_MouseClick(object sender, MouseEventArgs e)
        {
            dropBoxTextBox.Text = "";

            //tempCtrolOTextBox.ForeColor = Color.Green;
            //SendKeys.Send("^v");
            //ParseText();
        }

        //private void mmBidButton_Click(object sender, EventArgs e)
        //{
        //    if (sender.GetType() != typeof(Button)) return;

        //    Button button = (Button)sender;

        //    String displayMessage = String.Empty;
        //    if (mmBidButton.ForeColor == Color.Red)
        //    {



        //        displayMessage = this.fmSecForm.MatchingEngineParams.BuildTextForImportedOrderScreenList(
        //            this.fmSecForm.MatchingEngineParams.BidImportedOrderScreenList);

               
        //    }
        //    if (mmBidButton.ForeColor == Color.Green)
        //    {
        //        if (this.fmSecForm.MatchingEngineParams != null)
        //        {
        //            displayMessage = String.Format("Saving text file to {0}", @"c:\temp");
        //            MiniWindowController controller = new MiniWindowController();
        //            try
        //            {
        //                controller.WriteText(button.Name == "mmBidButton",  this.fmSecForm.MatchingEngineParams);
        //                mmBidButton.ForeColor = Color.Blue;
        //            }
        //            catch (Exception ex)
        //            {
        //                EventLog.WriteEntry(Constants.FMSecSource, ex.Message, EventLogEntryType.Error);
        //                displayMessage = String.Format("Unable to save the order to the path specified: {0}",this.fmSecForm.MatchingEngineParams.FilePath);
        //            }
        //        }
        //        else
        //        {
        //            displayMessage = String.Format("There are no values to be saved yet.  Check if iNAV is updating and/or price has been updated");

        //        }

        //    }

        //    if (mmBidButton.ForeColor == Color.Blue)
        //    {
        //        displayMessage = String.Format("Saving text file to {0}", @"c:\temp");

        //    }

        //    DialogResult result2 = MessageBox.Show(displayMessage,
        //    "CTRL-O >> Batch",
        //    MessageBoxButtons.OKCancel,
        //    MessageBoxIcon.Question);

        //    if (result2 == DialogResult.OK)
        //    {
        //        mmBidButton.ForeColor = Color.Blue;
        //    }

        //}

        private void mmButton_Click(object sender, EventArgs e)
        {

            Button button = (Button)sender;

            String displayMessage = String.Empty;

            if (button.ForeColor == Color.Blue)
            {
                displayMessage = String.Format("No Action is needed");

            }

            else if (button.ForeColor == Color.Red)
            {



                displayMessage = HandleRed(button, displayMessage);


            }
            else if (button.ForeColor == Color.Green)
            {
                displayMessage = HandleGreen(displayMessage, button);
            }

            


          DialogResult result2  =  MessageBox.Show(displayMessage,
"CTRL-O >> Batch",
MessageBoxButtons.OK,
MessageBoxIcon.Question);



          if (result2 == DialogResult.OK && button.ForeColor == Color.Red)
          {
              if (button.Name == "mmBidButton")
              {
                  this.fmSecForm.MatchingEngineParams.BidImportedOrderScreenList = null;
                  this.fmSecForm.BidImportedOrderScreenList = null;
                  //this.fmSecForm.BuyerListBox.Items.Clear();
              }
              else
              {
                  this.fmSecForm.MatchingEngineParams.AskImportedOrderScreenList = null;
                  this.fmSecForm.AskImportedOrderScreenList = null;
                  //this.fmSecForm.SellerListBox.Items.Clear();
              }

                button.ForeColor = Color.Blue;
              
          }
          else if (result2 == DialogResult.OK && button.ForeColor == Color.Green)
          {
              button.ForeColor = Color.Blue;
              //todo:
              //bring the saving of text file here.

          }



        }

        private string HandleRed(Button button, String displayMessage)
        {
            MiniWindowController controller = new MiniWindowController();
            controller.WriteTextForCancel(button.Name == "mmBidButton", this.fmSecForm.MatchingEngineParams);

            displayMessage = this.fmSecForm.MatchingEngineParams.BuildTextForImportedOrderScreenList(
            button.Name == "mmOfferButton" ? this.fmSecForm.MatchingEngineParams.AskImportedOrderScreenList
            : this.fmSecForm.MatchingEngineParams.BidImportedOrderScreenList);
            return displayMessage;
        }

        

        private string HandleGreen(String displayMessage, Button button)
        {
            if (this.fmSecForm.MatchingEngineParams != null)
            {
                displayMessage = String.Format("Saving text file to {0}", this.fmSecForm.MatchingEngineParams.FilePath);
                MiniWindowController controller = new MiniWindowController();
                try
                {
                    controller.WriteText(button.Name == "mmBidButton", this.fmSecForm.MatchingEngineParams);
                    mmBidButton.ForeColor = Color.Blue;
                }
                catch (Exception ex)
                {
                    EventLog.WriteEntry(Constants.FMSecSource, ex.Message, EventLogEntryType.Error);
                    displayMessage = String.Format("Unable to save the order to the path specified: {0}", this.fmSecForm.MatchingEngineParams.FilePath);
                }
            }
            else
            {
                displayMessage = String.Format("There are no values to be saved yet.  Check if iNAV is updating and/or price has been updated");

            }
            return displayMessage;
        }
    }
}
