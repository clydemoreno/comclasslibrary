﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using COMClassLibrary.Manager;
using System.IO;
using UserAuthentication;


namespace FirstMetroSystemTrayApp
{
    public partial class SpreadAlertWindow : Form
    {
        private StockParser p = null;

        public StockParser StockParser
        {
            get {
                if (p == null)
                {
                    p = new StockParser();
                    p.Connect();
                }
                return p; 
            }
        }

        const int WM_SYSCOMMAND = 0x0112;
        const int SC_MAXIMIZE = 0xF030;
        const int SC_MINIMIZE = 0xF020;

        protected override void WndProc(ref Message m)
        {
            if (m.Msg == WM_SYSCOMMAND)
            {
                switch ((int)m.WParam)
                {
                    case SC_MAXIMIZE:
                        notifyIcon_DoubleClick(null, EventArgs.Empty);
                        //MessageBox.Show("Maximized");
                        //notifyIcon_MouseDoubleClick(null, EventArgs.Empty);

                        return;

                    case SC_MINIMIZE:
                        //MessageBox.Show("Minimized");
                        OnLoad(EventArgs.Empty);
                        //MinimizeOld(null, EventArgs.Empty);
                        return;
                }
            }
            base.WndProc(ref m);
        }


        private async void RunAsync()
        {
            //changed to version 2 technistock datafeed
            var fmetfSec = await Task.Run(
                () => this.StockParser.RequestV2()
                );
            try
            {
                Process(fmetfSec);
            }
            catch (Exception ex)
            {
                LogManager.logError(ex.Message);
                LogManager.uploadErrorLog(ex.Message);
            }
            //return fmetfSec;
            //Task<string> web = 
                //Task.Run(() => Process());

            //string webResult = await web;

        }

        private async void RunViolationAsync()
        {
            var fmetfSec = await Task.Run(
                () => this.StockParser.RequestV2()
                );
            try
            {
                ProcessViolation(fmetfSec);
            }
            catch (Exception ex)
            {
                LogManager.logError(ex.Message);
                LogManager.uploadErrorLog(ex.Message);
            }
        }

        public SpreadAlertWindow()
        {
            //bool isAuthenticated = Auth.getAuth(Environment.UserName.ToLower());
            
            InitializeComponent();
            
            Init();
            //if (!isAuthenticated)
            //{
            //    DialogResult dr = MessageBox.Show("Not an authenticated user", "Warning", MessageBoxButtons.OK);
            //    if (dr == DialogResult.OK)
            //    {
            //        this.Close();
            //    }
            //    else
            //    {
            //        Application.Exit();
            //    }
            //}
            //else
            //{
                Application.ApplicationExit += new EventHandler(this.OnApplicationExit);
                try
                {
                    RunAsync();
                }
                catch (Exception ex)
                {
                    LogManager.uploadErrorLog(ex.Message);
                }
                //p = new StockParser();
                //p.Connect();
                //Run();
            //}
        }

        private void Init()
        {
            int seconds = 0;
            bool isValid = int.TryParse(ConfigurationManager.AppSettings["timeInterval"].ToString(), out seconds);
            if (!isValid) throw new Exception("Minute value from app config is invalid");
            this.runTimer.Interval = 1000 * seconds;
            this.violationTimer.Interval = 1000;
            //PlaceLowerRight();
        }

        private void OnApplicationExit(object sender, EventArgs e)
        {
            if(p != null) p.WebClient.Dispose();           
        }

        protected override void OnLoad(EventArgs e)
        {
            

            Visible = false; // Hide form window.
            ShowInTaskbar = false; // Remove from taskbar.
            var screen = Screen.FromPoint(this.Location);
            this.Location = new Point(screen.WorkingArea.Right - this.Width, screen.WorkingArea.Bottom - this.Height);
            //Point myPoint = new Point(this.Location.X + 15, this.Location.Y + 15);
            //this.violationTime.Location = myPoint;
            base.OnLoad(e);
        }

        private void notifyIcon_DoubleClick(object sender, EventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Normal;

        }

        //private void Run()
        //{
        //    String pattern = @"<td\b[^>]*?>(?<V>[\s\S]*?)</\s*td>";
        //    String iNAVURL = @"http://www.fmic.mdgms.com/iopv/quote.php";

        //    COMClassLibrary.Manager.CapitalCalculator calculator = new COMClassLibrary.Manager.CapitalCalculator();
        //    BoardLotFluctuationPriceTO[] list = calculator.BoardLotList;

        //    double inavPrice = iNAVController.GetPrice(iNAVURL, pattern);
        //    double lowerPrice = calculator.GetLowerPriceByFlucOffset(inavPrice, 10);
        //    double higherPrice = calculator.GetHigherPriceByFlucOffset(inavPrice, 10);

        //    StockParser parser = this.StockParser;
        //    Security fmetfSec = parser.GetPrice("FMETF");

        //    if (fmetfSec.Bids == null || fmetfSec.Bids.Count == 0)
        //    {
        //        throw new Exception("There are no bids");
        //    }
        //    if (fmetfSec.Bids == null || fmetfSec.Asks.Count == 0)
        //    {
        //        throw new Exception("There are no Asks");
        //    }
        //    //calculator.GetFlucsBetweenINavAndBid(inavPrice, Convert.ToDouble( fmetfSec.Bids[0].price));
        //    //calculator.GetFlucsBetweenINavAndAsk(inavPrice, Convert.ToDouble(fmetfSec.Bids[0].price));

        //    //log how long the count has been 
        //    int count = calculator.GetFlucsBetweenBidAndAsk(Convert.ToDouble(fmetfSec.Bids[0].price), Convert.ToDouble(fmetfSec.Asks[0].price));
        //    if (count > 10)
        //    {
        //        string message = "Bid and Ask spread along with volume are not within allowed threshold";
        //        MessageBox.Show(message);
        //        FileManager.Instance.AppendToFile(String.Format("{0},{1},{2},{3},{4},{5},{6}", DateTime.Now, fmetfSec.Bids[0].price, fmetfSec.Asks[0].price, count, fmetfSec.Bids[0].volume, fmetfSec.Asks[0].volume,message));
        //    }
        //    Console.WriteLine("Spread between bid:{0} with bid vol:{3} and ask:{1} with ask vol:{4} is {2}", fmetfSec.Bids[0].price, fmetfSec.Asks[0].price, count, fmetfSec.Bids[0].volume, fmetfSec.Asks[0].volume);
        //    //fmetfSec.Asks
            
 
        //}


        private void PlaceLowerRight()
        {
            //Determine "rightmost" screen
            Screen rightmost = Screen.AllScreens[0];
            foreach (Screen screen in Screen.AllScreens)
            {
                if (screen.WorkingArea.Right > rightmost.WorkingArea.Right)
                    rightmost = screen;
            }

            this.Left = rightmost.WorkingArea.Right - this.Width;
            this.Top = rightmost.WorkingArea.Bottom - this.Height;
        }
        private int counter = 1;
        private void runTimer_Tick(object sender, EventArgs e)
        {
            double startTime;
            bool isValid = double.TryParse( ConfigurationManager.AppSettings["startTradingTime"].ToString(),out startTime);
            double endTime;
            isValid = double.TryParse( ConfigurationManager.AppSettings["endTradingTime"].ToString(),out endTime);
            
            TimeSpan s = DateTime.Now.TimeOfDay;
            DateTime day = DateTime.Now;
            Console.WriteLine("Hours:{0}", s.Hours);
            if (Math.Round(s.TotalHours, 1) == startTime) counter = 1;

            if (Math.Round(s.TotalHours, 1) >= (endTime) && counter != 0)
            {
                counter = 0;
                violationGroup = 0;
                string path = String.Format("{0}{1}-violation.log", Environment.ExpandEnvironmentVariables(ConfigurationManager.AppSettings["violationLogPath"].ToString()), DateTime.Today.ToString("MMMM-dd-yyyy"));
                if (File.Exists(path))
                {
                    ReportManager.Create(path);
                }
                else
                {
                    ReportManager.Create();
                }
            }
                    
            try
            {
                RunAsync();
                messageLabel.Text = "";
            }
            catch (Exception ex)
            {
                messageLabel.Text = ex.Message;
            }
        
        }
        private int timenow = 0;
        private int timeBeforePreViolation = 180;
        private int timeBeforeViolation = 90;
        private void violationTimer_Tick(object sender, EventArgs e)
        {
           
            if (timeBeforePreViolation > 0)
                violationTime.Text = (timeBeforePreViolation--).ToString();
            if (timeBeforePreViolation <= 0)
                violationTime.Text = timeBeforeViolation--.ToString();
            if (timeBeforeViolation <= 0)
                violationTime.Text = timenow++.ToString();
            RunViolationAsync();
        }

        
        
        private int violationGroup = 0;
        private int notifyViolation = 0;
        private int minute = 60;
        private int monthlyEmail = 1;
        private void Process(Security fmetfSec)
        {
            double startTime;
            bool isValidTime = double.TryParse(ConfigurationManager.AppSettings["startTradingTime"].ToString(), out startTime);
            double endTime;
            isValidTime = double.TryParse(ConfigurationManager.AppSettings["endTradingTime"].ToString(), out endTime);

            double lunchTimeStart;
            bool isValidLunchTime = double.TryParse(ConfigurationManager.AppSettings["lunchStart"].ToString(), out lunchTimeStart);
            double lunchTimeEnd;
            isValidLunchTime = double.TryParse(ConfigurationManager.AppSettings["lunchEnd"].ToString(), out lunchTimeEnd);

            TimeSpan s = DateTime.Now.TimeOfDay;
            DateTime day = DateTime.Now;
            bool isOkToTrade = s.TotalHours >= startTime && s.TotalHours <= endTime;
            bool isLunchTime = s.TotalHours >= lunchTimeStart && s.TotalHours <= lunchTimeEnd;
            
            double emailTime;
            bool isEmailTimeValid = double.TryParse(ConfigurationManager.AppSettings["emailTime"].ToString(), out emailTime);

            if (!isEmailTimeValid) throw new Exception("email time not valid");

            //sends daily report every after trading time
            if (Math.Round(s.TotalHours, 1) >= (emailTime) && counter != 0)
            {
                counter = 0;
                violationGroup = 0;
                string path = String.Format(@"{0}\{1}\{2}-Violation.log", Environment.ExpandEnvironmentVariables(ConfigurationManager.AppSettings["violationLogPath"].ToString()), DateTime.Today.ToString("MMM-yyy"), DateTime.Today.ToString("MMM-dd"));
                if (File.Exists(path))
                {
                    ReportManager.Create(path);
                }
                else
                {
                    ReportManager.Create();
                }
            }

            if (!isOkToTrade || (isOkToTrade && isLunchTime))
            {               
                //LogManager.uploadData(null, 0, "Market is Closed");              
                this.violationTime.Text = "Market Closed";
                priceLabel.Text = "";
                return;
            } 
            Console.WriteLine(Math.Round(s.TotalHours, 1));
            Console.WriteLine(endTime - .1);
            Console.WriteLine(counter);

            //sends monthly report every 1st of the month
            if (day.ToString("dd") == "01" && monthlyEmail == 1)
            {
                ReportManager.CreateMonthlyReport(Environment.ExpandEnvironmentVariables(ConfigurationManager.AppSettings["violationLogPath"].ToString()), ConfigurationManager.AppSettings["logPath"].ToString());
                monthlyEmail = 0;
            }
            //resets the monthly counter to 1
            if (day.ToString("dd") == "02") monthlyEmail = 1;         
           
            //test this or refactor this to testable unit
            isOkToTrade = !day.ToString("dddd").Equals("Saturday") || !day.ToString("dddd").Equals("Sunday");
            if (!isOkToTrade) return;
            try
            {
                
                if (fmetfSec == null) throw new Exception("Security object is null");
                //Security fmetfSec = this.StockParser.Request("FMETF");
                if (fmetfSec.Bids == null || fmetfSec.Bids.Count == 0)
                {
                    Console.Error.WriteLine("No bids");

                }
                Console.WriteLine("Price:{0}", fmetfSec.lastPrice);

                 
                // if price did not change and secs,
                // use modulo to determine the min
                
                
                COMClassLibrary.Manager.CapitalCalculator calculator = new COMClassLibrary.Manager.CapitalCalculator();
                int spreadThreshold;
                string status = "No Violation";
                bool isValid = int.TryParse(ConfigurationManager.AppSettings["spreadThreshold"].ToString(), out spreadThreshold);
                if (!isValid) throw new Exception("Spread is not an integer in app.config");

                //string inavPattern = @"<td\b[^>]*?>(?<V>[\s\S]*?)</\s*td>";
                
                //double inav = iNAVController.GetPrice("http://www.fmic.mdgms.com/iopv/quote.php", inavPattern);
                //double inavAskFlucs = calculator.GetFlucsBetweenINavAndAsk(inav,Convert.ToDouble(fmetfSec.Asks[0].price));
                //double inavBidFlucs = calculator.GetFlucsBetweenINavAndBid(inav, Convert.ToDouble(fmetfSec.Bids[0].price));

                //inavAskLbl.Text = "inav ask flucs: " + inavAskFlucs.ToString();
                //inavBidLbl.Text = "inav bid flucs: " + inavBidFlucs.ToString();

                int count = calculator.GetFlucsBetweenBidAndAsk(Convert.ToDouble(fmetfSec.Bids[0].price), Convert.ToDouble(fmetfSec.Asks[0].price));
                if (count > spreadThreshold || fmetfSec.Bids[0].volume < 5 || fmetfSec.Asks[0].volume < 5)
                {
                    //start violation process
                    this.violationTimer.Start();
                }
                else
                {
                  
                    this.violationTimer.Stop();
                    minute = 60;
                    timenow = 0;
                    timeBeforePreViolation = 180;
                    timeBeforeViolation = 90;
                   
                    this.BackColor = (Color)System.Drawing.SystemColors.Window;
                    violationTime.Text = "";
                    LogManager.logData(fmetfSec, count, status);
                    LogManager.uploadData(fmetfSec, count, status);
                    if (notifyViolation != 0) notifyViolation = 0;
             
                    priceLabel.Text = status;
                }
                
            }
            catch (Exception ex)
            {
                LogManager.logError(ex.Message);
                LogManager.uploadErrorLog(ex.Message);
                //MessageBox.Show(ex.Message);
            }
        }
            
        private void ProcessViolation(Security fmetfSec)
        {
            COMClassLibrary.Manager.CapitalCalculator calculator = new COMClassLibrary.Manager.CapitalCalculator();
            string status = "No Violation";
            int count = calculator.GetFlucsBetweenBidAndAsk(Convert.ToDouble(fmetfSec.Bids[0].price), Convert.ToDouble(fmetfSec.Asks[0].price));
            if (timeBeforePreViolation <= 180 && timeBeforePreViolation >120)
            {
                this.BackColor = Color.Yellow;

                if (fmetfSec.Asks[0].price == 0 && fmetfSec.Bids[0].price == 0 && fmetfSec.Bids[0].volume == 0 && fmetfSec.Asks[0].volume == 0)
                {
                    status = "No Data Feed Available";
                }
                else{
                    status = "3-Minute Warning";
                }
       
                if(minute > 57)
                    ViolationManager.Beep();
                if (minute == 60)
                {
                    LogManager.logData(fmetfSec, count, status);
                    LogManager.uploadData(fmetfSec, count, status);
                }
            }
            else if (timeBeforePreViolation <= 120 && timeBeforePreViolation > 60)
            {
                this.BackColor = Color.Yellow;
                
                if (fmetfSec.Asks[0].price == 0 && fmetfSec.Bids[0].price == 0 && fmetfSec.Bids[0].volume == 0 && fmetfSec.Asks[0].volume == 0)
                {
                    status = "No Data Feed Available";
                }
                else
                {
                    status = "2-Minute Warning";
                }

                if (minute > 57)
                    ViolationManager.Beep();
                if (minute == 60)
                {
                    LogManager.logData(fmetfSec, count, status);
                    LogManager.uploadData(fmetfSec, count, status);
                }
            }
            else if (timeBeforePreViolation <= 60 && timeBeforePreViolation > 0)
            {
                this.BackColor = Color.Orange;            
                    if (fmetfSec.Asks[0].price == 0 && fmetfSec.Bids[0].price == 0 && fmetfSec.Bids[0].volume == 0 && fmetfSec.Asks[0].volume == 0)
                    {
                        status = "No Data Feed Available";
                        if (minute > 57)
                            ViolationManager.Beep();
                    }
                    else
                    {
                        ViolationManager.Beep(); 
                        status = "1-Minute Warning";
                    }
                if (minute == 60)
                {
                    LogManager.logData(fmetfSec, count, status);
                    LogManager.uploadData(fmetfSec, count, status);
                }
            }
            else if (timeBeforePreViolation <= 0 && timeBeforeViolation > 0)
            {              
                this.BackColor = Color.Orange;
                if (fmetfSec.Asks[0].price == 0 && fmetfSec.Bids[0].price == 0 && fmetfSec.Bids[0].volume == 0 && fmetfSec.Asks[0].volume == 0)
                {
                    if (minute > 57)
                        ViolationManager.Beep();
                    status = "No Data Feed Available";
                }
                else
                {
                    status = "Pre-Violation";
                    ViolationManager.Beep(); 
                }
               
                if (timeBeforeViolation == 89)
                {
                    LogManager.logData(fmetfSec, count, status);
                }
            }
            else if (timeBeforeViolation <= 0)
            {
                if (notifyViolation == 4) violationGroup++;
                this.BackColor = Color.Red;

                if (fmetfSec.Asks[0].price == 0 && fmetfSec.Bids[0].price == 0 && fmetfSec.Bids[0].volume == 0 && fmetfSec.Asks[0].volume == 0)
                {
                    if (minute > 57)
                        ViolationManager.Beep();
                    status = "No Data Feed Available";
                }
                else
                {
                    status = "Violation";
                    if (minute % 5 == 0)
                        ViolationManager.Beep();
                }
                string path = String.Format("{0}{1}-violation.log", Environment.ExpandEnvironmentVariables(ConfigurationManager.AppSettings["violationLogPath"].ToString()), DateTime.Today.ToString("MMMM-dd-yyyy"));
                if (minute == 28)
                {
                    LogManager.logData(fmetfSec, count, status);
                    if (status == "Violation") LogManager.violationLogData(fmetfSec, count, status);
                    
                   LogManager.uploadData(fmetfSec, count, status);
                }
          }
            Console.WriteLine(minute);
            minute--;
            priceLabel.Text = status;
            if (minute < 1) minute = 60;
        }
        private void notifyIcon_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Normal;
        }

        private void SpreadAlertWindow_Load(object sender, EventArgs e)
        {

        }

        private void SpreadAlertWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            //LogManager.logError(e.CloseReason.ToString());
            //LogManager.uploadErrorLog(e.CloseReason.ToString());
        }

        
        }
    }

