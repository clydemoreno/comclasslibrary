﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using COMClassLibrary;
using COMClassLibrary.Manager;

namespace FirstMetroSystemTrayApp
{
    public class MiniWindowController
    {
        public void WriteText()
        {
        }
        internal void WriteTextForCancel(bool isBuy, COMClassLibrary.Model.MatchingEngineParams matchingEngineParams)
        {

            FileManager manager = FileManager.Instance;
            String text = String.Empty;
            if (isBuy)
            {

                text = String.Format("CANCEL;{0};{1};{2};{3};{4}A;NORMAL;0;0;DAY;", matchingEngineParams.AccountID, Constants.FMETF, matchingEngineParams.BidVolume, Math.Round(matchingEngineParams.MMBid, 2), matchingEngineParams.Buyer);
            }
            else
            {
                text = String.Format("CANCEL;{0};{1};{2};{3};{4}A;NORMAL;0;0;DAY;", matchingEngineParams.AccountID, Constants.FMETF, matchingEngineParams.AskVolume, Math.Round(matchingEngineParams.MMBid, 2), matchingEngineParams.Buyer);
            }
            String header = "ACCOUNTID;SIDE;STOCK;QTY;PRICE;ACCOUNT;OCAP;MARKET;MINQTY;MAXFLOOR;TIF;EXPIRY;REMARKS";
            manager.WriteToAFile(String.Format("{0}\r\n{1}", header, text), matchingEngineParams.CancelFilePath);
        }

        internal void WriteText(bool isBuy, COMClassLibrary.Model.MatchingEngineParams matchingEngineParams)
        {

            FileManager manager = FileManager.Instance;
            String text = String.Empty;

            if (isBuy)
            {
                text = String.Format("BUY;{0};{1};{2};{3};A;NORMAL;0;0;DAY;", Constants.FMETF, matchingEngineParams.BidVolume , Math.Round( matchingEngineParams.MMBid,2), matchingEngineParams.Buyer);
            }
            else
            {
                text = String.Format("SELL;{0};{1};{2};{3};A;NORMAL;0;0;DAY;", Constants.FMETF, matchingEngineParams.AskVolume, Math.Round( matchingEngineParams.MMOffer,2), matchingEngineParams.Seller);

            }
            String header = "SIDE;STOCK;QTY;PRICE;ACCOUNT;OCAP;MARKET;MINQTY;MAXFLOOR;TIF;EXPIRY;REMARKS";
            manager.WriteToAFile(String.Format("{0}\r\n{1}", header, text), matchingEngineParams.FilePath);
        }
    }
}
