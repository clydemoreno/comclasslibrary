﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstMetroSystemTrayApp
{
    public class DiagnosticsEventArg : EventArgs
    {
        public String code;
        public DiagnosticsEventArg(String code)
        {
            this.code = code;
        }
    }
}
