﻿namespace FirstMetroSystemTrayApp
{
    partial class FMSecForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FMSecForm));
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.fmsecNotifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.displayToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bidLabel = new System.Windows.Forms.Label();
            this.ticketListBox = new System.Windows.Forms.ListBox();
            this.tabMenuControl = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.label51 = new System.Windows.Forms.Label();
            this.fmEtfStockTextBox = new System.Windows.Forms.TextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.cancelFileTextBox = new System.Windows.Forms.TextBox();
            this.label50 = new System.Windows.Forms.Label();
            this.cancelFilePathtextBox = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.fileNameTextBox = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.marketMakerFrequencyTextBox = new System.Windows.Forms.TextBox();
            this.label48 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.askVolumeTextBox = new System.Windows.Forms.TextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.bidVolumeTextBox = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.timebetweenRefreshLabel = new System.Windows.Forms.Label();
            this.timeBetweenRefreshTextBox = new System.Windows.Forms.TextBox();
            this.label = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.marketMakerBuyerTextBox = new System.Windows.Forms.TextBox();
            this.marketMakerSellerTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.minPostVolumeTextBox = new System.Windows.Forms.TextBox();
            this.skewViewTextBox = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.maxSpreadTextBox = new System.Windows.Forms.TextBox();
            this.marketMakerFilePathTextBox = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.fromEmailTextBox = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.emailSubscribersTextBox = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.iNAVPatternTextBox = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.iNAVLinkTextBox = new System.Windows.Forms.TextBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.label29 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.rsiTimeFrameTextBox = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.rsiFrequencyTextBox = new System.Windows.Forms.TextBox();
            this.rsiPeriodTextBox = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.addButton = new System.Windows.Forms.Button();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.listStocksLabel = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.marketMakerTab = new System.Windows.Forms.TabPage();
            this.bestOfferLabel = new System.Windows.Forms.Label();
            this.bestBidLabel = new System.Windows.Forms.Label();
            this.offerVolLabel = new System.Windows.Forms.Label();
            this.bidVolLabel = new System.Windows.Forms.Label();
            this.recommendedBidLabel = new System.Windows.Forms.Label();
            this.importButton = new System.Windows.Forms.Button();
            this.askLabel = new System.Windows.Forms.Label();
            this.iNAVValueLabel = new System.Windows.Forms.Label();
            this.defaOfferLabel = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.defaBidLabel = new System.Windows.Forms.Label();
            this.defaultOfferLabel = new System.Windows.Forms.Label();
            this.clientBlabel = new System.Windows.Forms.Label();
            this.clientALabel = new System.Windows.Forms.Label();
            this.sellerListBox = new System.Windows.Forms.ListBox();
            this.buyerListBox = new System.Windows.Forms.ListBox();
            this.recommendedOfferLabel = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.rsilistBox = new System.Windows.Forms.ListBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.rsiStockLabel = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.askBidRatioReachedLabel = new System.Windows.Forms.Label();
            this.askBidRatioLabel = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.rsiNegativeLabel = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label14 = new System.Windows.Forms.Label();
            this.recogniaStockLabel = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.bullishLabel = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.applicationServiceLabel = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.priceServiceLabel = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.chartTabPage = new System.Windows.Forms.TabPage();
            this.getChartButton = new System.Windows.Forms.Button();
            this.stockChartTextBox = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.stockChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.errorMessageLabel = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.runTimer = new System.Windows.Forms.Timer(this.components);
            this.diagnosticsTimer = new System.Windows.Forms.Timer(this.components);
            this.importedOrderScreenBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.fMSecFormBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.countdownTimer = new System.Windows.Forms.Timer(this.components);
            this.miniButton = new System.Windows.Forms.Button();
            this.strategyTimer = new System.Windows.Forms.Timer(this.components);
            this.intradayTimer = new System.Windows.Forms.Timer(this.components);
            this.outOfSpreadTitleLabel = new System.Windows.Forms.Label();
            this.isOutsideOfSpreadAnswerLabel = new System.Windows.Forms.Label();
            this.isOutsideOfSpreadButton = new System.Windows.Forms.Button();
            this.contextMenuStrip.SuspendLayout();
            this.tabMenuControl.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.tabPage7.SuspendLayout();
            this.tabPage8.SuspendLayout();
            this.marketMakerTab.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.chartTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.stockChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.importedOrderScreenBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fMSecFormBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // fmsecNotifyIcon
            // 
            this.fmsecNotifyIcon.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.fmsecNotifyIcon.BalloonTipText = "FirstMetroSec Notification Service";
            this.fmsecNotifyIcon.ContextMenuStrip = this.contextMenuStrip;
            this.fmsecNotifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("fmsecNotifyIcon.Icon")));
            this.fmsecNotifyIcon.Text = "Signals Indicator";
            this.fmsecNotifyIcon.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon1_MouseDoubleClick);
            // 
            // contextMenuStrip
            // 
            this.contextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.displayToolStripMenuItem,
            this.closeToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.contextMenuStrip.Name = "contextMenuStrip";
            this.contextMenuStrip.Size = new System.Drawing.Size(113, 70);
            // 
            // displayToolStripMenuItem
            // 
            this.displayToolStripMenuItem.Name = "displayToolStripMenuItem";
            this.displayToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
            this.displayToolStripMenuItem.Text = "Display";
            this.displayToolStripMenuItem.Click += new System.EventHandler(this.displayToolStripMenuItem_Click);
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
            this.closeToolStripMenuItem.Text = "Close";
            this.closeToolStripMenuItem.Click += new System.EventHandler(this.closeToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // bidLabel
            // 
            this.bidLabel.AutoSize = true;
            this.bidLabel.Location = new System.Drawing.Point(40, 37);
            this.bidLabel.Name = "bidLabel";
            this.bidLabel.Size = new System.Drawing.Size(49, 13);
            this.bidLabel.TabIndex = 2;
            this.bidLabel.Text = "Bid: 0.00";
            this.bidLabel.Click += new System.EventHandler(this.label1_Click);
            // 
            // ticketListBox
            // 
            this.ticketListBox.FormattingEnabled = true;
            this.ticketListBox.Items.AddRange(new object[] {
            "FMETF PM",
            "MBT PM"});
            this.ticketListBox.Location = new System.Drawing.Point(10, 12);
            this.ticketListBox.Name = "ticketListBox";
            this.ticketListBox.Size = new System.Drawing.Size(213, 95);
            this.ticketListBox.TabIndex = 3;
            // 
            // tabMenuControl
            // 
            this.tabMenuControl.Alignment = System.Windows.Forms.TabAlignment.Bottom;
            this.tabMenuControl.Controls.Add(this.tabPage2);
            this.tabMenuControl.Controls.Add(this.marketMakerTab);
            this.tabMenuControl.Controls.Add(this.tabPage1);
            this.tabMenuControl.Controls.Add(this.tabPage3);
            this.tabMenuControl.Controls.Add(this.chartTabPage);
            this.tabMenuControl.Location = new System.Drawing.Point(12, 44);
            this.tabMenuControl.Name = "tabMenuControl";
            this.tabMenuControl.SelectedIndex = 0;
            this.tabMenuControl.Size = new System.Drawing.Size(781, 277);
            this.tabMenuControl.TabIndex = 4;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.tabControl1);
            this.tabPage2.Location = new System.Drawing.Point(4, 4);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(773, 251);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Settings";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage7);
            this.tabControl1.Controls.Add(this.tabPage8);
            this.tabControl1.Location = new System.Drawing.Point(6, 15);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(761, 215);
            this.tabControl1.TabIndex = 20;
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.label51);
            this.tabPage6.Controls.Add(this.fmEtfStockTextBox);
            this.tabPage6.Controls.Add(this.label49);
            this.tabPage6.Controls.Add(this.cancelFileTextBox);
            this.tabPage6.Controls.Add(this.label50);
            this.tabPage6.Controls.Add(this.cancelFilePathtextBox);
            this.tabPage6.Controls.Add(this.label47);
            this.tabPage6.Controls.Add(this.fileNameTextBox);
            this.tabPage6.Controls.Add(this.label46);
            this.tabPage6.Controls.Add(this.groupBox1);
            this.tabPage6.Controls.Add(this.marketMakerFilePathTextBox);
            this.tabPage6.Controls.Add(this.label39);
            this.tabPage6.Controls.Add(this.fromEmailTextBox);
            this.tabPage6.Controls.Add(this.label37);
            this.tabPage6.Controls.Add(this.emailSubscribersTextBox);
            this.tabPage6.Controls.Add(this.label36);
            this.tabPage6.Controls.Add(this.iNAVPatternTextBox);
            this.tabPage6.Controls.Add(this.label35);
            this.tabPage6.Controls.Add(this.iNAVLinkTextBox);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(753, 189);
            this.tabPage6.TabIndex = 2;
            this.tabPage6.Text = "Market Maker";
            this.tabPage6.UseVisualStyleBackColor = true;
            this.tabPage6.Click += new System.EventHandler(this.tabPage6_Click);
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(273, 13);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(35, 13);
            this.label51.TabIndex = 46;
            this.label51.Text = "Stock";
            // 
            // fmEtfStockTextBox
            // 
            this.fmEtfStockTextBox.Location = new System.Drawing.Point(314, 11);
            this.fmEtfStockTextBox.Name = "fmEtfStockTextBox";
            this.fmEtfStockTextBox.Size = new System.Drawing.Size(39, 20);
            this.fmEtfStockTextBox.TabIndex = 45;
            this.fmEtfStockTextBox.Text = "FMETF PM";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(164, 150);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(90, 13);
            this.label49.TabIndex = 50;
            this.label49.Text = "Cancel File Name";
            // 
            // cancelFileTextBox
            // 
            this.cancelFileTextBox.Location = new System.Drawing.Point(258, 147);
            this.cancelFileTextBox.Name = "cancelFileTextBox";
            this.cancelFileTextBox.Size = new System.Drawing.Size(75, 20);
            this.cancelFileTextBox.TabIndex = 49;
            this.cancelFileTextBox.Text = "cancelOrders.csv";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(165, 124);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(84, 13);
            this.label50.TabIndex = 48;
            this.label50.Text = "Cancel File Path";
            // 
            // cancelFilePathtextBox
            // 
            this.cancelFilePathtextBox.Location = new System.Drawing.Point(257, 121);
            this.cancelFilePathtextBox.Name = "cancelFilePathtextBox";
            this.cancelFilePathtextBox.Size = new System.Drawing.Size(76, 20);
            this.cancelFilePathtextBox.TabIndex = 47;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(9, 151);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(54, 13);
            this.label47.TabIndex = 46;
            this.label47.Text = "File Name";
            // 
            // fileNameTextBox
            // 
            this.fileNameTextBox.Location = new System.Drawing.Point(74, 148);
            this.fileNameTextBox.Name = "fileNameTextBox";
            this.fileNameTextBox.Size = new System.Drawing.Size(75, 20);
            this.fileNameTextBox.TabIndex = 45;
            this.fileNameTextBox.Text = "orders.csv";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(9, 125);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(48, 13);
            this.label46.TabIndex = 44;
            this.label46.Text = "File Path";
            this.label46.Click += new System.EventHandler(this.label46_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.marketMakerFrequencyTextBox);
            this.groupBox1.Controls.Add(this.label48);
            this.groupBox1.Controls.Add(this.label45);
            this.groupBox1.Controls.Add(this.askVolumeTextBox);
            this.groupBox1.Controls.Add(this.label44);
            this.groupBox1.Controls.Add(this.bidVolumeTextBox);
            this.groupBox1.Controls.Add(this.label42);
            this.groupBox1.Controls.Add(this.timebetweenRefreshLabel);
            this.groupBox1.Controls.Add(this.timeBetweenRefreshTextBox);
            this.groupBox1.Controls.Add(this.label);
            this.groupBox1.Controls.Add(this.label41);
            this.groupBox1.Controls.Add(this.marketMakerBuyerTextBox);
            this.groupBox1.Controls.Add(this.marketMakerSellerTextBox);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.minPostVolumeTextBox);
            this.groupBox1.Controls.Add(this.skewViewTextBox);
            this.groupBox1.Controls.Add(this.label40);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.maxSpreadTextBox);
            this.groupBox1.Location = new System.Drawing.Point(383, 8);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(353, 172);
            this.groupBox1.TabIndex = 21;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "General";
            // 
            // marketMakerFrequencyTextBox
            // 
            this.marketMakerFrequencyTextBox.Location = new System.Drawing.Point(87, 134);
            this.marketMakerFrequencyTextBox.Name = "marketMakerFrequencyTextBox";
            this.marketMakerFrequencyTextBox.Size = new System.Drawing.Size(39, 20);
            this.marketMakerFrequencyTextBox.TabIndex = 44;
            this.marketMakerFrequencyTextBox.Text = "1";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(22, 137);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(57, 13);
            this.label48.TabIndex = 43;
            this.label48.Text = "Frequency";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(17, 107);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(63, 13);
            this.label45.TabIndex = 42;
            this.label45.Text = "Ask Volume";
            // 
            // askVolumeTextBox
            // 
            this.askVolumeTextBox.Location = new System.Drawing.Point(87, 106);
            this.askVolumeTextBox.Name = "askVolumeTextBox";
            this.askVolumeTextBox.Size = new System.Drawing.Size(39, 20);
            this.askVolumeTextBox.TabIndex = 41;
            this.askVolumeTextBox.Text = "5";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(17, 79);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(60, 13);
            this.label44.TabIndex = 40;
            this.label44.Text = "Bid Volume";
            // 
            // bidVolumeTextBox
            // 
            this.bidVolumeTextBox.Location = new System.Drawing.Point(87, 76);
            this.bidVolumeTextBox.Name = "bidVolumeTextBox";
            this.bidVolumeTextBox.Size = new System.Drawing.Size(39, 20);
            this.bidVolumeTextBox.TabIndex = 39;
            this.bidVolumeTextBox.Text = "5";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(22, 47);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(55, 13);
            this.label42.TabIndex = 38;
            this.label42.Text = "in Minutes";
            // 
            // timebetweenRefreshLabel
            // 
            this.timebetweenRefreshLabel.AutoSize = true;
            this.timebetweenRefreshLabel.Location = new System.Drawing.Point(6, 21);
            this.timebetweenRefreshLabel.Name = "timebetweenRefreshLabel";
            this.timebetweenRefreshLabel.Size = new System.Drawing.Size(186, 13);
            this.timebetweenRefreshLabel.TabIndex = 37;
            this.timebetweenRefreshLabel.Text = "Time allowed between manual update";
            // 
            // timeBetweenRefreshTextBox
            // 
            this.timeBetweenRefreshTextBox.Location = new System.Drawing.Point(87, 47);
            this.timeBetweenRefreshTextBox.Name = "timeBetweenRefreshTextBox";
            this.timeBetweenRefreshTextBox.Size = new System.Drawing.Size(39, 20);
            this.timeBetweenRefreshTextBox.TabIndex = 36;
            this.timeBetweenRefreshTextBox.Text = "5";
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.Location = new System.Drawing.Point(214, 17);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(81, 13);
            this.label.TabIndex = 28;
            this.label.Text = "iNav Bid Offset:";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(190, 138);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(33, 13);
            this.label41.TabIndex = 34;
            this.label41.Text = "Seller";
            // 
            // marketMakerBuyerTextBox
            // 
            this.marketMakerBuyerTextBox.Location = new System.Drawing.Point(232, 104);
            this.marketMakerBuyerTextBox.Name = "marketMakerBuyerTextBox";
            this.marketMakerBuyerTextBox.Size = new System.Drawing.Size(107, 20);
            this.marketMakerBuyerTextBox.TabIndex = 25;
            this.marketMakerBuyerTextBox.Text = "FMETF-MM";
            // 
            // marketMakerSellerTextBox
            // 
            this.marketMakerSellerTextBox.Location = new System.Drawing.Point(232, 134);
            this.marketMakerSellerTextBox.Name = "marketMakerSellerTextBox";
            this.marketMakerSellerTextBox.Size = new System.Drawing.Size(107, 20);
            this.marketMakerSellerTextBox.TabIndex = 35;
            this.marketMakerSellerTextBox.Text = "G00101-3";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(189, 108);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 24;
            this.label1.Text = "Buyer";
            // 
            // minPostVolumeTextBox
            // 
            this.minPostVolumeTextBox.Location = new System.Drawing.Point(300, 66);
            this.minPostVolumeTextBox.Name = "minPostVolumeTextBox";
            this.minPostVolumeTextBox.Size = new System.Drawing.Size(34, 20);
            this.minPostVolumeTextBox.TabIndex = 33;
            this.minPostVolumeTextBox.Text = "50";
            // 
            // skewViewTextBox
            // 
            this.skewViewTextBox.Location = new System.Drawing.Point(300, 14);
            this.skewViewTextBox.Name = "skewViewTextBox";
            this.skewViewTextBox.Size = new System.Drawing.Size(34, 20);
            this.skewViewTextBox.TabIndex = 29;
            this.skewViewTextBox.Text = "0";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(210, 69);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(89, 13);
            this.label40.TabIndex = 32;
            this.label40.Text = "Min Post Volume:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(217, 43);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 13);
            this.label4.TabIndex = 30;
            this.label4.Text = "Max Spread:";
            // 
            // maxSpreadTextBox
            // 
            this.maxSpreadTextBox.Location = new System.Drawing.Point(300, 40);
            this.maxSpreadTextBox.Name = "maxSpreadTextBox";
            this.maxSpreadTextBox.Size = new System.Drawing.Size(34, 20);
            this.maxSpreadTextBox.TabIndex = 31;
            this.maxSpreadTextBox.Text = "10";
            // 
            // marketMakerFilePathTextBox
            // 
            this.marketMakerFilePathTextBox.Location = new System.Drawing.Point(74, 122);
            this.marketMakerFilePathTextBox.Name = "marketMakerFilePathTextBox";
            this.marketMakerFilePathTextBox.Size = new System.Drawing.Size(76, 20);
            this.marketMakerFilePathTextBox.TabIndex = 43;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(82, 94);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(33, 13);
            this.label39.TabIndex = 27;
            this.label39.Text = "From:";
            // 
            // fromEmailTextBox
            // 
            this.fromEmailTextBox.Location = new System.Drawing.Point(136, 91);
            this.fromEmailTextBox.Multiline = true;
            this.fromEmailTextBox.Name = "fromEmailTextBox";
            this.fromEmailTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.fromEmailTextBox.Size = new System.Drawing.Size(226, 25);
            this.fromEmailTextBox.TabIndex = 26;
            this.fromEmailTextBox.Text = "jmarcelino@firstmetrosec.com.ph";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(25, 65);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(90, 13);
            this.label37.TabIndex = 23;
            this.label37.Text = "Email Subscribers";
            // 
            // emailSubscribersTextBox
            // 
            this.emailSubscribersTextBox.Location = new System.Drawing.Point(136, 62);
            this.emailSubscribersTextBox.Multiline = true;
            this.emailSubscribersTextBox.Name = "emailSubscribersTextBox";
            this.emailSubscribersTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.emailSubscribersTextBox.Size = new System.Drawing.Size(226, 25);
            this.emailSubscribersTextBox.TabIndex = 22;
            this.emailSubscribersTextBox.Text = "mpentinio@firstmetrosec.com.ph\r\n";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(10, 35);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(68, 13);
            this.label36.TabIndex = 20;
            this.label36.Text = "iNAV Pattern";
            // 
            // iNAVPatternTextBox
            // 
            this.iNAVPatternTextBox.Location = new System.Drawing.Point(79, 32);
            this.iNAVPatternTextBox.Name = "iNAVPatternTextBox";
            this.iNAVPatternTextBox.Size = new System.Drawing.Size(160, 20);
            this.iNAVPatternTextBox.TabIndex = 21;
            this.iNAVPatternTextBox.Text = "<td\\b[^>]*?>(?<V>[\\s\\S]*?)</\\s*td>";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(10, 11);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(50, 13);
            this.label35.TabIndex = 18;
            this.label35.Text = "iNAV link";
            // 
            // iNAVLinkTextBox
            // 
            this.iNAVLinkTextBox.Location = new System.Drawing.Point(79, 8);
            this.iNAVLinkTextBox.Name = "iNAVLinkTextBox";
            this.iNAVLinkTextBox.Size = new System.Drawing.Size(160, 20);
            this.iNAVLinkTextBox.TabIndex = 19;
            this.iNAVLinkTextBox.Text = "http://www.fmic.mdgms.com/iopv/quote.php";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.label29);
            this.tabPage4.Controls.Add(this.label31);
            this.tabPage4.Controls.Add(this.rsiTimeFrameTextBox);
            this.tabPage4.Controls.Add(this.textBox4);
            this.tabPage4.Controls.Add(this.label30);
            this.tabPage4.Controls.Add(this.label33);
            this.tabPage4.Controls.Add(this.rsiFrequencyTextBox);
            this.tabPage4.Controls.Add(this.rsiPeriodTextBox);
            this.tabPage4.Controls.Add(this.textBox7);
            this.tabPage4.Controls.Add(this.label32);
            this.tabPage4.Controls.Add(this.textBox6);
            this.tabPage4.Controls.Add(this.label34);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(753, 189);
            this.tabPage4.TabIndex = 0;
            this.tabPage4.Text = "RSI";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(211, 56);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(59, 13);
            this.label29.TabIndex = 6;
            this.label29.Text = "TimeFrame";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(66, 52);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(55, 13);
            this.label31.TabIndex = 10;
            this.label31.Text = "Vol Period";
            // 
            // rsiTimeFrameTextBox
            // 
            this.rsiTimeFrameTextBox.Location = new System.Drawing.Point(271, 54);
            this.rsiTimeFrameTextBox.Name = "rsiTimeFrameTextBox";
            this.rsiTimeFrameTextBox.Size = new System.Drawing.Size(34, 20);
            this.rsiTimeFrameTextBox.TabIndex = 7;
            this.rsiTimeFrameTextBox.Text = "MIN";
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(127, 49);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(34, 20);
            this.textBox4.TabIndex = 11;
            this.textBox4.Text = "14";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(211, 80);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(57, 13);
            this.label30.TabIndex = 8;
            this.label30.Text = "Frequency";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(15, 24);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(104, 13);
            this.label33.TabIndex = 14;
            this.label33.Text = "RSI Line Regression";
            // 
            // rsiFrequencyTextBox
            // 
            this.rsiFrequencyTextBox.Location = new System.Drawing.Point(271, 78);
            this.rsiFrequencyTextBox.Name = "rsiFrequencyTextBox";
            this.rsiFrequencyTextBox.Size = new System.Drawing.Size(34, 20);
            this.rsiFrequencyTextBox.TabIndex = 9;
            this.rsiFrequencyTextBox.Text = "3";
            // 
            // rsiPeriodTextBox
            // 
            this.rsiPeriodTextBox.Location = new System.Drawing.Point(271, 22);
            this.rsiPeriodTextBox.Name = "rsiPeriodTextBox";
            this.rsiPeriodTextBox.Size = new System.Drawing.Size(34, 20);
            this.rsiPeriodTextBox.TabIndex = 13;
            this.rsiPeriodTextBox.Text = "14";
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(128, 75);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(34, 20);
            this.textBox7.TabIndex = 17;
            this.textBox7.Text = "-0.1";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(211, 24);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(37, 13);
            this.label32.TabIndex = 12;
            this.label32.Text = "Period";
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(125, 20);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(34, 20);
            this.textBox6.TabIndex = 15;
            this.textBox6.Text = "40";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(23, 78);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(100, 13);
            this.label34.TabIndex = 16;
            this.label34.Text = "Breakout Threshold";
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.ticketListBox);
            this.tabPage5.Controls.Add(this.textBox1);
            this.tabPage5.Controls.Add(this.addButton);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(753, 189);
            this.tabPage5.TabIndex = 1;
            this.tabPage5.Text = "Tickers";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(109, 129);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(62, 20);
            this.textBox1.TabIndex = 4;
            // 
            // addButton
            // 
            this.addButton.Location = new System.Drawing.Point(177, 127);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(46, 23);
            this.addButton.TabIndex = 5;
            this.addButton.Text = "Add";
            this.addButton.UseVisualStyleBackColor = true;
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.textBox12);
            this.tabPage7.Controls.Add(this.label38);
            this.tabPage7.Location = new System.Drawing.Point(4, 22);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Size = new System.Drawing.Size(753, 189);
            this.tabPage7.TabIndex = 3;
            this.tabPage7.Text = "Recognia";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // textBox12
            // 
            this.textBox12.Location = new System.Drawing.Point(93, 10);
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(255, 20);
            this.textBox12.TabIndex = 10;
            this.textBox12.Text = "Breakout";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(12, 13);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(63, 13);
            this.label38.TabIndex = 7;
            this.label38.Text = "Subscribed:";
            // 
            // tabPage8
            // 
            this.tabPage8.Controls.Add(this.listStocksLabel);
            this.tabPage8.Controls.Add(this.label5);
            this.tabPage8.Controls.Add(this.textBox11);
            this.tabPage8.Location = new System.Drawing.Point(4, 22);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Size = new System.Drawing.Size(753, 189);
            this.tabPage8.TabIndex = 4;
            this.tabPage8.Text = "Intraday";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // listStocksLabel
            // 
            this.listStocksLabel.AutoSize = true;
            this.listStocksLabel.Location = new System.Drawing.Point(17, 16);
            this.listStocksLabel.Name = "listStocksLabel";
            this.listStocksLabel.Size = new System.Drawing.Size(45, 13);
            this.listStocksLabel.TabIndex = 28;
            this.listStocksLabel.Text = "Portfolio";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(582, 21);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(85, 13);
            this.label5.TabIndex = 26;
            this.label5.Text = "Ask to Bid Skew";
            // 
            // textBox11
            // 
            this.textBox11.Location = new System.Drawing.Point(669, 18);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(34, 20);
            this.textBox11.TabIndex = 27;
            this.textBox11.Text = "0.10";
            // 
            // marketMakerTab
            // 
            this.marketMakerTab.Controls.Add(this.bestOfferLabel);
            this.marketMakerTab.Controls.Add(this.bestBidLabel);
            this.marketMakerTab.Controls.Add(this.offerVolLabel);
            this.marketMakerTab.Controls.Add(this.bidVolLabel);
            this.marketMakerTab.Controls.Add(this.recommendedBidLabel);
            this.marketMakerTab.Controls.Add(this.importButton);
            this.marketMakerTab.Controls.Add(this.askLabel);
            this.marketMakerTab.Controls.Add(this.iNAVValueLabel);
            this.marketMakerTab.Controls.Add(this.defaOfferLabel);
            this.marketMakerTab.Controls.Add(this.label43);
            this.marketMakerTab.Controls.Add(this.bidLabel);
            this.marketMakerTab.Controls.Add(this.defaBidLabel);
            this.marketMakerTab.Controls.Add(this.defaultOfferLabel);
            this.marketMakerTab.Controls.Add(this.clientBlabel);
            this.marketMakerTab.Controls.Add(this.clientALabel);
            this.marketMakerTab.Controls.Add(this.sellerListBox);
            this.marketMakerTab.Controls.Add(this.buyerListBox);
            this.marketMakerTab.Controls.Add(this.recommendedOfferLabel);
            this.marketMakerTab.Controls.Add(this.label22);
            this.marketMakerTab.Controls.Add(this.label3);
            this.marketMakerTab.Controls.Add(this.label2);
            this.marketMakerTab.Location = new System.Drawing.Point(4, 4);
            this.marketMakerTab.Name = "marketMakerTab";
            this.marketMakerTab.Size = new System.Drawing.Size(773, 251);
            this.marketMakerTab.TabIndex = 3;
            this.marketMakerTab.Text = "MarketMaker";
            this.marketMakerTab.UseVisualStyleBackColor = true;
            // 
            // bestOfferLabel
            // 
            this.bestOfferLabel.AutoSize = true;
            this.bestOfferLabel.Location = new System.Drawing.Point(534, 216);
            this.bestOfferLabel.Name = "bestOfferLabel";
            this.bestOfferLabel.Size = new System.Drawing.Size(81, 13);
            this.bestOfferLabel.TabIndex = 19;
            this.bestOfferLabel.Text = "Best Offer: 0.00";
            // 
            // bestBidLabel
            // 
            this.bestBidLabel.AutoSize = true;
            this.bestBidLabel.Location = new System.Drawing.Point(157, 216);
            this.bestBidLabel.Name = "bestBidLabel";
            this.bestBidLabel.Size = new System.Drawing.Size(73, 13);
            this.bestBidLabel.TabIndex = 18;
            this.bestBidLabel.Text = "Best Bid: 0.00";
            // 
            // offerVolLabel
            // 
            this.offerVolLabel.AutoSize = true;
            this.offerVolLabel.Location = new System.Drawing.Point(147, 61);
            this.offerVolLabel.Name = "offerVolLabel";
            this.offerVolLabel.Size = new System.Drawing.Size(67, 13);
            this.offerVolLabel.TabIndex = 17;
            this.offerVolLabel.Text = "Bid Vol: 0.00";
            // 
            // bidVolLabel
            // 
            this.bidVolLabel.AutoSize = true;
            this.bidVolLabel.Location = new System.Drawing.Point(40, 61);
            this.bidVolLabel.Name = "bidVolLabel";
            this.bidVolLabel.Size = new System.Drawing.Size(67, 13);
            this.bidVolLabel.TabIndex = 9;
            this.bidVolLabel.Text = "Bid Vol: 0.00";
            // 
            // recommendedBidLabel
            // 
            this.recommendedBidLabel.AutoSize = true;
            this.recommendedBidLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.recommendedBidLabel.Location = new System.Drawing.Point(300, 62);
            this.recommendedBidLabel.Name = "recommendedBidLabel";
            this.recommendedBidLabel.Size = new System.Drawing.Size(37, 24);
            this.recommendedBidLabel.TabIndex = 15;
            this.recommendedBidLabel.Text = "Bid";
            // 
            // importButton
            // 
            this.importButton.Location = new System.Drawing.Point(697, 6);
            this.importButton.Name = "importButton";
            this.importButton.Size = new System.Drawing.Size(71, 54);
            this.importButton.TabIndex = 8;
            this.importButton.Text = "Import Order View";
            this.importButton.UseVisualStyleBackColor = true;
            this.importButton.Click += new System.EventHandler(this.importButton_Click);
            // 
            // askLabel
            // 
            this.askLabel.AutoSize = true;
            this.askLabel.Location = new System.Drawing.Point(147, 37);
            this.askLabel.Name = "askLabel";
            this.askLabel.Size = new System.Drawing.Size(52, 13);
            this.askLabel.TabIndex = 6;
            this.askLabel.Text = "Ask: 0.00";
            // 
            // iNAVValueLabel
            // 
            this.iNAVValueLabel.AutoSize = true;
            this.iNAVValueLabel.Location = new System.Drawing.Point(91, 17);
            this.iNAVValueLabel.Name = "iNAVValueLabel";
            this.iNAVValueLabel.Size = new System.Drawing.Size(58, 13);
            this.iNAVValueLabel.TabIndex = 5;
            this.iNAVValueLabel.Text = "iNAV: 0.00";
            // 
            // defaOfferLabel
            // 
            this.defaOfferLabel.AutoSize = true;
            this.defaOfferLabel.Location = new System.Drawing.Point(598, 48);
            this.defaOfferLabel.Name = "defaOfferLabel";
            this.defaOfferLabel.Size = new System.Drawing.Size(51, 13);
            this.defaOfferLabel.TabIndex = 14;
            this.defaOfferLabel.Text = "defaOffer";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(527, 31);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(59, 13);
            this.label43.TabIndex = 13;
            this.label43.Text = "Default Bid";
            // 
            // defaBidLabel
            // 
            this.defaBidLabel.AutoSize = true;
            this.defaBidLabel.Location = new System.Drawing.Point(598, 31);
            this.defaBidLabel.Name = "defaBidLabel";
            this.defaBidLabel.Size = new System.Drawing.Size(43, 13);
            this.defaBidLabel.TabIndex = 12;
            this.defaBidLabel.Text = "defaBid";
            // 
            // defaultOfferLabel
            // 
            this.defaultOfferLabel.AutoSize = true;
            this.defaultOfferLabel.Location = new System.Drawing.Point(527, 48);
            this.defaultOfferLabel.Name = "defaultOfferLabel";
            this.defaultOfferLabel.Size = new System.Drawing.Size(67, 13);
            this.defaultOfferLabel.TabIndex = 11;
            this.defaultOfferLabel.Text = "Default Offer";
            // 
            // clientBlabel
            // 
            this.clientBlabel.AutoSize = true;
            this.clientBlabel.Location = new System.Drawing.Point(540, 91);
            this.clientBlabel.Name = "clientBlabel";
            this.clientBlabel.Size = new System.Drawing.Size(43, 13);
            this.clientBlabel.TabIndex = 10;
            this.clientBlabel.Text = "Client B";
            // 
            // clientALabel
            // 
            this.clientALabel.AutoSize = true;
            this.clientALabel.Location = new System.Drawing.Point(163, 93);
            this.clientALabel.Name = "clientALabel";
            this.clientALabel.Size = new System.Drawing.Size(43, 13);
            this.clientALabel.TabIndex = 9;
            this.clientALabel.Text = "Client A";
            // 
            // sellerListBox
            // 
            this.sellerListBox.FormattingEnabled = true;
            this.sellerListBox.Location = new System.Drawing.Point(394, 118);
            this.sellerListBox.Name = "sellerListBox";
            this.sellerListBox.Size = new System.Drawing.Size(352, 95);
            this.sellerListBox.TabIndex = 8;
            // 
            // buyerListBox
            // 
            this.buyerListBox.FormattingEnabled = true;
            this.buyerListBox.Location = new System.Drawing.Point(23, 118);
            this.buyerListBox.Name = "buyerListBox";
            this.buyerListBox.Size = new System.Drawing.Size(331, 95);
            this.buyerListBox.TabIndex = 7;
            // 
            // recommendedOfferLabel
            // 
            this.recommendedOfferLabel.AutoSize = true;
            this.recommendedOfferLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.recommendedOfferLabel.Location = new System.Drawing.Point(418, 62);
            this.recommendedOfferLabel.Name = "recommendedOfferLabel";
            this.recommendedOfferLabel.Size = new System.Drawing.Size(50, 24);
            this.recommendedOfferLabel.TabIndex = 6;
            this.recommendedOfferLabel.Text = "Offer";
            this.recommendedOfferLabel.Click += new System.EventHandler(this.recommendedOfferLabel_Click);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(300, 39);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(43, 13);
            this.label22.TabIndex = 5;
            this.label22.Text = "MM Bid";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(413, 39);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "MM Offer";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(333, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(90, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Recommendation";
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.isOutsideOfSpreadButton);
            this.tabPage1.Controls.Add(this.isOutsideOfSpreadAnswerLabel);
            this.tabPage1.Controls.Add(this.outOfSpreadTitleLabel);
            this.tabPage1.Controls.Add(this.rsilistBox);
            this.tabPage1.Controls.Add(this.panel2);
            this.tabPage1.Controls.Add(this.panel1);
            this.tabPage1.Location = new System.Drawing.Point(4, 4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(773, 251);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Messages";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // rsilistBox
            // 
            this.rsilistBox.FormattingEnabled = true;
            this.rsilistBox.Location = new System.Drawing.Point(6, 7);
            this.rsilistBox.Name = "rsilistBox";
            this.rsilistBox.Size = new System.Drawing.Size(582, 186);
            this.rsilistBox.TabIndex = 5;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.rsiStockLabel);
            this.panel2.Controls.Add(this.button1);
            this.panel2.Controls.Add(this.askBidRatioReachedLabel);
            this.panel2.Controls.Add(this.askBidRatioLabel);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.rsiNegativeLabel);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Location = new System.Drawing.Point(594, 111);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(176, 123);
            this.panel2.TabIndex = 4;
            // 
            // rsiStockLabel
            // 
            this.rsiStockLabel.AutoSize = true;
            this.rsiStockLabel.Location = new System.Drawing.Point(57, 7);
            this.rsiStockLabel.Name = "rsiStockLabel";
            this.rsiStockLabel.Size = new System.Drawing.Size(33, 13);
            this.rsiStockLabel.TabIndex = 11;
            this.rsiStockLabel.Text = "ticker";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(21, 88);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(136, 23);
            this.button1.TabIndex = 10;
            this.button1.Text = "Approved to Trade";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // askBidRatioReachedLabel
            // 
            this.askBidRatioReachedLabel.AutoSize = true;
            this.askBidRatioReachedLabel.Enabled = false;
            this.askBidRatioReachedLabel.Location = new System.Drawing.Point(53, 53);
            this.askBidRatioReachedLabel.Name = "askBidRatioReachedLabel";
            this.askBidRatioReachedLabel.Size = new System.Drawing.Size(94, 13);
            this.askBidRatioReachedLabel.TabIndex = 9;
            this.askBidRatioReachedLabel.Text = "More than allowed";
            // 
            // askBidRatioLabel
            // 
            this.askBidRatioLabel.AutoSize = true;
            this.askBidRatioLabel.Location = new System.Drawing.Point(14, 38);
            this.askBidRatioLabel.Name = "askBidRatioLabel";
            this.askBidRatioLabel.Size = new System.Drawing.Size(98, 13);
            this.askBidRatioLabel.TabIndex = 8;
            this.askBidRatioLabel.Text = "Ask Bid Ratio: 0.00";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Enabled = false;
            this.label11.Location = new System.Drawing.Point(65, 70);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(109, 13);
            this.label11.TabIndex = 7;
            this.label11.Text = "Over moving average";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(18, 69);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(42, 13);
            this.label10.TabIndex = 6;
            this.label10.Text = "Volume";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Enabled = false;
            this.label9.Location = new System.Drawing.Point(106, 24);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(44, 13);
            this.label9.TabIndex = 5;
            this.label9.Text = "Positive";
            // 
            // rsiNegativeLabel
            // 
            this.rsiNegativeLabel.AutoSize = true;
            this.rsiNegativeLabel.Enabled = false;
            this.rsiNegativeLabel.Location = new System.Drawing.Point(54, 23);
            this.rsiNegativeLabel.Name = "rsiNegativeLabel";
            this.rsiNegativeLabel.Size = new System.Drawing.Size(50, 13);
            this.rsiNegativeLabel.TabIndex = 4;
            this.rsiNegativeLabel.Text = "Negative";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(18, 7);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(25, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "RSI";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.recogniaStockLabel);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.bullishLabel);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Location = new System.Drawing.Point(616, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(154, 93);
            this.panel1.TabIndex = 3;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(21, 68);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(28, 13);
            this.label14.TabIndex = 9;
            this.label14.Text = "AEV";
            // 
            // recogniaStockLabel
            // 
            this.recogniaStockLabel.AutoSize = true;
            this.recogniaStockLabel.Location = new System.Drawing.Point(21, 45);
            this.recogniaStockLabel.Name = "recogniaStockLabel";
            this.recogniaStockLabel.Size = new System.Drawing.Size(30, 13);
            this.recogniaStockLabel.TabIndex = 8;
            this.recogniaStockLabel.Text = "MBT";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(98, 69);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(0, 13);
            this.label16.TabIndex = 8;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(93, 68);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(42, 13);
            this.label15.TabIndex = 7;
            this.label15.Text = "Bearish";
            // 
            // bullishLabel
            // 
            this.bullishLabel.AutoSize = true;
            this.bullishLabel.Location = new System.Drawing.Point(93, 45);
            this.bullishLabel.Name = "bullishLabel";
            this.bullishLabel.Size = new System.Drawing.Size(37, 13);
            this.bullishLabel.TabIndex = 6;
            this.bullishLabel.Text = "Bullish";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(14, 25);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(63, 13);
            this.label13.TabIndex = 5;
            this.label13.Text = "Subscribed:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(98, 25);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(33, 13);
            this.label12.TabIndex = 4;
            this.label12.Text = "Alerts";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(49, 7);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 13);
            this.label7.TabIndex = 3;
            this.label7.Text = "Recognia";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.applicationServiceLabel);
            this.tabPage3.Controls.Add(this.label24);
            this.tabPage3.Controls.Add(this.label25);
            this.tabPage3.Controls.Add(this.label26);
            this.tabPage3.Controls.Add(this.label27);
            this.tabPage3.Controls.Add(this.label28);
            this.tabPage3.Controls.Add(this.label23);
            this.tabPage3.Controls.Add(this.priceServiceLabel);
            this.tabPage3.Controls.Add(this.label21);
            this.tabPage3.Controls.Add(this.label20);
            this.tabPage3.Controls.Add(this.label19);
            this.tabPage3.Location = new System.Drawing.Point(4, 4);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(773, 251);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Diagnostics";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // applicationServiceLabel
            // 
            this.applicationServiceLabel.AutoSize = true;
            this.applicationServiceLabel.Location = new System.Drawing.Point(156, 14);
            this.applicationServiceLabel.Name = "applicationServiceLabel";
            this.applicationServiceLabel.Size = new System.Drawing.Size(61, 13);
            this.applicationServiceLabel.TabIndex = 10;
            this.applicationServiceLabel.Text = "Checking...";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(414, 60);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(61, 13);
            this.label24.TabIndex = 9;
            this.label24.Text = "Checking...";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(414, 38);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(61, 13);
            this.label25.TabIndex = 8;
            this.label25.Text = "Checking...";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(308, 60);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(27, 13);
            this.label26.TabIndex = 7;
            this.label26.Text = "ETF";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(308, 38);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(53, 13);
            this.label27.TabIndex = 6;
            this.label27.Text = "Recognia";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(282, 14);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(71, 13);
            this.label28.TabIndex = 5;
            this.label28.Text = "Email Service";
            this.label28.Click += new System.EventHandler(this.label28_Click);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(156, 60);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(61, 13);
            this.label23.TabIndex = 4;
            this.label23.Text = "Checking...";
            // 
            // priceServiceLabel
            // 
            this.priceServiceLabel.AutoSize = true;
            this.priceServiceLabel.Location = new System.Drawing.Point(156, 38);
            this.priceServiceLabel.Name = "priceServiceLabel";
            this.priceServiceLabel.Size = new System.Drawing.Size(61, 13);
            this.priceServiceLabel.TabIndex = 3;
            this.priceServiceLabel.Text = "Checking...";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(50, 60);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(92, 13);
            this.label21.TabIndex = 2;
            this.label21.Text = "Database Service";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(50, 38);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(70, 13);
            this.label20.TabIndex = 1;
            this.label20.Text = "Price Service";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(24, 14);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(98, 13);
            this.label19.TabIndex = 0;
            this.label19.Text = "Application Service";
            // 
            // chartTabPage
            // 
            this.chartTabPage.Controls.Add(this.getChartButton);
            this.chartTabPage.Controls.Add(this.stockChartTextBox);
            this.chartTabPage.Controls.Add(this.label8);
            this.chartTabPage.Controls.Add(this.stockChart);
            this.chartTabPage.Location = new System.Drawing.Point(4, 4);
            this.chartTabPage.Name = "chartTabPage";
            this.chartTabPage.Size = new System.Drawing.Size(773, 251);
            this.chartTabPage.TabIndex = 4;
            this.chartTabPage.Text = "Charts";
            this.chartTabPage.UseVisualStyleBackColor = true;
            // 
            // getChartButton
            // 
            this.getChartButton.Location = new System.Drawing.Point(45, 64);
            this.getChartButton.Name = "getChartButton";
            this.getChartButton.Size = new System.Drawing.Size(75, 23);
            this.getChartButton.TabIndex = 3;
            this.getChartButton.Text = "Get";
            this.getChartButton.UseVisualStyleBackColor = true;
            this.getChartButton.Click += new System.EventHandler(this.getChartButton_Click);
            // 
            // stockChartTextBox
            // 
            this.stockChartTextBox.Location = new System.Drawing.Point(83, 23);
            this.stockChartTextBox.Name = "stockChartTextBox";
            this.stockChartTextBox.Size = new System.Drawing.Size(173, 20);
            this.stockChartTextBox.TabIndex = 2;
            this.stockChartTextBox.Text = "FMETF PM";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(42, 26);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "Stock";
            // 
            // stockChart
            // 
            chartArea2.Name = "ChartArea1";
            this.stockChart.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.stockChart.Legends.Add(legend2);
            this.stockChart.Location = new System.Drawing.Point(262, 13);
            this.stockChart.Name = "stockChart";
            series2.ChartArea = "ChartArea1";
            series2.Legend = "Legend1";
            series2.Name = "Series1";
            this.stockChart.Series.Add(series2);
            this.stockChart.Size = new System.Drawing.Size(499, 222);
            this.stockChart.TabIndex = 0;
            this.stockChart.Text = "Stock";
            // 
            // errorMessageLabel
            // 
            this.errorMessageLabel.AutoSize = true;
            this.errorMessageLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.errorMessageLabel.ForeColor = System.Drawing.Color.DarkRed;
            this.errorMessageLabel.Location = new System.Drawing.Point(407, 9);
            this.errorMessageLabel.Name = "errorMessageLabel";
            this.errorMessageLabel.Size = new System.Drawing.Size(74, 17);
            this.errorMessageLabel.TabIndex = 7;
            this.errorMessageLabel.Text = "ErrorMsg";
            this.errorMessageLabel.Click += new System.EventHandler(this.errorMessageLabel_Click);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(76, 7);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(196, 29);
            this.label18.TabIndex = 8;
            this.label18.Text = "FMETF MM Beta";
            // 
            // diagnosticsTimer
            // 
            this.diagnosticsTimer.Enabled = true;
            // 
            // countdownTimer
            // 
            this.countdownTimer.Enabled = true;
            this.countdownTimer.Interval = 10000;
            this.countdownTimer.Tick += new System.EventHandler(this.countdownTimer_Tick);
            // 
            // miniButton
            // 
            this.miniButton.Location = new System.Drawing.Point(12, 9);
            this.miniButton.Name = "miniButton";
            this.miniButton.Size = new System.Drawing.Size(47, 23);
            this.miniButton.TabIndex = 9;
            this.miniButton.Text = "Mini";
            this.miniButton.UseVisualStyleBackColor = true;
            this.miniButton.Click += new System.EventHandler(this.miniButton_Click);
            // 
            // strategyTimer
            // 
            this.strategyTimer.Enabled = true;
            // 
            // intradayTimer
            // 
            this.intradayTimer.Enabled = true;
            this.intradayTimer.Interval = 5000;
            // 
            // outOfSpreadTitleLabel
            // 
            this.outOfSpreadTitleLabel.AutoSize = true;
            this.outOfSpreadTitleLabel.Location = new System.Drawing.Point(6, 209);
            this.outOfSpreadTitleLabel.Name = "outOfSpreadTitleLabel";
            this.outOfSpreadTitleLabel.Size = new System.Drawing.Size(105, 13);
            this.outOfSpreadTitleLabel.TabIndex = 6;
            this.outOfSpreadTitleLabel.Text = "Is outside of spread?";
            // 
            // isOutsideOfSpreadAnswerLabel
            // 
            this.isOutsideOfSpreadAnswerLabel.AutoSize = true;
            this.isOutsideOfSpreadAnswerLabel.Location = new System.Drawing.Point(113, 209);
            this.isOutsideOfSpreadAnswerLabel.Name = "isOutsideOfSpreadAnswerLabel";
            this.isOutsideOfSpreadAnswerLabel.Size = new System.Drawing.Size(21, 13);
            this.isOutsideOfSpreadAnswerLabel.TabIndex = 7;
            this.isOutsideOfSpreadAnswerLabel.Text = "No";
            // 
            // isOutsideOfSpreadButton
            // 
            this.isOutsideOfSpreadButton.Location = new System.Drawing.Point(140, 204);
            this.isOutsideOfSpreadButton.Name = "isOutsideOfSpreadButton";
            this.isOutsideOfSpreadButton.Size = new System.Drawing.Size(75, 23);
            this.isOutsideOfSpreadButton.TabIndex = 8;
            this.isOutsideOfSpreadButton.Text = "Clear";
            this.isOutsideOfSpreadButton.UseVisualStyleBackColor = true;
            this.isOutsideOfSpreadButton.Click += new System.EventHandler(this.isOutsideOfSpreadButton_Click);
            // 
            // FMSecForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(805, 371);
            this.ControlBox = false;
            this.Controls.Add(this.miniButton);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.errorMessageLabel);
            this.Controls.Add(this.tabMenuControl);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FMSecForm";
            this.Text = "FirstMetroSec";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FMSecForm_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FMSecForm_FormClosed);
            this.Load += new System.EventHandler(this.FMSecForm_Load);
            this.ResizeEnd += new System.EventHandler(this.FMSecForm_ResizeEnd);
            this.SizeChanged += new System.EventHandler(this.FMSecForm_SizeChanged);
            this.contextMenuStrip.ResumeLayout(false);
            this.tabMenuControl.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage6.ResumeLayout(false);
            this.tabPage6.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.tabPage7.ResumeLayout(false);
            this.tabPage7.PerformLayout();
            this.tabPage8.ResumeLayout(false);
            this.tabPage8.PerformLayout();
            this.marketMakerTab.ResumeLayout(false);
            this.marketMakerTab.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.chartTabPage.ResumeLayout(false);
            this.chartTabPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.stockChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.importedOrderScreenBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fMSecFormBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NotifyIcon fmsecNotifyIcon;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem displayToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
        private System.Windows.Forms.Label bidLabel;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ListBox ticketListBox;
        private System.Windows.Forms.TabControl tabMenuControl;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label iNAVValueLabel;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label askLabel;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label rsiNegativeLabel;

        public System.Windows.Forms.Label RSINegativeLabel
        {
            get { return rsiNegativeLabel; }
            set { rsiNegativeLabel = value; }
        }
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label errorMessageLabel;
        private System.Windows.Forms.Label recogniaStockLabel;

        public System.Windows.Forms.Label RSIStockLabel
        {
            get { return rsiStockLabel; }
            set { rsiStockLabel = value; }
        }
        private System.Windows.Forms.Label askBidRatioReachedLabel;
        private System.Windows.Forms.Label askBidRatioLabel;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label bullishLabel;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Timer runTimer;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label priceServiceLabel;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox iNAVLinkTextBox;
        private System.Windows.Forms.TextBox rsiTimeFrameTextBox;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox rsiPeriodTextBox;
        private System.Windows.Forms.TextBox rsiFrequencyTextBox;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox iNAVPatternTextBox;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox emailSubscribersTextBox;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox marketMakerBuyerTextBox;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox fromEmailTextBox;
        private System.Windows.Forms.Label applicationServiceLabel;
        private System.Windows.Forms.Button importButton;
        private System.Windows.Forms.Timer diagnosticsTimer;
        private System.Windows.Forms.TabPage marketMakerTab;
        private System.Windows.Forms.Label recommendedOfferLabel;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.BindingSource importedOrderScreenBindingSource;
        private System.Windows.Forms.BindingSource fMSecFormBindingSource;
        private System.Windows.Forms.ListBox buyerListBox;
        private System.Windows.Forms.ListBox sellerListBox;
        private System.Windows.Forms.TextBox minPostVolumeTextBox;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox maxSpreadTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox skewViewTextBox;
        private System.Windows.Forms.Label label;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox marketMakerSellerTextBox;
        private System.Windows.Forms.Label clientBlabel;
        private System.Windows.Forms.Label clientALabel;
        private System.Windows.Forms.Label defaOfferLabel;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label defaBidLabel;
        private System.Windows.Forms.Label defaultOfferLabel;
        private System.Windows.Forms.Label recommendedBidLabel;
        private System.Windows.Forms.Label offerVolLabel;
        private System.Windows.Forms.Label bidVolLabel;
        private System.Windows.Forms.Label bestOfferLabel;
        private System.Windows.Forms.Label bestBidLabel;
        private System.Windows.Forms.Timer countdownTimer;
        private System.Windows.Forms.Button miniButton;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label timebetweenRefreshLabel;
        private System.Windows.Forms.TextBox timeBetweenRefreshTextBox;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.TextBox askVolumeTextBox;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.TextBox bidVolumeTextBox;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.TextBox marketMakerFilePathTextBox;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.TextBox fileNameTextBox;
        private System.Windows.Forms.TextBox marketMakerFrequencyTextBox;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.TextBox cancelFileTextBox;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.TextBox cancelFilePathtextBox;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.TextBox fmEtfStockTextBox;
        private System.Windows.Forms.Timer strategyTimer;
        private System.Windows.Forms.ListBox rsilistBox;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label rsiStockLabel;
        private System.Windows.Forms.Label listStocksLabel;
        private System.Windows.Forms.Timer intradayTimer;
        private System.Windows.Forms.TabPage chartTabPage;
        private System.Windows.Forms.TextBox stockChartTextBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DataVisualization.Charting.Chart stockChart;
        private System.Windows.Forms.Button getChartButton;
        private System.Windows.Forms.Label isOutsideOfSpreadAnswerLabel;
        private System.Windows.Forms.Label outOfSpreadTitleLabel;
        private System.Windows.Forms.Button isOutsideOfSpreadButton;

        public System.Windows.Forms.TextBox TimeBetweenRefreshTextBox
        {
            get { 
                return 
                    timeBetweenRefreshTextBox; 
            }
           
        }

        public System.Windows.Forms.Button MiniButton
        {
            get { return miniButton; }
            
        }
    }
}

