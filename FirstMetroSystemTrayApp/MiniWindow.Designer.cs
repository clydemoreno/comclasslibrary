﻿namespace FirstMetroSystemTrayApp
{
    partial class MiniWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MiniWindow));
            this.notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.checkManualRefreshTimer = new System.Windows.Forms.Timer(this.components);
            this.groupPanel = new System.Windows.Forms.Panel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rsiMiniWindowLabel = new System.Windows.Forms.Label();
            this.rsiStockLabel = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dropBoxTextBox = new System.Windows.Forms.RichTextBox();
            this.inavBackgroundPanel = new System.Windows.Forms.Panel();
            this.iNavTextPanel = new System.Windows.Forms.Panel();
            this.inavLinkLabel = new System.Windows.Forms.LinkLabel();
            this.mmBidButton = new System.Windows.Forms.Button();
            this.mmOfferButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.viewdRSIButton = new System.Windows.Forms.Button();
            this.groupPanel.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.inavBackgroundPanel.SuspendLayout();
            this.iNavTextPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // notifyIcon
            // 
            this.notifyIcon.BalloonTipText = "FMSec App";
            this.notifyIcon.BalloonTipTitle = "FMSec App";
            this.notifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon.Icon")));
            this.notifyIcon.Text = "notifyIcon";
            this.notifyIcon.Visible = true;
            this.notifyIcon.DoubleClick += new System.EventHandler(this.notifyIcon_DoubleClick);
            // 
            // checkManualRefreshTimer
            // 
            this.checkManualRefreshTimer.Enabled = true;
            this.checkManualRefreshTimer.Interval = 3000;
            this.checkManualRefreshTimer.Tick += new System.EventHandler(this.checkManualRefreshTimer_Tick);
            // 
            // groupPanel
            // 
            this.groupPanel.Controls.Add(this.groupBox3);
            this.groupPanel.Controls.Add(this.groupBox2);
            this.groupPanel.Controls.Add(this.groupBox1);
            this.groupPanel.Location = new System.Drawing.Point(5, 3);
            this.groupPanel.Name = "groupPanel";
            this.groupPanel.Size = new System.Drawing.Size(363, 125);
            this.groupPanel.TabIndex = 9;
            this.groupPanel.DoubleClick += new System.EventHandler(this.groupPanel_DoubleClick);
            // 
            // groupBox3
            // 
            this.groupBox3.Location = new System.Drawing.Point(19, 71);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(144, 42);
            this.groupBox3.TabIndex = 14;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Recognia";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.viewdRSIButton);
            this.groupBox2.Controls.Add(this.rsiMiniWindowLabel);
            this.groupBox2.Controls.Add(this.rsiStockLabel);
            this.groupBox2.Location = new System.Drawing.Point(182, 71);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(163, 42);
            this.groupBox2.TabIndex = 13;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "RSI Strategy";
            // 
            // rsiMiniWindowLabel
            // 
            this.rsiMiniWindowLabel.AutoSize = true;
            this.rsiMiniWindowLabel.Enabled = false;
            this.rsiMiniWindowLabel.Location = new System.Drawing.Point(19, 20);
            this.rsiMiniWindowLabel.Name = "rsiMiniWindowLabel";
            this.rsiMiniWindowLabel.Size = new System.Drawing.Size(37, 13);
            this.rsiMiniWindowLabel.TabIndex = 1;
            this.rsiMiniWindowLabel.Text = "Ticker";
            // 
            // rsiStockLabel
            // 
            this.rsiStockLabel.AutoSize = true;
            this.rsiStockLabel.Location = new System.Drawing.Point(19, 20);
            this.rsiStockLabel.Name = "rsiStockLabel";
            this.rsiStockLabel.Size = new System.Drawing.Size(0, 13);
            this.rsiStockLabel.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dropBoxTextBox);
            this.groupBox1.Controls.Add(this.inavBackgroundPanel);
            this.groupBox1.Controls.Add(this.mmBidButton);
            this.groupBox1.Controls.Add(this.mmOfferButton);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Location = new System.Drawing.Point(18, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(328, 63);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Market Making";
            // 
            // dropBoxTextBox
            // 
            this.dropBoxTextBox.BackColor = System.Drawing.SystemColors.Control;
            this.dropBoxTextBox.Location = new System.Drawing.Point(289, 23);
            this.dropBoxTextBox.Name = "dropBoxTextBox";
            this.dropBoxTextBox.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
            this.dropBoxTextBox.Size = new System.Drawing.Size(27, 20);
            this.dropBoxTextBox.TabIndex = 16;
            this.dropBoxTextBox.Text = "";
            this.dropBoxTextBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.tempCtrolOTextBox_MouseClick);
            this.dropBoxTextBox.TextChanged += new System.EventHandler(this.tempCtrolOTextBox_TextChanged);
            // 
            // inavBackgroundPanel
            // 
            this.inavBackgroundPanel.Controls.Add(this.iNavTextPanel);
            this.inavBackgroundPanel.Location = new System.Drawing.Point(103, 28);
            this.inavBackgroundPanel.Name = "inavBackgroundPanel";
            this.inavBackgroundPanel.Size = new System.Drawing.Size(72, 31);
            this.inavBackgroundPanel.TabIndex = 15;
            // 
            // iNavTextPanel
            // 
            this.iNavTextPanel.Controls.Add(this.inavLinkLabel);
            this.iNavTextPanel.Location = new System.Drawing.Point(3, 4);
            this.iNavTextPanel.Name = "iNavTextPanel";
            this.iNavTextPanel.Size = new System.Drawing.Size(65, 23);
            this.iNavTextPanel.TabIndex = 11;
            // 
            // inavLinkLabel
            // 
            this.inavLinkLabel.AutoSize = true;
            this.inavLinkLabel.DisabledLinkColor = System.Drawing.Color.Blue;
            this.inavLinkLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.inavLinkLabel.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            this.inavLinkLabel.Location = new System.Drawing.Point(14, 5);
            this.inavLinkLabel.Name = "inavLinkLabel";
            this.inavLinkLabel.Size = new System.Drawing.Size(40, 13);
            this.inavLinkLabel.TabIndex = 10;
            this.inavLinkLabel.TabStop = true;
            this.inavLinkLabel.Text = "000.00";
            this.inavLinkLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.inavLinkLabel.VisitedLinkColor = System.Drawing.Color.Blue;
            this.inavLinkLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.inavLinkLabel_LinkClicked);
            // 
            // mmBidButton
            // 
            this.mmBidButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mmBidButton.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.mmBidButton.Location = new System.Drawing.Point(23, 34);
            this.mmBidButton.Name = "mmBidButton";
            this.mmBidButton.Size = new System.Drawing.Size(65, 23);
            this.mmBidButton.TabIndex = 14;
            this.mmBidButton.Text = "000.00";
            this.mmBidButton.UseVisualStyleBackColor = true;
            this.mmBidButton.Click += new System.EventHandler(this.mmButton_Click);
            // 
            // mmOfferButton
            // 
            this.mmOfferButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mmOfferButton.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.mmOfferButton.Location = new System.Drawing.Point(198, 32);
            this.mmOfferButton.Name = "mmOfferButton";
            this.mmOfferButton.Size = new System.Drawing.Size(65, 23);
            this.mmOfferButton.TabIndex = 13;
            this.mmOfferButton.Text = "000.00";
            this.mmOfferButton.UseVisualStyleBackColor = true;
            this.mmOfferButton.Click += new System.EventHandler(this.mmButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(37, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "MMBid:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(121, 11);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "iNAV:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(204, 13);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "MMOffer:";
            // 
            // viewdRSIButton
            // 
            this.viewdRSIButton.Location = new System.Drawing.Point(108, 13);
            this.viewdRSIButton.Name = "viewdRSIButton";
            this.viewdRSIButton.Size = new System.Drawing.Size(44, 23);
            this.viewdRSIButton.TabIndex = 2;
            this.viewdRSIButton.Text = "View";
            this.viewdRSIButton.UseVisualStyleBackColor = true;
            // 
            // MiniWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(374, 133);
            this.Controls.Add(this.groupPanel);
            this.Name = "MiniWindow";
            this.Text = "FirstMetroSec App";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MiniWindow_FormClosed);
            this.Load += new System.EventHandler(this.MiniWindow_Load);
            this.DoubleClick += new System.EventHandler(this.MiniWindow_DoubleClick);
            this.groupPanel.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.inavBackgroundPanel.ResumeLayout(false);
            this.iNavTextPanel.ResumeLayout(false);
            this.iNavTextPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.NotifyIcon notifyIcon;
        private System.Windows.Forms.Timer checkManualRefreshTimer;
        private System.Windows.Forms.Panel groupPanel;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel iNavTextPanel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.LinkLabel inavLinkLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button mmOfferButton;
        private System.Windows.Forms.Button mmBidButton;
        private System.Windows.Forms.Panel inavBackgroundPanel;
        private System.Windows.Forms.RichTextBox dropBoxTextBox;
        private System.Windows.Forms.Label rsiStockLabel;
        private System.Windows.Forms.Label rsiMiniWindowLabel;
        private System.Windows.Forms.Button viewdRSIButton;
    }
}