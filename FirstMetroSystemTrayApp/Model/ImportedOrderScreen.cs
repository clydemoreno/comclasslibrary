﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstMetroSystemTrayApp.Model
{
    public class ImportedOrderScreen
    {
        public String orderNumber;
        public DateTime timeSent;
        public String type;
        public String accountID;
        public String stock;
        public String side;
        public int volume;
        public double price;
        public double avgpx;
        public String status;
        public double matched;
        public double pending;
        public String expiry;
        public String trader;
        public String remarks;
        public String action;
    }
}
