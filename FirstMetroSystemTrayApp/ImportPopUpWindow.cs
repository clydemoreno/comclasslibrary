﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using COMClassLibrary;
using COMClassLibrary.Manager;
using FirstMetroSystemTrayApp.Model;

namespace FirstMetroSystemTrayApp
{
    public partial class ImportPopUpWindow : Form
    {
        private String seller;
        private String buyer;
        // An event that clients can use to be notified whenever the
        // elements of the list change.
        public event ChangedEventHandler Changed;

        public delegate void ChangeHandler(OrderManager m, EventArgs e);
        // Invoke the Changed event; called whenever list changes
        protected virtual void OnChanged(Object sender, EventArgs e)
        {
            if (Changed != null)
                Changed(sender, e);
        }

        public ImportPopUpWindow(String buyer, String seller)
        {
            this.buyer = buyer;
            this.seller = seller;
            InitializeComponent();
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            this.Hide();
        }

        private async void importPopupButton_Click(object sender, EventArgs e)
        {
            String text = importedPopupWindowTextBox.Text;
            //String counterPartyAccountID = "G00101-3";
            Task<List<ImportedOrderScreen>> task = Task.Run(() => ImportPopupWindowController.Popup(text,this.buyer,this.seller));
            List<ImportedOrderScreen> list = await task;
           
            OnChanged(list, new ImportWindowPopUpEventArg());

            this.Hide();
            //this.Close();
        }

        private void ImportF4Screen()
        {
            
            //^BID VOL	BID VOL	       BID       	       ASK       	ASK VOL	^ASK VOL


        }


        //todo: right now. Add this to a utility.
        //create a controller for each window
        //start with this one
        private List<ImportedOrderScreen> PopupOld(String text, String buyer, string seller)
        {
            //import it first then
            //close the window
            List<ImportedOrderScreen> list = new List<ImportedOrderScreen>();
            //String orderText = importTextBox.Text;
            String orderText = text;
            String[] lines = orderText.Split("\r\n\r\n".ToCharArray());
            ImportedOrderScreen order = null;
            foreach (String line in lines)
            {
                order = new ImportedOrderScreen();
                String[] columns = line.Split("\t".ToCharArray());
                if (columns.Length == 14)
                {

                    //Double.TryParse(columns[10].Replace(",", ""), out order.pending);

                    if (columns[0] == "TIME SENT") continue;
                    if (columns[2] != buyer && columns[2] != seller) continue;
                    if (columns[3] != "FMETF")  continue;
                    //if (String.IsNullOrEmpty(order.pending)) continue;
                    //if (columns[8] != Constants.OrderFilledCode) continue;
                    //disable this for now.
                    //TIME SENT	TYPE	ACCOUNT ID	STOCK	SIDE	VOLUME	PRICE	AVG PX	STATUS	MATCHED	PENDING	EXPIRY	TRADER	REMARKS
                    order.timeSent = DateTime.Parse(columns[0]);
                    order.type = columns[1];
                    order.accountID = columns[2];
                    order.stock = columns[3];
                    order.side = columns[4];

                    Int32.TryParse(columns[5].Replace(",",""), out order.volume);
                    Double.TryParse(columns[6].Replace(",", ""), out order.price);
                    Double.TryParse(columns[7].Replace(",", ""), out order.avgpx);
                    order.status = columns[8];
                    Double.TryParse(columns[9].Replace(",", ""), out order.matched);
                    order.expiry = columns[11];
                    order.trader = columns[12];
                    order.remarks = columns[13];

                    list.Add(order);

                    //foreach (String column in columns)
                    //{
                    //    Debug.WriteLine(column);

                    //}


                }
            }
            return list;
        }

        private void importedPopupWindowTextBox_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
