﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using COMClassLibrary;
using FirstMetroSystemTrayApp.Model;

namespace FirstMetroSystemTrayApp
{
    public class ImportPopupWindowController
    {
        public static List<ImportedOrderScreen> Popup(String text, String buyer, string seller)
        {
            //import it first then
            //close the window
            List<ImportedOrderScreen> list = new List<ImportedOrderScreen>();
            //String orderText = importTextBox.Text;
            String orderText = text;
            String[] lines = orderText.Split("\r\n\r\n".ToCharArray());
            ImportedOrderScreen order = null;
            foreach (String line in lines)
            {
                order = new ImportedOrderScreen();
                String[] columns = line.Split("\t".ToCharArray());
                if (columns.Length == 15)
                {

                    //Double.TryParse(columns[10].Replace(",", ""), out order.pending);

                    if (columns[0] == "ORDER NO.") continue;
                    if (columns[4] != buyer && columns[4] != seller) continue;
                    if (columns[5] != Constants.FMETF) continue;
                    double pending = 0;
                    bool isPendingANumber = Double.TryParse(columns[11],out pending);
                    if (!isPendingANumber) continue;
                    //if (String.IsNullOrEmpty( order.pending)) continue;
                    //if (columns[8] != Constants.OrderFilledCode) continue;
                    //disable this for now.
                    //TIME SENT	TYPE	ACCOUNT ID	STOCK	SIDE	VOLUME	PRICE	AVG PX	STATUS	MATCHED	PENDING	EXPIRY	TRADER	REMARKS

                    order.orderNumber = columns[0];
                    Double.TryParse(columns[1].Replace(",", ""), out order.price);
                    order.timeSent = DateTime.Parse(columns[2]);
                    order.type = columns[3];
                    order.accountID = columns[4];
                    order.stock = columns[5];
                    order.side = columns[6];
                    Int32.TryParse(columns[7].Replace(",", ""), out order.volume);
                    Double.TryParse(columns[8].Replace(",", ""), out order.avgpx);
                    order.status = columns[9];
                    Double.TryParse(columns[10].Replace(",", ""), out order.matched);
                    order.expiry = columns[12];
                    order.trader = columns[13];
                    order.remarks = columns[14];
                    order.pending = pending; 
                    list.Add(order);

                    //foreach (String column in columns)
                    //{
                    //    Debug.WriteLine(column);

                    //}


                }
            }
            return list;
        }

    }
}
