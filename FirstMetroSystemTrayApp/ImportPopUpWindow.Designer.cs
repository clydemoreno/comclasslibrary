﻿namespace FirstMetroSystemTrayApp
{
    partial class ImportPopUpWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.importPopupButton = new System.Windows.Forms.Button();
            this.importedPopupWindowTextBox = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // importPopupButton
            // 
            this.importPopupButton.Location = new System.Drawing.Point(472, 239);
            this.importPopupButton.Name = "importPopupButton";
            this.importPopupButton.Size = new System.Drawing.Size(110, 23);
            this.importPopupButton.TabIndex = 1;
            this.importPopupButton.Text = "Import";
            this.importPopupButton.UseVisualStyleBackColor = true;
            this.importPopupButton.Click += new System.EventHandler(this.importPopupButton_Click);
            // 
            // importedPopupWindowTextBox
            // 
            this.importedPopupWindowTextBox.Location = new System.Drawing.Point(12, 12);
            this.importedPopupWindowTextBox.Name = "importedPopupWindowTextBox";
            this.importedPopupWindowTextBox.Size = new System.Drawing.Size(1017, 206);
            this.importedPopupWindowTextBox.TabIndex = 2;
            this.importedPopupWindowTextBox.Text = "";
            this.importedPopupWindowTextBox.TextChanged += new System.EventHandler(this.importedPopupWindowTextBox_TextChanged);
            // 
            // ImportPopUpWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1041, 274);
            this.Controls.Add(this.importedPopupWindowTextBox);
            this.Controls.Add(this.importPopupButton);
            this.Name = "ImportPopUpWindow";
            this.Text = "ImportPopUpWindow";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button importPopupButton;
        private System.Windows.Forms.RichTextBox importedPopupWindowTextBox;
    }
}