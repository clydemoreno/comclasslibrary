﻿using COMClassLibrary.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace FirstMetroSystemTrayApp
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //change
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            try
            {
                Application.Run(new SpreadAlertWindow());
            }
            catch (Exception e)
            {
                LogManager.uploadErrorLog(e.Message);
                Environment.Exit(1);
            }
        }
    }
}
