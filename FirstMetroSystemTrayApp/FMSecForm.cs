﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using COMClassLibrary.Manager;
using COMClassLibrary;
using System.Diagnostics;
using COMClassLibrary.FMServiceReference;
using System.Threading.Tasks;
using System.IO;
using FirstMetroSystemTrayApp.Model;
using System.Media;
using COMClassLibrary.Model;
using System.Configuration;
using System.Collections;

namespace FirstMetroSystemTrayApp
{

    

    public delegate void FormEventHandler(object sender, EventArgs e);
    public partial class FMSecForm : Form
    {
        private BindingSource bindingSource = null;
        private ImportPopUpWindow importPopWindow;
        private MatchingEngineParams matchingEngineParams;
        public MatchingEngineParams MatchingEngineParams
        {
            get { return matchingEngineParams; }
            set { matchingEngineParams = value; }
        }

        private Hashtable stockHashSet;


        public Hashtable StockHashSet
        {
            get {
                if (stockHashSet == null) stockHashSet = new Hashtable();
                    return stockHashSet; 
            }
            
        }


        public ImportPopUpWindow ImportPopWindow
        {
            get {
                if (importPopWindow == null || importPopWindow.IsDisposed)
                {
                    importPopWindow = new ImportPopUpWindow(marketMakerBuyerTextBox.Text, marketMakerSellerTextBox.Text);
                    importPopWindow.Changed += ImportPopWindow_Changed;
                }
                //if(importPopWindow.IsDisposed)

                return importPopWindow; 
            }
        }

        public void ImportPopWindow_Changed(object sender, EventArgs e)
        {
            if (e.GetType() == typeof(ImportWindowPopUpEventArg))
            {
                

                List<ImportedOrderScreen> list = (List<ImportedOrderScreen>)sender;
                var buyerList = from order in list where order.side == "BUY" orderby order.price descending select order;

                var sellerList = from order in list where order.side == "SELL" orderby order.price ascending select order;

                List<ImportedOrderScreen> bList = buyerList.ToList<ImportedOrderScreen>();
                var aggregateBuyerList = from ordered in bList
                                   group ordered by ordered.price;
                //into g
                  //                 select new { key = g.Key, TotalAmount = g.Sum(a => a.price)};


                List<ImportedOrderScreen> sList = sellerList.ToList<ImportedOrderScreen>();
                var aggregateSellerList = from ordered in sList
                                         group ordered by ordered.price;



                buyerListBox.Items.Clear();

                List<ImportedOrderScreen> newBuyerList = new List<ImportedOrderScreen>();

                ImportedOrderScreen buyerOrderScreen;

                foreach (var buyerGroup in aggregateBuyerList)
                {
                    double pendingVolume = 0;
                    Console.WriteLine(buyerGroup.Key);
                    ImportedOrderScreen o = null;
                    buyerOrderScreen = new ImportedOrderScreen();
                    foreach (ImportedOrderScreen order in buyerGroup)
                    {
                        o = order;
                        
                        pendingVolume += order.pending;
                        buyerOrderScreen = o;
                        Console.WriteLine("    {0} , {1}", order.stock,order.price);
                    }
                    String text = String.Format("{0}, Price:{1} Side:{2},Vol:{3}", o.stock, o.price, o.side, pendingVolume);
                    //buyerListBox.Items.Add(text);
                    buyerOrderScreen.pending = pendingVolume;
                    //sellerListBox.Items.Add(text);
                    newBuyerList.Add(buyerOrderScreen);

                }




                sellerListBox.Items.Clear();


                List<ImportedOrderScreen> newSellerList = new List<ImportedOrderScreen>();

                ImportedOrderScreen sellerOrderScreen;

                
                foreach (var sellerGroup in aggregateSellerList)
                {
                    double pendingVolume = 0;
                    Console.WriteLine(sellerGroup.Key);
                    ImportedOrderScreen o = null;
                    sellerOrderScreen = new ImportedOrderScreen();
                    foreach (ImportedOrderScreen order in sellerGroup)
                    {
                        o = order;
                        pendingVolume += order.pending;
                        Console.WriteLine("    {0} , {1}", order.stock, order.price);
                        sellerOrderScreen = o;
                    }
                    sellerOrderScreen.pending = pendingVolume;
                    
                    newSellerList.Add(sellerOrderScreen);
                }


                foreach (ImportedOrderScreen s in newSellerList)
                {
                    String text = String.Format("{0}, Price:{1} Side:{2},Pend vol:{3}", s.stock, s.price, s.side, s.pending);
                    sellerListBox.Items.Add(text);

                    //Debug.WriteLine("stock:{0}, price:{1}, side:{2}, vol:{3}",s.stock, s.price, s.side, s.volume);
                }


                foreach (ImportedOrderScreen o in newBuyerList)
                {
                    String text = String.Format("{0}, Price:{1} Side:{2},Vol:{3}", o.stock, o.price, o.side, o.volume);
                    buyerListBox.Items.Add(text);

                    //Debug.WriteLine("stock:{0}, price:{1}, side:{2}, vol:{3}",s.stock, s.price, s.side, s.volume);
                }


                AskImportedOrderScreenList = newSellerList;
                BidImportedOrderScreenList = newBuyerList;

                
                isUpdatedDateTime = DateTime.Now;

                //reset the timer here.

               



                //foreach (ImportedOrderScreen o in (List<ImportedOrderScreen>)sender)
                //{
                //    item = new ListViewItem();

                //    Debug.WriteLine("{0}, Price:{1} Side:{2},Vol:{3}",o.stock,o.price,o.side,o.volume);
                //    String text = String.Format("{0}, Price:{1} Side:{2},Vol:{3}", o.stock, o.price, o.side, o.volume);
                //    //item.Text = string.Format( "Order: {0} Price:{1} Side:{2} Status:{3} AccountId:{4} Volume:{5}",o.stock,o.price,o.side,o.status,o.accountID,o.volume);
                //    //bindingSource.Add(o);
                //    if (o.side == "BUY")
                //    {
                //        buyerListBox.Items.Add(text);
                //    }
                //    else if (o.side == "SELL")
                //    {
                //        sellListBox.Items.Add(text);
                //    }
                //}
                //buyerDataGridView.DataSource = bindingSource;
                //buyerDataGridView.DataSource
                // Initialize and add a check box column.
                
                
                //buyerDataGridView.Invalidate();       

                runTimer_Tick(null, EventArgs.Empty);
               
            }
        }
        private bool isSystemUp = false;

        public bool IsSystemUp
        {
            get { return isSystemUp; }

        }
        public FMSecForm()
        {

            InitializeComponent();
            stockChart.Series.Add("FMETF");
            
        }
        // An event that clients can use to be notified whenever the
        // elements of the list change.
        public event ChangedEventHandler Changed;

        public delegate void ChangeHandler(OrderManager m, EventArgs e);
        // Invoke the Changed event; called whenever list changes
        protected virtual void OnChanged(Object sender, EventArgs e)
        {
            if (Changed != null)
                Changed(sender, e);
        }

        public ListBox BuyerListBox
        {
            get { return buyerListBox; }
        }

        public ListBox SellerListBox
        {
            get { return sellerListBox; }
        }

        private async void RunDiagnostics(int anyFrequency)
        {
            DiagnosticsManager mgr = new DiagnosticsManager();

            Task<string> web = Task<string>.Run(() => mgr.RunWebServiceDiagnostics());
            string webResult = await web;

            OnChanged(webResult, new DiagnosticsEventArg("web"));


            Task<bool> pricecheck = Task<bool>.Run(() => mgr.IsPriceServerRunning(anyFrequency));

            bool isPriceRunning = await pricecheck;

            OnChanged(isPriceRunning, new DiagnosticsEventArg("price"));
            //finish this code.
            //make this a unit method that can be tested independently.


        }
        //List<ImportedOrderScreen> importedOrderScreen;
        //private List<ImportedOrderScreen> ImportedOrderScreen
        //{
        //    get
        //    {
        //        return importedOrderScreen;

        //    }
        //    set
        //    {
        //        ImportedOrderScreen = value;
        //    }
        //}
        private void diagnosticsMgr_Changed(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }
        private void RunRecognia(PriceWithTimeFrameTO item)
        {
        }

        private RSIBreakoutStrategy rSIBreakoutStrategy;

        public RSIBreakoutStrategy RSIBreakoutStrategy
        {
            get {
                if (rSIBreakoutStrategy == null)
                {
                    rSIBreakoutStrategy = new RSIBreakoutStrategy();
                    rSIBreakoutStrategy.Changed += rsi_Changed;
                }
                return rSIBreakoutStrategy; }
        }
        private void RunRSI(PriceWithTimeFrameTO item)
        {
            RSIBreakoutStrategy rsi = this.RSIBreakoutStrategy;
            

            Dictionary<String, Object> parameters = new Dictionary<string, object>();


            SetParametersForRSI(item, rsi, parameters, item.StockName);

            rsi.GetOrderStatus(item.StockName, parameters);
            rsi.GetRSIList(item.StockName, parameters);

            rsi.GetProfitTargetAndStopLossPeriod(item.StockName, parameters);
            rsi.GetLastVolumeMA(item.StockName, parameters);
     

            rsi.Run(item.StockName, parameters);
       
        
        }

        private void SetParametersForRSI(PriceWithTimeFrameTO item, RSIBreakoutStrategy target, Dictionary<string, object> parameters, string stockName)
        {

            target.SetParameters(stockName, "stockName", stockName, parameters);


            target.SetParameters(stockName, "bid", item.Bid, parameters);
            target.SetParameters(stockName, "ask", item.Ask, parameters);
            target.SetParameters(stockName, "last", item.Price, parameters);
            target.SetParameters(stockName, "period", Convert.ToInt32( rsiPeriodTextBox.Text), parameters);


            target.SetParameters(stockName, "startingX", 41, parameters);
            target.SetParameters(stockName, "frequency", Convert.ToInt32( rsiFrequencyTextBox.Text), parameters);
            target.SetParameters(stockName, "timeFrame", rsiTimeFrameTextBox.Text, parameters);
         //   target.SetParameters(stockName, Constants.RSIStatus, Constants.RSIWaitingMode, parameters);
            target.SetParameters(stockName, "requiredDerivative",  0.001, parameters);
            target.SetParameters(stockName, "requiredSeparation", 1.05, parameters);
            target.SetParameters(stockName, "volumePeriod", 14, parameters);
            target.SetParameters(stockName, "requiredVolume", 1.3, parameters);

            target.SetParameters(stockName, "profitTargetLookupPeriod", 10, parameters);
            target.SetParameters(stockName, "stopLossLookupPeriod", 10, parameters);
            //7% loss
            target.SetParameters(stockName, "requiredAbsoluteStopLossFactor", 0.93, parameters);
            target.SetParameters(stockName, "techniStockfilePath", @"C:\temp\technistock\orders.csv", parameters);

            //just pretend we got the right amount
            target.SetParameters(stockName, "capital", 1000000.00, parameters);
            target.SetParameters(stockName, "capitalMultiplier", 0.8, parameters);
            //capital divisor is the baseline or the minimum capital
            target.SetParameters(stockName, "capitalDivisor", 5, parameters);
            //RSIX_AxisLinearRegressionPeriodList

            //pending or partial.  check if that stock has no pending status. or someone already sent an unfilled order
            //a db call on quotation screen.

            target.SetParameters(stockName, "volume", item.Volume, parameters);//
            target.SetParameters(stockName, "lineRegressionRSIPeriod", 40, parameters);//
            target.SetParameters(stockName, "rsiPeriod", Convert.ToInt32(rsiPeriodTextBox.Text), parameters);//
            target.SetParameters(stockName, "cutoffDate", item.Date, parameters);//

        }

        void rsi_Changed(object sender, EventArgs e)
        {
            if (sender.GetType() != typeof(Dictionary<String, object>)) return;

            Dictionary<String, object> parameters = (Dictionary<String, object>)sender;

            if (e.GetType() == typeof(COMClassLibrary.Manager.MessageEventArg))
            {

                MessageEventArg eventArg = (MessageEventArg)e;

                //IMessageHandler handler = eventArg.MessageHandler;
                //handler.Handle(parameters, e);

                if (eventArg.Code == Constants.RSIRequiredDerivativeIsMet)
                {
                    rsiNegativeLabel.Enabled = true;
                    //rsiStockLabel.Text = eventArg.StockName;
                    
                }
                if (eventArg.Code == Constants.MyRSIErrorCode)
                {
                    errorMessageLabel.Text = eventArg.Message;
                }
            }

        }
        private void RunIntraday(PriceWithTimeFrameTO item)
        {
            //MarketMakerStrategy mmtarget = new MarketMakerStrategy();
            //mmtarget.Changed += mmtarget_Changed;
            //Dictionary<String, Object> parameters = new Dictionary<string, object>();


        }

        void intradayTarget_Changed(object sender, EventArgs e)
        {
            if (sender.GetType() != typeof(Dictionary<String, object>)) return;

            Dictionary<String, object> parameters = (Dictionary<String, object>)sender;

            if (e.GetType() == typeof(COMClassLibrary.Manager.MessageEventArg))
            {

                MessageEventArg eventArg = (MessageEventArg)e;

                //IMessageHandler handler = eventArg.MessageHandler;
                //handler.Handle(parameters, e);

                if (eventArg.Code == Constants.IntradayRatioReached)
                {
                    askBidRatioReachedLabel.Enabled = true;
                    rsiStockLabel.Text = eventArg.StockName;

                }

                

                

                if (eventArg.Code == Constants.MyRSIErrorCode)
                {
                    errorMessageLabel.Text = eventArg.Message;
                }

            }




            //foreach (ImportedOrderScreen s in newSellerList)
            //{
            //    String text = String.Format("{0}, Price:{1} Side:{2},Vol:{3}", s.stock, s.price, s.side, s.volume);
            //    sellerListBox.Items.Add(text);

            //    //Debug.WriteLine("stock:{0}, price:{1}, side:{2}, vol:{3}",s.stock, s.price, s.side, s.volume);
            //}


            //foreach (ImportedOrderScreen o in newBuyerList)
            //{
            //    String text = String.Format("{0}, Price:{1} Side:{2},Vol:{3}", o.stock, o.price, o.side, o.volume);
            //    buyerListBox.Items.Add(text);

            //    //Debug.WriteLine("stock:{0}, price:{1}, side:{2}, vol:{3}",s.stock, s.price, s.side, s.volume);
            //}

        }

        private MarketMakerStrategy marketMakerStrategy;

        public MarketMakerStrategy MarketMakerStrategy
        {
            get {
                if (marketMakerStrategy == null)
                {
                    marketMakerStrategy = new MarketMakerStrategy();
                    marketMakerStrategy.Changed += mm_Changed;
                    //target.Changed += new ChangedEventHandler(mm_Changed);
                }
                return marketMakerStrategy; }
        }

        private void RunMarketMaker(PriceWithTimeFrameTO item)
        {

            if (item.StockName != Constants.FMETF_PM) return;

            MarketMakerStrategy target = this.MarketMakerStrategy;
            

            Dictionary<String, Object> parameters = new Dictionary<string,object>();



            

            SetParametersForMarketMaker(item, target, parameters, item.StockName);
            SetFormValues(item);
            target.Run(item.StockName, parameters);




        }

        private void LoadValuesFromSettings()
        {
            askVolumeTextBox.Text = Properties.Settings.Default.askVolume.ToString();
            bidVolumeTextBox.Text = Properties.Settings.Default.bidVolume.ToString();
            marketMakerBuyerTextBox.Text = Properties.Settings.Default.marketMakerBuyer;
            marketMakerSellerTextBox.Text = Properties.Settings.Default.marketMakerSeller;
            maxSpreadTextBox.Text = Properties.Settings.Default.spreadThreshold;
            fileNameTextBox.Text = Properties.Settings.Default.marketMakerFileName;
            marketMakerFilePathTextBox.Text = Properties.Settings.Default.marketMakerFilePath;
            cancelFilePathtextBox.Text = Properties.Settings.Default.cancelFilePath;
            skewViewTextBox.Text = Properties.Settings.Default.skewView.ToString();
            //if (Properties.Settings.Default.tickerListBoxSetting != null )
            //{
            //    ticketListBox.Items.Clear();
            //    foreach (String item in Properties.Settings.Default.tickerListBoxSetting.Items)
            //    {
            //        ticketListBox.Items.Add(item);
            //    }
            //}
            if (ConfigurationManager.AppSettings["stockList"] != null)
            {
               String stockList = ConfigurationManager.AppSettings["stockList"];
               String[] items = stockList.Split(",".ToCharArray());
               if (items.Length > 0) ticketListBox.Items.Clear();
               foreach (String item in items)
               {
                   ticketListBox.Items.Add(item);
               }

            }
            //if (Properties.Settings.Default.stockList != null)
            //{
            //    String[] items = Properties.Settings.Default.stockList.Split(",".ToCharArray());
            //    if (items.Length > 0) ticketListBox.Items.Clear();
            //    foreach (String item in items)
            //    {
            //        ticketListBox.Items.Add(item);
            //    }
            //}


        }

        private void SetFormValues(PriceWithTimeFrameTO item)
        {
            if (item == null ) return;
            if (item.StockName.StartsWith(Constants.FMETF))
            {
                bidLabel.Text = String.Format("Bid: Php{0:000.00}", item.Bid);
                askLabel.Text = String.Format("Ask: Php{0:000.00}", item.Ask);
                clientALabel.Text = marketMakerBuyerTextBox.Text;
                clientBlabel.Text = marketMakerSellerTextBox.Text;
                offerVolLabel.Text = String.Format("Offer Vol: {0}", item.AskSize);
                bidVolLabel.Text = String.Format("Bid Vol: {0}", item.BidSize);

            }
        }

        private void SetParametersForMarketMaker(PriceWithTimeFrameTO item, MarketMakerStrategy target, Dictionary<String, Object> parameters, String stockName)
        {

            target.SetParameters(stockName, "timeFrame", item.TimeFrame, parameters);
            target.SetParameters(stockName, "stockName", stockName, parameters);

            target.SetParameters(stockName, "bidSize", item.BidSize, parameters);
            target.SetParameters(stockName, "bid", item.Bid, parameters);
            target.SetParameters(stockName, "askSize", item.AskSize, parameters);

            //user defined bid and ask volume
            target.SetParameters(stockName, "bidVolume", Convert.ToInt32(bidVolumeTextBox.Text), parameters);
            target.SetParameters(stockName, "askVolume", Convert.ToInt32(askVolumeTextBox.Text),  parameters);
            target.SetParameters(stockName, "marketMakerFilePath", String.Format(@"{0}\{1}",marketMakerFilePathTextBox.Text,fileNameTextBox.Text), parameters);
            target.SetParameters(stockName, "marketMakerBuyer", marketMakerBuyerTextBox.Text, parameters);
            target.SetParameters(stockName, "marketMakerSeller", marketMakerSellerTextBox.Text, parameters);

            target.SetParameters(stockName, "cancelFilePath", String.Format(@"{0}\{1}", cancelFilePathtextBox.Text, cancelFileTextBox.Text), parameters);

            target.SetParameters(stockName, "ask", item.Ask, parameters);
            target.SetParameters(stockName, "last", item.Price, parameters);
            target.SetParameters(stockName, "period", Convert.ToInt32( rsiPeriodTextBox.Text) , parameters);


            target.SetParameters(stockName, "startingX", 41, parameters);
            target.SetParameters(stockName, "frequency", 1, parameters);
            target.SetParameters(stockName, "timeFrame", "MIN", parameters);
            target.SetParameters(stockName, Constants.RSIStatus, Constants.RSIWaitingMode, parameters);
            target.SetParameters(stockName, "requiredDerivative", 0.001, parameters);
            target.SetParameters(stockName, "requiredSeparation", 1.05, parameters);
            target.SetParameters(stockName, "volumePeriod", 14, parameters);
            target.SetParameters(stockName, "requiredVolume", 1.3, parameters);

            target.SetParameters(stockName, "profitTargetLookupPeriod", 10, parameters);
            target.SetParameters(stockName, "stopLossLookupPeriod", 10, parameters);
            //7% loss
            target.SetParameters(stockName, "requiredAbsoluteStopLossFactor", 0.93, parameters);
            target.SetParameters(stockName, "techniStockfilePath", @"C:\temp\technistock\orders.csv", parameters);

            //just pretend we got the right amount
            target.SetParameters(stockName, "capital", 1000000.00, parameters);
            target.SetParameters(stockName, "capitalMultiplier", 0.8, parameters);
            //capital divisor is the baseline or the minimum capital
            target.SetParameters(stockName, "capitalDivisor", 5, parameters);

            //pending or partial.  check if that stock has no pending status. or someone already sent an unfilled order
            //a db call on quotation screen.

            //target.SetParameters(stockName, "volume", 15678, parameters);//
            //target.SetParameters(stockName, "lineRegressionRSIPeriod", 40, parameters);//
            //target.SetParameters(stockName, "rsiPeriod", 14, parameters);//
            target.SetParameters(stockName, "cutoffDate", item.Date, parameters);//
            
            target.SetParameters(stockName, "spreadThreshold", Convert.ToDouble(maxSpreadTextBox.Text), parameters);//

            target.SetParameters(stockName, Constants.iNAVURL, iNAVLinkTextBox.Text, parameters);//

            target.SetParameters(stockName, Constants.iNAVPattern, iNAVPatternTextBox.Text, parameters);//

            target.SetParameters(stockName, Constants.fromEmail, fromEmailTextBox.Text, parameters);

            //todo: test first
            //String subscribers = emailSubscribersTextBox.Text.Replace("\t", ";");

            target.SetParameters(stockName, Constants.toEmail, emailSubscribersTextBox.Text.Trim(), parameters);
 
            target.SetParameters(stockName, "skewView", skewViewTextBox.Text, parameters);

            target.SetParameters(stockName, "minPostVolume", minPostVolumeTextBox.Text, parameters);

            target.SetParameters(stockName, Constants.ClientA, marketMakerBuyerTextBox.Text, parameters);

            target.SetParameters(stockName, Constants.ClientB, marketMakerSellerTextBox.Text, parameters);

            //bool isBid = (bool)GetParameter(stockName, Constants.IsBid, parameters);

            //List<ImportedOrderScreen> list = (List<ImportedOrderScreen>)GetParameter(stockName, Constants.ImportedOrderScreenList, parameters);

            ////spreadThreshold is maxSpread
            //double spreadThreshold = (double)GetParameter(stockName, "spreadThreshold", parameters);

            //if (!DoesKeyExist(stockName, Constants.iNAV, parameters)) throw new Exception("iNAV is missing as a key");
            //double iNAVprice = (double)GetParameter(stockName, Constants.iNAV, parameters);

            target.SetParameters(stockName, Constants.AskImportedOrderScreenList, AskImportedOrderScreenList, parameters);
            target.SetParameters(stockName, Constants.BidImportedOrderScreenList, BidImportedOrderScreenList, parameters);



            SaveSettings();


            

            
        }

        private void SaveSettings()
        {
            Properties.Settings.Default.askVolume = Convert.ToInt32(askVolumeTextBox.Text);
            Properties.Settings.Default.bidVolume = Convert.ToInt32(bidVolumeTextBox.Text);
            Properties.Settings.Default.marketMakerBuyer = marketMakerBuyerTextBox.Text;
            Properties.Settings.Default.marketMakerSeller = marketMakerSellerTextBox.Text;
            Properties.Settings.Default.spreadThreshold = maxSpreadTextBox.Text;
            Properties.Settings.Default.marketMakerFileName = fileNameTextBox.Text;
            Properties.Settings.Default.marketMakerFilePath = marketMakerFilePathTextBox.Text;
            Properties.Settings.Default.skewView = Convert.ToDouble( skewViewTextBox.Text);
            Properties.Settings.Default.cancelFilePath = cancelFilePathtextBox.Text;
            //if (ticketListBox != null)
            //{
            //    if(Properties.Settings.Default.tickerListBoxSetting != null && Properties.Settings.Default.tickerListBoxSetting.Items.Count > 0){
            //        Properties.Settings.Default.tickerListBoxSetting.Items.Clear();
            //    }
            //    foreach(String item in ticketListBox.Items){
            //        if(Properties.Settings.Default.tickerListBoxSetting == null){
            //            Properties.Settings.Default.tickerListBoxSetting = new ListBox();
            //        }
            //        Properties.Settings.Default.tickerListBoxSetting.Items.Add(item);
            //    }
            //}

            //if (ticketListBox != null)
            //{
            //    String stockList = String.Empty;
            //    foreach (String item in ticketListBox.Items)
            //    {
            //        stockList += item + ",";
            //    }
            //    stockList.TrimEnd(",".ToCharArray());
            //    Properties.Settings.Default.stockList = stockList;
            //}



            

            //Properties.Settings.Default.spreadThreshold = .Text;

            Properties.Settings.Default.Save();
        }

        private List<ImportedOrderScreen> askImportedOrderScreenList;

        public List<ImportedOrderScreen> AskImportedOrderScreenList
        {
            get { return askImportedOrderScreenList; }
            set { askImportedOrderScreenList = value; }
        }

        private List<ImportedOrderScreen> bidImportedOrderScreenList;

        public List<ImportedOrderScreen> BidImportedOrderScreenList
        {
            get { return bidImportedOrderScreenList; }
            set { bidImportedOrderScreenList = value; }
        }

        private void ScheduleTimer()
        {
            //Timer timer = new Timer();
            try
            {
                runTimer.Interval = 15000;
                runTimer.Tick += new EventHandler(runTimer_Tick);
                runTimer.Start();

                diagnosticsTimer.Interval = 3000;
                diagnosticsTimer.Tick += diagnosticsTimer_Tick;
                diagnosticsTimer.Start();

                int rsiFrequency = 1;
                int.TryParse(rsiFrequencyTextBox.Text, out rsiFrequency);

                strategyTimer.Interval = 1000 * 60 * rsiFrequency;
                strategyTimer.Tick += strategyTimer_Tick;
                strategyTimer.Start();

                intradayTimer.Interval = 5000;
                intradayTimer.Tick +=intradayTimer_Tick;
                intradayTimer.Start();


            }
            catch (Exception ex)
            {
                errorMessageLabel.Text = "Error on Scheduling";
                EventLog.WriteEntry("FMSec",String.Format("Error Message:{0}", ex.Message),EventLogEntryType.Error);
                runTimer.Stop();
            }
        }

        private void intradayTimer_Tick(object sender, EventArgs e)
        {
            CapitalCalculator c = new CapitalCalculator();
            int frequency = 5;
            String timeFrame = "SEC";
            int period = 1;

            String listOfTickers = String.Empty;

            if (ConfigurationManager.AppSettings["stockList"] != null)
            {
                listOfTickers = ConfigurationManager.AppSettings["stockList"];
            }
            rsilistBox.Items.Clear();
            PriceWithTimeFrameTO[] list = null;
            try
            {
                IntradayStrategy mgr = new IntradayStrategy();
                list = mgr.GeBatchPricetimeFrameList(listOfTickers, frequency, timeFrame, period);

                StockInfo stockInfo = null;
                object obje = new object();
               
                StringBuilder b = new StringBuilder();
                foreach (PriceWithTimeFrameTO item in list)
                {
                    lock (obje)
                    {
                        if (!StockHashSet.ContainsKey(item.StockName))
                        {
                            stockInfo = new StockInfo();
                            stockInfo.CurrentPrice = Convert.ToDouble(item.Price);
                            
                            StockHashSet.Add(item.StockName, stockInfo);
                        }
                    }
                    //text = String.Format("Ticker:{0} Bid:{1}", item.StockName, item.Bid);
                    double price = Convert.ToDouble(item.Price);
                    //BoardLotFluctuationPriceTO boardLot = c.GetBoardLotBasedOnPrice(price);
                    double capital = c.GetCapital(1, price);
                    //Console.WriteLine(item.Bid);
                    
                    //b.Append(String.Format("Date: {7}, Stock:{6}, Price:{0}, Bid:{1}, Ask:{2}, Vol:{3}, Stop Loss:{4}, Profit Target:{5}, # of shares: {7}\r\n"
                    //    , item.Price, item.Bid, item.Ask, item.Volume
                    //    , c.GetLowerPriceByFlucOffset(price, 4.00)
                    //    , c.GetHigherPriceByFlucOffset(price, 8.00)
                    //    , item.StockName
                    //    , c.GetNumberOfShares(c.GetCapital(1, price), price)
                    //    , item.Date.ToShortTimeString()));
                    //text = b.ToString();

                    rsilistBox.Items.Add(String.Format("Date: {7}, Stock:{6}, Price:{0}, Bid:{1}, Ask:{2}, Vol:{3}, Stop Loss:{4}, Profit Target:{5}, # of shares: {7}\r\n"
                        , item.Price, item.Bid, item.Ask, item.Volume
                        , c.GetLowerPriceByFlucOffset(price, 4.00)
                        , c.GetHigherPriceByFlucOffset(price, 8.00)
                        , item.StockName
                        , c.GetNumberOfShares(c.GetCapital(1, price), price)
                        , item.Date.ToShortTimeString()));
                }





                
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry(Constants.FMSecSource, ex.Message, EventLogEntryType.Error);
                errorMessageLabel.Text = "Error on getting prices";
                return;
            }
            
        }

        void strategyTimer_Tick(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("running...");

            int rsiFrequency = 1;
            int.TryParse(rsiFrequencyTextBox.Text, out rsiFrequency);

            string timeFrame = "MIN";
            int onePeriod = 1;
            int period = 14;
            DateTime cutoffDate = DateTime.Now;


            PriceWithTimeFrameTO[] list = null;
            try
            {
                // list =  GetTimeFrame(timeFrame, frequency, onePeriod, cutoffDate, stockName);
                foreach (String stock in ticketListBox.Items)
                {
                    GetRSITimeFrame(timeFrame, rsiFrequency, onePeriod, cutoffDate, stock, list);

                }//list = await task;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry(Constants.FMSecSource, ex.Message, EventLogEntryType.Error);
                errorMessageLabel.Text = "Error on getting prices";
                return;
            }

           
        }

        void diagnosticsTimer_Tick(object sender, EventArgs e)
        {
            int anyFrequency = 0;
            int.TryParse(marketMakerFrequencyTextBox.Text, out anyFrequency);
            //int anyFrequency = Convert.ToInt32( marketMakerFrequencyTextBox.Text);
            RunDiagnostics(anyFrequency);
        }

        void runTimer_Tick(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("running...");

            //todo: fix this
            string timeFrame = "MIN";
            int frequency = 1;
            int onePeriod = 1;
            int period = 14;
            DateTime cutoffDate = DateTime.Now;


            PriceWithTimeFrameTO[] list = null;
            try
            {
                // list =  GetTimeFrame(timeFrame, frequency, onePeriod, cutoffDate, stockName);
                GetTimeFrame(timeFrame, frequency, onePeriod, cutoffDate, fmEtfStockTextBox.Text, list);
                //list = await task;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry(Constants.FMSecSource, ex.Message, EventLogEntryType.Error);
                errorMessageLabel.Text = "Error on getting prices";
                return;
            }
            

            //foreach (String stockName in this.ticketListBox.Items)
            //{

            //    PriceWithTimeFrameTO[] list = null;
            //    try
            //    {
            //       // list =  GetTimeFrame(timeFrame, frequency, onePeriod, cutoffDate, stockName);
            //        GetTimeFrame(timeFrame, frequency, onePeriod, cutoffDate, stockName,  list);
            //        //list = await task;
            //    }
            //    catch (Exception ex)
            //    {
            //        EventLog.WriteEntry(Constants.FMSecSource, ex.Message, EventLogEntryType.Error);
            //        errorMessageLabel.Text = "Error on getting prices";
            //        return;
            //    }
               
            //}
            
        }

        private async void GetRSITimeFrame(string timeFrame, int frequency, int onePeriod, DateTime cutoffDate, String stockName, PriceWithTimeFrameTO[] list)
        {
            //PriceWithTimeFrameTO[] list = null;
            TimeFrameManager mgr = new TimeFrameManager();
            Task<PriceWithTimeFrameTO[]> task = Task.Run(() => mgr.GetTimeFrame(stockName, timeFrame, frequency, onePeriod, cutoffDate));
            list = await task;
            if (list != null && list.Length > 0)
            {
                OnChanged(list[0], new RSIEventArg());
            }
            //  return list;
        }


        private double currentInav;
        private double previousInav;

        private async void GetTimeFrame(string timeFrame, int frequency, int onePeriod, DateTime cutoffDate, String stockName, PriceWithTimeFrameTO[] list)
        {
            //PriceWithTimeFrameTO[] list = null;
            TimeFrameManager mgr = new TimeFrameManager();
            Task<PriceWithTimeFrameTO[]> task = Task.Run(() => mgr.GetTimeFrame(stockName, timeFrame, frequency, onePeriod, cutoffDate));
            list = await task;
            if (list != null && list.Length > 0)
            {
                OnChanged(list[0], new TimeFrameEventArg());
            }
              //  return list;
        }


        //private static void GetTimeFrame()
        //{
        //    TimeFrameManager mgr = new TimeFrameManager();

        //    string stockName ;
        //    string timeFrame;
        //    int frequency;
        //    int period;
        //    DateTime cutoffDate;

            


            
        //    //mgr.GetTimeFrame(stockName, timeFrame, frequency, period, cutoffDate);
        //}

        void mm_Changed(object sender, EventArgs e)
        {
            if (sender.GetType() != typeof(Dictionary<String, object>)) return;



            Dictionary<String, object> parameters = (Dictionary<String, object>)sender;

            if (e.GetType() == typeof(COMClassLibrary.Manager.MessageEventArg))
            {

                MessageEventArg eventArg = (MessageEventArg)e;

                //IMessageHandler handler = eventArg.MessageHandler;
                //handler.Handle(parameters, e);

                

                if (eventArg.Code == Constants.iNAV)
                {
                    String inav = String.Format("iNAV: {0}", eventArg.Message);
                    Double.TryParse(eventArg.Message, out currentInav);

                    if (previousInav == 0)
                    {
                        previousInav = currentInav;
                    }
                    else{
                        double remaining = Math.Abs(currentInav - previousInav);
                        //if((1 + remaining) > 
                        CapitalCalculator c = new CapitalCalculator();
                        BoardLotFluctuationPriceTO lot = c.GetBoardLotBasedOnPrice(previousInav);

                       if ((lot.Fluctuation - remaining) <= 0 )
                            errorMessageLabel.Text =      "iNAV is more than or less then one fluctuation" ;

                    }
                    
                    

                    if (iNAVValueLabel.Text != inav)
                    {
                        // 
                        //SystemSounds.Beep.Play();


                    }
                    iNAVValueLabel.Text = String.Format("iNAV: {0}", eventArg.Message);

                }

                if (eventArg.Code == Constants.SpreadWidthMessage)
                {

                    //todo:  test first using the tester

                    //messageLabel.Text = Constants.iNAVIsOutsideOfSpread;
                   // outsideOfSpreadLabel.Enabled = eventArg.Code == Constants.iNAVIsOutsideOfSpread;
                    //rsiStockLabel.Text = eventArg.StockName;
                    isOutsideOfSpreadAnswerLabel.Text = "Yes";

                    //append to a log file.
                    //string path = Environment.SpecialFolder.UserProfile + @"\Downloads";
                    //FileManager.Instance.WriteToAFile(string.Format("time:{0} | {1}",DateTime.Now.ToShortDateString(),"Outside of Spread"), path);
                    FileManager.Instance.AppendToFile(string.Format("time:{0} | {1}", DateTime.Now.ToString(), "Outside of Spread"));

                }
                //if (eventArg.Code == Constants.SpreadWidthMessage)
                //{
                //    messageLabel.Text = Constants.SpreadWidthMessage;

                //    spreadTooWideLabel.Enabled = eventArg.Code == Constants.SpreadWidthMessage;
                //    rsiStockLabel.Text = eventArg.StockName;
                //}
                //if (eventArg.Code == Constants.numberOfFlux)
                //{
                //    Double numberOfFlux = Convert.ToDouble(eventArg.Message);
                //    iNAVSpreadLabel.Text = String.Format("Spread: {0:0.00}", numberOfFlux);
                    
                //    if (numberOfFlux > 4.00 && numberOfFlux < 11.00)
                //    {
                //        iNAVSpreadLabel.BackColor = Color.Yellow;
                //    }
                //    else if (numberOfFlux > 10.00)
                //    {
                //        iNAVSpreadLabel.BackColor = Color.Red;
                //    }
                //    else if (numberOfFlux < 5.00)
                //    {
                //        iNAVSpreadLabel.BackColor = Color.Green;
                //    }
                //}
                if (eventArg.Code == Constants.MyRSIErrorCode)
                {
                    errorMessageLabel.Text = eventArg.Message;
                    rsiStockLabel.Text = eventArg.StockName;
                }

            }

            if (e.GetType() == typeof(COMClassLibrary.Manager.MMMaintainSpreadEventArg))
            {
                sellerListBox.Items.Clear();
                MMMaintainSpreadEventArg mmEventArg = (MMMaintainSpreadEventArg)e;

                this.matchingEngineParams = mmEventArg.matchingEngineParams;


                foreach (ImportedOrderScreen o in mmEventArg.matchingEngineParams.AskImportedOrderScreenList)
                {
                    String text = String.Format("{0}, Price:{1} Side:{2},Pending:{3}, Action:{4}", o.stock, o.price, o.side, o.pending, o.action);
                    sellerListBox.Items.Add(text);
                    
                    //Debug.WriteLine("stock:{0}, price:{1}, side:{2}, vol:{3}",s.stock, s.price, s.side, s.volume);
                }
                buyerListBox.Items.Clear();
                foreach (ImportedOrderScreen o in mmEventArg.matchingEngineParams.BidImportedOrderScreenList)
                {
                    String text = String.Format("{0}, Price:{1} Side:{2},Pending:{3}, Action:{4}", o.stock, o.price, o.side, o.pending, o.action);
                    buyerListBox.Items.Add(text);

                    //Debug.WriteLine("stock:{0}, price:{1}, side:{2}, vol:{3}",s.stock, s.price, s.side, s.volume);
                }


                recommendedBidLabel.Text = String.Format("Php{0:000.00}", mmEventArg.matchingEngineParams.MMBid);
                recommendedOfferLabel.Text = String.Format("Php{0:000.00}", mmEventArg.matchingEngineParams.MMOffer);
                defaBidLabel.Text = String.Format("Php{0:000.00}", mmEventArg.matchingEngineParams.DefaBid);
                defaOfferLabel.Text = String.Format("Php{0:000.00}", mmEventArg.matchingEngineParams.DefaOffer);
                bestBidLabel.Text = String.Format("Best Bid: Php{0:000.00}", mmEventArg.matchingEngineParams.BestBid);
                bestOfferLabel.Text = String.Format("Best Offer: Php{0:000.00}", mmEventArg.matchingEngineParams.BestOffer);


                this.BackColor = SystemColors.Control;

                if (mmEventArg.matchingEngineParams.BidBalance < 0 || mmEventArg.matchingEngineParams.OfferBalance < 0)
                {
                    this.BackColor = Color.Yellow;
                }
                //else
                //{
                   
                //}
                //Debug.WriteLine("here");
                
                
                //List<ImportedOrderScreen> askList = (List<ImportedOrderScreen>)GetPra  parameters
            }

            
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            Show();
            WindowState = FormWindowState.Normal;
            
        }

        private void FMSecForm_ResizeEnd(object sender, EventArgs e)
        {
            Minimize(sender, e);
        }
        private void Minimize(object sender, System.EventArgs e)
        {
            if (FormWindowState.Minimized == WindowState)
            {
                Hide();
            }
        }

     

        private void FMSecForm_SizeChanged(object sender, EventArgs e)
        {
            if (FormWindowState.Minimized == WindowState)
            {
                Hide();
            }
        }
        private Icon icon;
        private void FMSecForm_Load(object sender, EventArgs e)
        {
            errorMessageLabel.Text = "";
            LoadValuesFromSettings();

            this.Changed += FMSecForm_Changed;
            bindingSource = new BindingSource();

            icon = fmsecNotifyIcon.Icon;
            try
            {
                ScheduleTimer();
            }
            catch(Exception ex){
                errorMessageLabel.Text = ex.Message;
            }


 
            //Setup();
        }

        void FMSecForm_Changed(object sender, EventArgs e)
        {
            if (e.GetType() == typeof(RSIEventArg))
            {
                PriceWithTimeFrameTO item = (PriceWithTimeFrameTO)sender;
                if (item != null)
                {
                    //RunDiagnostics();
                    if (isSystemUp)
                    {
                        try
                        {
                            RunRecognia(item);
                            RunRSI(item);

                        }
                        catch (Exception ex)
                        {
                        }
                    }
                }
            }
            if (e.GetType() == typeof(TimeFrameEventArg))
            {
                PriceWithTimeFrameTO item = (PriceWithTimeFrameTO)sender;
                if (item != null)
                {
                    //RunDiagnostics();
                    if (isSystemUp)
                    {
                        try
                        {
                            RunMarketMaker(item);
                            //RunRecognia(item);
                            //RunRSI(item);
                        }
                        catch (Exception ex)
                        {
                            EventLog.WriteEntry(Constants.FMSecSource, ex.Message, EventLogEntryType.Error);
                            errorMessageLabel.Text = ex.Message;
                        }
                    }
                }

            }
            if (e.GetType() == typeof(DiagnosticsEventArg) && ((DiagnosticsEventArg)e).code == "web")
            {


                applicationServiceLabel.Text = (string) sender;
                isSystemUp = (applicationServiceLabel.Text == "Up");
                //errorMessageLabel.Text = isSystemUp ?  "Connected": "Disconnected";
                DateTime oldDate = DateTime.Now.Date.AddMinutes(-5);
                return;
            }
            if (e.GetType() == typeof(DiagnosticsEventArg) && ((DiagnosticsEventArg)e).code == "price")
            {
                bool isPriceServerRunning = (bool)sender;
                priceServiceLabel.Text =isPriceServerRunning ? "Connected" : "Disconnected";

                if (isPriceServerRunning)
                {
                    this.BackColor = SystemColors.Control;
                }
                else
                {
                    this.BackColor = Color.Red;
                }

            }

        }

        private void displayToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Show();
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Hide();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void messageLabel_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label28_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void errorMessageLabel_Click(object sender, EventArgs e)
        {
            if ((MessageBox.Show("Yes or no", "Clear Error",
            MessageBoxButtons.YesNo, MessageBoxIcon.Question,
                MessageBoxDefaultButton.Button1) == System.Windows.Forms.DialogResult.Yes))
            {
                errorMessageLabel.Text = "";
            }
            
        }

        private void importButton_Click(object sender, EventArgs e)
        {
            try
            {
                ImportPopUpWindow window = this.ImportPopWindow;
                
                window.Show();
            }
            catch (Exception ex)
            {
                errorMessageLabel.Text = "Unable to open the import window";
            }
        }

        private void recommendedOfferLabel_Click(object sender, EventArgs e)
        {

        }

        private void countdownTimer_Tick(object sender, EventArgs e)
        {
            //this.BackColor = SystemColors.Control;

            //if (!IsUpdated)
            //{
            //    this.BackColor = Color.Yellow;
            //}
        }




        //public List<ImportedOrderScreen> bidImportedOrderScreenList { get; set; }
        private DateTime isUpdatedDateTime;

        public DateTime IsUpdatedDateTime
        {
            get { return isUpdatedDateTime; }
            set { isUpdatedDateTime = value; }
        }

        public bool IsFormManuallyRefreshed()
        {
            //if true then form has been manually updated with control O data.

            
            int time = 0;
            bool isOk = Int32.TryParse(timeBetweenRefreshTextBox.Text, out time);
            if (!isOk) time = 10;

            return  DateTime.Now.AddMinutes(time * -1) < isUpdatedDateTime;
            
        }


        private void FMSecForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (!this.ImportPopWindow.IsDisposed)
            {
                this.ImportPopWindow.Dispose();
                this.marketMakerStrategy = null;
             
            }
        }

        private void miniButton_Click(object sender, EventArgs e)
        {

        }

        private void label46_Click(object sender, EventArgs e)
        {

        }

        private void FMSecForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            SaveSettings();
        }

        private void tabPage6_Click(object sender, EventArgs e)
        {

        }

        private void getChartButton_Click(object sender, EventArgs e)
        {
            IntradayStrategy mgr = new IntradayStrategy();
            
            PriceWithTimeFrameTO[] list = mgr.GeBatchPricetimeFrameList(stockChartTextBox.Text, 5, "SEC", 10);
            //TimeFrameManager mgr = new TimeFrameManager();
           
    //        PriceWithTimeFrameTO[] list = mgr.GetTimeFrame(stockChartTextBox.Text, "SEC", 5, 10, DateTime.Now);




            stockChart.Series.Clear();
            stockChart.Series.Add("FMETF");
            stockChart.Series["FMETF"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;

            double lowest = Double.MaxValue;
            double hightest = 0.0;
            foreach (PriceWithTimeFrameTO item in list)
            {
                if (Convert.ToDouble(item.Price) > hightest)
                {
                    hightest = Convert.ToDouble(item.Price);
                }
                if (Convert.ToDouble(item.Price) < lowest)
                {
                    lowest = Convert.ToDouble(item.Price);
                }
            }

            stockChart.ChartAreas[0].AxisY.Maximum = 118.00;
            stockChart.ChartAreas[0].AxisY.Minimum = 117.00;


            stockChart.Series["FMETF"].Points.AddXY("x", 117.20);
            stockChart.Series["FMETF"].Points.AddXY("x", 117.10);
            stockChart.Series["FMETF"].Points.AddXY("x", 117.20);
            stockChart.Series["FMETF"].Points.AddXY("x", 117.30);
            stockChart.Series["FMETF"].Points.AddXY("x", 117.40);

            stockChart.Series["FMETF"].Points.AddXY("x", 117.50);

            stockChart.Series["FMETF"].Points.AddXY("x", 117.40);

            stockChart.Series["FMETF"].Points.AddXY("x", 117.30);
            //foreach (PriceWithTimeFrameTO item in list)
            //{
            //    Console.WriteLine("Last:{0}", item.Price);
            //    stockChart.Series["FMETF"].Points.AddXY("x", item.Price);
               
            //}

            
        }

        private void isOutsideOfSpreadButton_Click(object sender, EventArgs e)
        {
            isOutsideOfSpreadAnswerLabel.Text = "No";
        }
        
    }
}
